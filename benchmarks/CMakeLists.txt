
if(BENCHMARKS)

link_directories(
    ${CMAKE_BINARY_DIR}/src
    ${Boost_LIBRARY_DIRS})
include_directories(
    ${CMAKE_SOURCE_DIR}/include
    ${CMAKE_BINARY_DIR})
include_directories(SYSTEM
    ${DIM_INCLUDE_DIRS}
    ${NTOFUTILS_INCLUDE_DIRS}
    ${Boost_INCLUDE_DIRS}
    ${PUGI_INCLUDE_DIRS}
    ${GBENCHMARK_INCLUDE_DIRS})

set(benchmark_SRCS benchmark_main.cpp
    bench_ZeroSuppressionAlgorithm.cpp)

add_executable(benchmark_all ${benchmark_SRCS})
target_link_libraries(benchmark_all
    DaqManagerLib
    ${NTOFUTILS_LIBRARIES}
    ${DIM_LIBRARIES}
    ${Boost_LIBRARIES}
    ${PTHREAD_LIBRARIES}
    ${PUGI_LIBRARIES}
    ${GBENCHMARK_LIBRARIES})

endif()
