/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-30T10:35:00+01:00
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#include <string>

#include <boost/filesystem.hpp>
#include <boost/log/expressions.hpp>

#include <benchmark.h>

#include "CZeroSuppression.h"
#include "DaqTypes.h"
#include "config.h"


using namespace ntof::ZeroSuppression;
using std::string;
namespace bfs = boost::filesystem;
namespace bl = boost::log;

const bfs::path data_path = bfs::path(SRCDIR) / "tests" / "data";

class ZspAlgorithmFixture : public benchmark::Fixture
{
protected:
  std::unique_ptr<CZeroSuppression> m_zsp;
  std::vector<std::vector<int16_t>> m_channel_list;
  float m_sampleRate;

public:
  ZspAlgorithmFixture()
  {
    // Trace Logs Filtering
    bl::core::get()->set_filter(bl::trivial::severity >= bl::trivial::info);

    extractDataFromRawFile(
      (data_path / "run920081_1_s1.raw").string()); // Triangle signal

    // Get Sample Rate from the correspondig .run file
    m_sampleRate = getSampleRateFromRunFile(
      (data_path / "run920081.run").string());

    // Generate bigger sample vectors through self-concatenation
    for (int i = 0; i < 10; ++i) // 8068 samples * 2^10 ~ 16 MB
      m_channel_list[0].insert(m_channel_list[0].end(),
                               m_channel_list[0].begin(),
                               m_channel_list[0].end());
  }

  void SetUp(benchmark::State &st) override
  {
    m_zsp.reset(new CZeroSuppression(0U, 1U));
  }

  void TearDown(benchmark::State &st) override { m_zsp.reset(); }

  void extractDataFromRawFile(const string &file_path)
  {
    m_channel_list.clear();
    std::vector<int16_t> data;
    std::ifstream ifs(file_path);

    SkipHeader sh;
    ifs >> sh;

    ifs.seekg(sh.start, std::ios_base::beg); // Skip zero byte padding
    HeaderLookup hl;
    AcquisitionHeader ah;

    do
    {
      ifs >> hl;
      ifs >> ah; // Read ACQC Header

      Data pulse;
      size_t size = ah.dataSize;
      data.clear();
      while (size > sizeof(Data))
      {
        ifs >> pulse; // Read Samples

        // Pulse size = uint64_t * 2 + uint16_t * pulse_length
        size -= (sizeof(pulse.detectorTimeStamp) << 1) +
          (pulse.detectorData.size() << 1);

        data.insert(data.end(),
                    reinterpret_cast<int16_t *>(pulse.detectorData.data()),
                    reinterpret_cast<int16_t *>(pulse.detectorData.data() +
                                                pulse.detectorData.size()));
      }
      if (data.size() > 0)
        m_channel_list.push_back(data);

      // handle word alignment
      if (size != 0)
      {
        ifs.seekg(2, std::ios_base::cur);
      }

    } while (ifs.good() && ifs.tellg() < std::ios::pos_type(sh.end));
  }

  float getSampleRateFromRunFile(const string &file_path)
  {
    std::ifstream ifs(file_path);

    RunControlHeader rch;
    ifs >> rch;

    ModuleHeader mh;
    ifs >> mh;

    return mh.channelsConfig[0].sampleRate;
  }
};

BENCHMARK_DEFINE_F(ZspAlgorithmFixture, ExecutionTimeTest)
(benchmark::State &st)
{
  const size_t sampleSize = m_channel_list[0].size();
  const int16_t *sampleData = m_channel_list[0].data();

  // ZSP Alg. Setting
  m_zsp->SetTimeWindow(static_cast<uint32_t>(st.range(0) * 1E-3 * m_sampleRate),
                       sampleSize);
  m_zsp->SetPulseDetectionType(static_cast<PULSE_TYPE_t>(st.range(1)));
  m_zsp->SetPre(static_cast<uint32_t>(st.range(2)));
  m_zsp->SetPost(static_cast<uint32_t>(st.range(3)));
  m_zsp->SetThreshold(static_cast<int16_t>(st.range(4)));

  for (auto _ : st)
    m_zsp->PerformZeroSuppression(sampleData, sampleSize, nullptr);
}

BENCHMARK_REGISTER_F(ZspAlgorithmFixture, ExecutionTimeTest)
  ->Unit(benchmark::kNanosecond)
  ->Iterations(1)
  ->Repetitions(10)
  // --- Tests ---
  // ZSP_Start | PulseType | PreSamples | PostSamples | Threshold
  ->Args({0, NEGATIVE, 0, 0, -11750});