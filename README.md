# n_TOF DaqManager

Data acquisition and processing application.

# Build

A prebuilt docker environment is ready for this project, to compile it:
```bash
# Only needed first time to fetch the docker image
docker pull gitlab-registry.cern.ch/ntof/daq/daqmanager/builder

# Start a temporary container, binding current directory inside
docker run -v "$PWD:$PWD" -w "$PWD" --rm -it gitlab-registry.cern.ch/ntof/daq/daqmanager/builder /bin/bash

# From within the container
source /opt/rh/devtoolset-7/enable

rm -rf build && mkdir build
cd build && cmake ..

make -j4
```

# Build Without Docker

**Note:** Using Docker is the recommended way to build this app.

The following packages must be installed to compile this app:
```bash
sudo yum install cmake make gcc-c++ git freeipmi-devel numactl-devel readline-devel libblkid-devel
```

To enable SPDevices driver, the additional packages must be installed:
```bash
sudo yum-config-manager --add-repo http://ntofci.web.cern.ch/ntofci/distfiles/ntof/cc7/nToF.repo
sudo yum install -y libadq0 ntof-boost
```

Install latest gcc:
```bash
sudo yum install -y centos-release-scl
sudo yum install -y devtoolset-7-gcc*
source /opt/rh/devtoolset-7/enable
```

To build the application:
```bash
rm -rf build && mkdir build
cd build && cmake ..

make -j4
```

**Note:** A valid kerberos token is required to compile this app since it'll download its dependencies on gitlab.

## Eclipse support

To build for eclipse:
```bash
# From the project's root directory
cmake . -G "Eclipse CDT4 - Unix Makefiles"

# Then import as existing project in Eclipse
```
**Note:** Eclipse e-git plugin won't work with out-of-source builds

# Testing

TBD

# Debugging

TBD
