/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-25T17:31:50+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <memory>

#include <DIMParamListClient.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestAssert.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "CDIMZeroSuppressionParameters.h"
#include "test_helpers.hpp"

using namespace ntof;
using namespace ntof::dim;

class TestCDIMZeroSuppressionParameters : public CppUnit::TestFixture
{
protected:
  CPPUNIT_TEST_SUITE(TestCDIMZeroSuppressionParameters);
  CPPUNIT_TEST(simple);
  CPPUNIT_TEST_SUITE_END();

  std::unique_ptr<DimTestHelper> m_dim;

public:
  void setUp() override { m_dim.reset(new DimTestHelper()); }

  void tearDown() override { m_dim.reset(); }

  DIMData makeNode(DIMData::Index idx,
                   const std::string &name,
                   const std::string &sn,
                   uint32_t channel)
  {
    DIMData node(idx, name, "", DIMData::List());
    node.getNestedValue().emplace_back(0, "sn", "", sn);
    node.getNestedValue().emplace_back(1, "channel", "", channel);
    return node;
  }

  void simple()
  {
    DIMParamListClient cli("TEST");
    SignalWaiter waiter;

    waiter.listen(cli.dataSignal);
    CDIMZeroSuppressionParameters service("TEST");

    EQ(true, waiter.wait([&cli] { return cli.getParameters().size() == 3; }));
    EQ(std::string("independent"),
       cli.getParameters()[1].getValue<DIMEnum>().getName());

    /* set single mode */
    DIMData::List params;
    params.emplace_back(CDIMZeroSuppressionParameters::ParamIdx::MODE, "mode",
                        "", DIMEnum(ZeroSuppression::zsMode::SINGLE_MASTER));
    {
      DIMData::List config;
      config.push_back(makeNode(0, "master", std::string("1234"), uint32_t(12)));
      params.emplace_back(CDIMZeroSuppressionParameters::ParamIdx::CONFIGURATION,
                          "configuration", "", config);
    }
    EQ(DIMAck::OK, cli.sendParameters(params).getStatus());
    EQ(true, waiter.wait([&cli] {
      return cli.getParameters().at(1).getValue<DIMEnum>().getName() ==
        "singlemaster";
    }));

    EQ(std::string("1234"),
       getData("master.sn", cli.getParameters()[2]).getValue<std::string>());
    EQ(ZeroSuppression::zsMode::SINGLE_MASTER, service.GetMode());
    EQ(std::size_t(1), service.GetMasterList().size());
    EQ(std::string("1234"), service.GetMasterList()[0].masterSn);

    /* multi-master mode */
    params.clear();
    params.emplace_back(CDIMZeroSuppressionParameters::ParamIdx::MODE, "mode",
                        "", DIMEnum(ZeroSuppression::zsMode::MULTIPLE_MASTER));

    {
      DIMData::List config;
      for (int i = 0; i < 2; ++i)
      {
        DIMData master(
          makeNode(i, "master", std::to_string(1234 + i), uint32_t(12)));
        master.getNestedValue().push_back(
          makeNode(2, "slave", std::to_string(1234 + i), uint32_t(13)));
        master.getNestedValue().push_back(
          makeNode(3, "slave", std::to_string(1234 + i), uint32_t(14)));
        config.push_back(std::move(master));
      }

      params.emplace_back(CDIMZeroSuppressionParameters::ParamIdx::CONFIGURATION,
                          "configuration", "", config);
    }
    EQ(DIMAck::OK, cli.sendParameters(params).getStatus());
    EQ(true, waiter.wait([&cli] {
      return cli.getParameters().at(1).getValue<DIMEnum>().getName() == "master";
    }));
    EQ(std::string("1235"),
       getData("1.sn", cli.getParameters()[2]).getValue<std::string>());
    EQ(ZeroSuppression::zsMode::MULTIPLE_MASTER, service.GetMode());
    EQ(std::size_t(2), service.GetMasterList().size());
    EQ(std::string("1235"), service.GetMasterList()[1].masterSn);
  }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestCDIMZeroSuppressionParameters);
