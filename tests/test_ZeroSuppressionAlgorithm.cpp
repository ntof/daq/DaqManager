/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-05-29T12:00:44+01:00
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#include <algorithm>
#include <string>

#include <boost/filesystem.hpp>

#include <cppunit/TestAssert.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "CZeroSuppression.h"
#include "DaqTypes.h"
#include "config.h"

using namespace ntof::ZeroSuppression;
using std::string;

namespace bfs = boost::filesystem;

const bfs::path data_path = bfs::path(SRCDIR) / "tests" / "data";

struct TestData
{
  // Zero-Suppression Settings
  uint32_t startTime; // ns
  PULSE_TYPE_t pulseType;
  uint32_t preSamples;
  uint32_t postSamples;
  int16_t threshold;

  // Expected Output
  size_t expectedPulses;
  size_t outZspWindowPulses;
  size_t inZspWindowPulses;
  int64_t FirstPulseLength;
  bool belowThr; // Pulse data are below the threshold if true, above if false
};

class TestZeroSuppressionAlgorithm : public CppUnit::TestFixture
{
protected:
  CPPUNIT_TEST_SUITE(TestZeroSuppressionAlgorithm);
  CPPUNIT_TEST(triangleSignal);
  CPPUNIT_TEST(squareSignal);
  CPPUNIT_TEST_SUITE_END();

  std::unique_ptr<CZeroSuppression> m_zso;
  std::vector<std::vector<int16_t>> m_channel_list;
  std::vector<TestData> m_test_cases;

public:
  void setUp()
  {
    m_zso.reset(new CZeroSuppression(0U, 1U));
    srand(time(NULL));
  }

  void tearDown() { m_zso.reset(); }

  void extractDataFromRawFile(const string &file_path)
  {
    m_channel_list.clear();
    std::vector<int16_t> data;
    std::ifstream ifs(file_path);
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Error opening " + file_path, true,
                                 ifs.is_open() && ifs.good());

    SkipHeader sh;
    ifs >> sh;
    CPPUNIT_ASSERT_EQUAL(true, ifs.good());

    ifs.seekg(sh.start, std::ios_base::beg); // Skip zero byte padding
    HeaderLookup hl;
    AcquisitionHeader ah;

    do
    {
      ifs >> hl;
      CPPUNIT_ASSERT_EQUAL(true, ifs.good());
      // Check if the next header is of type ACQC
      CPPUNIT_ASSERT_EQUAL(AcquisitionHeader::TITLE, hl.type);

      ifs >> ah; // Read ACQC Header
      CPPUNIT_ASSERT_EQUAL(true, ifs.good());

      Data pulse;
      size_t size = ah.dataSize;
      data.clear();
      while (size > sizeof(Data))
      {
        ifs >> pulse; // Read Samples
        CPPUNIT_ASSERT_EQUAL(true, ifs.good());

        // Pulse size = uint64_t * 2 + uint16_t * pulse_length
        size -= (sizeof(pulse.detectorTimeStamp) << 1) +
          (pulse.detectorData.size() << 1);

        data.insert(data.end(),
                    reinterpret_cast<int16_t *>(pulse.detectorData.data()),
                    reinterpret_cast<int16_t *>(pulse.detectorData.data() +
                                                pulse.detectorData.size()));
      }
      if (data.size() > 0)
        m_channel_list.push_back(data);

      // handle word alignment
      if (size != 0)
      {
        ifs.seekg(2, std::ios_base::cur);
      }

    } while (ifs.good() && ifs.tellg() < std::ios::pos_type(sh.end));
    CPPUNIT_ASSERT_EQUAL(ifs.tellg(), std::ios::pos_type(sh.end));
  }

  float getSampleRateFromRunFile(const string &file_path)
  {
    std::ifstream ifs(file_path);
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Error opening " + file_path, true,
                                 ifs.is_open() && ifs.good());

    RunControlHeader rch;
    ifs >> rch;
    CPPUNIT_ASSERT_EQUAL(true, ifs.good());

    ModuleHeader mh;
    ifs >> mh;
    CPPUNIT_ASSERT_EQUAL(true, ifs.good());
    CPPUNIT_ASSERT(mh.channelsConfig.size() > 0);
    return mh.channelsConfig[0].sampleRate;
  }

  void triangleSignal()
  {
    std::vector<int16_t> filtered_samples;
    extractDataFromRawFile(
      (data_path / "run920081_1_s1.raw").string()); // Triangle signal

    // Get Sample Rate from the correspondig .run file
    const float sampleRate = getSampleRateFromRunFile(
      (data_path / "run920081.run").string());

    CPPUNIT_ASSERT_EQUAL(sampleRate, (float) 14.0625); // MS/s
    // Only one channel is used
    CPPUNIT_ASSERT_EQUAL(m_channel_list.size(), (size_t) 1);
    const size_t sampleSize = m_channel_list[0].size();
    CPPUNIT_ASSERT(sampleSize > 0);
    const int16_t *sampleData = m_channel_list[0].data();

    m_test_cases = std::vector<TestData>{
      // Threshold below the signal
      {(uint32_t) 0, NEGATIVE, (uint32_t) 0, (uint32_t) 0, (int16_t) -15000,
       (size_t) 0, (size_t) 0, (size_t) 0},
      // (See https://gitlab.cern.ch/apc/experiments/ntof/DaqManager/-/issues/4)
      // {(uint32_t) 500, NEGATIVE, (uint32_t) 0, (uint32_t) 0, (int16_t) -15000,
      //  (size_t) 1, (size_t) 1, (size_t) 0, (int64_t) 8},
      // Threshold above the signal
      {(uint32_t) 0, POSITIVE, (uint32_t) 0, (uint32_t) 0, (int16_t) 15000,
       (size_t) 0, (size_t) 0, (size_t) 0},
      // {(uint32_t) 500, POSITIVE, (uint32_t) 0, (uint32_t) 0, (int16_t) 15000,
      //  (size_t) 1, (size_t) 1, (size_t) 0, (int64_t) 8},
      // Threshold between the maximum and the minimum sample
      {(uint32_t) 0, POSITIVE, (uint32_t) 0, (uint32_t) 0, (int16_t) 12500,
       (size_t) 12, (size_t) 0, (size_t) 12, (int64_t) 8, false},
      {(uint32_t) 0, NEGATIVE, (uint32_t) 0, (uint32_t) 0, (int16_t) -5800,
       (size_t) 9, (size_t) 0, (size_t) 9, (int64_t) 225, true},
      // Pre-samples
      {(uint32_t) 0, POSITIVE, (uint32_t) 32, (uint32_t) 0, (int16_t) 12200,
       (size_t) 12, (size_t) 0, (size_t) 12, (int64_t) 32 + 18},
      {(uint32_t) 0, NEGATIVE, (uint32_t) 64, (uint32_t) 0, (int16_t) -1000,
       (size_t) 9, (size_t) 0, (size_t) 9, (int64_t) 64 + 369},
      {(uint32_t) 0, POSITIVE, (uint32_t) 16, (uint32_t) 0, (int16_t) -5000,
       (size_t) 10, (size_t) 0, (size_t) 10, (int64_t) 329},
      {(uint32_t) 0, NEGATIVE, (uint32_t) 256, (uint32_t) 0, (int16_t) -13000,
       (size_t) 9, (size_t) 0, (size_t) 9, (int64_t) 256 + 11},
      // Post-samples
      {(uint32_t) 0, POSITIVE, (uint32_t) 0, (uint32_t) 64, (int16_t) -5000,
       (size_t) 10, (size_t) 0, (size_t) 10, (int64_t) 329 + 64},
      {(uint32_t) 0, POSITIVE, (uint32_t) 0, (uint32_t) 128, (int16_t) 12200,
       (size_t) 12, (size_t) 0, (size_t) 12, (int64_t) 18 + 128},
      {(uint32_t) 0, NEGATIVE, (uint32_t) 0, (uint32_t) 256, (int16_t) -11750,
       (size_t) 9, (size_t) 0, (size_t) 9, (int64_t) 49 + 256}};

    for (auto &test : m_test_cases)
    {
      m_zso->SetTimeWindow(
        static_cast<uint32_t>(test.startTime * 1E-3 * sampleRate), sampleSize);
      m_zso->SetPulseDetectionType(test.pulseType);
      m_zso->SetPre(test.preSamples);
      m_zso->SetPost(test.postSamples);
      m_zso->SetThreshold(test.threshold);

      // Run ZSP algorithm
      m_zso->PerformZeroSuppression(sampleData, sampleSize, nullptr);

      // Output
      CPPUNIT_ASSERT_EQUAL(test.expectedPulses,
                           m_zso->GetPulseDataCollection().size());

      filtered_samples.clear();
      // Pulse count outside and inside the ZSP window and check their
      // position against the threshold
      size_t out = 0, in = 0;
      std::vector<PULSE_DATA_t>::iterator it =
        m_zso->GetPulseDataCollection().begin();
      for (; it != m_zso->GetPulseDataCollection().end(); ++it)
      {
        if (it->isNoSuppressionWindow)
          out += 1;
        else
        {
          in += 1;
          if (test.preSamples == 0 && test.postSamples == 0)
          {
            if (test.belowThr)
              CPPUNIT_ASSERT_EQUAL(
                true,
                std::all_of(
                  it->data, it->data + static_cast<int16_t>(it->length),
                  [&test](int16_t s) -> bool { return s < test.threshold; }));
            else
              CPPUNIT_ASSERT_EQUAL(
                true,
                std::all_of(
                  it->data, it->data + static_cast<int16_t>(it->length),
                  [&test](int16_t s) -> bool { return s > test.threshold; }));

            // Extract filtered samples
            if (it == m_zso->GetPulseDataCollection().begin())
            {
              filtered_samples.insert(filtered_samples.end(), sampleData,
                                      sampleData + it->startIndex);
            }
            else
            {
              filtered_samples.insert(
                filtered_samples.end(),
                sampleData + (it - 1)->startIndex + (it - 1)->length,
                sampleData + it->startIndex);
            }

            if ((it + 1) == m_zso->GetPulseDataCollection().end())
            {
              filtered_samples.insert(filtered_samples.end(),
                                      sampleData + it->startIndex + it->length,
                                      sampleData + sampleSize);
            }
          }
        }
      }
      CPPUNIT_ASSERT_EQUAL(test.outZspWindowPulses, out);
      CPPUNIT_ASSERT_EQUAL(test.inZspWindowPulses, in);

      // Test on the first Pulse ---
      if (test.expectedPulses > 0)
        CPPUNIT_ASSERT_EQUAL(test.FirstPulseLength,
                             m_zso->GetPulseDataCollection()[0].length);

      // When pre and post samples are set to zero, filtered samples of a
      // triangle signal must be in the opposite position to the pulse with
      // respect to the threshold
      if (filtered_samples.size() > 0)
      {
        if (test.belowThr)
        {
          CPPUNIT_ASSERT_EQUAL(
            true,
            std::all_of(
              filtered_samples.begin(), filtered_samples.end(),
              [&test](int16_t s) -> bool { return s > test.threshold; }));
        }
        else
        {
          CPPUNIT_ASSERT_EQUAL(
            true,
            std::all_of(
              filtered_samples.begin(), filtered_samples.end(),
              [&test](int16_t s) -> bool { return s < test.threshold; }));
        }
      }
    }
  }

  void squareSignal()
  {
    extractDataFromRawFile(
      (data_path / "run920085_1_s1.raw").string()); // Square signal

    // Get Sample Rate from the correspondig .run file
    float sampleRate = getSampleRateFromRunFile(
      (data_path / "run920085.run").string());

    CPPUNIT_ASSERT_EQUAL(sampleRate, (float) 14.0625); // MS/s
    // Only one channel is used
    CPPUNIT_ASSERT_EQUAL(m_channel_list.size(), (size_t) 1);
    const size_t sampleSize = m_channel_list[0].size();
    CPPUNIT_ASSERT(sampleSize > 0);
    const int16_t *sampleData = m_channel_list[0].data();

    m_test_cases = std::vector<TestData>{
      {(uint32_t) 0, NEGATIVE, (uint32_t) 0, (uint32_t) 0, (int16_t) -10000,
       (size_t) 9, (size_t) 0, (size_t) 9, (int64_t) 390},
      {(uint32_t) 500, NEGATIVE, (uint32_t) 128, (uint32_t) 128, (int16_t) 12000,
       (size_t) 9, (size_t) 1, (size_t) 8, (int64_t) 107 + 390 + 128},
      {(uint32_t) 0, POSITIVE, (uint32_t) 0, (uint32_t) 0, (int16_t) 12000,
       (size_t) 10, (size_t) 0, (size_t) 10, (int64_t) 107},
      {(uint32_t) 0, POSITIVE, (uint32_t) 0, (uint32_t) 0, (int16_t) 12000,
       (size_t) 10, (size_t) 0, (size_t) 10, (int64_t) 107},
      {(uint32_t) 35342, NEGATIVE, (uint32_t) 64, (uint32_t) 0, (int16_t) 0,
       (size_t) 9, (size_t) 1, (size_t) 8, (int64_t) 107 + 390}};
    // Overlapped pulses
    // (See https://gitlab.cern.ch/apc/experiments/ntof/DaqManager/-/issues/4)
    // {(uint32_t) 0, POSITIVE, (uint32_t) 0, (uint32_t) 392, (int16_t) 12000,
    //  (size_t) 1, (size_t) 0, (size_t) 1, (int64_t) sampleSize}};

    for (auto &test : m_test_cases)
    {
      m_zso->SetTimeWindow(
        static_cast<uint32_t>(test.startTime * 1E-3 * sampleRate), sampleSize);
      m_zso->SetPulseDetectionType(test.pulseType);
      m_zso->SetPre(test.preSamples);
      m_zso->SetPost(test.postSamples);
      m_zso->SetThreshold(test.threshold);

      // Run ZSP algorithm
      m_zso->PerformZeroSuppression(sampleData, sampleSize, nullptr);

      // Output
      CPPUNIT_ASSERT_EQUAL(test.expectedPulses,
                           m_zso->GetPulseDataCollection().size());

      // Pulse count outside and inside the ZSP window
      size_t out = 0, in = 0;
      for (auto &pulse : m_zso->GetPulseDataCollection())
        if (pulse.isNoSuppressionWindow)
          out += 1;
        else
          in += 1;

      CPPUNIT_ASSERT_EQUAL(test.outZspWindowPulses, out);
      CPPUNIT_ASSERT_EQUAL(test.inZspWindowPulses, in);

      // Test on the first Pulse ---
      if (test.expectedPulses > 0)
        CPPUNIT_ASSERT_EQUAL(test.FirstPulseLength,
                             m_zso->GetPulseDataCollection()[0].length);
    }
  }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestZeroSuppressionAlgorithm);
