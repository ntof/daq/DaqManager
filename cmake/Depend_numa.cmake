
include(Tools)

find_path(NUMA_INCLUDE_DIRS numa.h)
find_library(NUMA_LIBRARIES numa)
assert(NUMA_INCLUDE_DIRS AND NUMA_LIBRARIES MESSAGE "Could not find numa, please install numactl-devel package")
