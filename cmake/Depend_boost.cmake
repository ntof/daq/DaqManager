
include(xProject)
include(Tools)

if(NOT BOOST_ROOT AND IS_DIRECTORY "/opt/ntof/boost")
    message(STATUS "Using preferred boost version: /opt/ntof/boost")
    set(BOOST_ROOT "/opt/ntof/boost" CACHE PATH "")
endif()

find_package(Boost COMPONENTS system thread regex log date_time atomic filesystem program_options chrono)

assert(Boost_FOUND
    MESSAGE "Boost not found, please install ntof-boost package and use BOOST_ROOT option")
assert(NOT "${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION}" VERSION_LESS "1.61"
    MESSAGE "Boost version too old (${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION} < 1.61), please install ntof-boost package and use BOOST_ROOT option")

# CMake <= 2.8 does not create Boost_LIBRARIES variable
set(Boost_LIBRARIES
	${Boost_ATOMIC_LIBRARY} ${Boost_REGEX_LIBRARY} ${Boost_CHRONO_LIBRARY}
	${Boost_LOG_LIBRARY} ${Boost_SYSTEM_LIBRARY} ${Boost_THREAD_LIBRARY}
	${Boost_DATE_TIME_LIBRARY} ${Boost_PROGRAM_OPTIONS_LIBRARY} ${Boost_FILESYSTEM_LIBRARY})
