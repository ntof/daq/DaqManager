
include(Tools)

if(SPDEVICES)

    find_path(SPDEVICES_INCLUDE_DIRS ADQAPI.h)
    find_library(SPDEVICES_LIBRARIES adq)
    assert(SPDEVICES_INCLUDE_DIRS AND SPDEVICES_LIBRARIES
        MESSAGE "Failed to find libadq, please install libadq0 package (ntof cc7 repo)")

    # Required to compile using ADQAPI.h
    add_definitions(-DLINUX)
endif()
