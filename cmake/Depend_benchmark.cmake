
include(xProject)
include(ExtCache)

option(BENCHMARKS "Enable Google Benchmark" OFF)

if(BENCHMARKS)
set(cache_file ${CMAKE_BINARY_DIR}/xProjectCache.cmake)
create_initial_cache(${cache_file})

xProject_Add(GBENCHMARK
    GIT_REPOSITORY "https://github.com/google/benchmark.git"
    GIT_TAG "master"
    CMAKE_ARGS -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=
        -DBENCHMARK_DOWNLOAD_DEPENDENCIES=ON
        -DBENCHMARK_ENABLE_GTEST_TESTS=OFF
        -DEXT_CACHE:path=${CMAKE_BINARY_DIR}
        -DEXTERNAL_DEPENDENCIES=ON -C ${cache_file}
    INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot"
    LIBRARIES "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_LIBDIR}/libbenchmark.a"
    INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_INCLUDEDIR}/benchmark")

endif()