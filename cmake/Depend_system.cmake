
include(Tools) # assert

# Since we statically link with other libs we need to add some libraries
find_library(PTHREAD_LIBRARIES "pthread")
assert(PTHREAD_LIBRARIES MESSAGE "Could not find pthread library")

# Readline is required
find_library(READLINE_LIBRARIES "readline")
find_path(READLINE_INCLUDE_DIRS "readline/readline.h")
assert(READLINE_LIBRARIES AND READLINE_INCLUDE_DIRS MESSAGE "Could not find readline, please install readline-devel package")

# blkird is required
find_library(BLKID_LIBRARIES "blkid")
find_path(BLKID_INCLUDE_DIRS "blkid/blkid.h")
assert(BLKID_LIBRARIES AND BLKID_INCLUDE_DIRS MESSAGE "Could not find blkid, please install libblkid-devel package")
