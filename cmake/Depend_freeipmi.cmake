
include(Tools)

find_path(FREEIPMI_INCLUDE_DIRS freeipmi/freeipmi.h)
find_library(FREEIPMI_LIBRARIES freeipmi)
find_library(FREEIPMI_MONITORING ipmimonitoring)

assert(FREEIPMI_INCLUDE_DIRS AND FREEIPMI_LIBRARIES AND FREEIPMI_MONITORING
    MESSAGE "Failed to find freeipmi, please install freeipmi-devel package")
set(FREEIPMI_LIBRARIES ${FREEIPMI_MONITORING} ${FREEIPMI_LIBRARIES})
