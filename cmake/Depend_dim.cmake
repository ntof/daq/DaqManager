
include(xProject)
include(Cern)
include(ExtCache)

set(cache_file ${CMAKE_BINARY_DIR}/xProjectCache.cmake)
create_initial_cache(${cache_file})

xProject_Add(DIM
    GIT_SUBMODULE
    CMAKE_ARGS -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=
        -DJDIM=OFF
	-DEXT_CACHE:path=${CMAKE_BINARY_DIR}
        -C ${cache_file}
    INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot"
    LIBRARIES "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_LIBDIR}/libdim.a"
    INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_INCLUDEDIR}/dim")
