/*! \file CDaq.h
 \brief CDaq class
 This class aims at discovering, initializing all the cards present in the
 system. It also manages the acquisition and writer threads.
 */

#ifndef CDAQ_H_
#define CDAQ_H_

#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <boost/thread.hpp>

#include <stdint.h>

#include "CAcquisition.h"
#include "CDaqEx.h"
#include "CWriter.h"
#include "CZeroSuppression.h"
#include "DIMData.h"
#include "DIMState.h"
#include "DIMSuperCommand.h"
#include "Queue.h"
#include "defines.h"
#include "typedefs.h"

/**
 * Enumeration of the different DAQ states
 */
enum class DAQ_STATE_t : int32_t
{
  ERROR = STATE_ERROR,
  DETECTING_HARDWARE = 0, //!< detecting cards present in the system
  UNCONFIGURED,           //!< Not configured
  CALIBRATING_CARDS,
  WAITING_FOR_CMD, //!< Waiting for a command
  PROCESSING_CMD,  //!< Processing the command
  WAITING_FOR_HEADERS,
  WAITING_FOR_MODH,
  WAITING_FOR_RCTR,
  STOPPING_ACQ,
  STARTING_ACQ,
  RESETING_DAQ,
  INITIALIZING_ACQ,
  WAITING_FOR_START_ACQ_CMD,
  SUPER_USER
};

typedef struct _xmlHwParams
{
  std::string sn;
  int cpuCore;
  int crate;
  int zsmaster;
  _xmlHwParams() : sn("unknown"), cpuCore(0), crate(0), zsmaster(-1) {}
} XML_HW_PARAMS_t;

/**
 * CDaq class
 */
class CDaq : public ntof::dim::DIMSuperCommand
{
public:
  /**
   * Class ctor
   */
  CDaq(std::string hwConfigFilename,
       std::string hostName,
       CMD_LINE_PARAM_t cmdLineParams);
  /**
   * Class dtor
   */
  ~CDaq();
  /**
   * Get the current state of the DAQ
   * @return
   */
  DAQ_STATE_t GetState();
  /**
   * Set the next state of the DAQ
   * @param state
   */
  void SetState(DAQ_STATE_t state);
  /**
   * Set DAQ state in error
   * @param err the error message
   */
  void SetStateError(std::string err);
  /**
   * Find all acquisition cards present in the chassis
   */
  void FindDevices();
  /**
   * Delete all the CCard instances
   */
  void DeleteAll();
  /**
   * Start the acquisition and writer threads
   */
  void InitAcq();
  /**
   * Start the acquisition and writer threads
   */
  void StartAcq();
  /**
   * Stop the acquisition and writer threads
   */
  void StopAcq();
  /**
   * Initialize DIM services
   */
  void InitServiceDim();
  /**
   * Initialize acquisition and writer threads
   * @param[in] lockFile advisory lock for writer and acquisition threads
   */
  void InitializeThreads(const std::string &lockFile = std::string());
  /**
   * Update card's configuration after the reception of valid parameters through
   * DIM service
   */
  void UpdateCardConfiguration();
  /**
   * Get the collection of CCard objects
   * @return vector of CCard
   */
  std::vector<std::shared_ptr<CCard>> &GetCardCollection();
  /**
   * Check if module header is valid
   * @return true if valid
   */
  bool IsModhConfigurationValid();
  /**
   * Check if run control header is valid
   * @return true if valid
   */
  bool IsRctrConfigurationValid();
  /**
   * Reset DAQ errors
   */
  void ResetErrors();
  /**
   * Invalidate the card configurations
   */
  void SetConfigurationInvalid();
  /**
   * Check if calibration has been required by the user
   * @return true if card should be calibrated during hardware detection
   */
  bool isCardCalibrationEnabled();
  /**
   * Enable the card calibration procedure
   * @param cardCalibrationEnabled should be set to true when calibration is
   * required
   */
  void setCardCalibrationEnabled(bool cardCalibrationEnabled);

private:
  /**
   * Create the CCard instances for all the devices
   */
  void CreateCards();
  /**
   * Test function that configures cards with software trigger and trigger them
   * periodically
   */
  void TestSoftTriggeredAcq();
  /**
   * Create SP Devices card objects
   */
  void CreateSpDevicesCards();
  /**
   * callback for DIM command
   * @param cmdData
   */
  void commandReceived(ntof::dim::DIMCmd &cmdData);
  /**
   * Process "stop" command
   * @param cmdData
   */
  void ProcessStopCommand(ntof::dim::DIMCmd &cmdData);
  /**
   * Process "initialization" command
   * @param cmdData
   */
  void ProcessInitializationCommand(ntof::dim::DIMCmd &cmdData);
  /**
   * Process "start" command
   * @param cmdData
   */
  void ProcessStartCommand(ntof::dim::DIMCmd &cmdData);
  /**
   * Process "super_user" command
   * @param cmdData
   */
  void ProcessSuperUserCommand(ntof::dim::DIMCmd &cmdData);
  /**
   * Process "reset" command
   * @param cmdData
   */
  void ProcessResetCommand(ntof::dim::DIMCmd &cmdData);
  /**
   * Process "update" command
   * @param cmdData
   */
  void ProcessUpdateCommand(ntof::dim::DIMCmd &cmdData);
  /**
   * Process "calibrate" command
   * @param cmdData
   */
  void ProcessCalibrateCommand(ntof::dim::DIMCmd &cmdData);
  /**
   * Get card parameters from xml file
   * @param model
   * @return
   */
  XML_HW_PARAMS_t GetXmlHardwareParams(
    const pugi::xml_object_range<pugi::xml_node_iterator> &children);
  /**
   *
   * @param res
   * @param str
   * @param delimiter
   */
  void ProcessDimCmdParam(std::vector<std::string> &res,
                          std::string str,
                          std::string delimiter);

  ntof::dim::DIMState state_;                 //!< state of the DAQ
  std::vector<std::shared_ptr<CCard>> cards_; //!< Collection of cards
  uint32_t nbCards_; //!< Number of cards present in the system
  CBoundedCircularBuffer<CAcquisitionParameters> acqQueue_;
  std::string hostName_;              //!< host's name used for DIM objects
  std::shared_ptr<CWriter> writer_;   //!< writer
  std::shared_ptr<CAcquisition> acq_; //!< acquisition
  std::string hwConfigFilename_;   //!< file with the hardware configuration of
                                   //!< the chassis
  CMD_LINE_PARAM_t cmdLineParams_; //!< command line parameters
  ntof::dim::DIMXMLService dimListDaqElements_; //!< list the cards and number
                                                //!< of channels
  bool acquisitionRunning_;     //!< set to true when acquisition is running
  bool cardCalibrationEnabled_; //!< set to true when card calibration is
                                //!< requested
  pugi::xml_document xmlDigitizerList_; //!< digitizer list in xml format
  uint32_t runNumber_;
};

#endif /* CDAQ_H_ */
