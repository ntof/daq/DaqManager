#ifndef TYPEDEFS_H_
#define TYPEDEFS_H_

#include <string>
#include <vector>

#include <boost/align/aligned_alloc.hpp>
#include <boost/align/aligned_allocator.hpp>
#include <boost/log/trivial.hpp>

#include <fcntl.h>

#include "defines.h"

namespace ntof {
typedef struct
{
  boost::log::trivial::severity_level loggerLevel;
  std::string logPath;
  std::string hwConfigFilename;
  bool cardCalibrationEnabled;
  std::string dimDnsServer;
  bool writePulseRawData;
  bool writeCardRawData;
  std::string timingEventName;
  bool standAlone;
  std::string dataPath;
  float zsThreshold;
  bool cardProbingMode;
  uint32_t sampleSize;
  size_t sectorSize;
  int32_t fullScale;
  float offset;
  bool simulation;
  uint32_t simBufferSize;
  std::string simBufferSizeString;
  std::string writerFileBufferSizeString;
  uint64_t writerFileBufferSize;
  int nbVirtualCards;
  int acqPoolSize;
  bool spdLogTrace;
  std::string lockFile;
} CMD_LINE_PARAM_t;

template<class T, std::size_t Alignment>
using aligned_vector =
  std::vector<T, boost::alignment::aligned_allocator<T, Alignment>>;

// skip header structure
struct skipHeader
{
  const uint32_t title = 0x50494B53; // SKIP string in hexa
  const uint32_t rev = WRITER_REV_NUMBER;
  const uint32_t rsvd1 = 0;
  const uint32_t words = 4;
  const uint64_t start = 512;
  uint64_t end = 0;
  const int32_t rsvd2[120] = {0};
} /*__attribute__((__aligned__(DIRECTIO_BLOCK_SIZE)))*/;

// acqc structure
struct acqc
{
  const uint32_t title = 0x43514341; // ACQC string in hexa
  const uint32_t revNumber = WRITER_REV_NUMBER;
  const uint32_t reserved = 0;
  uint32_t length;
  uint32_t detectorType;
  uint32_t detectorId;
  uint32_t str_crate_mod_ch;
};

// zero suppression parameter structure
namespace ZeroSuppression {
struct zsSlave
{
  std::string sn;
  int cardIndex;
  int index;
};

struct zsMaster
{
  int id;
  std::string masterSn;
  int cardIndex;
  int channel;
  std::vector<zsSlave> slaveList;
};

enum zsMode : int
{
  INDEPENDENT = 0,
  SINGLE_MASTER = 1,
  MULTIPLE_MASTER = 2
};

} // namespace ZeroSuppression
} // namespace ntof

#endif
