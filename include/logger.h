#ifndef LOGGER_H_
#define LOGGER_H_

#include <iostream>
#include <string>

#include <boost/log/attributes/scoped_attribute.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/trivial.hpp>

#include <config.h>

//#include <g3log/logmessage.hpp>

//#define ENABLE_TRACE_LOGS
#ifndef ENABLE_TRACE_LOGS
#define LOG_TRACE \
  if (0)          \
  BOOST_LOG_SEV(global_logger::get(), boost::log::trivial::trace)
#else
#define LOG_TRACE \
  BOOST_LOG_SEV(global_logger::get(), boost::log::trivial::trace)
#endif // ENABLE_TRACE_LOGS
#define LOG_DEBUG \
  BOOST_LOG_SEV(global_logger::get(), boost::log::trivial::debug)
#define LOG_INFO BOOST_LOG_SEV(global_logger::get(), boost::log::trivial::info)
#define LOG_WARNING \
  BOOST_LOG_SEV(global_logger::get(), boost::log::trivial::warning)
#define LOG_ERROR \
  BOOST_LOG_SEV(global_logger::get(), boost::log::trivial::error)
#define LOG_FATAL \
  BOOST_LOG_SEV(global_logger::get(), boost::log::trivial::fatal)

typedef boost::log::sources::severity_logger_mt<boost::log::trivial::severity_level>
  logger_t;

// declares a global logger with a custom initialization
BOOST_LOG_INLINE_GLOBAL_LOGGER_DEFAULT(global_logger, logger_t)

/*struct g3logSink {

// Linux xterm color
//
http://stackoverflow.com/questions/2616906/how-do-i-output-coloured-text-to-a-linux-terminal
  enum FG_Color {YELLOW = 33, RED = 31, GREEN=32, WHITE = 97};

  FG_Color GetColor(const LEVELS level) const {
     if (level.value == WARNING.value) { return YELLOW; }
     if (level.value == DEBUG.value) { return GREEN; }
     if (g3::internal::wasFatal(level)) { return RED; }

     return WHITE;
  }

  void ReceiveLogMessage(g3::LogMessageMover logEntry) {
     auto level = logEntry.get()._level;
     auto color = GetColor(level);

     std::cout << "\033[" << color << "m"
       << logEntry.get().toString() << "\033[m" << std::endl;
  }
};*/

void InitLogger(const std::string &logPath,
                boost::log::trivial::severity_level level);

#endif
