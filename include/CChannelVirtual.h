#ifndef CCHANNELVIRTUAL_H_
#define CCHANNELVIRTUAL_H_

#include <CChannel.h>

/**
 * CChannel class for virtual card
 */
namespace virtualcard {
class CChannelVirtual : public ntof::CChannel
{
public:
  /**
   * Constructor
   * @param logger logger
   * @param cardIndex card's index
   * @param channelIndex card channel's index
   * @param dimServiceName DIM service name
   */
  CChannelVirtual(uint32_t location,
                  std::string dimServiceName,
                  ntof::dim::DIMState *state);
  /**
   * destructor
   */
  ~CChannelVirtual();
  /**
   * Callback for DIM parameter list
   * @param settingsChanged
   * @param list
   * @param errCode
   * @param errMsg
   * @return
   */
  virtual int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                               const ntof::dim::DIMParamList &list,
                               int &errCode,
                               std::string &errMsg) override;

private:
  const std::string cardName_ = "VIRT";
  const double maxSamplingRate_ = 1000.0;
  const double maxSamplingSize_ = 256E6;
  const double maxFullScale_ = 5000.0;
  const double minFullScale_ = 100.0;
  const int nbChannels_ = 4;
};
} // namespace virtualcard

#endif /* CCHANNELVIRTUAL_H_ */
