#ifndef CCARDVIRTUAL_H_
#define CCARDVIRTUAL_H_

#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include "CCard.h"
#include "CChannel.h"
#include "EventReader.h"

namespace virtualcard {

struct VirtualCardParameters
{
  int16_t **buffer_;
  int8_t *dumpBuffer_;
  uint cardIndex_;
  boost::atomic<int> refcount_;
  bool isReady_;
  int cpu_;
  int node_;
  int parsingZsStatus_;
  int dumpStatus_;
  std::string sn_;

  VirtualCardParameters() :
    buffer_(nullptr),
    dumpBuffer_(nullptr),
    cardIndex_(0),
    refcount_(0),
    isReady_(false),
    cpu_(0),
    node_(0),
    parsingZsStatus_(0),
    dumpStatus_(0)
  {}
  ~VirtualCardParameters() { _mm_free(dumpBuffer_), dumpBuffer_ = nullptr; }
};

class CCardVirtual : public CCard, public ntof::dim::EventHandler
{
public:
  CCardVirtual(std::string type,
               std::string serialNumber,
               int cpuCore,
               uint32_t cardIndex,
               uint32_t nbDevices,
               ntof::dim::DIMState *state,
               uint32_t bufferSize,
               bool enableTrace);
  virtual ~CCardVirtual();

  void FindDevices();
  /**
   *
   * @return
   */
  uint32_t GetNbDevices();
  /**
   * Arm the card's trigger
   */
  virtual void ArmTrigger() final;
  /**
   * Check if card has triggered
   * @return true is card has triggered
   */
  virtual bool IsTriggered() final;
  /**
   * Get the acquisition buffer pointer
   * @return pointer to the buffers
   */
  virtual int16_t **GetAcquisitionBuffer() final;
  /**
   * Free allocated memory
   */
  virtual void SetChannelBufferFree() final;
  /**
   * Get memory dump buffer
   * @return a pointer to the buffer used for offline parsing
   */
  virtual int8_t *GetMemoryDumpBuffer() final;
  /**
   * Get the dump buffer's size
   * @return size in byte
   */
  virtual uint32_t GetDumpBufferSize() final;
  /**
   * Get the size of a channel's buffer
   * @return size in byte
   */
  virtual uint32_t GetChannelBufferSize() final;
  /**
   * Get the index of the card
   * @return card's index
   */
  virtual uint32_t GetCardIndex() final;
  /**
   * Get the number of physical channels on the card
   * @return number of channels
   */
  virtual uint32_t GetNbChannels() final;
  /**
   * Calibrate the card
   */
  virtual void Calibrate();
  virtual void LoadDefaultConfiguration();
  virtual void configure();
  virtual bool HasOfflineParsing();
  virtual void DumpCardMemory();
  virtual void Delete();
  virtual int GetResolution() final;
  /**
   * Set buffer for memory dump
   * @param buffer
   */
  virtual void SetDumpMemoryBuffer(int8_t *buffer);
  virtual uint32_t GetNbBytesDumped();

protected:
  virtual void ConfigureChannels();
  static bool ready_;
  static std::vector<std::shared_ptr<VirtualCardParameters>> cards_;
  uint cardIndex_;

private:
  uint32_t nbDevices_;
  uint32_t bufferSize_;
  std::mutex mutex_;
  int64_t eventNumber_; //!< Event number
  int64_t cycleStamp_;  //!< Cycle stamp
  std::string type_;    //!< Beam type
  bool hasTriggered_;
  ntof::dim::EventReader eventReader_;
  void eventReceived(const ntof::dim::EventReader::Data &event);
  // void errorReceived(std::string errMsg, const dim::DIMXMLInfo* info);
  // void dataReceived(pugi::xml_document& doc, const dim::DIMXMLInfo* info);
  void noLink();
};
} // namespace virtualcard

#endif /* CCARDVIRTUAL_H_ */
