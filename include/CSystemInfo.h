/*
 * CSystemInfo.h
 *
 *  Created on: Oct 29, 2014
 *      Author: pperonna
 */

#ifndef CSYSTEMINFO_H_
#define CSYSTEMINFO_H_

#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/thread.hpp>

#include "CIpmi.h"
#include "DIMXMLService.h"
#include "sys/sysinfo.h"
#include "sys/times.h"
#include "sys/types.h"
//#include "sys/vtimes.h"

class CSystemInfo
{
public:
  CSystemInfo();
  virtual ~CSystemInfo();
  long long GetTotalVirtualMemory();
  long long GetSystemVirtualMemoryUsage();
  int GetProcessVirtualMemoryUsage();
  long long GetTotalPhysicalMemory();
  long long GetSystemPhysicalMemoryUsage();
  int GetProcessPhysicalMemoryUsage();
  double GetSystemCpuUsage();
  double GetProcessCpuUsage();
  static double GetDiskUsedSpace();
  static double GetDiskTotalCapacity();
  static std::string GetFilesystem(std::string path);

private:
  void InfoThread();
  int parseLine(char *line);
  struct sysinfo memInfo;
  long long totalVirtualMemory_;
  long long systemVirtualMemoryUsed_;
  int processVirtualMem_;
  long long totalPhysicalMemory_;
  long long systemPhysicalMemoryUsed_;
  int processPhysicalMem_;
  double systemCpuUsage_;
  double processCpuUsage_;
  boost::thread thread_;
  // std::shared_ptr<ntof::dim::DIMXMLService> dimXmlService_;
};

#endif /* CSYSTEMINFO_H_ */
