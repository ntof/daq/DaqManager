#ifndef SPD_ADQ412_H
#define SPD_ADQ412_H

#include <cstdint>
#include <vector>

#include <ADQAPI.h>

#include "spd/CSpd.h"
#include "spd/spd_typedef.h"

namespace spdevices {
class Adq412 : public CSpd
{
public:
  Adq412(void *driverInstance, uint adqNumber);
  ~Adq412();
  /**
   *
   * @param samplingFreq
   * @param nbSamples
   * @param delay
   */
  void SetAcquisitionParameters(float samplingFreq,
                                int32_t nbSamples,
                                int32_t delay);
  /**
   *
   * @return
   */
  double GetBaseSamplingFreq();
  /**
   *
   */
  void MemoryDump();
  /**
   *
   * @param dumpBuffer
   */
  void GetAcquisition(int8_t *dumpBuffer);
  void Calibrate();
  void CalibrationUpdate(bool enable);

protected:
private:
  bool performanceCounterEnabled;
  double triggerTime;
  double transferDataRate;
  double parsingDataRate;
  double hddDataRate;
  double hddTransferTime;
  double hddTransferSize;
};
} // namespace spdevices
#endif // ADQ412_H
