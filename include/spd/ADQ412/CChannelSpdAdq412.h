/*
 * CChannelSpdAdq412.h
 *
 *  Created on: Nov 28, 2014
 *      Author: pperonna
 */

#ifndef CCHANNELSPDADQ412_H_
#define CCHANNELSPDADQ412_H_

#include <CChannel.h>

namespace spdevices {

/**
 * CChannel class for SP Devices ADQ412 card
 */
class CChannelSpdAdq412 : public ntof::CChannel
{
public:
  /**
   * Constructor
   * @param logger logger
   * @param cardIndex card's index
   * @param channelIndex card channel's index
   * @param dimServiceName DIM service name
   */
  CChannelSpdAdq412(uint32_t location,
                    std::string dimServiceName,
                    ntof::dim::DIMState *state);
  /**
   * destructor
   */
  ~CChannelSpdAdq412();
  /**
   * Callback for DIM parameter list
   * @param settingsChanged
   * @param list
   * @param errCode
   * @param errMsg
   * @return
   */
  virtual int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                               const ntof::dim::DIMParamList &list,
                               int &errCode,
                               std::string &errMsg) override;

private:
};

} /* namespace spdevices */

#endif /* CCHANNELSPDADQ412_H_ */
