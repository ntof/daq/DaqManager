#ifndef CSPD_H
#define CSPD_H

#include <cstdint>
#include <vector>

#include <ADQAPI.h>

#include "CCard.h"
#include "spd/spd_typedef.h"

namespace spdevices {
/**
 * Class for SP Devices digitizer
 */
class CSpd
{
public:
  /**
   * Class's constructor
   * @param instrID intrument's ID used by API
   * @param cardIndex card's index
   */
  CSpd(std::string cardModelName,
       void *driverInstance,
       int adqNumber,
       uint nbChannels,
       uint resolution,
       uint realResolution);
  /**
   * Class's destructor
   */
  virtual ~CSpd();
  /**
   * Get number of resolution bits
   * @return number of resolution bits
   */
  uint GetResolutionBits();
  /**
   * Get number of channels present on this card
   * @return number of channels
   */
  uint GetNbChannels();
  /**
   * Get card's index
   * @return index
   */
  virtual int32_t GetCardIndex() final;
  /**
   * Get card's model name
   * @return string with model name
   */
  virtual std::string GetCardModelName();
  /**
   * Get various on-board temperatures
   * @param temperature
   */
  virtual CARD_TEMPERATURE_t GetTemperature();
  /**
   * Returns whether there has been an overflow of the internal FPGA data buffers
   * @return 1 for overflow condition detected and 0 for no overflow condition
   * detected
   */
  virtual int32_t GetBufferOverflow() final;
  /**
   * Enable sample skip of input data
   * @param divider
   */
  virtual void SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER divider) final;
  /**
   *
   * @param samplingFreq
   * @param nbSamples
   * @param delay
   */
  virtual void SetAcquisitionParameters(float samplingFreq,
                                        int32_t nbSamples,
                                        int32_t delay) = 0;
  /**
   * Returns the serial number of the ADQ device.
   * @return serial number string
   */
  virtual std::string GetSerialNumber() final;
  /**
   * Sets how the ADQ should be triggered
   * @param triggerMode selected trigger mode
   */
  virtual void SetTriggerMode(TRIGGER_MODE triggerMode) final;
  /**
   *
   * @param clkSource
   */
  virtual void SetClockSource(CLOCK_SOURCE clkSource) final;
  /**
   * Specifies the length of time from the trigger event to the first point in
   * the waveform record. If the value is positive, the first point in the
   * waveform record occurs after the trigger event. If the value is negative,
   * the first point in the waveform record occurs before the trigger event.
   * @param delay
   * @param samplingFreq
   */
  virtual void SetTriggerDelay(int32_t delay,
                               float samplingFreq,
                               uint rounding) final;
  /**
   *
   * @param intMode
   */
  virtual void SetInterleavingMode(INTERLEAVING_MODE intMode) final;
  /**
   *
   * @param ch
   * @param inputRange
   * @param actualInputRange
   */
  virtual void SetInputRange(int32_t ch,
                             double inputRange,
                             float *actualInputRange) final;
  /**
   *
   * @param ch
   * @param range
   * @param offset
   * @param impedance
   * @param enabled
   */
  virtual void ConfigureChannel(uint32_t ch,
                                double range,
                                double offset,
                                double impedance,
                                bool enabled) final;
  /**
   *
   * @param ch
   * @param inputRange
   */
  virtual void SetInputRange(int32_t ch, double inputRange) final;
  /**
   *
   * @param inputRange
   */
  virtual void SetInputRange(double inputRange) final;
  /**
   *
   * @param ch
   * @param bias
   */
  virtual void SetAdjustableBias(int32_t ch, double bias) final;
  /**
   *
   * @param ch
   * @param bias
   */
  virtual void SetAdjustableBias(int32_t ch, int bias) final;
  /**
   *
   * @param bias
   */
  virtual void SetAdjustableBias(double bias) final;
  /**
   *
   * @return
   */
  virtual std::string GetMultiRecordInfo() final;
  /**
   * Get base sampling frequency
   * @return sampling frequency in MHz
   */
  virtual double GetBaseSamplingFreq() = 0;
  /**
   *
   * @return
   */
  virtual uint32_t GetDumpBufferSize() final;
  /**
   *
   * @return
   */
  virtual int16_t **GetAcquisitionBuffer() final;
  /**
   *
   */
  virtual void ArmTrigger() final;
  /**
   *
   * @return
   */
  virtual bool IsTriggered() final;
  /**
   *
   * @param buffer
   */
  virtual void SetAcquisitionBuffer(int16_t **) final;
  void SetDumpMemoryBuffer(int8_t *buffer);
  int8_t *GetMemoryDumpBuffer();
  void FreeMemory();
  virtual uint32_t GetChannelBufferSize() final;
  virtual uint32_t GetNbBytesDumped() final;
  virtual void Calibrate();
  virtual void CalibrationUpdate(bool enable);
  virtual void MemoryDump() = 0;
  virtual void GetAcquisition(int8_t *dumpBuffer);
  virtual std::vector<float> &GetCalibratedFullScale();
  virtual void SetCalibratedFullScale(int ch, float fs);
  virtual std::vector<float> &GetCalibratedOffset();
  virtual float GetCalibratedFullScale(int index);
  virtual float GetCalibratedOffset(int index);

protected:
  void LogAcquisitionParams(double samplingFreq,
                            double baseFreq,
                            double divider);

  void *driverInstance_; //!< driver instance
  uint32_t adqNumber_;
  uint cardIndex_;
  multiRecordInfo_t mrInfo_;
  int16_t **targetBuffer_;
  int8_t *dramShadow_;
  uint32_t nbSamples_;      //!< number of acquisition samples
  uint32_t nbValidSamples_; //!< number of valid samples
  uint nbChannels_;         //!< number of channels
  uint resolution_;         //!< number of resolution bits
  uint realResolution_;
  std::vector<bool> channelEnabled_;
  std::string cardModelName_;
  uint32_t nbChannelEnabled_;
  uint32_t slotNumber_;
  uint32_t nbBytesDumped_;
  CARD_TEMPERATURE_t temperature_;
  TRIGGER_MODE triggerMode_;
  INTERLEAVING_MODE interleavingMode_;
  bool offlineParsingEnabled_;
  uint32_t memoryDumpByteRead_;
  uint8_t channelMask_;
  uint32_t maxSamples_;
  std::vector<float> calibratedFullScale_;
  std::vector<float> calibratedOffset_;
  std::vector<float> fullScale_;
};

} // namespace spdevices
#endif // CSPD_H
