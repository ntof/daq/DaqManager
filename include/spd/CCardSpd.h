#ifndef CCARDSPD_H_
#define CCARDSPD_H_

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <ADQAPI.h>

#include "ADQ14/CSpdAdq14.h"
#include "ADQ412/CSpdAdq412.h"
#include "CCard.h"

namespace spdevices {
class CCardSpd : public CCard
{
public:
  /**
   * Class constructor
   */
  CCardSpd(std::string type,
           std::string serialNumber,
           int cpuCore,
           uint32_t cardIndex,
           uint32_t crateIndex,
           ntof::dim::DIMState *state,
           bool enableTrace);
  /**
   * Class destructor
   */
  virtual ~CCardSpd();
  /**
   *
   */
  void FindDevices();
  /**
   *
   * @return
   */
  uint32_t GetNbDevices();
  /**
   * Arm the card's trigger
   */
  virtual void ArmTrigger() final;
  /**
   * Check if card has triggered
   * @return true is card has triggered
   */
  virtual bool IsTriggered() final;
  /**
   * Get the acquisition buffer pointer
   * @return pointer to the buffers
   */
  virtual int16_t **GetAcquisitionBuffer() final;
  /**
   * Free allocated memory
   */
  virtual void SetChannelBufferFree() final;
  /**
   * Get memory dump buffer
   * @return a pointer to the buffer used for offline parsing
   */
  virtual int8_t *GetMemoryDumpBuffer() final;
  /**
   * Get the dump buffer's size
   * @return size in byte
   */
  virtual uint32_t GetDumpBufferSize() final;
  /**
   * Get the size of a channel's buffer
   * @return size in byte
   */
  virtual uint32_t GetChannelBufferSize() final;
  /**
   * Get the index of the card
   * @return card's index
   */
  virtual uint32_t GetCardIndex() final;
  /**
   * Get the number of physical channels on the card
   * @return number of channels
   */
  virtual uint32_t GetNbChannels() final;
  /**
   * Calibrate the card
   */
  virtual void Calibrate();
  virtual void LoadDefaultConfiguration() = 0;
  virtual void configure() = 0;
  virtual void OfflineParsing(int8_t *dumpBuffer);
  virtual bool HasOfflineParsing() = 0;
  virtual void DumpCardMemory();
  virtual void Delete() = 0;
  virtual int GetResolution() final;
  /**
   * Set buffer for memory dump
   * @param buffer
   */
  virtual void SetDumpMemoryBuffer(int8_t *buffer);
  virtual uint32_t GetNbBytesDumped();
  virtual uint32_t GetPCIeLinkRate();
  virtual uint32_t GetPCIeLinkWidth();

protected:
  virtual void ConfigureChannels();
  static bool ready_;
  static void *driverInstance_; //!< driver's instance
  static std::vector<std::shared_ptr<CSpd>> spdCards_;
  static bool enableTrace_;
  uint spdIndex_;
  bool readOutDone_; //!< true if readout was done
  bool dumpDone_;    //!< true if memory dump was
  int adqNumber_;

private:
  uint32_t nbDevices_;
};
} // namespace spdevices

#endif /* CCARDSPD_H_ */
