/*! \file CCardSpDevices.h
 \brief CCardSpDevices class
 detailed description
 */

#ifndef CCARDSPDADQ14_H
#define CCARDSPDADQ14_H

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <boost/thread.hpp>
#include <boost/thread/condition_variable.hpp>

#include <ADQAPI.h>

#include "CCard.h"
#include "CChannelSpdAdq14.h"
#include "CSpdAdq14.h"
#include "CZeroSuppression.h"
#include "DIMData.h"
#include "DIMException.h"
#include "DIMParamList.h"
#include "DaqTypes.h"
#include "spd/CCardSpd.h"
#include "spd/spd_typedef.h"

#include <dis.hxx>

namespace spdevices {
/**
 * SP Devices ADQ14 class
 */
class CCardSpdAdq14 : public CCardSpd
{
public:
  /**
   * Class constructor
   */
  CCardSpdAdq14(std::string serialNumber,
                int cpuCore,
                uint32_t cardIndex,
                uint32_t crateIndex,
                ntof::dim::DIMState *state,
                bool enableTrace);
  /**
   * Class destructor
   */
  ~CCardSpdAdq14();
  /**
   * Load a default configuration in the card (range, sampling rate...)
   */
  void LoadDefaultConfiguration();
  /**
   *
   */
  void configure();
  /**
   *
   * @return
   */
  bool HasOfflineParsing();
  /**
   *
   */
  void Delete();

private:
  void ClassInit();
};
} // namespace spdevices
#endif // CCARDSPDADQ14_H
