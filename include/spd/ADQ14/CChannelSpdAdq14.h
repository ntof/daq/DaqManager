
#ifndef CCHANNELSPDADQ14_H_
#define CCHANNELSPDADQ14_H_

#include <CChannel.h>

namespace spdevices {

/**
 * CChannel class for SP Devices ADQ14 card
 */
class CChannelSpdAdq14 : public ntof::CChannel
{
public:
  /**
   * Constructor
   * @param logger logger
   * @param cardIndex card's index
   * @param channelIndex card channel's index
   * @param dimServiceName DIM service name
   */
  CChannelSpdAdq14(uint32_t location,
                   std::string dimServiceName,
                   ntof::dim::DIMState *state);
  /**
   * destructor
   */
  ~CChannelSpdAdq14();
  /**
   * Callback for DIM parameter list
   * @param settingsChanged
   * @param list
   * @param errCode
   * @param errMsg
   * @return
   */
  virtual int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                               const ntof::dim::DIMParamList &list,
                               int &errCode,
                               std::string &errMsg) override;

private:
  const std::string cardName_ = "S014";
  const double maxSamplingRate_ = 1000.0;
  const double maxSamplingSize_ = 256E6;
  const double maxFullScale_ = 5000.0;
  const double minFullScale_ = 50.0;
  const int nbChannels_ = 4;
};

} /* namespace spdevices */

#endif /* CCHANNELSPDADQ14_H_ */
