#ifndef SPD_ADQ14_H
#define SPD_ADQ14_H

#include <cstdint>
#include <vector>

#include <ADQAPI.h>

#include "spd/CSpd.h"
#include "spd/spd_typedef.h"

namespace spdevices {
class Adq14 : public CSpd
{
public:
  Adq14(void *driverInstance, int adqNumber);
  ~Adq14();
  /**
   * Get number of resolution bits
   * @return number of resolution bits
   */
  static uint GetResolutionBits();
  /**
   * Get number of channels present on this card
   * @return number of channels
   */
  static uint GetNbChannels();
  /**
   *
   * @param samplingFreq
   * @param nbSamples
   * @param delay
   */
  void SetAcquisitionParameters(float samplingFreq,
                                int32_t nbSamples,
                                int32_t delay);
  /**
   *
   * @return
   */
  double GetBaseSamplingFreq();
  /**
   *
   */
  void MemoryDump();

private:
  bool performanceCounterEnabled;
  double triggerTime;
  double transferDataRate;
  double parsingDataRate;
  double hddDataRate;
  double hddTransferTime;
  double hddTransferSize;
};
} // namespace spdevices
#endif // ADQ14_H
