#ifndef SPD_TYPEDEF
#define SPD_TYPEDEF

#include <ADQAPI.h>

#define SPD_SUCCESS 0

namespace spdevices {
enum TRIGGER_MODE
{
  SOFTWARE_ONLY = ADQ_SW_TRIGGER_MODE,
  EXTERNAL1 = ADQ_EXT_TRIGGER_MODE,
  LEVEL = ADQ_LEVEL_TRIGGER_MODE,
  INTERNAL,
  EXTERNAL2 = 7,
  EXTERNAL3
};

enum TRIGGER_EDGE
{
  FALLING,
  RISING
};

enum CLOCK_SOURCE
{
  INT_10M,    // Internal clock source, Internal 10 MHz reference
  EXT_10M,    // Internal clock source, External 10 MHz reference
  EXTERNAL,   // External clock source
  EXT_10M_PXI // Internal clock source, External 10 MHz reference from PXIsync
};

enum INTERLEAVING_MODE
{
  FOUR_CHANNEL,   // Four channel mode (default)
  TWO_CHANNEL_AC, // Two channel mode, inputs A / C
  TWO_CHANNEL_BD, // Two channel mode, inputs B / D
  TWO_CHANNEL_ALL // Two channel mode, all inputs active
};

enum SAMPLING_FREQ_DIVIDER
{
  NONE = 1,
  DIVIDER_2 = 2,
  DIVIDER_4 = 4,
  DIVIDER_8 = 8,
  DIVIDER_16 = 16,
  DIVIDER_32 = 32,
  DIVIDER_64 = 64,
  DIVIDER_128 = 128
};

typedef struct
{
  uint32_t dramStartAddress;
  uint32_t dramEndAddress;
  uint32_t dramAddressPerRecord;
  uint32_t dramBytePerAddress;
  uint32_t setupRecords;
  uint32_t setupSamples;
  uint32_t setupPaddedSamplesSize;
  uint32_t maxNumberRecords;
  uint32_t shadowSize;
  uint32_t rsvd;
} multiRecordInfo_t;

typedef struct
{
  double triggerTime;
  double transferDataRate;
  double parsingDataRate;
  double hddDataRate;
  double hddTransferTime;
  double hddTransferSize;
} performances_t;
} // namespace spdevices

#endif // SPD_TYPEDEF_INCLUDED
