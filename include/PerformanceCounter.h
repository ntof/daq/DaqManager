/*
 * PerformanceCounter.h
 *
 *  Created on: Oct 31, 2014
 *      Author: pperonna
 */

#ifndef PERFORMANCECOUNTER_H_
#define PERFORMANCECOUNTER_H_

#include <ctime>

#include <sys/time.h>
#include <unistd.h>

inline double StartCounter()
{
  double CounterStart;

  timeval tval;
  gettimeofday(&tval, NULL);
  CounterStart = static_cast<double>(tval.tv_sec) +
    static_cast<double>(tval.tv_usec) * 1E-6;

  return CounterStart;
}

inline double GetCounter(double CounterStart)
{
  double count;

  timeval tval;
  gettimeofday(&tval, NULL);
  count = static_cast<double>(tval.tv_sec) +
    static_cast<double>(tval.tv_usec) * 1E-6 - CounterStart;

  return count;
}

#endif /* PERFORMANCECOUNTER_H_ */
