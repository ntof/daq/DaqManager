
#ifndef CCHANNEL_H_
#define CCHANNEL_H_

#include <cstdint>
#include <iostream>
#include <string>

#include "CZeroSuppression.h"
#include "DIMData.h"
#include "DIMException.h"
#include "DIMParamList.h"
#include "DIMState.h"
#include "DaqTypes.h"

// for DIM
#include "dic.hxx"
#include "dis.hxx"

namespace ntof {

class CChannel : public dim::DIMParamListHandler
{
public:
  /**
   * @brief internal convenience struct
   */
  struct Config
  {
    ChannelConfig header;
    bool enabled;
    float inputImpedance;

    bool operator==(const Config &other) const;
  };

  /**
   * Class constructor
   * @param logger logging variable
   * @param cardIndex card's index
   * @param channelIndex channel's index
   * @param dimServiceName DIM service's name
   */
  CChannel(uint32_t location,
           const std::string &moduleType,
           float sampleRate,
           uint32_t sampleSize,
           int32_t threshold,
           const std::string &dimServiceName,
           ntof::dim::DIMState *state);
  /**
   * Destructor
   */
  ~CChannel();
  /**
   * Check if configuration is valid
   * @return true if configuration is valid
   */
  virtual bool IsConfigurationValid() final;
  /**
   * Get the zero suppression object for this channel
   * @return A reference to the zero suppression object
   */
  virtual ZeroSuppression::CZeroSuppression &GetZeroSuppression() final;
  /**
   * Callback called when a new configuration is received by DIM
   * @param settingsChanged
   * @param list
   * @param errCode
   * @param errMsg
   * @return
   */
  virtual int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                               const ntof::dim::DIMParamList &list,
                               int &errCode,
                               std::string &errMsg) = 0;
  /**
   * Set the configuration as invalid
   */
  virtual void SetConfigurationInvalid() final;

  /**
   * Get configuration structure
   * @return a pointer to the configuration structure
   */
  std::shared_ptr<CChannel::Config> &getConfiguration();

  /**
   * Get channel's index
   * @return index
   */
  uint32_t GetIndex();
  /**
   *
   */
  void UpdateZeroSuppressionConfiguration();

  /**
   *
   * @return
   */
  float GetCalibratedFullScale();
  /**
   *
   * @param fs
   */
  void SetCalibratedFullScale(float fs);
  /**
   * @details calibrated offset is relative to the configured offset
   */
  float GetCalibratedOffset();
  /**
   *
   * @return
   */
  float GetCalibratedThreshold();
  /**
   *
   * @param fs
   */
  void SetCalibratedOffset(float fs);
  /**
   *
   */
  void SetCalibratedThreshold(float th);
  /**
   *
   * @param master
   */
  void SetAsMaster(bool master);
  /**
   *
   * @param independent
   */
  void SetAsIndependent(bool independent);
  /**
   *
   * @return
   */
  bool isMaster();
  /**
   *
   * @return
   */
  bool isIndependent();
  /**
   *
   */
  void copyListParam();

  /**
   * @brief update calibration service with latest values
   * @details updates /CARDx/CHANNELx/Calibration DIM service
   */
  void updateCalibrationService();

protected:
  bool isEqual();

  uint32_t cardIndex_;
  uint32_t channelIndex_;
  std::string hostName_; //!< host's name for DIM services
  ZeroSuppression::CZeroSuppression zeroSuppression_; //!< performs zero
                                                      //!< suppression in
                                                      //!< acquisition buffer
  dim::DIMParamList dimParamList_;
  dim::DIMDataSet dimCalibration_;
  std::shared_ptr<Config> configuration_;
  std::shared_ptr<Config> configurationLast_;
  bool configurationValid_; //!< set true when received configuration is valid
  ntof::dim::DIMState *state_;
  std::string cardName_;
  double maxSamplingRate_;
  double maxSamplingSize_;
  double maxFullScale_;
  double minFullScale_;
  float fullScale_;
  float offset_;
  int nbChannels_;
  bool isMaster_;
  bool isIndependent_;
  float calibratedThreshold_;

private:
};

/**
 * Exception class for CChannel
 */
class CChannelEx : public std::exception
{
public:
  CChannelEx() throw();
  CChannelEx(std::string msg) throw();
  virtual ~CChannelEx() throw();
  virtual const char *what() const throw();
  void SetMessage(std::string msg);

private:
  std::string exceptionMsg_;
};
} // namespace ntof

#endif /* CCHANNEL_H_ */
