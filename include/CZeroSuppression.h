/*
 * CZeroSuppresion.h
 *
 *  Created on: Nov 6, 2014
 *      Author: pperonna
 */

#ifndef CZEROSUPPRESSION_H_
#define CZEROSUPPRESSION_H_

#include <cstdint>
#include <exception>
#include <string>

#include <boost/optional.hpp>

#include "DIMException.h"
#include "DIMParamList.h"
#include "DaqTypes.h"
#include "typedefs.h"

#include "dis.hxx"

namespace ntof {
namespace ZeroSuppression {
typedef struct _pulse_data
{
  const int16_t *data;
  int64_t timeStamp;
  int64_t length;
  int64_t startIndex;
  int64_t endIndex;
  bool noSuppression;
  bool isNoSuppressionWindow;
  _pulse_data() :
    data(nullptr),
    timeStamp(0),
    length(0),
    startIndex(0),
    endIndex(0),
    noSuppression(false),
    isNoSuppressionWindow(false)
  {}
} PULSE_DATA_t;

typedef enum
{
  POSITIVE,
  NEGATIVE
} PULSE_TYPE_t;

class CZeroSuppression
{
public:
  /**
   *
   * @param cardIndex
   * @param channelIndex
   */
  CZeroSuppression(uint32_t cardIndex, uint32_t channelIndex);
  /**
   *
   */
  virtual ~CZeroSuppression();
  /**
   *
   * @param data
   */
  void SetDataPointer(int16_t *&data);
  /**
   *
   * @param threshold
   */
  void SetThreshold(float threshold);
  /**
   *
   * @param threshold
   */
  void SetThreshold(int16_t threshold);
  /**
   *
   * @param size
   */
  void SetDataSize(uint64_t size);
  /**
   *
   * @param pre
   */
  void SetPre(uint32_t pre);
  /**
   *
   * @param post
   */
  void SetPost(uint32_t post);
  /**
   *
   * @param pre
   * @param post
   */
  void SetPrePost(uint32_t pre, uint32_t post);
  /**
   *
   * @param gain
   * @param calibratedGain
   * @param offset
   */
  void SetGainOffset(float gain, float calibratedGain, float offset);
  /**
   *
   * @param tw
   * @param nbSamples
   */
  void SetTimeWindow(uint32_t tw, uint64_t nbSamples);
  /**
   *
   * @param type
   */
  void SetPulseDetectionType(PULSE_TYPE_t type);
  /**
   *
   * @return
   */
  std::vector<PULSE_DATA_t> &GetPulseDataCollection();
  /**
   *
   * @param data
   * @param nbSamples
   * @param result
   * @return
   */
  boost::optional<uint64_t> PerformZeroSuppression(
    const int16_t *__restrict data,
    uint64_t nbSamples,
    const int16_t *__restrict result);
  /**
   *
   * @param data
   * @param nbSamples
   * @param masterPulseCollection
   * @param channelConfig
   * @param masterChannelConfig
   */
  void SingleMaster(const int16_t *__restrict data,
                    uint64_t nbSamples,
                    std::vector<PULSE_DATA_t> &masterPulseCollection,
                    const ChannelConfig &channelConfig,
                    const ChannelConfig &masterChannelConfig);
  /**
   *
   * @return
   */
  int8_t *MultipleMaster();
  /**
   *
   */
  void InitDimService();
  /**
   *
   * @param bit
   */
  void SetResolution(int bit);
  /**
   *
   * @param mask
   */
  void SetBitMask(int mask);
  /**
   *
   * @return
   */
  uint32_t GetChannelIndex();
  /**
   *
   * @return
   */
  uint32_t GetCardIndex();
  /**
   *
   * @return
   */
  int16_t GetThresholdCode();

private:
  int resolution_;
  uint32_t cardIndex_;
  uint32_t channelIndex_;
  void ClassInit();
  int16_t *data_;
  int16_t *result_;
  float threshold_;
  int16_t thresholdCode_;
  uint64_t size_;
  uint32_t pre_;
  uint32_t post_;
  PULSE_TYPE_t pulseDetectionType_;
  std::vector<PULSE_DATA_t> pulseData_;
  float gain_, calibratedGain_;
  float offset_;
  uint32_t timeWindow_;
  bool isZsOff_;
};

class CZeroSuppressionEx : public std::exception
{
public:
  CZeroSuppressionEx() throw();
  explicit CZeroSuppressionEx(const std::string &msg) throw();
  virtual ~CZeroSuppressionEx() throw();
  virtual const char *what() const throw();
  void SetMessage(const std::string &msg);

private:
  std::string exceptionMsg_;
};
} // namespace ZeroSuppression
} // namespace ntof

#endif /* CZEROSUPPRESION_H_ */
