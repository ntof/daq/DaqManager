/*
 * CIpmi.h
 *
 *  Created on: Jul 9, 2015
 *      Author: pperonna
 */

#ifndef CIPMI_H_
#define CIPMI_H_

#include <string>
#include <vector>

#include <ipmi_monitoring.h>

namespace ntof {

typedef struct
{
  std::string name;
  std::string type;
  std::string value;
  std::string unit;
  std::string state;
} IPMI_SENSOR_t;

class CIpmi
{
public:
  CIpmi();
  virtual ~CIpmi();

  std::vector<IPMI_SENSOR_t> &GetMonitoring();

private:
  std::string GetSensorType(int sensor_type);
  void Config();

  struct ipmi_monitoring_ipmi_config ipmi_config;

  std::vector<IPMI_SENSOR_t> sensorList;

  char *hostname = NULL;
  /* In-band Communication Configuration */
  int driver_type = IPMI_MONITORING_DRIVER_TYPE_KCS; /* or -1 for default */
  int disable_auto_probe = 0;        /* probe for in-band device */
  unsigned int driver_address = 0;   /* not used if probing */
  unsigned int register_spacing = 0; /* not used if probing */
  char *driver_device = NULL;        /* not used if probing */

  /* Out-of-band Communication Configuration */
  int protocol_version = IPMI_MONITORING_PROTOCOL_VERSION_1_5; /* or -1 for
                                                                  default */
  unsigned char *k_g = NULL;
  unsigned int k_g_len = 0;
  int privilege_level = IPMI_MONITORING_PRIVILEGE_LEVEL_USER; /* or -1 for
                                                                 default */
  int authentication_type =
    IPMI_MONITORING_AUTHENTICATION_TYPE_MD5; /* or -1 for default */
  int cipher_suite_id = 0;                   /* or -1 for default */
  int session_timeout = 0;                   /* 0 for default */
  int retransmission_timeout = 0;            /* 0 for default */

  /* Workarounds - specify workaround flags if necessary */
  unsigned int workaround_flags = 0;

  /* Initialize w/ record id numbers to only monitor specific record ids */
  std::vector<unsigned int> record_ids;
  unsigned int record_ids_length = 0;

  /* Initialize w/ sensor types to only monitor specific sensor types
   * see ipmi_monitoring.h sensor types list.
   */
  std::vector<unsigned int> sensor_types;
  unsigned int sensor_types_length = 0;

  /* Set to an appropriate alternate if desired */
  std::string sdr_cache_directory = "/tmp";
  char *sensor_config_file = NULL;

  /* Set to 1 or 0 to enable these sensor reading flags
   * - See ipmi_monitoring.h for descriptions of these flags.
   */
  int reread_sdr_cache = 0;
  int ignore_non_interpretable_sensors = 1;
  int bridge_sensors = 0;
  int interpret_oem_data = 0;
  int shared_sensors = 0;
  int discrete_reading = 0;
  int ignore_scanning_disabled = 0;
  int assume_bmc_owner = 0;
  int entity_sensor_names = 0;

  /* Initialization flags
   *
   * Most commonly bitwise OR IPMI_MONITORING_FLAGS_DEBUG and/or
   * IPMI_MONITORING_FLAGS_DEBUG_IPMI_PACKETS for extra debugging
   * information.
   */
  unsigned int ipmimonitoring_init_flags = 0;

  ipmi_monitoring_ctx_t ctx = NULL;
  int errnum;
};

} /* namespace ntof */

#endif /* CIPMI_H_ */
