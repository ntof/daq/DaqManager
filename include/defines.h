
#ifndef DEFINES_H_
#define DEFINES_H_

#define UNUSED(p) (void) (p)

#define DIRECTIO_BLOCK_SIZE 512

#ifndef ACQ_BUFFER_POOL_SIZE
#define ACQ_BUFFER_POOL_SIZE 20
#endif

#define STATE_ERROR -2

#define WRITER_REV_NUMBER 2

#endif /* DEFINES_H_ */
