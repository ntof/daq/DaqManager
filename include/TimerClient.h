/*
 * TimerClient.h
 *
 *  Created on: Oct 29, 2014
 *      Author: agiraud
 */

#ifndef TIMERCLIENT_H_
#define TIMERCLIENT_H_

#include <mutex>

#include <DIMParamListClient.h>
#include <EventReader.h>
#include <stdint.h>

/*! \namespace ntof
 *
 * namespace used for all the element of the nTOF
 */
namespace ntof {

struct TRIGGER_TIMESTAMP_STATUS
{
  std::string err;
  int code;
  TRIGGER_TIMESTAMP_STATUS() : err(""), code(0) {}
};

class TimerClient :
  public ntof::dim::EventHandler,
  public ntof::dim::DIMParamListClientHandler
{
public:
  explicit TimerClient(const std::string &name);
  /*!
   *  \brief Destructor
   *
   *  Destructor of the class TimerClient
   */
  virtual ~TimerClient();

  /*!
   *  \brief  This function is called each time a new event is received
   *  \param timeStamp Timestamp of the event
   *  \param cycleStamp Cyclestamp of the event
   *  \param cycle Cycle number into super-cycle
   *  \param type Type of the event (CALIBRATION, PARASITIC, PRIMARY)
   *  \param dest Particule destination 1
   *  \param dest2 Particule destination 2
   *  \param eventNumber n_TOF event number counter
   */
  void eventReceived(const ntof::dim::EventReader::Data &event);

  double Get();
  TRIGGER_TIMESTAMP_STATUS Get(double &value);
  TRIGGER_TIMESTAMP_STATUS GetStatus();
  void ResetStatus();

  /*
   * \brief Get the cyclestamp
   * \return Return the cyclestamp
   */
  int64_t getCycleStamp();

  /*
   * \brief Get the event number
   * \return Return the event number
   */
  int64_t getEventNumber();

  /*
   * \brief Get the beam type
   * \return Return the beam type
   */
  std::string &getType();

private:
  void errorReceived(std::string errMsg, const dim::DIMXMLInfo *info);
  void dataReceived(pugi::xml_document &doc, const dim::DIMXMLInfo *info);
  void noLink();
  /*
   * \brief Called when the DIM service is refreshed by the server
   * \param settingsChanged: New list of parameters
   * \param list: List responsible of this callback
   */
  virtual void parameterChanged(
    const std::vector<ntof::dim::DIMData> &settingsChanged,
    const ntof::dim::DIMParamListClient &list) override;

  std::mutex mutex_;
  TRIGGER_TIMESTAMP_STATUS status_;
  ntof::dim::EventReader::Data event_;   //!< last event
  ntof::dim::DIMParamListClient client_; //!< Client to the timing machine
  ntof::dim::EventReader eventReader_;   //!< Object to parse the content of the
                                       //!< service publish by the timing machine
                                       //!< in order to get the event number
};
} // namespace ntof

#endif /* TIMERCLIENT_H_ */
