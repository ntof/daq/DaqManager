#ifndef CCIRCULARBUFFER_H_
#define CCIRCULARBUFFER_H_

#include <boost/bind.hpp>
#include <boost/call_traits.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/timer/timer.hpp> // for auto_cpu_timer

template<class T>
class CBoundedCircularBuffer
{
public:
  typedef boost::circular_buffer<T> container_type;
  typedef typename container_type::size_type size_type;
  typedef typename container_type::value_type value_type;
  typedef typename boost::call_traits<value_type>::param_type param_type;

  explicit CBoundedCircularBuffer(size_type capacity) :
    m_unread(0),
    m_container(capacity)
  {}

  void push(typename boost::call_traits<value_type>::param_type item)
  { // `param_type` represents the "best" way to pass a parameter of type
    // `value_type` to a method.

    boost::mutex::scoped_lock lock(m_mutex);
    m_not_full.wait(
      lock, boost::bind(&CBoundedCircularBuffer<value_type>::is_not_full, this));
    m_container.push_front(item);
    ++m_unread;
    lock.unlock();
    m_not_empty.notify_one();
  }

  void pop(value_type *pItem)
  {
    boost::mutex::scoped_lock lock(m_mutex);
    m_not_empty.wait(
      lock,
      boost::bind(&CBoundedCircularBuffer<value_type>::is_not_empty, this));
    *pItem = m_container[--m_unread];
    lock.unlock();
    m_not_full.notify_one();
  }

  value_type pop()
  {
    value_type item;
    pop(&item);
    return item;
  }

  void clear()
  {
    boost::mutex::scoped_lock lock(m_mutex);
    m_unread = 0;
    lock.unlock();
    m_not_full.notify_all();
  }

private:
  CBoundedCircularBuffer(const CBoundedCircularBuffer &); // Disabled copy
                                                          // constructor.
  CBoundedCircularBuffer &operator=(
    const CBoundedCircularBuffer &); // Disabled assign operator.

  bool is_not_empty() const { return m_unread > 0; }
  bool is_not_full() const { return m_unread < m_container.capacity(); }

  size_type m_unread;
  container_type m_container;
  boost::mutex m_mutex;
  boost::condition m_not_empty;
  boost::condition m_not_full;
};

#endif /* CCIRCULARBUFFER_H_ */
