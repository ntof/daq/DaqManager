/*
 * CDIMZeroSuppressionParameters.h
 *
 *  Created on: Jun 6, 2016
 *      Author: pperonna
 */

#ifndef CDIMZEROSUPPRESSIONPARAMETERS_H_
#define CDIMZEROSUPPRESSIONPARAMETERS_H_

#include <mutex>
#include <string>
#include <vector>

#include <DIMData.h>
#include <DIMParamList.h>
#include <pugixml.hpp>
#include <stdint.h>

#include "typedefs.h"

namespace ntof {

class CDIMZeroSuppressionParameters :
  public dim::DIMParamList,
  public dim::DIMParamListHandler

{
public:
  typedef std::vector<ntof::ZeroSuppression::zsMaster> MasterList;

  CDIMZeroSuppressionParameters(const std::string &serviceName);
  virtual ~CDIMZeroSuppressionParameters() = default;

  CDIMZeroSuppressionParameters(const CDIMZeroSuppressionParameters &) = delete;
  CDIMZeroSuppressionParameters &operator=(
    const CDIMZeroSuppressionParameters &) = delete;

  enum ParamIdx : dim::DIMParamList::Index
  {
    MODE = 1,
    CONFIGURATION = 2
  };

  enum ChannelParamIdx : dim::DIMParamList::Index
  {
    SN = 0,
    CHANNEL = 1
  };

  /**
   * @brief used to inject parameters saved in an hwconfig file
   * @param root an xml node containing the "parameters" service content
   */
  void setXmlParameters(const pugi::xml_node &root);

  ntof::ZeroSuppression::zsMode GetMode() const;
  MasterList GetMasterList() const;

  /**
   * @brief parameter checking callback
   * @details will actually modify the service content
   */
  int parameterChanged(dim::DIMData::List &settingsChanged,
                       const dim::DIMParamList &list,
                       int &errCode,
                       std::string &errMsg) override;

protected:
  ntof::ZeroSuppression::zsMaster getMaster(dim::DIMData &master) const;
  void updateService();

  mutable std::mutex mutex_;
  MasterList zsMasterList_;
  ntof::ZeroSuppression::zsMode zsMode_;
};

} // namespace ntof
#endif /* CDIMZEROSUPPRESSIONPARAMETERS_H_ */
