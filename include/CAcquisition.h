/*
 * CAcquisition.h
 *
 *  Created on: Nov 16, 2014
 *      Author: pperonna
 */

#ifndef CACQUISITION_H_
#define CACQUISITION_H_

#include <condition_variable>
#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include <boost/thread.hpp>

#include <DaqTypes.h>
#include <Flock.hpp>
#include <stdint.h>

#include "CAcquisitionBuffer.h"
#include "CCard.h"
#include "CCircularBuffer.h"
#include "DIMState.h"
#include "TimerClient.h"
#include "defines.h"
#include "typedefs.h"

/**
 * Enumerations for acquisition state
 */
enum class ACQUISITION_STATE_t : int32_t
{
  ERROR = STATE_ERROR, //!< state in case of error
  IDLE = 0,
  THREAD_INIT,       //!< initialization of thread
  ALLOCATING_MEMORY, //!< allocate memory for acquisition buffer
  WAITING_TO_START,
  ARMING_TRIGGER,  //!< arms trigger
  WAITING_TRIGGER, //!< waits for first incoming trigger. Can be either from
                   //!< card of time server.
  WAITING_END_ACQUISITION, //!< waiting for all triggers
  QUEUING_MESSAGE,         //!< post the acquired buffer in queue
  WAITING_FREE_MEMORY,     //!< wait for memory space for acquisition buffer
  WAITING_QUEUE_SPACE,     //!< wait for free space in queue in case it is full
  DUMPING_CARDS,
  STOPPING
};
//!< States for acquisition FSM

/**
 * Acquisition class
 */
class CAcquisition
{
public:
  /**
   * Constructor
   * @param acqQueue synchronized queue. Acquired data are pushed into.
   * @param cards Collection of card objects
   */
  CAcquisition(std::string hostName,
               CBoundedCircularBuffer<CAcquisitionParameters> &acqQueue,
               std::vector<std::shared_ptr<CCard>> &cards,
               ntof::dim::DIMState *writerState,
               std::string timingEventName,
               CMD_LINE_PARAM_t cmdLineParams);
  /**
   * Destructor
   */
  virtual ~CAcquisition();

  /**
   * configure lockFile for the acquisition thread
   * @param[in] lockFile lock file path
   * @details the lock will be held when acquisition thread is running
   * @details use an empty string to disable the feature
   */
  void setLockFile(const std::string &lockFile);

  /**
   * Start the acquisition threads
   */
  void Start(uint32_t runNumber);

  /**
   * Start acquisition
   */

  void StartAcq();
  /**
   * Stop the acquisition threads
   */

  void Stop();
  void ForceStop();
  void Reset();
  ACQUISITION_STATE_t GetState();

private:
  /**
   * acquisition thread
   */
  void Acquisition();
  void AcqThreadInit();
  /**
   * acquisition thread with data parsing
   */
  void AcquisitionOnlineParsing();
  /**
   * Thread to dump card's buffer to host's memory
   * @param cardIndex card's index
   */
  void CardMemoryDump(int cardIndex);
  /**
   *
   * @param crateIndex
   */
  void CrateMemoryDump(uint32_t crateIndex);

  /**
   *
   * @param cardIndex
   * @param acqBuffer
   * @return
   */
  int32_t WaitForTriggerAndDump(uint32_t cardIndex,
                                AcquisitionBuffer *acqBuffer);
  /**
   *
   * @param cpu
   */
  void PinThreadToCpu(int cpu);
  /**
   *
   * @param cpu
   */
  void PinThreadToNode(int cpu);
  /**
   *
   */
  void AllocateMemory();
  /**
   *
   */
  void StopThread(bool error);
  /**
   *
   */
  bool RetrieveEvent(void);

  ntof::dim::DIMState state_;                 //!< acquisition state variable
  boost::thread thread_;                      //!< acquisition thread
  boost::thread_group cardMemoryDumpThreads_; //!< thread to copy data from card
                                              //!< to host's memory
  boost::thread_group crateMemoryDumpThreads_; //!< thread to copy data from
                                               //!< card to host's memory
  CBoundedCircularBuffer<CAcquisitionParameters> &acqQueue_;
  std::vector<std::shared_ptr<CCard>> &cards_; //!< collection of CCard objects
  std::string hostName_;                       //!< host's name
  double previousTimeServer_;
  ntof::dim::DIMState *writerState_;
  ntof::TimerClient triggerTimeStamp_;
  std::vector<std::shared_ptr<CBoundedCircularBuffer<int>>> cardPoolIndex_;
  std::vector<int> threadStatus_;
  std::vector<int> cardEnabled_;
  bool stop_;
  std::multimap<uint32_t, uint32_t> crateCardMap_;
  CMD_LINE_PARAM_t cmdLineParams_;
  static int bufferPoolIndex_;
  ntof::dim::EventReader eventReader_;
  EventHeader eveh_;
  std::mutex startMutex_;
  std::condition_variable startCV_;
  bool startAcquisition_;
  std::shared_ptr<ntof::utils::Flock> lockFile_;
};

/**
 * Exception thrown by Acquisition class
 */
class CAcquisitionEx : public std::exception
{
public:
  CAcquisitionEx() throw();
  explicit CAcquisitionEx(const std::string &msg) throw();
  virtual ~CAcquisitionEx() throw();
  virtual const char *what() const throw();

private:
  std::string exceptionMsg_;
};

#endif /* CACQUISITION_H_ */
