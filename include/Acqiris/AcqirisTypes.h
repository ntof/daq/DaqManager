/*
 * AcqirisTypes.h
 *
 *  Created on: Dec 9, 2014
 *      Author: pperonna
 */

#ifndef ACQIRISTYPES_H_
#define ACQIRISTYPES_H_

#define ACQIRIS_SUCCESS 0

namespace Agilent {
namespace Acqiris {

/**
 * Configures the trigger class control parameters of the digitizer
 */
typedef enum trigClass
{
  EDGE,
  TV,
  OR,
  NOR,
  AND,
  NAND
} TRIG_CLASS_t;

typedef enum
{
  CHANNEL1,
  CHANNEL2,
  CHANNEL3,
  CHANNEL4,
  EXT_TRIG1,
  EXT_TRIG2
} TRIG_SOURCE_t;

typedef enum
{
  NEGATIVE,
  POSITIVE
} TRIG_SLOPE_t;

typedef enum
{
  INTERNAL,
  EXTERNAL,
  EXTERNAL_REF,
  EXTERNAL_SEQ
} CLOCK_TYPE_t;

} // namespace Acqiris
} // namespace Agilent

#endif /* ACQIRISTYPES_H_ */
