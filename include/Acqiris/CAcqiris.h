#ifndef CACQIRIS_H
#define CACQIRIS_H

#include <cstdint>
#include <vector>

#include <AgMD1.h>

#include "AcqirisTypes.h"
#include "CCard.h"

namespace Agilent {
namespace Acqiris {

/**
 * Class for CAqiris digitizer
 */
class CAqiris
{
public:
  /**
   * Class's constructor
   * @param instrID intrument's ID used by API
   * @param cardIndex card's index
   */
  CAqiris(std::string cardModelName,
          ViSession instrID,
          uint32_t cardIndex,
          uint nbChannels,
          uint resolution);
  /**
   * Class's destructor
   */
  virtual ~CAqiris();
  /**
   * Set trigger mode
   * @param source trigger's source
   * @param slope falling or rising edge
   * @param level trigger threshold in volts
   */
  virtual void SetTriggerMode(TRIG_SOURCE_t source,
                              TRIG_SLOPE_t slope,
                              double level) final;
  /**
   * Set clock source
   * @param clkSource
   */
  virtual void SetClockSource(CLOCK_TYPE_t clkSource) final;
  /**
   * Specifies the length of time from the trigger event to the first point in
   * the waveform record. If the value is positive, the first point in the
   * waveform record occurs after the trigger event. If the value is negative,
   * the first point in the waveform record occurs before the trigger event.
   * @param delay in ns
   */
  virtual void SetTriggerDelay(int32_t delay) final;
  /**
   * Set acquisition parameters
   * @param samplingFreq sampling frequency
   * @param nbSegments number of segments
   * @param nbSamples number of ksamples
   */
  virtual void SetAcquisitionParameters(float samplingFreq,
                                        uint32_t nbSegments,
                                        uint32_t nbSamples) final;
  virtual void SetAcquisitionBuffer(int16_t **buffer) final;
  virtual int16_t **GetAcquisitionBuffer() final;
  virtual int8_t *GetMemoryDumpBuffer() final;
  /**
   * Allocates memory buffer for channels
   */
  virtual void AllocateChannelMemoryBuffers() final;
  virtual void GetAcquisition(int8_t *dumpBuffer);
  /**
   * Set input range for a channel
   * @param ch channel to be set
   * @param inputRange range in mV
   */
  virtual void SetInputRange(uint32_t ch, double inputRange) final;
  virtual void ConfigureChannel(uint32_t ch,
                                double range,
                                double offset,
                                double impedance,
                                bool enabled) final;
  virtual void ConfigureChannels(double range,
                                 double offset,
                                 double impedance) final;
  /**
   * Set input range for all channels
   * @param inputRange range in mV
   */
  virtual void SetInputRange(double inputRange) final;
  virtual void SetAdjustableBias(uint32_t ch, double bias) final;
  virtual void SetAdjustableBias(double bias) final;
  /**
   * Get LSB value in Volt for a given channel
   * @param ch channel
   * @param lsb pointer to LSB variable
   */
  virtual void GetLsbValue(uint32_t ch, double *lsb);
  /**
   * Get LSB value in Volt for a given channel
   * @param ch channel
   * @return LSB value in Volt
   */
  virtual double GetLsbValue(uint32_t ch);
  virtual uint32_t GetChannelBufferSize();
  virtual uint32_t GetChannelAllocationSize();
  /**
   * Set dump memory buffer pointer
   * @param buffer pointer to the buffer
   */
  virtual void SetDumpMemoryBuffer(int8_t *buffer);
  virtual void SetChannelBufferFree();
  /**
   * Arm card's trigger
   */
  virtual void ArmTrigger() final;
  /**
   * Check if card is triggered
   * @return true if trigger occurred
   */
  virtual bool IsTriggered() final;
  /**
   *
   */
  virtual void MemoryDump8() final;
  /**
   *
   */
  virtual void MemoryDump16() final;
  /**
   * Calibrate card
   */
  virtual void Calibrate() final;
  /**
   * Cancel ongoing calibration
   */
  virtual void CancelCalibration() final;
  /**
   * Get card's index
   * @return index
   */
  virtual int32_t GetCardIndex() final;
  /**
   * Get card's model name
   * @return string with model name
   */
  virtual std::string GetCardModelName();
  /**
   * Dump card's memory to host's one
   */
  virtual void MemoryDump() = 0;
  /**
   * Get dump buffer size
   * @return dump buffer size in bytes
   */
  virtual uint32_t GetDumpBufferSize() final;

  virtual uint32_t GetSlotNumber() final;
  virtual ViSession GetInstrumentId() final;
  virtual uint32_t GetNbBytesDumped() final;
  std::vector<float> &GetCalibratedFullScale();
  std::vector<float> &GetCalibratedOffset();
  float GetCalibratedFullScale(int index);
  float GetCalibratedOffset(int index);

protected:
  ViSession instrumentID_; //!< card's ID number
  uint32_t cardIndex_;     //!< card's index
  int16_t **targetBuffer_;
  int8_t *dumpBuffer_;
  uint32_t nbSegments_;     //!< number of acquisition segments
  uint32_t nbSamples_;      //!< number of acquisition samples
  uint32_t nbValidSamples_; //!< number of valid samples
  uint nbChannels_;         //!< number of channels
  uint resolution_;         //!< number of resolution bits
  std::string cardModelName_;
  std::vector<int> channelEnabled_;
  bool hasTriggered_;
  uint32_t nbChannelEnabled_;

  std::vector<int64_t> firstValidPoint_; //!< used by API
  std::vector<int64_t> actualPoints_;    //!< used by API
  ViReal64 initialXOffset_;              //!< used by API
  ViReal64 initialXTimeSeconds_;         //!< used by API
  ViReal64 initialXTimeFraction_;        //!< used by API
  ViReal64 xIncrement_;                  //!< used by API
  ViReal64 scaleFactor_;                 //!< used by API
  ViReal64 scaleOffset_;                 //!< used by API
  ViInt64 nbSamplesToDump_;              //!< used by API
  uint32_t slotNumber_;
  uint32_t nbBytesDumped_;
  std::vector<float> calibratedFullScale_;
  std::vector<float> calibratedOffset_;
};

} // namespace Acqiris
} // namespace Agilent
#endif // CACQIRIS_H
