/*! \file CCardSpDevices.h
 \brief CCardSpDevices class
 detailed description
 */

#ifndef CCARDACQIRISDC240_H_
#define CCARDACQIRISDC240_H_

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <boost/thread.hpp>
#include <boost/thread/condition_variable.hpp>

#include <AgMD1Fundamental.h>

#include "CAcqirisDC240.h"
#include "CCard.h"
#include "CCardAcqiris.h"
#include "CChannelAcqirisDC240.h"
#include "CZeroSuppression.h"
#include "DIMData.h"
#include "DIMException.h"
#include "DIMParamList.h"
#include "DaqTypes.h"

#include <dis.hxx>

namespace Agilent {
namespace Acqiris {

/**
 * Agilent Acqiris DC240 class
 */
class CCardAcqirisDC240 : public CCard, public CCardAcqiris
{
public:
  /**
   * Class constructor
   */
  CCardAcqirisDC240(std::string serialNumber,
                    int cpuCore,
                    uint32_t cardIndex,
                    uint32_t crateIndex,
                    ntof::dim::DIMState *state);
  /**
   * Class destructor
   */
  ~CCardAcqirisDC240();
  /**
   * Get the number of DC240 card presents in the system
   * @return number of devices
   */
  size_t GetNbDevices();
  /**
   * Allocate memory for acquisition buffer
   */
  void AllocateAcquisitionBuffer();
  /**
   * Get the acquisition buffer pointer
   * @return pointer to the buffers
   */
  int16_t **GetAcquisitionBuffer();
  /**
   * Free allocated memory
   */
  void SetChannelBufferFree();
  /**
   * Get the dump buffer's size
   * @return the size in byte
   */
  uint32_t GetDumpBufferSize();
  /**
   * Get a pointer to the card's memory dumped
   * @return pointer to the buffer
   */
  int8_t *GetMemoryDumpBuffer();
  /**
   * Set buffer address for card's memory dump
   * @param buffer a pointer to the buffer
   */
  void SetDumpMemoryBuffer(int8_t *buffer);
  /**
   * Dump card's memory to host's memory
   */
  void DumpCardMemory();
  /**
   * Get the size of a channel's buffer
   * @return size in byte
   */
  uint32_t GetChannelBufferSize();
  /**
   * Get the index of the card
   * @return card's index
   */
  uint32_t GetCardIndex();
  /**
   * Get the number of physical channels on the card
   * @return number of channels
   */
  uint32_t GetNbChannels();
  /**
   * Arm the card's trigger
   */
  void ArmTrigger();
  /**
   * Check if card has triggered
   * @return true is card has triggered
   */
  bool IsTriggered();
  /**
   * Load a default configuration in the card (range, sampling rate...)
   */
  void LoadDefaultConfiguration();
  /**
   * Calibrate the card
   */
  void Calibrate();
  void configure();
  /**
   * Perform offline parsing of the dumped buffer from card to host's memory
   * @param dumpBuffer pointer to the buffer
   */
  // void OfflineParsing(int8_t* dumpBuffer);
  void Delete();
  int GetResolution();
  /**
   * Offline parsing feature
   * @return true if offline parsing is available
   */
  bool HasOfflineParsing();
  bool IsDumpSequential();
  uint32_t GetNbBytesDumped();

private:
  /**
   * Class initialization function
   */
  void ClassInit();
  uint nbInitDeviceFailed_;  //!< number of non-initialized cards
  std::string strLastError_; //!< last error code
  std::string strLastMsg_;   //!< last error message
  int lastErrorCode_;        //!< last error code
  bool readOutDone_;         //!< true if readout was done
  bool dumpDone_;            //!< true if memory dump was
};
} // namespace Acqiris
} // namespace Agilent
#endif // CCARDACQIRISDC240_H_
