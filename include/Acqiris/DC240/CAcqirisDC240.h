#ifndef CACQIRIS_DC240_H
#define CACQIRIS_DC240_H

#include <cstdint>
#include <vector>

#include <AgMD1.h>

#include "AcqirisTypes.h"
#include "CAcqiris.h"

namespace Agilent {
namespace Acqiris {

/**
 * Class for DC240 digitizer
 */
class Dc240 : public CAqiris
{
public:
  /**
   * Class's constructor
   * @param instrID intrument's ID used by API
   * @param cardIndex card's index
   */
  Dc240(ViSession instrID, uint32_t cardIndex);
  /**
   * Class's destructor
   */
  ~Dc240();

  // void GetAcquisition(int8_t* dumpBuffer);
  void MemoryDump();
  /**
   * Get number of resolution bits
   * @return number of resolution bits
   */
  static uint32_t GetResolutionBits();
  /**
   * Get number of channels present on this card
   * @return number of channels
   */
  static uint32_t getNbChannels();

private:
  /**
   * Initialize class
   */
  void init();
};

} // namespace Acqiris
} // namespace Agilent
#endif // CACQIRIS_DC240_H
