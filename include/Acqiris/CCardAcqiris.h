/*
 * CCardAcqiris.h
 *
 *  Created on: Feb 9, 2015
 *      Author: pperonna
 */

#ifndef CCARDACQIRIS_H_
#define CCARDACQIRIS_H_

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <AgMD1.h>
#include <AgMD1Fundamental.h>

#include "CAcqirisDC240.h"
#include "CAcqirisDC270.h"
#include "CAcqirisDC282.h"
#include "CCard.h"

namespace Agilent {
namespace Acqiris {

class CCardAcqiris
{
public:
  /**
   * Class constructor
   */
  CCardAcqiris(uint32_t cardIndex);
  /**
   * Class destructor
   */
  ~CCardAcqiris();
  uint32_t GetNbDevices();
  std::string GetSN(ViSession instrID);

protected:
  static bool ready_;
  static std::vector<std::shared_ptr<Dc240>> dc240_; //!< vector of DC240 cards
  static std::vector<std::shared_ptr<Dc270>> dc270_; //!< vector of DC270 cards
  static std::vector<std::shared_ptr<Dc282>> dc282_; //!< vector of DC282 cards

private:
  uint32_t cardIndex_;
  ViInt32 nbDevices_;
  void FindDevices();
  std::string GetOptions(ViSession instrID);
};
} // namespace Acqiris
} // namespace Agilent

#endif /* CCARDACQIRIS_H_ */
