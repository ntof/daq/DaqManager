/*
 * CChannelAcqirisDC270.h
 *
 *  Created on: Nov 28, 2014
 *      Author: pperonna
 */

#ifndef CCHANNELACQIRISDC270_H_
#define CCHANNELACQIRISDC270_H_

#include <CChannel.h>

namespace Agilent {
namespace Acqiris {
/**
 * CChannel class for SP Devices ADQ412 card
 */
class CChannelAcqirisDC270 : public ntof::CChannel
{
public:
  /**
   * Constructor
   * @param logger logger
   * @param cardIndex card's index
   * @param channelIndex card channel's index
   * @param dimServiceName DIM service name
   */
  CChannelAcqirisDC270(uint32_t location,
                       std::string dimServiceName,
                       ntof::dim::DIMState *state);
  /**
   * destructor
   */
  ~CChannelAcqirisDC270();
  /**
   * Callback for DIM parameter list
   * @param settingsChanged
   * @param list
   * @param errCode
   * @param errMsg
   * @return
   */
  virtual int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                               const ntof::dim::DIMParamList &list,
                               int &errCode,
                               std::string &errMsg) override;

private:
};
} // namespace Acqiris
} /* namespace Agilent */

#endif /* CCHANNELSPDADQ412_H_ */
