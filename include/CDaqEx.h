/*
 * CDaqException.h
 *
 *  Created on: Oct 13, 2014
 *      Author: pperonna
 */

#ifndef CDAQEXCEPTION_H_
#define CDAQEXCEPTION_H_

#include <exception>
#include <string>

class CDaqException : public std::exception
{
public:
  CDaqException() throw();
  explicit CDaqException(const std::string &msg) throw();
  virtual ~CDaqException() throw();
  virtual const char *what() const throw();
  void SetMessage(const std::string &msg);

private:
  std::string exceptionMsg_;
};

#endif /* CDAQEXCEPTION_H_ */
