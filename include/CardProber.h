/*
 * CardProber.h
 *
 *  Created on: Nov 13, 2015
 *      Author: pperonna
 */

#ifndef CARDPROBER_H_
#define CARDPROBER_H_

#include <string>

void SpdProbe();
void AcqirisProbe(const std::string &resource);

#endif /* CARDPROBER_H_ */
