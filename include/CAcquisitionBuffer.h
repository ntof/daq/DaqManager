/*
 * CAcquisitionBuffer.h
 *
 *  Created on: Oct 21, 2014
 *      Author: pperonna
 */

#ifndef CACQUISITIONBUFFER_H_
#define CACQUISITIONBUFFER_H_

#include <atomic>
#include <cstdint>
#include <string>
#include <vector>

#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/thread.hpp>
#include <boost/thread/condition_variable.hpp>

#include <mm_malloc.h>

#include "DaqTypes.h"
#include "defines.h"

struct AcquisitionBuffer
{
  AcquisitionBuffer();
  ~AcquisitionBuffer();
  void AllocateBufferPool(std::size_t poolSize,
                          std::size_t bufferSize,
                          int align = DIRECTIO_BLOCK_SIZE);
  void FreeBufferPool();

  void AllocateDumpBuffer(std::size_t bufferSize,
                          int align = DIRECTIO_BLOCK_SIZE);
  void FreeDumpBuffer();

  std::vector<int16_t *> buffer_;
  int8_t *dumpBuffer_;
  std::vector<int64_t> firstValidPoint_;
  std::vector<int64_t> nbOfPoints_;
  uint cardIndex_;
  uint crateIndex_;
  std::atomic_bool used_;
  bool isReady_;
  int cpu_;
  int node_;
  int parsingZsStatus_;
  std::atomic_int dumpStatus_;
};

class CAcquisitionParameters
{
public:
  CAcquisitionParameters() : bufferPoolIndex_(-1), stop_(false) {}

  CAcquisitionParameters(const EventHeader &evtHeader, int bufferPoolIndex) :
    evtHeader_(evtHeader),
    bufferPoolIndex_(bufferPoolIndex),
    stop_(false)
  {}

  ~CAcquisitionParameters() {}
  const EventHeader &GetEventHeader() const { return evtHeader_; }

  int GetBufferPoolIndex() const { return bufferPoolIndex_; }

  void SetStop() { stop_ = true; }
  bool GetStop() const { return stop_; }

  uint32_t GetEventNumber() const { return evtHeader_.eventNumber; }
  void SetEventNumber(uint32_t eventNumber)
  {
    evtHeader_.eventNumber = eventNumber;
  }

  uint32_t GetRunNumber() const { return evtHeader_.runNumber; }

private:
  EventHeader evtHeader_;
  int bufferPoolIndex_;
  bool stop_;
};

class CAcquisitionBuffer
{
public:
  CAcquisitionBuffer();
  ~CAcquisitionBuffer();
  std::vector<AcquisitionBuffer *> card_;
  EVENT_HEADER evtHeader_;
};

#endif /* CACQUISITIONBUFFER_H_ */
