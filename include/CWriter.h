/*
 * CWriter.h
 *
 *  Created on: Aug 1, 2014
 *      Author: agiraud
 */

#ifndef CWRITER_H_
#define CWRITER_H_

#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <boost/thread.hpp>

#include <Flock.hpp>
#include <numa.h>
#include <stdint.h>

#include "CAcquisitionBuffer.h"
#include "CCard.h"
#include "CCircularBuffer.h"
#include "CDIMZeroSuppressionParameters.h"
#include "CZeroSuppression.h"
#include "DIMException.h"
#include "DIMState.h"
#include "DaqTypes.h"
#include "TimerClient.h"
#include "defines.h"
#include "typedefs.h"

#ifndef WRITER_FILE_BUFFER_SIZE
#define WRITER_FILE_BUFFER_SIZE 16 * 1024 * 1024 * 1024LL
#endif

/**
 * enumeration for writer state
 */
enum class WRITER_STATE_t : int32_t
{
  ERROR = STATE_ERROR, //<! final state in case of error
  IDLE = 0,
  INIT,
  MEMORY_ALLOCATION,
  WAITING_QUEUE, //<! waiting for elements pushed in queue
  PROCESSING_DATA,
  WRITING_DATA,
  FREEING_BUFFER, //<! delete buffers
  STOPPING
};

class CWriter
{
public:
  /**
   * Class constructor
   * @param logger logging variable (trace, debug, info, fatal ...)
   * @param acqQueue synchronized queue. Elements are pushed by the acquisition
   * thread
   * @param cards collection of card objects
   */
  CWriter(std::string hwConfigFilename,
          std::string hostname,
          CBoundedCircularBuffer<CAcquisitionParameters> &acqQueue,
          std::vector<std::shared_ptr<CCard>> &cards,
          ntof::dim::DIMState *daqState,
          CMD_LINE_PARAM_t cmdLineParams);
  /**
   * destructor
   */
  virtual ~CWriter();

  /**
   * configure lockFile for the writer thread
   * @param[in] lockFile lock file path
   * @details the lock will be held when writer thread is running
   * @details use an empty string to disable the feature
   */
  void setLockFile(const std::string &lockFile);

  /**
   * Start the writer thread
   */
  void Start(uint32_t runNumber);

  /**
   * Stop the writer thread, pending buffer will be writen first
   */

  void Stop();
  /**
   * Immediately stop the writer thread
   */

  void ForceStop();
  /**
   * Checks if writer's configuration is valid
   * @return true if configuration is valid
   */

  bool IsConfigurationValid();
  /**
   * Invalids writer's configuration
   */

  void SetConfigurationInvalid();
  bool IsModhConfigurationValid();
  bool IsRctrConfigurationValid();
  ntof::dim::DIMState *GetState();
  /**
   *
   */
  void Reset();

private:
  /**
   * writer thread
   */
  void Writer();

  /**
   * Create filename
   * @param eventHeader
   * @return filename
   */
  std::string createName(int64_t runNumber, int64_t eventNumber);

  /**
   * Write pulse data
   * @param fd file descriptor
   * @param cardIndex the card index to retrieve zero suppressed data buffers
   */
  void WritePulseData(int &fd, uint cardIndex);

  /**
   * Write raw channel data without zero suppression
   * @param acqBuffer pointer to the acquisition buffer
   */
  void WriteRawData(AcquisitionBuffer *acqBuffer);
  /**
   * Write pulse data to dedicated file for each channel
   * @param acqBuffer
   */
  void WritePulseData(AcquisitionBuffer *acqBuffer);
  /**
   *
   * @param lastWroteNumber
   * @return
   */
  void publishWroteNumber(int64_t runNumber,
                          int64_t eventNumber,
                          uint64_t size,
                          pugi::xml_document &doc);
  /**
   *
   * @param cpu
   */
  void PinThreadToCpu(int cpu);
  /**
   *
   * @param cpu
   */
  void PinThreadToNode(int cpu);
  /**
   *
   * @param acqBuffer
   */
  void FreeMemory(CAcquisitionBuffer *acqBuffer);
  /**
   *
   * @param cardIndex
   */
  void ParsingZsThread(int cardIndex);
  /**
   *
   */
  void ParsingZsThreadAwaitStart();
  /**
   *
   */
  void WriterThreadInit();
  /**
   *
   */
  void WriterThreadStop(bool error);
  /**
   *
   */
  void MemoryAllocation();

  /**
   * @brief create/check run directory exists
   * @return the run directory path
   */
  std::string CreateRunDir(uint32_t runNumber);

  /**
   *
   * @param evtHeader
   * @param length
   */
  void WriteDataToDisk(const std::string &runDirectory,
                       const CAcquisitionParameters &acqParams,
                       ssize_t length);

  /**
   * @brief create an event file
   * @param acqParams the event to write
   */
  void WriteEventToDisk(const std::string &runDirectory,
                        const EventHeader &header);

  /**
   * @brief create a run file on disk
   * @details the run file will contain RCTR and MODH headers
   */
  void WriteRunToDisk(const std::string &runDirectory, uint32_t runNumber);

  /**
   *
   * @param buffer
   * @param cardIndex
   * @return
   */
  int8_t *FillFileBuffer(CAcquisitionParameters &acqParams,
                         int8_t *buffer,
                         int cardIndex);
  /**
   *
   */
  void ParseXmlZsParameters();
  /**
   *
   * @param acqParams
   */
  void ZsIndependent(CAcquisitionParameters &acqParams);
  /**
   *
   * @param acqParams
   */
  void ZsSingleMaster(CAcquisitionParameters &acqParams);
  /**
   *
   * @param acqParams
   */
  void ZsMultipleMaster(CAcquisitionParameters &acqParams);

  std::string hwConfigFilename_;
  ntof::dim::DIMState state_; //<! writer state
  boost::thread thread_;      //<! thread for writing data
  CBoundedCircularBuffer<CAcquisitionParameters> &acqQueue_;
  std::vector<std::shared_ptr<CCard>> &cards_; //<! collection of card objects
  std::string hostName_;                       //<! host's name
  ntof::dim::DIMState *daqState_;
  int32_t streamNumber_;   //<! stream number
  ACQC_HEADER *acqHeader_; //<! acquisition header
  ntof::TimerClient timing_;
  bool stop_; //<! flag to stop the thread
  ntof::dim::DIMXMLService dimRunExtension_;
  bool configurationValid_;
  boost::thread_group parsingZsThreads_; //!< thread to perform zero suppression
                                         //!< and offline parsing
  std::vector<int> cardEnabled_;
  std::string extensionNumber_;
  CMD_LINE_PARAM_t cmdLineParams_;
  int8_t *fileBuffer_;
  ssize_t fileBufferSize_;
  std::vector<std::shared_ptr<CBoundedCircularBuffer<int>>> cardPoolIndex_;
  ntof::CDIMZeroSuppressionParameters zeroSuppressionParameters_;
  skipHeader skipHeader_;
  acqc acqcHeader_;
  std::vector<ntof::ZeroSuppression::zsMaster> zsMasterList_;
  ntof::ZeroSuppression::zsMode zsMode_;
  std::shared_ptr<ntof::utils::Flock> lockFile_;
};

/**
 * Exception class thrown by CWriter
 */
class CWriterEx : public std::exception
{
public:
  CWriterEx() throw();
  explicit CWriterEx(const std::string &msg) throw();
  virtual ~CWriterEx() throw();
  virtual const char *what() const throw();
  void SetMessage(const std::string &msg);

private:
  std::string exceptionMsg_;
};

#endif /* CWRITER_H_ */
