/*
 * CCard.h
 *
 *  Created on: Aug 1, 2014
 *      Author: agiraud
 */

#ifndef CCARD_H_
#define CCARD_H_

#include <exception>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "CAcquisitionBuffer.h"
#include "CChannel.h"
#include "CZeroSuppression.h"
#include "DIMXMLService.h"
#include "defines.h"

#include "dic.hxx"
#include "dis.hxx"

using namespace ntof;

typedef struct
{
  std::vector<float> value;
  std::vector<std::string> name;
} CARD_TEMPERATURE_t;

class CCard
{
public:
  typedef std::vector<std::shared_ptr<ntof::CChannel>> ChannelCollection;

  CCard(std::string type,
        std::string serialNumber,
        int cpuCore,
        uint32_t cardIndex,
        uint32_t crateIndex,
        ntof::dim::DIMState *state,
        bool enableTrace);
  virtual ~CCard();
  virtual void SetUnparsedBuffer(int8_t *buffer);
  virtual int16_t **GetAcquisitionBuffer() = 0;
  virtual int8_t *GetMemoryDumpBuffer();
  /**
   * @brief offline-parsing buffer size in bytes
   */
  virtual uint32_t GetDumpBufferSize();
  virtual uint32_t GetCardIndex() = 0;

  /**
   * @brief used buffer size in bytes for a single channel
   * @details mainly nbSamples * sampleByteSize
   */
  virtual uint32_t GetChannelBufferSize() = 0;
  /**
   * @brief allocated buffer size in bytes for a single channel
   * @details may differ from GetChannelBufferSize depending on cards
   */
  virtual uint32_t GetChannelAllocationSize();
  virtual void SetChannelBufferFree() = 0;
  virtual uint32_t GetNbChannels() = 0;
  virtual void ArmTrigger() = 0;
  virtual bool IsTriggered() = 0;
  virtual void LoadDefaultConfiguration() = 0;
  virtual void configure() = 0;
  virtual void Calibrate() = 0;
  virtual void readout();
  virtual void OfflineParsing(int8_t *dumpBuffer);
  virtual void DumpCardMemory() = 0;
  virtual void Delete() = 0;
  virtual int GetResolution() = 0;
  virtual int GetBitMask();
  virtual int CheckStatus();
  virtual std::shared_ptr<ntof::CChannel> &GetChannel(uint channel) final;
  virtual void PerformZeroSuppression(ZeroSuppression::zsMode mode,
                                      const int16_t **channelBuffers) final;
  virtual ChannelCollection &GetChannelCollection() final;
  virtual bool HasOfflineParsing() = 0;
  virtual bool HasHardwareZeroSuppression();
  virtual uint32_t GetSlotNumber() final;
  virtual uint32_t GetNbUsedChannels() final;
  /**
   * Set the configuration as invalid
   */
  virtual void SetConfigurationInvalid() final;
  /**
   * Check if configuration is valid
   * @return true if configuration is valid
   */
  virtual bool IsConfigurationValid() final;
  virtual std::string GetType() final;
  virtual std::string GetSerialNumber() final;
  virtual bool IsCardEnabled() final;
  virtual bool IsDumpSequential();
  virtual uint32_t GetCrateIndex() final;
  virtual uint32_t GetNbBytesDumped() = 0;
  void SetCpuCore(int acq, int writer);
  void GetCpuCore(int &acq, int &writer);
  int GetWriterCore();
  int GetAcqCore();
  void AllocateBufferPool(int poolSize);
  int16_t **GetChannelBuffer(uint index);
  int8_t *GetUnparsedBuffer(uint index);
  uint GetBufferPoolIndex();
  void SetBufferPoolIndex(uint index);
  std::unique_ptr<AcquisitionBuffer> &GetBuffer(int index);
  void FreeBufferPool();
  bool isMaster();
  void SetAsMaster(bool master);

protected:
  std::string hostName_;      //!< host's name
  bool configurationValid_;   //!< set true when received configuration is valid
  int nbDevices_;             //!< number of cards present in the system
  uint32_t cardIndex_;        //!< card's index
  uint32_t crateIndex_;       //!< card's crate index
  ChannelCollection channel_; //!< vector of channels
  uint32_t slotNumber_;
  uint32_t vectorIndex_;
  bool cardEnabled_;
  std::string serialNumber_;
  uint32_t nbBytesDumped_;
  ntof::dim::DIMState *state_;
  std::string type_;
  int acqCore_;
  int writerCore_;
  std::vector<std::unique_ptr<AcquisitionBuffer>> bufferPool_;
  static uint bufferPoolIndex_;
  bool isMaster_;
  int poolSize_;
};

// CCard's exception class
class CCardEx : public std::exception
{
public:
  CCardEx() throw();
  CCardEx(std::string msg) throw();
  virtual ~CCardEx() throw();
  virtual const char *what() const throw();
  void SetMessage(std::string msg);

private:
  std::string exceptionMsg_;
};

#endif /* CCARD_H_ */
