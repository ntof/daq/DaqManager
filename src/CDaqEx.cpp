/*
 * CDaqException.cpp
 *
 *  Created on: Oct 13, 2014
 *      Author: pperonna
 */

#include "CDaqEx.h"

#include <exception>
#include <string>

using namespace std;

CDaqException::CDaqException() throw()
{
  exceptionMsg_ = "none";
}

CDaqException::CDaqException(const std::string &msg) throw() :
  exceptionMsg_(msg)
{}

CDaqException::~CDaqException() throw() {}

const char *CDaqException::what() const throw()
{
  return exceptionMsg_.c_str();
}

void CDaqException::SetMessage(const string &msg)
{
  exceptionMsg_ = msg;
}
