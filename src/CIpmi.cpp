
#include "CIpmi.h"

#include <algorithm>
#include <cctype>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <freeipmi/freeipmi.h>
#include <ipmi_monitoring_bitmasks.h>

#include "logger.h"

namespace ntof {

CIpmi::CIpmi()
{
  Config();
}

CIpmi::~CIpmi()
{
  if (ctx)
    ipmi_monitoring_ctx_destroy(ctx);
}

void CIpmi::Config()
{
  ipmi_config.driver_type = driver_type;
  ipmi_config.disable_auto_probe = disable_auto_probe;
  ipmi_config.driver_address = driver_address;
  ipmi_config.register_spacing = register_spacing;
  ipmi_config.driver_device = driver_device;

  ipmi_config.protocol_version = protocol_version;
  ipmi_config.k_g = k_g;
  ipmi_config.k_g_len = k_g_len;
  ipmi_config.privilege_level = privilege_level;
  ipmi_config.authentication_type = authentication_type;
  ipmi_config.cipher_suite_id = cipher_suite_id;
  ipmi_config.session_timeout_len = session_timeout;
  ipmi_config.retransmission_timeout_len = retransmission_timeout;

  ipmi_config.workaround_flags = workaround_flags;

  if (ipmi_monitoring_init(ipmimonitoring_init_flags, &errnum) < 0)
  {
    std::string err = "ipmi_monitoring_init: " +
      std::string(ipmi_monitoring_ctx_strerror(errnum));
    throw err;
  }
}

std::vector<IPMI_SENSOR_t> &CIpmi::GetMonitoring()
{
  unsigned int sensor_reading_flags = 0;
  int sensor_count;

  if (!(ctx = ipmi_monitoring_ctx_create()))
  {
    throw "ipmi_monitoring_ctx_create: " + std::string(strerror(errno));
  }

  if (sdr_cache_directory.length())
  {
    if (ipmi_monitoring_ctx_sdr_cache_directory(
          ctx, sdr_cache_directory.c_str()) < 0)
    {
      std::string err = "ipmi_monitoring_ctx_sdr_cache_directory: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }
  }

  /* Must call otherwise only default interpretations ever used */
  if (sensor_config_file)
  {
    if (ipmi_monitoring_ctx_sensor_config_file(ctx, sensor_config_file) < 0)
    {
      std::string err = "ipmi_monitoring_ctx_sensor_config_file: %s\n" +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }
  }
  else
  {
    if (ipmi_monitoring_ctx_sensor_config_file(ctx, NULL) < 0)
    {
      std::string err = "ipmi_monitoring_ctx_sensor_config_file: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }
  }

  if (reread_sdr_cache)
    sensor_reading_flags |= IPMI_MONITORING_SENSOR_READING_FLAGS_REREAD_SDR_CACHE;

  if (ignore_non_interpretable_sensors)
    sensor_reading_flags |=
      IPMI_MONITORING_SENSOR_READING_FLAGS_IGNORE_NON_INTERPRETABLE_SENSORS;

  if (bridge_sensors)
    sensor_reading_flags |= IPMI_MONITORING_SENSOR_READING_FLAGS_BRIDGE_SENSORS;

  if (interpret_oem_data)
    sensor_reading_flags |=
      IPMI_MONITORING_SENSOR_READING_FLAGS_INTERPRET_OEM_DATA;

  if (shared_sensors)
    sensor_reading_flags |= IPMI_MONITORING_SENSOR_READING_FLAGS_SHARED_SENSORS;

  if (discrete_reading)
    sensor_reading_flags |= IPMI_MONITORING_SENSOR_READING_FLAGS_DISCRETE_READING;

  if (ignore_scanning_disabled)
    sensor_reading_flags |=
      IPMI_MONITORING_SENSOR_READING_FLAGS_IGNORE_SCANNING_DISABLED;

  if (assume_bmc_owner)
    sensor_reading_flags |= IPMI_MONITORING_SENSOR_READING_FLAGS_ASSUME_BMC_OWNER;

  if (entity_sensor_names)
    sensor_reading_flags |=
      IPMI_MONITORING_SENSOR_READING_FLAGS_ENTITY_SENSOR_NAMES;

  if (!record_ids_length && !sensor_types_length)
  {
    if ((sensor_count = ipmi_monitoring_sensor_readings_by_record_id(
           ctx, hostname, &ipmi_config, sensor_reading_flags, NULL, 0, NULL,
           NULL)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_readings_by_record_id: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }
  }
  else if (record_ids_length)
  {
    if ((sensor_count = ipmi_monitoring_sensor_readings_by_record_id(
           ctx, hostname, &ipmi_config, sensor_reading_flags, &record_ids[0],
           record_ids_length, NULL, NULL)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_readings_by_record_id: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }
  }
  else
  {
    if ((sensor_count = ipmi_monitoring_sensor_readings_by_sensor_type(
           ctx, hostname, &ipmi_config, sensor_reading_flags, &sensor_types[0],
           sensor_types_length, NULL, NULL)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_readings_by_sensor_type: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }
  }

  sensorList.clear();
  sensorList.reserve(sensor_count);

  for (int i = 0; i < sensor_count;
       ++i, ipmi_monitoring_sensor_iterator_next(ctx))
  {
    int record_id, sensor_number, sensor_type, sensor_state, sensor_units,
      sensor_bitmask_type, sensor_bitmask, sensor_reading_type,
      event_reading_type_code;
    char **sensor_bitmask_strings = NULL;

    IPMI_SENSOR_t sensor;

    if ((record_id = ipmi_monitoring_sensor_read_record_id(ctx)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_read_record_id: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    if ((sensor_number = ipmi_monitoring_sensor_read_sensor_number(ctx)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_read_sensor_number: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    if ((sensor_type = ipmi_monitoring_sensor_read_sensor_type(ctx)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_read_sensor_type: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    sensor.name = std::string(ipmi_monitoring_sensor_read_sensor_name(ctx));

    if (!sensor.name.length())
      sensor.name = "N/A";

    replace(sensor.name.begin(), sensor.name.end(), ' ', '_');
    if (sensor.name[0] == '+')
      sensor.name.erase(0, 1);
    if (isdigit(sensor.name[0]))
      sensor.name.insert(sensor.name.begin(), '_');

    if (!sensor.name.length())
    {
      std::string err = "ipmi_monitoring_sensor_read_sensor_name: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    if ((sensor_state = ipmi_monitoring_sensor_read_sensor_state(ctx)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_read_sensor_state: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    if ((sensor_units = ipmi_monitoring_sensor_read_sensor_units(ctx)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_read_sensor_units: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    if ((sensor_bitmask_type = ipmi_monitoring_sensor_read_sensor_bitmask_type(
           ctx)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_read_sensor_bitmask_type: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    if ((sensor_bitmask = ipmi_monitoring_sensor_read_sensor_bitmask(ctx)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_read_sensor_bitmask: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    if (!(sensor_bitmask_strings =
            ipmi_monitoring_sensor_read_sensor_bitmask_strings(ctx)))
    {
      std::string err = "ipmi_monitoring_sensor_read_sensor_bitmask_strings: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    if ((sensor_reading_type = ipmi_monitoring_sensor_read_sensor_reading_type(
           ctx)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_read_sensor_reading_type: " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    void *sensor_reading = ipmi_monitoring_sensor_read_sensor_reading(ctx);

    if ((event_reading_type_code =
           ipmi_monitoring_sensor_read_event_reading_type_code(ctx)) < 0)
    {
      std::string err = "ipmi_monitoring_sensor_read_event_reading_type_code:"
                        " " +
        std::string(ipmi_monitoring_ctx_errormsg(ctx));
      throw err;
    }

    sensor.type = GetSensorType(sensor_type);

    if (sensor_state == IPMI_MONITORING_STATE_NOMINAL)
      sensor.state = "Nominal";
    else if (sensor_state == IPMI_MONITORING_STATE_WARNING)
      sensor.state = "Warning";
    else if (sensor_state == IPMI_MONITORING_STATE_CRITICAL)
      sensor.state = "Critical";
    else
      sensor.state = "N/A";

    if (sensor_reading)
    {
      if (sensor_reading_type ==
          IPMI_MONITORING_SENSOR_READING_TYPE_UNSIGNED_INTEGER8_BOOL)
        sensor.value = *((uint8_t *) sensor_reading) ? "true" : "false";
      else if (sensor_reading_type ==
               IPMI_MONITORING_SENSOR_READING_TYPE_UNSIGNED_INTEGER32)
        sensor.value = boost::lexical_cast<std::string>(
          *((uint32_t *) sensor_reading));
      else if (sensor_reading_type == IPMI_MONITORING_SENSOR_READING_TYPE_DOUBLE)
      {
        if (sensor.type == "Voltage")
          sensor.value = boost::str(boost::format("%.2f") %
                                    (*((double *) sensor_reading)));
        else
          sensor.value = boost::str(boost::format("%.0f") %
                                    (*((double *) sensor_reading)));
      }
      else
        sensor.value = "N/A";

      if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_CELSIUS)
        sensor.unit = "C";
      else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_FAHRENHEIT)
        sensor.unit = "F";
      else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_VOLTS)
        sensor.unit = "V";
      else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_AMPS)
        sensor.unit = "A";
      else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_RPM)
        sensor.unit = "RPM";
      else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_WATTS)
        sensor.unit = "W";
      else if (sensor_units == IPMI_MONITORING_SENSOR_UNITS_PERCENT)
        sensor.unit = "%";
      else
        sensor.unit = "N/A";
    }

    // printf(", %Xh", event_reading_type_code);

    /* It is possible you may want to monitor specific event
     * conditions that may occur.  If that is the case, you may want
     * to check out what specific bitmask type and bitmask events
     * occurred.  See ipmi_monitoring_bitmasks.h for a list of
     * bitmasks and types.
     */
#if 0
		if (sensor_bitmask_type != IPMI_MONITORING_SENSOR_BITMASK_TYPE_UNKNOWN)
		printf(", %Xh", sensor_bitmask);
		else
		printf(", N/A");

		if (sensor_bitmask_type != IPMI_MONITORING_SENSOR_BITMASK_TYPE_UNKNOWN)
		{
			unsigned int i = 0;

			printf(",");

			while (sensor_bitmask_strings[i])
			{
				printf(" ");

				printf("'%s'", sensor_bitmask_strings[i]);

				i++;
			}
		}
		else
		printf(", N/A");
#endif
    sensorList.push_back(sensor);
  }

  if (ctx)
    ipmi_monitoring_ctx_destroy(ctx);

  return sensorList;
}

std::string CIpmi::GetSensorType(int sensor_type)
{
  switch (sensor_type)
  {
  case IPMI_MONITORING_SENSOR_TYPE_RESERVED: return ("Reserved");
  case IPMI_MONITORING_SENSOR_TYPE_TEMPERATURE: return ("Temperature");
  case IPMI_MONITORING_SENSOR_TYPE_VOLTAGE: return ("Voltage");
  case IPMI_MONITORING_SENSOR_TYPE_CURRENT: return ("Current");
  case IPMI_MONITORING_SENSOR_TYPE_FAN: return ("Fan");
  case IPMI_MONITORING_SENSOR_TYPE_PHYSICAL_SECURITY:
    return ("Physical Security");
  case IPMI_MONITORING_SENSOR_TYPE_PLATFORM_SECURITY_VIOLATION_ATTEMPT:
    return ("Platform Security Violation Attempt");
  case IPMI_MONITORING_SENSOR_TYPE_PROCESSOR: return ("Processor");
  case IPMI_MONITORING_SENSOR_TYPE_POWER_SUPPLY: return ("Power Supply");
  case IPMI_MONITORING_SENSOR_TYPE_POWER_UNIT: return ("Power Unit");
  case IPMI_MONITORING_SENSOR_TYPE_COOLING_DEVICE: return ("Cooling Device");
  case IPMI_MONITORING_SENSOR_TYPE_OTHER_UNITS_BASED_SENSOR:
    return ("Other Units Based Sensor");
  case IPMI_MONITORING_SENSOR_TYPE_MEMORY: return ("Memory");
  case IPMI_MONITORING_SENSOR_TYPE_DRIVE_SLOT: return ("Drive Slot");
  case IPMI_MONITORING_SENSOR_TYPE_POST_MEMORY_RESIZE:
    return ("POST Memory Resize");
  case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_FIRMWARE_PROGRESS:
    return ("System Firmware Progress");
  case IPMI_MONITORING_SENSOR_TYPE_EVENT_LOGGING_DISABLED:
    return ("Event Logging Disabled");
  case IPMI_MONITORING_SENSOR_TYPE_WATCHDOG1: return ("Watchdog 1");
  case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_EVENT: return ("System Event");
  case IPMI_MONITORING_SENSOR_TYPE_CRITICAL_INTERRUPT:
    return ("Critical Interrupt");
  case IPMI_MONITORING_SENSOR_TYPE_BUTTON_SWITCH: return ("Button/Switch");
  case IPMI_MONITORING_SENSOR_TYPE_MODULE_BOARD: return ("Module/Board");
  case IPMI_MONITORING_SENSOR_TYPE_MICROCONTROLLER_COPROCESSOR:
    return ("Microcontroller/Coprocessor");
  case IPMI_MONITORING_SENSOR_TYPE_ADD_IN_CARD: return ("Add In Card");
  case IPMI_MONITORING_SENSOR_TYPE_CHASSIS: return ("Chassis");
  case IPMI_MONITORING_SENSOR_TYPE_CHIP_SET: return ("Chip Set");
  case IPMI_MONITORING_SENSOR_TYPE_OTHER_FRU: return ("Other Fru");
  case IPMI_MONITORING_SENSOR_TYPE_CABLE_INTERCONNECT:
    return ("Cable/Interconnect");
  case IPMI_MONITORING_SENSOR_TYPE_TERMINATOR: return ("Terminator");
  case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_BOOT_INITIATED:
    return ("System Boot Initiated");
  case IPMI_MONITORING_SENSOR_TYPE_BOOT_ERROR: return ("Boot Error");
  case IPMI_MONITORING_SENSOR_TYPE_OS_BOOT: return ("OS Boot");
  case IPMI_MONITORING_SENSOR_TYPE_OS_CRITICAL_STOP:
    return ("OS Critical Stop");
  case IPMI_MONITORING_SENSOR_TYPE_SLOT_CONNECTOR: return ("Slot/Connector");
  case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_ACPI_POWER_STATE:
    return ("System ACPI Power State");
  case IPMI_MONITORING_SENSOR_TYPE_WATCHDOG2: return ("Watchdog 2");
  case IPMI_MONITORING_SENSOR_TYPE_PLATFORM_ALERT: return ("Platform Alert");
  case IPMI_MONITORING_SENSOR_TYPE_ENTITY_PRESENCE: return ("Entity Presence");
  case IPMI_MONITORING_SENSOR_TYPE_MONITOR_ASIC_IC: return ("Monitor ASIC/IC");
  case IPMI_MONITORING_SENSOR_TYPE_LAN: return ("LAN");
  case IPMI_MONITORING_SENSOR_TYPE_MANAGEMENT_SUBSYSTEM_HEALTH:
    return ("Management Subsystem Health");
  case IPMI_MONITORING_SENSOR_TYPE_BATTERY: return ("Battery");
  case IPMI_MONITORING_SENSOR_TYPE_SESSION_AUDIT: return ("Session Audit");
  case IPMI_MONITORING_SENSOR_TYPE_VERSION_CHANGE: return ("Version Change");
  case IPMI_MONITORING_SENSOR_TYPE_FRU_STATE: return ("FRU State");
  }

  return ("Unrecognized");
}

} /* namespace ntof */
