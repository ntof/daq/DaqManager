/*
 * TimerClient.cpp
 *
 *  Created on: Oct 29, 2014
 *      Author: agiraud
 */

#include "TimerClient.h"

#include <sstream>
#include <string>

#include <boost/lexical_cast.hpp>

#include "defines.h"
#include "logger.h"

namespace ntof {

TimerClient::TimerClient(const std::string &name) :
  client_(name),
  eventReader_(name + "/event")
{
  eventReader_.registerHandler(this);
  ResetStatus();
}

TimerClient::~TimerClient() {}

void TimerClient::ResetStatus()
{
  std::lock_guard<std::mutex> lock(mutex_);
  status_.err = "no error";
  status_.code = 0;
}

TRIGGER_TIMESTAMP_STATUS TimerClient::GetStatus()
{
  std::lock_guard<std::mutex> lock(mutex_);
  return status_;
}

void TimerClient::noLink()
{
  std::lock_guard<std::mutex> lock(mutex_);
  status_.code = -1;
  status_.err = "no link";
}

void TimerClient::eventReceived(const ntof::dim::EventReader::Data &event)
{
  BOOST_LOG_SCOPED_THREAD_ATTR(
    "Scope", boost::log::attributes::constant<std::string>("Timing/event"));

  std::lock_guard<std::mutex> lock(mutex_);
  event_ = event;
  LOG_INFO << "event number : " << event.evtNumber;
}

int64_t TimerClient::getCycleStamp()
{
  std::lock_guard<std::mutex> lock(mutex_);
  return event_.cycleStamp;
}

int64_t TimerClient::getEventNumber()
{
  std::lock_guard<std::mutex> lock(mutex_);
  return event_.evtNumber;
}

std::string &TimerClient::getType()
{
  std::lock_guard<std::mutex> lock(mutex_);
  return event_.evtType;
}

void TimerClient::parameterChanged(
  const std::vector<ntof::dim::DIMData> &settingsChanged,
  const ntof::dim::DIMParamListClient &list)
{
  UNUSED(settingsChanged);
  UNUSED(list);
}

} // namespace ntof
