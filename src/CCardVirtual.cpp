#include "CCardVirtual.h"

#include <cstdint>
#include <sstream>
#include <string>
#include <vector>

#include <boost/lexical_cast.hpp>

#include <unistd.h>

#include "CChannelVirtual.h"
#include "DIMException.h"
#include "DaqTypes.h"
#include "logger.h"

#include "dis.hxx"

namespace virtualcard {

bool CCardVirtual::ready_ = false;
std::vector<std::shared_ptr<VirtualCardParameters>> CCardVirtual::cards_;

CCardVirtual::CCardVirtual(std::string type,
                           std::string serialNumber,
                           int cpuCore,
                           uint32_t cardIndex,
                           uint32_t nbDevices,
                           ntof::dim::DIMState *state,
                           uint32_t bufferSize,
                           bool enableTrace) :
  CCard(type, serialNumber, cpuCore, cardIndex, 0, state, enableTrace),
  cardIndex_(cardIndex),
  nbDevices_(nbDevices),
  bufferSize_(bufferSize),
  hasTriggered_(false),
  eventReader_("Timing/event")
{
  LOG_TRACE << "Entering CCardVirtual::CCardVirtual with index " +
      boost::lexical_cast<std::string>(cardIndex_);

  eventReader_.registerHandler(this);

  try
  {
    FindDevices();

    if (!ready_)
    {
      throw CCardEx("virtual card instance not ready");
    }

    // create channel collection for the card
    channel_.reserve(GetNbChannels());
    for (uint i = 0; i < GetNbChannels(); ++i)
    {
      std::ostringstream oss;
      oss << hostName_ << "/CARD" << cardIndex_ << "/CHANNEL" << i;
      LOG_TRACE << "creating channel: " + oss.str();
      channel_.push_back(std::make_shared<CChannelVirtual>(
        ChannelConfig::makeLocation(1, CCard::crateIndex_, cardIndex, i),
        oss.str(), state_));
    }
  }
  catch (const ntof::ZeroSuppression::CZeroSuppressionEx &ex)
  {
    throw CCardEx(ex.what());
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardVirtual::CCardVirtual with index " +
      boost::lexical_cast<std::string>(cardIndex_);
}

CCardVirtual::~CCardVirtual() {}

void CCardVirtual::FindDevices()
{
  LOG_TRACE << "Entering CCardVirtual::findDevices for index " +
      boost::lexical_cast<std::string>(cardIndex_);

  if (ready_)
  {
    LOG_TRACE << "Exiting CCardVirtual::findDevices - cards already probed" +
        boost::lexical_cast<std::string>(cardIndex_);
    return;
  }

  cards_.reserve(nbDevices_);

  // Creating collection of cards
  for (uint i = 0; i < nbDevices_; ++i)
  {
    cards_.push_back(std::make_shared<VirtualCardParameters>());
  }

  ready_ = true;

  LOG_TRACE << boost::lexical_cast<std::string>(nbDevices_) +
      " virtual instruments found";
  LOG_TRACE << "Exiting CCardVirtual::findDevices";
}

uint32_t CCardVirtual::GetNbChannels()
{
  return 4;
}

uint32_t CCardVirtual::GetNbBytesDumped()
{
  return bufferSize_ * sizeof(*cards_[cardIndex_]->dumpBuffer_);
}

int CCardVirtual::GetResolution()
{
  return 16;
}

void CCardVirtual::ConfigureChannels()
{
  for (auto &ch : channel_)
  {
    ch->getConfiguration()->enabled = true;
    ch->copyListParam();
  }
}

void CCardVirtual::SetDumpMemoryBuffer(int8_t *buffer)
{
  cards_[cardIndex_]->dumpBuffer_ = buffer;
}

int8_t *CCardVirtual::GetMemoryDumpBuffer()
{
  return cards_[cardIndex_]->dumpBuffer_;
}

int16_t **CCardVirtual::GetAcquisitionBuffer()
{
  return cards_[cardIndex_]->buffer_;
}

void CCardVirtual::SetChannelBufferFree()
{
  throw std::runtime_error(
    "CCardVirtual::SetChannelBufferFree not implemented");
}

void CCardVirtual::ArmTrigger()
{
  std::lock_guard<std::mutex> lock(mutex_);
  hasTriggered_ = false;
}

bool CCardVirtual::IsTriggered()
{
  std::lock_guard<std::mutex> lock(mutex_);
  return hasTriggered_;
}

uint32_t CCardVirtual::GetDumpBufferSize()
{
  return bufferSize_ * sizeof(*cards_[cardIndex_]->dumpBuffer_);
}

uint32_t CCardVirtual::GetChannelBufferSize()
{
  return bufferSize_ * sizeof(*cards_[cardIndex_]->dumpBuffer_);
}

uint32_t CCardVirtual::GetCardIndex()
{
  return cardIndex_;
}

void CCardVirtual::DumpCardMemory()
{
  LOG_TRACE << "Entering CCardVirtual::DumpCardMemory for card " +
      boost::lexical_cast<std::string>(cardIndex_);

  LOG_TRACE << "Exiting CCardVirtual::DumpCardMemory for card " +
      boost::lexical_cast<std::string>(cardIndex_);
}

void CCardVirtual::Calibrate()
{
  LOG_TRACE << "Entering CCardVirtual::Calibrate for card " +
      boost::lexical_cast<std::string>(cardIndex_);

  for (auto &ch : channel_)
  {
    if (ch->getConfiguration()->enabled == false)

    {
      ch->SetCalibratedFullScale(5000);
      ch->SetCalibratedOffset(0);
      ch->SetCalibratedThreshold(0);
      continue;
    }

    ch->SetCalibratedFullScale(5000.);
    ch->SetCalibratedOffset(0.);

    // update the threshold for the zero suppression
    int16_t th = (ch->getConfiguration()->header.threshold +
                  ch->getConfiguration()->header.offset +
                  ch->GetCalibratedOffset()) /
      (ch->GetCalibratedFullScale() / static_cast<float>(1 << GetResolution()));

    ch->GetZeroSuppression().SetThreshold(th);
    ch->SetCalibratedThreshold(th);
    ch->GetZeroSuppression().SetGainOffset(
      static_cast<float>(ch->getConfiguration()->header.fullScale) /
        (1 << GetResolution()),
      ch->GetCalibratedFullScale(),
      ch->getConfiguration()->header.threshold +
        ch->getConfiguration()->header.offset + ch->GetCalibratedOffset());
  }

  LOG_TRACE << "Exiting CCardVirtual::Calibrate for card " +
      boost::lexical_cast<std::string>(cardIndex_);
}

void CCardVirtual::LoadDefaultConfiguration() {}

void CCardVirtual::configure()
{
  LOG_TRACE << "Entering CCardVirtual::configure for index " +
      boost::lexical_cast<std::string>(cardIndex_);

  if (ready_)
  {
    try
    {
      ConfigureChannels();
    }
    catch (const ntof::CChannelEx &ex)
    {
      throw CCardEx(ex.what());
    }
    catch (const CCardEx &ex)
    {
      throw CCardEx(ex.what());
    }
  }
  else
    LOG_TRACE << "Exiting CCardVirtual::configure - class not ready";

  LOG_TRACE << "Exiting CCardVirtual::configure";
}

void CCardVirtual::Delete()
{
  LOG_TRACE << "Entering CCardVirtual::Delete with index " +
      boost::lexical_cast<std::string>(cardIndex_);

  if (ready_ == false)
  {
    LOG_TRACE << "driver instance is already NULL, exiting ...";
    return;
  }

  ready_ = false;

  LOG_TRACE << "Exiting CCardVirtual::Delete with index " +
      boost::lexical_cast<std::string>(cardIndex_);
}

bool CCardVirtual::HasOfflineParsing()
{
  return false;
}

void CCardVirtual::eventReceived(const ntof::dim::EventReader::Data &event)
{
  std::lock_guard<std::mutex> lock(mutex_);
  hasTriggered_ = true;

  // Save the data in memory
  cycleStamp_ = event.cycleStamp;
  type_ = event.evtType;
  eventNumber_ = event.evtNumber;
}

void CCardVirtual::noLink()
{
  std::lock_guard<std::mutex> lock(mutex_);
}

} // namespace virtualcard
