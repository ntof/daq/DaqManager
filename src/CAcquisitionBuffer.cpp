/*
 * CAquisitionBuffer.cpp
 *
 *  Created on: Oct 21, 2014
 *      Author: pperonna
 */

#include "CAcquisitionBuffer.h"

#include <algorithm>

#include "CCard.h" // CCardEx

AcquisitionBuffer::AcquisitionBuffer() :
  dumpBuffer_(nullptr),
  cardIndex_(0),
  crateIndex_(0),
  used_(false),
  isReady_(false),
  cpu_(0),
  node_(0),
  parsingZsStatus_(0),
  dumpStatus_(0)
{}

AcquisitionBuffer::~AcquisitionBuffer()
{
  FreeBufferPool();
  FreeDumpBuffer();
}

void AcquisitionBuffer::AllocateBufferPool(std::size_t poolSize,
                                           std::size_t bufferSize,
                                           int align)
{
  FreeBufferPool();
  buffer_.reserve(poolSize);
  for (std::size_t i = 0; i < poolSize; ++i)
  {
    buffer_.emplace_back(static_cast<int16_t *>(_mm_malloc(bufferSize, align)));
    if (buffer_.back() == nullptr)
    {
      throw CCardEx("memory allocation for channels failed");
    }
    ::memset(buffer_.back(), 0, bufferSize);
  }
}

void AcquisitionBuffer::FreeBufferPool()
{
  std::for_each(buffer_.begin(), buffer_.end(), _mm_free);
  buffer_.resize(0);
}

void AcquisitionBuffer::AllocateDumpBuffer(std::size_t bufferSize, int align)
{
  FreeDumpBuffer();
  dumpBuffer_ = static_cast<int8_t *>(_mm_malloc(bufferSize, align));
  if (dumpBuffer_ == nullptr)
  {
    throw CCardEx("memory allocation for unparsed data failed");
  }
  ::memset(dumpBuffer_, 0, bufferSize);
}

void AcquisitionBuffer::FreeDumpBuffer()
{
  _mm_free(dumpBuffer_);
  dumpBuffer_ = nullptr;
}

CAcquisitionBuffer::CAcquisitionBuffer() {}

CAcquisitionBuffer::~CAcquisitionBuffer()
{
  for (auto &it : card_)
  {
    delete it, it = nullptr;
  }
}
