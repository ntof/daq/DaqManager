#include "CDaq.h"

#include <memory>
#include <regex>

#include <boost/algorithm/string.hpp>
#include <boost/chrono/chrono.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>

#include <sched.h>
#include <sys/time.h>

#include "DIMSuperCommand.h"
#include "NTOFException.h"
#include "PerformanceCounter.h"
#include "config.h"
#include "logger.h"
#include "pugixml.hpp"

#include "dis.hxx"

#ifdef ACQIRIS
#include "Acqiris/DC240/CCardAcqirisDC240.h"
#include "Acqiris/DC270/CCardAcqirisDC270.h"
#include "Acqiris/DC282/CCardAcqirisDC282.h"
#endif

#ifdef SPDEVICES
#include "spd/ADQ14/CCardSpdAdq14.h"
#include "spd/ADQ412/CCardSpdAdq412.h"
#endif

#include "CCardVirtual.h"

using namespace boost::interprocess;
using namespace std;
using namespace ntof::dim;

CDaq::CDaq(std::string hwConfigFilename,
           std::string hostName,
           CMD_LINE_PARAM_t cmdLineParams) :
  DIMSuperCommand(hostName + "/Daq/Command"),
  state_(hostName + "/DaqState"),
  acqQueue_(ACQ_BUFFER_POOL_SIZE),
  hostName_(hostName),
  hwConfigFilename_(hwConfigFilename),
  cmdLineParams_(cmdLineParams),
  dimListDaqElements_(hostName + "/ListDaqElements")
{
  nbCards_ = 0;
  acquisitionRunning_ = false;
  cardCalibrationEnabled_ = false;
  runNumber_ = 0;
}

CDaq::~CDaq()
{
  LOG_TRACE << "Entering CDaq::~CDaq";

  DeleteAll();

  LOG_TRACE << "Entering CDaq::~CDaq";
}

void CDaq::InitServiceDim()
{
  LOG_TRACE << "Entering CDaq::InitServiceDim";

  // Initialize DIM service for state
  try
  {
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::DETECTING_HARDWARE),
                         "Hardware initialization");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::UNCONFIGURED),
                         "Not configured");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::WAITING_FOR_CMD),
                         "Waiting for command");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::PROCESSING_CMD),
                         "Processing command");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::INITIALIZING_ACQ),
                         "Initializing acquisition");
    state_.addStateValue(
      static_cast<int32_t>(DAQ_STATE_t::WAITING_FOR_START_ACQ_CMD),
      "Waiting for start acquisition command");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::WAITING_FOR_HEADERS),
                         "Waiting for headers");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::WAITING_FOR_MODH),
                         "Waiting for MODH");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::WAITING_FOR_RCTR),
                         "Waiting for RCTR");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::STOPPING_ACQ),
                         "Stopping acquisition");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::STARTING_ACQ),
                         "Starting acquisition");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::RESETING_DAQ),
                         "Reseting");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::CALIBRATING_CARDS),
                         "Calibrating cards");
    state_.addStateValue(static_cast<int32_t>(DAQ_STATE_t::SUPER_USER),
                         "Super user");

    state_.setValue(static_cast<int32_t>(DAQ_STATE_t::DETECTING_HARDWARE));
  }
  catch (const DIMException &ex)
  {
    throw CDaqException(ex.what());
  }
  catch (const std::bad_alloc &ex)
  {
    throw CDaqException(ex.what());
  }

  LOG_TRACE << "Exiting CDaq::InitServiceDim";
}

void CDaq::InitializeThreads(const std::string &lockFile)
{
  LOG_TRACE << "Entering CDaq::InitializeThreads";

  try
  {
    writer_ = std::make_shared<CWriter>(hwConfigFilename_, hostName_, acqQueue_,
                                        cards_, &state_, cmdLineParams_);
    writer_->setLockFile(lockFile);

    acq_ = std::make_shared<CAcquisition>(
      hostName_, acqQueue_, cards_, writer_->GetState(),
      cmdLineParams_.timingEventName, cmdLineParams_);
    acq_->setLockFile(lockFile);
  }
  catch (const std::bad_alloc &ex)
  {
    throw CDaqException(ex.what());
  }
  catch (const CWriterEx &ex)
  {
    throw CDaqException(ex.what());
  }
  catch (const CAcquisitionEx &ex)
  {
    throw CDaqException(ex.what());
  }

  LOG_TRACE << "Exiting CDaq::InitializeThreads";
}

void CDaq::UpdateCardConfiguration()
{
  for (auto &card : cards_)
  {
    if (card->IsConfigurationValid() == false)
    {
      throw CDaqException(
        "configuration for card " +
        boost::lexical_cast<std::string>(card->GetCardIndex()) +
        " is not valid");
    }

    try
    {
      card->configure();
    }
    catch (const CCardEx &ex)
    {
      throw CDaqException(ex.what());
    }
  }
}

void CDaq::SetConfigurationInvalid()
{
  for (auto &card : cards_)
  {
    card->SetConfigurationInvalid();
  }
  writer_->SetConfigurationInvalid();
}

void CDaq::StartAcq()
{
  LOG_TRACE << "Entering CDaq::StartAcq";

  acq_->StartAcq();
  acquisitionRunning_ = true;

  LOG_TRACE << "Exiting CDaq::StartAcq";
}

void CDaq::InitAcq()
{
  LOG_TRACE << "Entering CDaq::InitAcq";

  // If no cards have been detected, get away ...
  if (cards_.size() == 0)
  {
    std::string err = "could not start the acquisition, no card detected in "
                      "the system";
    throw CDaqException(err);
  }

  for (auto &card : cards_)
  {
    try
    {
      if (!cardCalibrationEnabled_)
      {
        card->configure();
      }
    }
    catch (const CCardEx &ex)
    {
      std::ostringstream oss;
      oss << "could not configure card " << card->GetCardIndex();
      oss << ": " << ex.what();
      throw CDaqException(oss.str());
    }
  }

  // Starting acquisition and writer threads
  try
  {
    //#warning "writer disabled"
    writer_->Start(runNumber_);
  }
  catch (const CWriterEx &ex)
  {
    throw CDaqException(ex.what());
  }

  try
  {
    acq_->Start(runNumber_);

    int timeout = 0;
    while (acq_->GetState() != ACQUISITION_STATE_t::WAITING_TO_START)
    {
      boost::this_thread::sleep(boost::posix_time::millisec(10));
      timeout++;
      if (timeout > 1000 * 60 * 5)
      {
        throw CDaqException(
          "acquisistion thread did not complete memory allocation after 5min");
      }
    }
  }
  catch (const CAcquisitionEx &ex)
  {
    throw CDaqException(ex.what());
  }

  // acquisitionRunning_ = true;

  LOG_TRACE << "Exiting CDaq::InitAcq";
}

void CDaq::StopAcq()
{
  LOG_TRACE << "Entering CDaq::StopAcq";

  try
  {
    if (acquisitionRunning_)
    {
      acq_->Stop();
    }
    else
    {
      acq_->ForceStop();
    }
  }
  catch (const CAcquisitionEx &ex)
  {
    throw CDaqException(ex.what());
  }

  try
  {
    if (acquisitionRunning_)
    {
      writer_->Stop();
    }
    else
    {
      writer_->ForceStop();
    }
  }
  catch (const CWriterEx &ex)
  {
    throw CDaqException(ex.what());
  }

  for (auto &card : cards_)
  {
    card->SetConfigurationInvalid();
  }

  // ForceStop may have left some messages in acqQueue
  // Writer has freed buffers, so we can release everything
  acqQueue_.clear();
  acquisitionRunning_ = false;

  writer_->SetConfigurationInvalid();

  LOG_TRACE << "Exiting CDaq::StopAcq";
}

XML_HW_PARAMS_t CDaq::GetXmlHardwareParams(
  const pugi::xml_object_range<pugi::xml_node_iterator> &children)
{
  XML_HW_PARAMS_t hwParams;

  for (auto &child : children)
  {
    if (std::string(child.name()) == "sn")
    {
      hwParams.sn = child.text().as_string();
    }

    if (std::string(child.name()) == "core")
    {
      hwParams.cpuCore = child.text().as_int();
    }

    if (std::string(child.name()) == "crate")
    {
      hwParams.crate = child.text().as_int();
    }

    if (std::string(child.name()) == "zsmaster")
    {
      hwParams.zsmaster = child.text().as_int();
    }
  }

  return hwParams;
}

void CDaq::FindDevices()
{
  LOG_TRACE << "Entering CDaq::FindDevices";

  if (state_.getValue() != static_cast<int32_t>(DAQ_STATE_t::DETECTING_HARDWARE))
  {
    string err = "trying to find cards in a configured environment";
    throw CDaqException(err);
  }

  // Open XML hardware configuration file
  pugi::xml_document xmlHwSettingsFile;

  if (!xmlHwSettingsFile.load_file(hwConfigFilename_.c_str()))
  {
    throw CDaqException("could not open hardware configuration file");
  }

  if (xmlHwSettingsFile.child("Digitizer").empty() == 0)
  {
    std::string s = "no digitizer node found in hardware configuration file";
    throw CDaqException(s);
  }

  // Check if declared cards are presents in the system
  pugi::xml_node root = xmlDigitizerList_.append_child("listDaqElements");

  nbCards_ = 0;
  uint cardIndex = 0;
  XML_HW_PARAMS_t xmlHwParams;

  if (cmdLineParams_.simulation)
  {
    std::regex r("(ntofdaq-)(m[[:digit:]]+)");
    std::smatch match;

    for (int i = 0; i < cmdLineParams_.nbVirtualCards; ++i)
    {
      std::string sn = "VIRTUAL-MX-" + boost::lexical_cast<std::string>(i + 1);

      if (std::regex_search(hostName_, match, r) && match.length() >= 3)
      {
        if (match[2].length() > 0)
          sn = "VIRTUAL-" + match[2].str() + "-" +
            boost::lexical_cast<std::string>(i + 1);
      }

      boost::to_upper(sn);

      cards_.push_back(std::make_shared<virtualcard::CCardVirtual>(
        "VIRT", sn, 0, cardIndex, cmdLineParams_.nbVirtualCards, &state_,
        cmdLineParams_.simBufferSize, false));

      // Updating DIM service giving number of channels for each card
      pugi::xml_node nodeCard = root.append_child(
        (char *) std::string("card" +
                             boost::lexical_cast<std::string>(cardIndex))
          .c_str());

      pugi::xml_attribute attNbChannel = nodeCard.append_attribute("nbChannel");
      attNbChannel.set_value(cards_.back()->GetNbChannels());

      pugi::xml_attribute attType = nodeCard.append_attribute("type");
      attType.set_value(cards_.back()->GetType().c_str());

      pugi::xml_attribute attSN = nodeCard.append_attribute("serialNumber");
      attSN.set_value(cards_.back()->GetSerialNumber().c_str());

      cardIndex++;
    }
  }
  else
  {
    for (auto &digitizer :
         xmlHwSettingsFile.child("Devices").children("Digitizer"))
    {
      for (auto &model : digitizer.children())
      {
        xmlHwParams = GetXmlHardwareParams(model.children());

        try
        {
#ifdef SPDEVICES
          if (std::string(digitizer.attribute("type").value()) == "SPDevices")
          {
            if (std::string(model.name()) == "ADQ412")
            {
              cards_.push_back(std::make_shared<spdevices::CCardSpdAdq412>(
                xmlHwParams.sn, xmlHwParams.cpuCore, cardIndex,
                xmlHwParams.crate, &state_, cmdLineParams_.spdLogTrace));
            }
            else if (std::string(model.name()) == "ADQ14")
            {
              cards_.push_back(std::make_shared<spdevices::CCardSpdAdq14>(
                xmlHwParams.sn, xmlHwParams.cpuCore, cardIndex,
                xmlHwParams.crate, &state_, cmdLineParams_.spdLogTrace));
            }
            else
            {
              throw CDaqException("unknown SP Devices digitizer model");
            }
          }
          else
#endif
#ifdef ACQIRIS
            /* this is an else if when SPDEVICES is delcared */
            if (std::string(digitizer.attribute("type").value()) == "Acqiris")
          {
            if (std::string(model.name()) == "DC240")
            {
              cards_.push_back(
                std::make_shared<Agilent::Acqiris::CCardAcqirisDC240>(
                  xmlHwParams.sn, xmlHwParams.cpuCore, cardIndex,
                  xmlHwParams.crate, &state_));
            }
            else if (std::string(model.name()) == "DC270")
            {
              cards_.push_back(
                std::make_shared<Agilent::Acqiris::CCardAcqirisDC270>(
                  xmlHwParams.sn, xmlHwParams.cpuCore, cardIndex,
                  xmlHwParams.crate, &state_));
            }
            else if (std::string(model.name()) == "DC282")
            {
#ifndef DC282_8BIT_MODE
              cards_.push_back(
                std::make_shared<Agilent::Acqiris::CCardAcqirisDC282>(
                  xmlHwParams.sn, xmlHwParams.cpuCore, cardIndex,
                  xmlHwParams.crate, &state_));
#else
              cards_.push_back(new Agilent::Acqiris::CCardAcqirisDC270(
                xmlHwParams.sn, xmlHwParams.cpuCore, cardIndex,
                xmlHwParams.crate, logger_, state_));
#endif
            }
            else
            {
              throw CDaqException("unknown acqiris digitizer model");
            }
          }
          else
#endif
          {
            throw CDaqException("unknown digitizer type");
          }

          LOG_INFO << std::string(model.name()) +
              " found in hw config file with serial number " + xmlHwParams.sn;
          // configuring card with default values
          cards_.back()->configure();
        }
        catch (const std::bad_alloc &ex)
        {
          throw CDaqException(ex.what());
        }
        catch (const CCardEx &ex)
        {
          throw CDaqException(ex.what());
        }

        // Updating DIM service giving number of channels for each card
        pugi::xml_node nodeCard = root.append_child(
          (char *) std::string("card" +
                               boost::lexical_cast<std::string>(cardIndex))
            .c_str());

        pugi::xml_attribute attNbChannel = nodeCard.append_attribute(
          "nbChannel");
        attNbChannel.set_value(cards_.back()->GetNbChannels());

        pugi::xml_attribute attType = nodeCard.append_attribute("type");
        attType.set_value(cards_.back()->GetType().c_str());

        pugi::xml_attribute attSN = nodeCard.append_attribute("serialNumber");
        attSN.set_value(cards_.back()->GetSerialNumber().c_str());

        cardIndex++;
      }
    }
  }

  // creating DIM service to publish number of channels by module
  try
  {
    dimListDaqElements_.setData(xmlDigitizerList_);
  }
  catch (...)
  {
    std::string err = "could not create dimListDaqElements_ DIM service";
    throw CDaqException(err);
  }

  LOG_TRACE << "Exiting CDaq::FindDevices";
}

DAQ_STATE_t CDaq::GetState()
{
  return static_cast<DAQ_STATE_t>(state_.getValue());
}

std::vector<std::shared_ptr<CCard>> &CDaq::GetCardCollection()
{
  return cards_;
}

void CDaq::SetState(DAQ_STATE_t state)
{
  state_.setValue(static_cast<int32_t>(state));
}

void CDaq::SetStateError(std::string err)
{
  state_.addError(-1, err);
}

void CDaq::DeleteAll()
{
  LOG_TRACE << "Entering CDaq::DeleteAll";

  try
  {
    if (acquisitionRunning_)
    {
      acq_->Stop();
      writer_->Stop();
    }
  }
  catch (const CAcquisitionEx &ex)
  {
    throw CDaqException(ex.what());
  }
  catch (const CWriterEx &ex)
  {
    throw CDaqException(ex.what());
  }

  acquisitionRunning_ = false;
  cards_.clear();

  LOG_TRACE << "Exiting CDaq::DeleteAll";
}

void CDaq::ResetErrors()
{
  acquisitionRunning_ = false;
  StopAcq();
  writer_->Reset();
  acq_->Reset();
  state_.clearError();
}

bool CDaq::isCardCalibrationEnabled()
{
  return cardCalibrationEnabled_;
}

void CDaq::setCardCalibrationEnabled(bool cardCalibrationEnabled)
{
  cardCalibrationEnabled_ = cardCalibrationEnabled;
}

void CDaq::ProcessDimCmdParam(std::vector<std::string> &res,
                              std::string str,
                              std::string delimiter)
{
  // Clear the vector to be sure it contains only the results
  res.clear();

  size_t pos = 0;
  std::string token;
  while ((pos = str.find(delimiter)) != std::string::npos)
  {
    token = str.substr(0, pos);
    res.push_back(token);
    str.erase(0, pos + delimiter.length());
  }

  res.push_back(str);
}

void CDaq::commandReceived(DIMCmd &cmdData)
{
  BOOST_LOG_SCOPED_THREAD_ATTR(
    "Scope", boost::log::attributes::constant<std::string>("Command"));

  // If the client sends non-valid data, the DIMSuperCommand sends automatically
  // a error to the client
  pugi::xml_node parent = cmdData.getData();

  if (parent)
  {
    std::string cmd = parent.child("command").child_value();

    std::vector<std::string> cmdParameters;
    ProcessDimCmdParam(cmdParameters, cmd, "?");
    std::vector<std::string> params;

    if (cmdParameters.size() > 1)
    {
      ProcessDimCmdParam(params, cmdParameters[1], "&");
      // For each parameter separate the key and the value
      for (auto &p : params)
      {
        std::vector<std::string> extractedParam;
        ProcessDimCmdParam(extractedParam, p, "=");
        std::transform(extractedParam[0].begin(), extractedParam[0].end(),
                       extractedParam[0].begin(), ::tolower);
        if (extractedParam[0] == "runnumber")
        {
          runNumber_ = boost::lexical_cast<uint32_t>(extractedParam[1]);
        }
      }
    }

    cmd = cmdParameters[0];

    // process reset command
    if (cmd == "reset")
    {
      ProcessResetCommand(cmdData);
      return;
    }

    // process "super user" command, allowing to set the state to
    // WAITING_FOR_CMD thus bypassing the headers sent by the EACS
    if (cmd == "super_user")
    {
      ProcessSuperUserCommand(cmdData);
      return;
    }

    // process start command
    if (cmd == "start")
    {
      if (DAQ_STATE_t::WAITING_FOR_START_ACQ_CMD !=
            static_cast<DAQ_STATE_t>(state_.getValue()) ||
          acq_->GetState() != ACQUISITION_STATE_t::WAITING_TO_START)
      {
        const std::string errMsg = "start command rejected, DAQ is not in "
                                   "WAITING_FOR_START_ACQ_CMD state !";
        LOG_ERROR << errMsg;
        setError(cmdData.getKey(), 103, errMsg);
        return;
      }
      ProcessStartCommand(cmdData);
      return;
    }

    // Check if DAQ is in WAITING_FOR_CMD state to process the other commands
    if (DAQ_STATE_t::WAITING_FOR_CMD !=
        static_cast<DAQ_STATE_t>(state_.getValue()))
    {
      const std::string errMsg = "command rejected, DAQ is not in "
                                 "WAITING_FOR_CMD state !";
      LOG_ERROR << errMsg;
      setError(cmdData.getKey(), 103, errMsg);
      return;
    }

    state_.setValue(static_cast<int32_t>(DAQ_STATE_t::PROCESSING_CMD));

    // process initialize command
    if (cmd == "initialization")
    {
      LOG_INFO << "run number: " << runNumber_;
      ProcessInitializationCommand(cmdData);
    }
    else if (cmd == "stop")
    {
      ProcessStopCommand(cmdData);
    }
    else if (cmd == "calibrate")
    {
      ProcessCalibrateCommand(cmdData);
    }
    else if (cmd == "update")
    {
      ProcessUpdateCommand(cmdData);
    }
    else
    {
      // unknown command received
      const std::string errMsg = "Error command unknown !";
      LOG_ERROR << errMsg;
      setError(cmdData.getKey(), 101, errMsg);
    }

    if (static_cast<DAQ_STATE_t>(state_.getValue()) ==
        DAQ_STATE_t::PROCESSING_CMD)
    {
      // command rejected, but no errors triggered, let's go back idle
      state_.setValue(static_cast<int32_t>(DAQ_STATE_t::WAITING_FOR_CMD));
    }
  }
  else
  {
    const std::string errMsg = "could not process command, xml file incorrect "
                               "(parent node is "
                               "empty)";
    LOG_ERROR << errMsg;
    setError(cmdData.getKey(), 100, errMsg);
  }
}

void CDaq::ProcessUpdateCommand(DIMCmd &cmdData)
{
  LOG_TRACE << "update command received";

  try
  {
    StopAcq();
    SetConfigurationInvalid();
    state_.setValue(static_cast<int32_t>(DAQ_STATE_t::UNCONFIGURED));
  }
  catch (const CDaqException &ex)
  {
    const std::string errMsg("could not stop acquisition: " +
                             std::string(ex.what()));
    LOG_FATAL << errMsg;
    SetStateError(errMsg);
  }

  const std::string okMsg("update command executed");
  LOG_INFO << okMsg;
  setOk(cmdData.getKey(), okMsg);
}

void CDaq::ProcessResetCommand(DIMCmd &cmdData)
{
  LOG_TRACE << "reset command received";

  if (DAQ_STATE_t::DETECTING_HARDWARE ==
      static_cast<DAQ_STATE_t>(state_.getValue()))
  {
    const std::string errMsg(
      "reset command rejected, DAQ is in DETECTING_HARDWARE state");
    LOG_ERROR << errMsg;
    setError(cmdData.getKey(), 101, errMsg);
    return;
  }

  state_.setValue(static_cast<int32_t>(DAQ_STATE_t::RESETING_DAQ));

  const std::string okMsg("reset command accepted");
  LOG_INFO << okMsg;
  setOk(cmdData.getKey(), okMsg);
}

void CDaq::ProcessSuperUserCommand(DIMCmd &cmdData)
{
  LOG_INFO << "super user command received, going to Super user";
  state_.setValue(static_cast<int32_t>(DAQ_STATE_t::SUPER_USER));
  setOk(cmdData.getKey(), "super_user command accepted");
}

void CDaq::ProcessInitializationCommand(DIMCmd &cmdData)
{
  LOG_TRACE << "initialization command received";

  if (acquisitionRunning_)
  {
    const std::string errMsg(
      "initialization command rejected, acquisition is already running");
    LOG_ERROR << errMsg;
    setError(cmdData.getKey(), 105, errMsg);
    return;
  }

  state_.setValue(static_cast<int32_t>(DAQ_STATE_t::INITIALIZING_ACQ));

  const std::string okMsg("initialization command accepted");
  LOG_INFO << okMsg;
  setOk(cmdData.getKey(), okMsg);
}

void CDaq::ProcessStartCommand(DIMCmd &cmdData)
{
  LOG_TRACE << "start command received";

  if (acquisitionRunning_)
  {
    const std::string errMsg(
      "start command rejected, acquisition is already running");
    LOG_ERROR << errMsg;
    setError(cmdData.getKey(), 105, errMsg);
    return;
  }

  state_.setValue(static_cast<int32_t>(DAQ_STATE_t::STARTING_ACQ));

  const std::string okMsg("start command accepted");
  LOG_INFO << okMsg;
  setOk(cmdData.getKey(), okMsg);
}

void CDaq::ProcessStopCommand(DIMCmd &cmdData)
{
  LOG_TRACE << "stop command received";

  if (!acquisitionRunning_)
  {
    const std::string errMsg(
      "stop command rejected, acquisition is already stopped");
    LOG_ERROR << errMsg;
    setError(cmdData.getKey(), 107, errMsg);
    return;
  }

  state_.setValue(static_cast<int32_t>(DAQ_STATE_t::STOPPING_ACQ));

  const std::string okMsg("stop command accepted");
  LOG_INFO << okMsg;
  setOk(cmdData.getKey(), okMsg);
}

void CDaq::ProcessCalibrateCommand(DIMCmd &cmdData)
{
  LOG_TRACE << "calibration command received";

  if (true == acquisitionRunning_)
  {
    const std::string errMsg(
      "calibration command rejected, acquisition is already running");
    LOG_ERROR << errMsg;
    setError(cmdData.getKey(), 105, errMsg);
    return;
  }

  state_.setValue(static_cast<int32_t>(DAQ_STATE_t::CALIBRATING_CARDS));

  const std::string okMsg("calibration command accepted");
  LOG_INFO << okMsg;
  setOk(cmdData.getKey(), okMsg);
}
