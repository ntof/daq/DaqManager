/*
 * CDIMZeroSuppressionParameters.cpp
 *
 *  Created on: Jun 6, 2016
 *      Author: pperonna
 */

#include "CDIMZeroSuppressionParameters.h"

#include <boost/lexical_cast.hpp>

#include <DIMCmd.h>
#include <NTOFLogging.hpp>

#include "logger.h"

namespace ntof {

/* incoming values can be partial, this is used to rebuild it */
static const dim::DIMData masterInfo(0, "master", "", 0);
static const dim::DIMData snInfo(0, "sn", "", 0);
static const dim::DIMData channelInfo(1, "channel", "", 0);
static const dim::DIMData slaveInfo(0, "slave", "", 0);

CDIMZeroSuppressionParameters::CDIMZeroSuppressionParameters(
  const std::string &serviceName) :
  dim::DIMParamList(serviceName),
  zsMode_(ZeroSuppression::zsMode::INDEPENDENT)
{
  dim::DIMEnum mode;
  mode.addItem(ZeroSuppression::zsMode::INDEPENDENT, "independent");
  mode.addItem(ZeroSuppression::zsMode::SINGLE_MASTER, "singlemaster");
  mode.addItem(ZeroSuppression::zsMode::MULTIPLE_MASTER, "master");
  addParameter(ParamIdx::MODE, "mode", "", std::move(mode),
               dim::AddMode::CREATE, false);
  addParameter(ParamIdx::CONFIGURATION, "configuration", "",
               dim::DIMData::List(), dim::AddMode::CREATE, false);

  setHandler(this);
  updateService();
}

void CDIMZeroSuppressionParameters::setXmlParameters(const pugi::xml_node &zsp)
{
  pugi::xml_document doc;
  doc.append_copy(zsp).set_name("parameters");
  dim::DIMCmd cmd(0, doc);
  commandReceived(cmd);
}

int CDIMZeroSuppressionParameters::parameterChanged(
  dim::DIMData::List &settingsChanged,
  const dim::DIMParamList & /*list*/,
  int & /*errCode*/,
  std::string & /*errMsg*/)
{
  ZeroSuppression::zsMode mode = GetMode();
  MasterList masterList;
  dim::DIMData::List configData;

  for (dim::DIMData &data : settingsChanged)
  {
    if (data.getIndex() == ParamIdx::MODE)
    {
      mode = static_cast<ZeroSuppression::zsMode>(
        data.getValue<dim::DIMEnum>().getValue());
    }
    else if (data.getIndex() == ParamIdx::CONFIGURATION)
    {
      configData = data.getNestedValue();
      for (dim::DIMData &masterData : configData)
        masterList.push_back(std::move(getMaster(masterData)));
    }
  }

  switch (mode)
  {
  case ZeroSuppression::zsMode::INDEPENDENT:
    if (masterList.size() != 0)
      throw dim::DIMException("can't set masters in independent mode", __LINE__);
    break;
  case ZeroSuppression::zsMode::SINGLE_MASTER:
    if (masterList.size() != 1)
      throw dim::DIMException(
        "a single master must be set in singlemaster mode", __LINE__);
    else if (masterList[0].slaveList.size() != 0)
      throw dim::DIMException("slave list must be empty in singlemaster mode",
                              __LINE__);
    break;
  case ZeroSuppression::zsMode::MULTIPLE_MASTER: break;
  default:
    throw dim::DIMException("invalid mode: " + std::to_string(mode), __LINE__);
  }

  /* directly pre-replace content */
  setValue(ParamIdx::CONFIGURATION, std::move(configData), false);

  std::lock_guard<std::mutex> lock(mutex_);
  zsMasterList_.swap(masterList);
  zsMode_ = mode;

  return 0;
}

ZeroSuppression::zsMaster CDIMZeroSuppressionParameters::getMaster(
  dim::DIMData &master) const
{
  ZeroSuppression::zsMaster ret;
  ret.channel = -1;

  for (dim::DIMData &data : master.getNestedValue())
  {
    if (data.getIndex() == ChannelParamIdx::SN)
    {
      ret.masterSn = data.getValue<std::string>();
      data.copyParameterInfo(snInfo);
    }
    else if (data.getIndex() == ChannelParamIdx::CHANNEL)
    {
      ret.channel = data.getValue<uint32_t>();
      data.copyParameterInfo(channelInfo);
    }
    else
    {
      ZeroSuppression::zsSlave slave;
      slave.index = -1;
      data.copyParameterInfo(slaveInfo);

      for (dim::DIMData &slaveData : data.getNestedValue())
      {
        if (slaveData.getIndex() == 0)
        {
          slave.sn = slaveData.getValue<std::string>();
          slaveData.copyParameterInfo(snInfo);
        }
        else if (slaveData.getIndex() == 1)
        {
          slave.index = slaveData.getValue<uint32_t>();
          slaveData.copyParameterInfo(channelInfo);
        }
      }

      if (slave.sn.empty() || (slave.index < 0))
        throw dim::DIMException("invalid channel on master: " + ret.masterSn +
                                  std::to_string(slave.index),
                                __LINE__);
      else if (slave.index == ret.channel && slave.sn == ret.masterSn)
        throw dim::DIMException("channel " + ret.masterSn + ":" +
                                  std::to_string(ret.channel) +
                                  " alread set as master",
                                __LINE__);

      ret.slaveList.push_back(std::move(slave));
    }
  }
  if (ret.masterSn.empty() || ret.channel < 0)
    throw dim::DIMException("invalid master channel: " + ret.masterSn + ":" +
                              std::to_string(ret.channel),

                            __LINE__);

  master.copyParameterInfo(masterInfo);
  return ret;
}

ntof::ZeroSuppression::zsMode CDIMZeroSuppressionParameters::GetMode() const
{
  std::lock_guard<std::mutex> lock(mutex_);
  return zsMode_;
}

CDIMZeroSuppressionParameters::MasterList CDIMZeroSuppressionParameters::
  GetMasterList() const
{
  std::lock_guard<std::mutex> lock(mutex_);
  return zsMasterList_;
}

void CDIMZeroSuppressionParameters::updateService()
{
  dim::DIMData::List masterList;
  ZeroSuppression::zsMode mode;
  {
    std::lock_guard<std::mutex> lock(mutex_);
    mode = zsMode_;

    for (MasterList::const_iterator masterIt = zsMasterList_.begin();
         masterIt != zsMasterList_.end(); ++masterIt)
    {
      dim::DIMData::List masterData;
      masterData.emplace_back(0, "sn", "", masterIt->masterSn);
      masterData.emplace_back(1, "channel", "", int32_t(masterIt->channel));

      if (zsMode_ == ZeroSuppression::zsMode::MULTIPLE_MASTER)
      {
        for (const ZeroSuppression::zsSlave &slave : masterIt->slaveList)
        {
          dim::DIMData::List slaveData;
          slaveData.emplace_back(0, "sn", "", slave.sn);
          slaveData.emplace_back(1, "channel", "", int32_t(slave.index));
          masterData.emplace_back(masterData.size(), "slave", "",
                                  std::move(slaveData));
        }
      }

      masterList.emplace_back(masterList.size(), "master", "",
                              std::move(masterData));
    }
  }

  lockParameterAt(ParamIdx::MODE)->getEnumValue().setValue(mode);
  setValue(ParamIdx::CONFIGURATION, std::move(masterList), true);
}

} // namespace ntof
