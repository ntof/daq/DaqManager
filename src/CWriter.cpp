#include "CWriter.h"

#include <fstream>

#include <boost/chrono/chrono.hpp>
#include <boost/filesystem.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>

#include <fcntl.h>
#include <linux/fs.h>
#include <numa.h>
#include <sched.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>

#include "CSystemInfo.h"
#include "NTOFException.h"
#include "PerformanceCounter.h"
#include "logger.h"

#include "dis.hxx"

using namespace boost::interprocess;
using namespace ntof;
using ntof::utils::Flock;

CWriter::CWriter(std::string hwConfigFilename,
                 std::string hostName,
                 CBoundedCircularBuffer<CAcquisitionParameters> &acqQueue,
                 std::vector<std::shared_ptr<CCard>> &cards,
                 ntof::dim::DIMState *daqState,
                 CMD_LINE_PARAM_t cmdLineParams) :
  hwConfigFilename_(hwConfigFilename),
  state_(hostName + "/WriterState"),
  acqQueue_(acqQueue),
  cards_(cards),
  hostName_(hostName),
  daqState_(daqState),
  timing_("Timing"),
  dimRunExtension_(hostName + "/WRITER/RunExtensionNumber"),
  cmdLineParams_(cmdLineParams),
  zeroSuppressionParameters_(hostName + "/ZeroSuppression")
{
  // Initialize DIM service for state
  try
  {
    state_.addStateValue(static_cast<int32_t>(WRITER_STATE_t::IDLE), "Idle");
    state_.addStateValue(static_cast<int32_t>(WRITER_STATE_t::INIT),
                         "Initialization");
    state_.addStateValue(static_cast<int32_t>(WRITER_STATE_t::MEMORY_ALLOCATION),
                         "Allocating memory");
    state_.addStateValue(static_cast<int32_t>(WRITER_STATE_t::WAITING_QUEUE),
                         "Waiting for data");
    state_.addStateValue(static_cast<int32_t>(WRITER_STATE_t::PROCESSING_DATA),
                         "Processing data");
    state_.addStateValue(static_cast<int32_t>(WRITER_STATE_t::WRITING_DATA),
                         "Writing data to disk");
    state_.addStateValue(static_cast<int32_t>(WRITER_STATE_t::FREEING_BUFFER),
                         "Freeing buffers");
    state_.addStateValue(static_cast<int32_t>(WRITER_STATE_t::STOPPING),
                         "Stopping thread");
    state_.setValue(static_cast<int32_t>(WRITER_STATE_t::IDLE));
  }
  catch (const ntof::dim::DIMException &ex)
  {
    throw CWriterEx(ex.what());
  }
  catch (const std::bad_alloc &ex)
  {
    throw CWriterEx(ex.what());
  }

  // dim services
  try
  {
    pugi::xml_document doc;
    publishWroteNumber(-1, -1, 0, doc);
    dimRunExtension_.setData(doc);
  }
  catch (...)
  {
    throw CWriterEx("could not create module header object");
  }

  acqHeader_ = nullptr;
  streamNumber_ = 0;
  stop_ = false;
  configurationValid_ = false;
  zsMode_ = ZeroSuppression::zsMode::INDEPENDENT;
}

void CWriter::setLockFile(const std::string &lockFile)
{
  if (lockFile.empty())
    lockFile_.reset();
  else
    lockFile_.reset(new Flock(Flock::Shared, lockFile));
}

ntof::dim::DIMState *CWriter::GetState()
{
  return &state_;
}

void CWriter::SetConfigurationInvalid()
{
  configurationValid_ = false;
}

CWriter::~CWriter() {}

void CWriter::Start(uint32_t runNumber)
{
  LOG_TRACE << "Entering CWriter::Start";

  stop_ = false;

  // reset published run extension number
  {
    pugi::xml_document doc;
    publishWroteNumber(runNumber, -1, 0, doc);
    dimRunExtension_.setData(doc);
  }

  WriteRunToDisk(CreateRunDir(runNumber), runNumber);

  thread_ = boost::thread(boost::bind(&CWriter::Writer, this));

  LOG_TRACE << "Exiting CWriter::Start";
}

void CWriter::Reset()
{
  state_.clearError();
  state_.clearWarnings();
  state_.setValue(static_cast<int32_t>(WRITER_STATE_t::IDLE));
}

void CWriter::Stop()
{
  LOG_TRACE << "Entering CWriter::Stop";

  if (state_.getValue() != static_cast<int32_t>(WRITER_STATE_t::ERROR))
    thread_.join();

  LOG_TRACE << "Exiting CWriter::Stop";
}

void CWriter::ForceStop()
{
  LOG_TRACE << "Entering CWriter::ForceStop";

  stop_ = true;
  //#warning "to be commented"
  // pthread_kill(thread_.native_handle(), -9);
  //#warning "to be uncommented"
  thread_.interrupt();
  thread_.join();
  state_.setValue(static_cast<int32_t>(WRITER_STATE_t::IDLE));

  LOG_TRACE << "Exiting CWriter::ForceStop";
}

void CWriter::WriterThreadInit()
{
  // zero suppression parameters, GUI should have already sent the parameters
  // through DIM
  zsMode_ = ZeroSuppression::zsMode::INDEPENDENT;

  // initialize all channels as independent
  for (auto &card : cards_)
  {
    card->SetAsMaster(false);
    for (auto &ch : card->GetChannelCollection())
    {
      ch->SetAsMaster(false);
      ch->SetAsIndependent(true);
    }
  }

  zsMasterList_.clear();

  zsMode_ = zeroSuppressionParameters_.GetMode();
  zsMasterList_ = zeroSuppressionParameters_.GetMasterList();

  // associate serial numbers to card indexes
  for (auto &master : zsMasterList_)
  {
    bool masterExists = false;
    for (auto &card : cards_)
    {
      if (card->GetSerialNumber() == master.masterSn)
      {
        if (master.channel >= card->GetChannelCollection().size())
          throw CWriterEx("zs master channel does not exist");

        card->SetAsMaster(true);
        card->GetChannel(master.channel)->SetAsMaster(true);
        master.cardIndex = card->GetCardIndex();
        masterExists = true;
        break;
      }
    }

    if (!masterExists)
    {
      throw CWriterEx(master.masterSn + "master not found in the card list");
    }

    LOG_INFO << "zs master = " << master.masterSn
             << ", channel = " << master.channel;

    if (zsMode_ == ZeroSuppression::zsMode::MULTIPLE_MASTER)
    {
      for (auto &ch : master.slaveList)
      {
        bool channelExists = false;
        for (auto &card : cards_)
        {
          if (card->GetSerialNumber() == ch.sn)
          {
            if (ch.index >= card->GetChannelCollection().size())
              throw CWriterEx("zs slave channel does not exist");

            ch.cardIndex = card->GetCardIndex();
            card->GetChannel(ch.index)->SetAsIndependent(false);
            channelExists = true;
            break;
          }
        }

        if (!channelExists)
        {
          throw CWriterEx("zs slave channel not found in the card list");
        }

        LOG_INFO << "\t-> slave = " << ch.sn << ", channel = " << ch.index;
      }
    }
  }

  // creating offline parsing/zero suppression threads
  // clear vectors
  cardPoolIndex_.clear();
  cardEnabled_.clear();

  // reserve space for vectors
  cardPoolIndex_.reserve(cards_.size());
  cardEnabled_.reserve(cards_.size());

  for (auto &card : cards_)
  {
    try
    {
      cardPoolIndex_.push_back(std::make_shared<CBoundedCircularBuffer<int>>(1));
    }
    catch (const std::bad_alloc &ex)
    {
      std::string err = "card #" +
        boost::lexical_cast<std::string>(card->GetCardIndex()) + " (" +
        card->GetSerialNumber() + ")" +
        " could not allocate memory for acqBufferQueue_";
      state_.setError(-1, err);
      break;
    }

    cardEnabled_.push_back(0);

    if (card->IsCardEnabled() == false)
    {
      LOG_TRACE << "writer thread: card #" << card->GetCardIndex() << " ("
                << card->GetSerialNumber() << ")"
                << " is disabled";
      continue;
    }

    cardEnabled_.back() = 1;
    LOG_TRACE << "writer thread: card #" << card->GetCardIndex() << " ("
              << card->GetSerialNumber() << ")"
              << " is enabled";

    parsingZsThreads_.create_thread(
      boost::bind(&CWriter::ParsingZsThread, this, card->GetCardIndex()));
  }
}

void CWriter::MemoryAllocation()
{
  LOG_TRACE << "Entering CWriter::MemoryAllocation";

  fileBuffer_ = static_cast<int8_t *>(
    _mm_malloc(cmdLineParams_.writerFileBufferSize * sizeof(*fileBuffer_),
               cmdLineParams_.sectorSize));

  memset(fileBuffer_, 0,
         cmdLineParams_.writerFileBufferSize * sizeof(*fileBuffer_));

  if (fileBuffer_ == nullptr)
  {
    std::string e = "could not allocate memory for writer";
    throw CWriterEx(e);
  }

  LOG_TRACE << "Exiting CWriter::MemoryAllocation";
}

void CWriter::Writer()
{
  BOOST_LOG_SCOPED_THREAD_ATTR(
    "Scope", boost::log::attributes::constant<std::string>("Writer"));

  LOG_TRACE << "Entering CWriter::Writer";

  PinThreadToCpu(14);
  PinThreadToNode(14);

  // setting low priority for this thread
  struct sched_param p;
  p.sched_priority = 0;
  if (pthread_setschedparam(pthread_self(), SCHED_OTHER, &p) != 0)
  {
    std::string err = "could not set priority for writer thread";
    LOG_ERROR << err;
  }

  pthread_setname_np(pthread_self(), "Writer");

  CAcquisitionParameters acqParams;

  state_.clearError();
  state_.clearWarnings();
  state_.setValue(static_cast<int32_t>(WRITER_STATE_t::INIT));

  std::shared_ptr<Flock> lockFile(lockFile_);
  std::unique_lock<Flock> fileLocker;
  if (lockFile)
    fileLocker = std::unique_lock<Flock>(*lockFile);

  while (!stop_)
  {
    try
    {
      switch (static_cast<WRITER_STATE_t>(state_.getValue()))
      {
      case WRITER_STATE_t::IDLE: break;

      case WRITER_STATE_t::INIT:
        LOG_TRACE << "state = INIT";
        try
        {
          WriterThreadInit();
        }
        catch (const CWriterEx &ex)
        {
          LOG_ERROR << ex.what();
          state_.setError(-1, ex.what());
          break;
        }
        state_.setValue(static_cast<int32_t>(WRITER_STATE_t::MEMORY_ALLOCATION));
        break;

      case WRITER_STATE_t::MEMORY_ALLOCATION:
        LOG_TRACE << "state = MEMORY_ALLOCATION";
        try
        {
          MemoryAllocation();
        }
        catch (const CWriterEx &ex)
        {
          std::string err = "could not allocate memory for file buffer";
          LOG_FATAL << "!!! " + err;
          state_.setError(-1, err);
          break;
        }
        state_.setValue(static_cast<int32_t>(WRITER_STATE_t::WAITING_QUEUE));
        break;

      case WRITER_STATE_t::WAITING_QUEUE:
        LOG_TRACE << "state = WAITING_QUEUE";

        acqQueue_.pop(&acqParams);
        if (acqParams.GetStop())
        {
          stop_ = true;
          break;
        }

        LOG_TRACE << "pool index: " +
            boost::lexical_cast<std::string>(acqParams.GetBufferPoolIndex());

        state_.setValue(static_cast<int32_t>(WRITER_STATE_t::PROCESSING_DATA));
        break;

      case WRITER_STATE_t::PROCESSING_DATA:
      {
        LOG_TRACE << "state = PROCESSING_DATA";

        if (CSystemInfo::GetDiskUsedSpace() > 95.0)
        {
          std::string err = "Used space on disk is over 95%";
          LOG_ERROR << "!!! " + err;
          state_.setError(-1, err);
          break;
        }

        // queuing acquisition buffers for zero suppression/offline parsing
        // threads
        for (auto &card : cards_)
        {
          card->GetBuffer(acqParams.GetBufferPoolIndex())->parsingZsStatus_ = 0;

          if (cardEnabled_[card->GetCardIndex()] == false)
            continue;

          cardPoolIndex_[card->GetCardIndex()]->push(
            acqParams.GetBufferPoolIndex());
        }

        try
        {
          switch (zsMode_)
          {
          case ZeroSuppression::zsMode::INDEPENDENT:
            ZsIndependent(acqParams);
            break;

          case ZeroSuppression::zsMode::SINGLE_MASTER:
            ZsSingleMaster(acqParams);
            break;

          case ZeroSuppression::zsMode::MULTIPLE_MASTER:
            ZsMultipleMaster(acqParams);
            break;

          default: break;
          }
        }
        catch (const CWriterEx &ex)
        {
          std::string err = "could not perform zero suppression: " +
            std::string(ex.what());
          LOG_FATAL << "!!! " + err;
          state_.setError(-1, err);
          break;
        }

        if (static_cast<WRITER_STATE_t>(state_.getValue()) ==
            WRITER_STATE_t::ERROR)
          break;

        // check if event number was updated
        if (!cmdLineParams_.standAlone)
        {
          LOG_TRACE << "event number from acquisition: "
                    << acqParams.GetEventNumber();
        }
        state_.setValue(static_cast<int32_t>(WRITER_STATE_t::WRITING_DATA));
        break;
      }

      case WRITER_STATE_t::WRITING_DATA:
        LOG_TRACE << "state = WRITING_DATA";
        if (CSystemInfo::GetDiskUsedSpace() > 95.0)
        {
          std::string err = "Used space on disk is over 95%";
          LOG_ERROR << "!!! " + err;
          state_.setError(-1, err);
          break;
        }

#ifdef CHARACTERIZATION
        if (cmdLineParams_.writeCardRawData)
        {
          static int fileIndex = 1;
          for (auto &card : cards_)
          {
            for (auto &ch : card->GetChannelCollection())
            {
              int flag = cmdLineParams_.sampleSize == 0 ?
                O_WRONLY | O_CREAT | O_APPEND :
                O_WRONLY | O_CREAT;

              std::string fileName = "/DAQ/data/" + card->GetSerialNumber() +
                "_ch" + boost::lexical_cast<std::string>(ch->GetIndex());

              fileName += cmdLineParams_.sampleSize == 0 ?
                ".raw" :
                "_" + boost::lexical_cast<std::string>(fileIndex) + ".raw";

              int fd = open(fileName.c_str(), flag, 0777);

              if (fd == -1)
              {
                LOG_ERROR << "could not create file for raw data: " +
                    std::string(strerror(errno));
                return;
              }

              int16_t *buffer = card->GetBuffer(acqParams.GetBufferPoolIndex())
                                  ->buffer_[ch->GetIndex()];
              ssize_t s = write(fd, buffer, card->GetChannelBufferSize());
              if (s != card->GetChannelBufferSize())
              {
                LOG_ERROR << "could not write shadow file, byte written: " +
                    boost::lexical_cast<std::string>(s);
              }

              fsync(fd);
              close(fd);
            }
          }
          fileIndex++;
        }
#else
        try
        {
          const std::string runDirectory = CreateRunDir(
            acqParams.GetRunNumber());
          WriteDataToDisk(runDirectory, acqParams, fileBufferSize_);
          WriteEventToDisk(runDirectory, acqParams.GetEventHeader());
        }
        catch (const CWriterEx &ex)
        {
          state_.setError(-1, ex.what());
          break;
        }

        // publish run extension number
        LOG_TRACE << "publishing event number " << acqParams.GetEventNumber();
        {
          pugi::xml_document xmlRunExtensionNumber;
          publishWroteNumber(
            acqParams.GetRunNumber(), acqParams.GetEventNumber(),
            skipHeader_.end - skipHeader_.start, xmlRunExtensionNumber);
          dimRunExtension_.setData(xmlRunExtensionNumber);
        }
#endif

        state_.setValue(static_cast<int32_t>(WRITER_STATE_t::FREEING_BUFFER));
        break;

      case WRITER_STATE_t::FREEING_BUFFER:
        LOG_TRACE << "state = FREEING_BUFFER";

        for (auto &card : cards_)
        {
          card->GetBuffer(acqParams.GetBufferPoolIndex())->used_ = false;
        }

        state_.setValue(static_cast<int32_t>(WRITER_STATE_t::WAITING_QUEUE));
        break;

      case WRITER_STATE_t::ERROR:
        LOG_TRACE << "state = ERROR";
        if (acqQueue_.pop().GetStop())
        {
          WriterThreadStop(true);
          LOG_TRACE << "Exiting CWriter::Writer";
          return;
        }
        boost::this_thread::sleep(boost::posix_time::seconds(2));
        break;

      default:
        state_.setValue(static_cast<int32_t>(WRITER_STATE_t::ERROR));
        break;
      }
      boost::this_thread::sleep(boost::posix_time::microseconds(1));
    }
    catch (boost::thread_interrupted const &)
    {
      break;
    }
  }

  WriterThreadStop(false);

  LOG_TRACE << "Exiting CWriter::Writer";
}

void CWriter::ZsMultipleMaster(CAcquisitionParameters &acqParams)
{
  uint nbProcessedCards = 0;
  fileBufferSize_ = 0;

  while (nbProcessedCards != cards_.size())
  {
    boost::this_thread::interruption_point();

    nbProcessedCards = 0;
    for (auto &card : cards_)
    {
      if (!cardEnabled_[card->GetCardIndex()])
      {
        nbProcessedCards++;
        continue;
      }

      // error reported by the thread
      if (card->GetBuffer(acqParams.GetBufferPoolIndex())->parsingZsStatus_ ==
          -1)
      {
        std::ostringstream err;
        err << "card #" << card->GetCardIndex() << " ("
            << card->GetSerialNumber() << ")"
            << " did not complete zero suppression or offline parsing";
        LOG_ERROR << "!!! " << err.str();
        state_.setError(-1, err.str());
        return;
      }

      // no error reported by the thread
      if (card->GetBuffer(acqParams.GetBufferPoolIndex())->parsingZsStatus_ == 1)
        nbProcessedCards++;
    }

    if (static_cast<WRITER_STATE_t>(state_.getValue()) == WRITER_STATE_t::ERROR)
      return;

    boost::this_thread::sleep(boost::posix_time::microseconds(100));
  }

  // compute file size with the difference of 2 pointers
  fileBufferSize_ = FillFileBuffer(acqParams, fileBuffer_ + sizeof(skipHeader_),
                                   -1) -
    fileBuffer_;
}

void CWriter::ZsSingleMaster(CAcquisitionParameters &acqParams)
{
  uint nbProcessedCards = 0;
  fileBufferSize_ = 0;
  int8_t *cardFileBuffer = fileBuffer_ + sizeof(skipHeader_);

  while (nbProcessedCards != cards_.size())
  {
    boost::this_thread::interruption_point();

    nbProcessedCards = 0;
    for (auto &card : cards_)
    {
      if (!cardEnabled_[card->GetCardIndex()])
      {
        nbProcessedCards++;
        continue;
      }

      // error reported by the thread
      if (card->GetBuffer(acqParams.GetBufferPoolIndex())->parsingZsStatus_ ==
          -1)
      {
        std::ostringstream err;
        err << "card #" << card->GetCardIndex() << " ("
            << card->GetSerialNumber() << ")"
            << " did not complete zero suppression or offline parsing";
        LOG_ERROR << "!!! " << err.str();
        state_.setError(-1, err.str());
        return;
      }

      // no error reported by the thread
      if (card->GetBuffer(acqParams.GetBufferPoolIndex())->parsingZsStatus_ == 1)
        nbProcessedCards++;
    }

    if (static_cast<WRITER_STATE_t>(state_.getValue()) == WRITER_STATE_t::ERROR)
      return;

    boost::this_thread::sleep(boost::posix_time::microseconds(100));
  }

  for (auto &card : cards_)
  {
    cardFileBuffer = FillFileBuffer(acqParams, cardFileBuffer,
                                    card->GetCardIndex());
  }

  // compute file size with the difference of 2 pointers
  fileBufferSize_ = cardFileBuffer - fileBuffer_;
}

void CWriter::ZsIndependent(CAcquisitionParameters &acqParams)
{
  uint nbProcessedCards = 0;
  fileBufferSize_ = 0;
  int8_t *cardFileBuffer = fileBuffer_ + sizeof(skipHeader_);

  // checking if offline parsing and zero suppression went fine for each card
  while (nbProcessedCards != cards_.size())
  {
    boost::this_thread::interruption_point();

    nbProcessedCards = 0;
    for (auto &card : cards_)
    {
      if (!cardEnabled_[card->GetCardIndex()])
      {
        nbProcessedCards++;
        continue;
      }

      // error reported by the thread
      if (card->GetBuffer(acqParams.GetBufferPoolIndex())->parsingZsStatus_ ==
          -1)
      {
        std::ostringstream err;
        err << "card #" << card->GetCardIndex() << " ("
            << card->GetSerialNumber() << ")"
            << " did not complete zero suppression or offline parsing";
        LOG_ERROR << "!!! " << err.str();
        state_.setError(-1, err.str());
        return;
      }

      // no error reported by the thread, writing data
      if (card->GetBuffer(acqParams.GetBufferPoolIndex())->parsingZsStatus_ == 1)
      {
        try
        {
          cardFileBuffer = FillFileBuffer(acqParams, cardFileBuffer,
                                          card->GetCardIndex());
        }
        catch (const CWriterEx &ex)
        {
          LOG_ERROR << "!!! " + std::string(ex.what());
          state_.setError(-1, ex.what());
          return;
        }
        card->GetBuffer(acqParams.GetBufferPoolIndex())->parsingZsStatus_ = 2;
      }

      if (card->GetBuffer(acqParams.GetBufferPoolIndex())->parsingZsStatus_ == 2)
        nbProcessedCards++;
    }

    if (static_cast<WRITER_STATE_t>(state_.getValue()) == WRITER_STATE_t::ERROR)
      return;

    boost::this_thread::sleep(boost::posix_time::microseconds(100));
  }

  // compute file size with the difference of 2 pointers
  fileBufferSize_ = cardFileBuffer - fileBuffer_;
}

int8_t *CWriter::FillFileBuffer(CAcquisitionParameters &acqParams,
                                int8_t *cardFileBuffer,
                                int cardIndex)
{
  if (cardIndex == -1)
  {
    LOG_DEBUG << "entering FillFileBuffer, multiple master mode";
  }
  else
    LOG_DEBUG << "entering FillFileBuffer for card #" << cardIndex << ", "
              << cards_[cardIndex]->GetSerialNumber();

  int8_t *chBuffer = cardFileBuffer;

  if (zsMode_ == ZeroSuppression::zsMode::MULTIPLE_MASTER)
  {
    if (zsMasterList_.size() == 0)
      throw CWriterEx("master list is empty");

    for (auto &master : zsMasterList_)
    {
      ntof::CChannel *masterChannel =
        cards_[master.cardIndex]->GetChannel(master.channel).get();

      // write pulses of the master
      {
        chBuffer += sizeof(acqcHeader_);

        acqcHeader_.detectorType =
          masterChannel->getConfiguration()->header.detectorType;
        acqcHeader_.detectorId =
          masterChannel->getConfiguration()->header.detectorId;
        acqcHeader_.str_crate_mod_ch =
          masterChannel->getConfiguration()->header.str_mod_crate;

        for (auto &pulse :
             masterChannel->GetZeroSuppression().GetPulseDataCollection())
        {
          if (pulse.length == 0)
            continue;

          // timestamp
          memcpy(chBuffer, &pulse.timeStamp, sizeof(pulse.timeStamp));
          chBuffer += sizeof(pulse.timeStamp);

          // length of pulse
          memcpy(chBuffer, &pulse.length, sizeof(pulse.length));
          chBuffer += sizeof(pulse.length);

          // pulse data
          memcpy(chBuffer, pulse.data, pulse.length * sizeof(*pulse.data));
          chBuffer += pulse.length * sizeof(*pulse.data);
        }

        // update ACQC header length
        acqcHeader_.length = static_cast<uint32_t>(
          chBuffer - cardFileBuffer - sizeof(acqcHeader_) + 3 * sizeof(int32_t));
        while (((chBuffer - cardFileBuffer - sizeof(acqcHeader_)) % 4) != 0)
        {
          *chBuffer = 0;
          chBuffer++;
          acqcHeader_.length++;
        }

        acqcHeader_.length = static_cast<uint32_t>(acqcHeader_.length /
                                                   sizeof(int32_t));

        memcpy(cardFileBuffer, &acqcHeader_, sizeof(acqcHeader_));

        cardFileBuffer = chBuffer;
      }

      // write pulses of the slaves
      for (auto &slave : master.slaveList)
      {
        ntof::CChannel *ch =
          cards_[slave.cardIndex]->GetChannel(slave.index).get();

        if (ch->getConfiguration()->enabled == false)
          continue;

        chBuffer += sizeof(acqcHeader_);

        acqcHeader_.detectorType = ch->getConfiguration()->header.detectorType;
        acqcHeader_.detectorId = ch->getConfiguration()->header.detectorId;
        acqcHeader_.str_crate_mod_ch =
          ch->getConfiguration()->header.str_mod_crate;

        AcquisitionBuffer *acqBuffer = cards_[slave.cardIndex]
                                         ->GetBuffer(
                                           acqParams.GetBufferPoolIndex())
                                         .get();

        ch->GetZeroSuppression().SingleMaster(
          acqBuffer->buffer_[ch->GetIndex()],
          ch->getConfiguration()->header.sampleSize * 1000,
          masterChannel->GetZeroSuppression().GetPulseDataCollection(),
          ch->getConfiguration()->header,
          masterChannel->getConfiguration()->header);

        for (auto &pulse : ch->GetZeroSuppression().GetPulseDataCollection())
        {
          // timestamp
          memcpy(chBuffer, &pulse.timeStamp, sizeof(pulse.timeStamp));
          chBuffer += sizeof(pulse.timeStamp);

          // length of pulse
          memcpy(chBuffer, &pulse.length, sizeof(pulse.length));
          chBuffer += sizeof(pulse.length);

          // data
          memcpy(chBuffer, pulse.data, pulse.length * sizeof(*pulse.data));

          chBuffer += pulse.length * sizeof(*pulse.data);
        }

        // update ACQC header length
        acqcHeader_.length = static_cast<uint32_t>(
          chBuffer - cardFileBuffer - sizeof(acqcHeader_) + 3 * sizeof(int32_t));
        while (((chBuffer - cardFileBuffer - sizeof(acqcHeader_)) % 4) != 0)
        {
          *chBuffer = 0;
          chBuffer++;
          acqcHeader_.length++;
        }

        acqcHeader_.length = static_cast<uint32_t>(acqcHeader_.length /
                                                   sizeof(int32_t));

        memcpy(cardFileBuffer, &acqcHeader_, sizeof(acqcHeader_));

        cardFileBuffer = chBuffer;
      }

      // write pulses of the independent channels
      for (auto &card : cards_)
      {
        for (auto &ch : card->GetChannelCollection())
        {
          if (ch->getConfiguration()->enabled == false)
            continue;

          if (!ch->isIndependent())
            continue;

          chBuffer += sizeof(acqcHeader_);

          acqcHeader_.detectorType = ch->getConfiguration()->header.detectorType;
          acqcHeader_.detectorId = ch->getConfiguration()->header.detectorId;
          acqcHeader_.str_crate_mod_ch =
            ch->getConfiguration()->header.str_mod_crate;

          for (auto &pulse : ch->GetZeroSuppression().GetPulseDataCollection())
          {
            if (pulse.length == 0)
              continue;

            // timestamp
            memcpy(chBuffer, &pulse.timeStamp, sizeof(pulse.timeStamp));
            chBuffer += sizeof(pulse.timeStamp);

            // length of pulse
            memcpy(chBuffer, &pulse.length, sizeof(pulse.length));
            chBuffer += sizeof(pulse.length);

            // pulse data
            memcpy(chBuffer, pulse.data, pulse.length * sizeof(*pulse.data));
            chBuffer += pulse.length * sizeof(*pulse.data);
          }

          // update ACQC header length
          acqcHeader_.length = static_cast<uint32_t>(chBuffer - cardFileBuffer -
                                                     sizeof(acqcHeader_) +
                                                     3 * sizeof(int32_t));
          while (((chBuffer - cardFileBuffer - sizeof(acqcHeader_)) % 4) != 0)
          {
            *chBuffer = 0;
            chBuffer++;
            acqcHeader_.length++;
          }

          acqcHeader_.length = static_cast<uint32_t>(acqcHeader_.length /
                                                     sizeof(int32_t));

          memcpy(cardFileBuffer, &acqcHeader_, sizeof(acqcHeader_));

          cardFileBuffer = chBuffer;
        }
      }
    }
  }
  else
  {
    for (auto &ch : cards_[cardIndex]->GetChannelCollection())
    {
      if (ch->getConfiguration()->enabled == false)
        continue;

      chBuffer += sizeof(acqcHeader_);

      acqcHeader_.detectorType = ch->getConfiguration()->header.detectorType;
      acqcHeader_.detectorId = ch->getConfiguration()->header.detectorId;
      acqcHeader_.str_crate_mod_ch = ch->getConfiguration()->header.str_mod_crate;

      switch (zsMode_)
      {
      case ZeroSuppression::zsMode::INDEPENDENT:
        for (auto &pulse : ch->GetZeroSuppression().GetPulseDataCollection())
        {
          if (pulse.length == 0)
            continue;

          // timestamp
          memcpy(chBuffer, &pulse.timeStamp, sizeof(pulse.timeStamp));
          chBuffer += sizeof(pulse.timeStamp);

          // length of pulse
          memcpy(chBuffer, &pulse.length, sizeof(pulse.length));
          chBuffer += sizeof(pulse.length);

          // pulse data
          memcpy(chBuffer, pulse.data, pulse.length * sizeof(*pulse.data));
          chBuffer += pulse.length * sizeof(*pulse.data);
        }
        break;

      case ZeroSuppression::zsMode::SINGLE_MASTER:
      {
        auto &masterCard = cards_[zsMasterList_[0].cardIndex];
        auto &masterChannel = masterCard->GetChannel(zsMasterList_[0].channel);

        AcquisitionBuffer *acqBuffer =
          cards_[cardIndex]->GetBuffer(acqParams.GetBufferPoolIndex()).get();

        ch->GetZeroSuppression().SingleMaster(
          acqBuffer->buffer_[ch->GetIndex()],
          ch->getConfiguration()->header.sampleSize * 1000,
          masterChannel->GetZeroSuppression().GetPulseDataCollection(),
          ch->getConfiguration()->header,
          masterChannel->getConfiguration()->header);

        for (auto &pulse : ch->GetZeroSuppression().GetPulseDataCollection())
        {
          // timestamp
          memcpy(chBuffer, &pulse.timeStamp, sizeof(pulse.timeStamp));
          chBuffer += sizeof(pulse.timeStamp);

          // length of pulse
          memcpy(chBuffer, &pulse.length, sizeof(pulse.length));
          chBuffer += sizeof(pulse.length);

          // data
          memcpy(chBuffer, pulse.data, pulse.length * sizeof(*pulse.data));

          chBuffer += pulse.length * sizeof(*pulse.data);
        }

        break;
      }

      default: break;
      }

      // update ACQC header length
      acqcHeader_.length = static_cast<uint32_t>(
        chBuffer - cardFileBuffer - sizeof(acqcHeader_) + 3 * sizeof(int32_t));
      while (((chBuffer - cardFileBuffer - sizeof(acqcHeader_)) % 4) != 0)
      {
        *chBuffer = 0;
        chBuffer++;
        acqcHeader_.length++;
      }

      acqcHeader_.length = static_cast<uint32_t>(acqcHeader_.length /
                                                 sizeof(int32_t));

      memcpy(cardFileBuffer, &acqcHeader_, sizeof(acqcHeader_));

      cardFileBuffer = chBuffer;
    }
  }

  LOG_DEBUG << "Exiting FillFileBuffer";

  return cardFileBuffer;
}

void CWriter::WriterThreadStop(bool error)
{
  boost::this_thread::disable_interruption di;

  LOG_TRACE << "Entering CWriter::WriterThreadStop";

  if (!error)
    state_.setValue(static_cast<int32_t>(WRITER_STATE_t::STOPPING));

  if (cardPoolIndex_.size())
    for (auto &card : cards_)
    {
      cardPoolIndex_[card->GetCardIndex()]->push(-1);
    }

  parsingZsThreads_.join_all();

  cardPoolIndex_.clear();
  LOG_TRACE << "freeing buffer pool";
  for (auto &card : cards_)
    card->FreeBufferPool();

  LOG_TRACE << "freeing file buffer";
  _mm_free(fileBuffer_), fileBuffer_ = nullptr;

  if (!error)
    state_.setValue(static_cast<int32_t>(WRITER_STATE_t::IDLE));

  LOG_TRACE << "Exiting CWriter::WriterThreadStop";
}

void CWriter::ParsingZsThread(int cardIndex)
{
  std::string scope = "Parse-ZS C" + boost::lexical_cast<std::string>(cardIndex);
  BOOST_LOG_SCOPED_THREAD_ATTR(
    "Scope", boost::log::attributes::constant<std::string>(scope));

  LOG_TRACE << "Entering ParsingZsThread";

  // Allocate memory on dedicated CPU's memory controller associated to each card
  try
  {
    PinThreadToCpu(cards_[cardIndex]->GetWriterCore());
    PinThreadToNode(cards_[cardIndex]->GetWriterCore());
  }
  catch (const CWriterEx &ex)
  {
    LOG_ERROR << ex.what();
  }

  int bufferPoolIndex = -1;
  AcquisitionBuffer *acqBuffer = nullptr;

  while (true)
  {
    try
    {
      LOG_TRACE << "waiting for acquisition buffer";

      cardPoolIndex_[cardIndex]->pop(&bufferPoolIndex);
      if (bufferPoolIndex == -1)
      {
        LOG_INFO << "negative buffer pool index, stopping the thread ...";
        break;
      }

      try
      {
        acqBuffer = cards_[cardIndex]->GetBuffer(bufferPoolIndex).get();
      }
      catch (const CCardEx &ex)
      {
        LOG_FATAL << ex.what();
        return;
      }

      LOG_TRACE << "acquisition buffer popped out from the queue";

      // offline parsing
      if (cards_[cardIndex]->HasOfflineParsing())
      {
        if (acqBuffer->dumpBuffer_ != nullptr)
        {
          double perfStart = StartCounter();
          try
          {
            cards_[cardIndex]->OfflineParsing(acqBuffer->dumpBuffer_);
          }
          catch (const CCardEx &ex)
          {
            acqBuffer->parsingZsStatus_ = -1;
            LOG_TRACE << "Exiting ParsingZsThread because of error: "
                      << ex.what();
            return;
          }

          // performance logging
          {
            double opTime = GetCounter(perfStart);
            double opSize = cards_[cardIndex]->GetDumpBufferSize() / 1024. /
              1024.;
            double opRate = opSize / opTime;
            LOG_INFO << "offline parsing data rate: " << opRate << "MB/s";
          }
        }
        else
        {
          std::string err = "dump buffer pointer is null, stopping writer "
                            "thread";
          LOG_ERROR << err;
          acqBuffer->parsingZsStatus_ = -1;
          LOG_TRACE << "Exiting ParsingZsThread because of error";
          return;
        }
      }

      // zero suppression
      try
      {
        double perfStart = StartCounter();

        cards_[cardIndex]->PerformZeroSuppression(
          zsMode_, (const int16_t **) acqBuffer->buffer_.data());

        // performance logging
        {
          double zsTime = GetCounter(perfStart);
          double zsSize = cards_[cardIndex]->GetChannelBufferSize() *
            cards_[cardIndex]->GetNbChannels() / 1024. / 1024.;
          double zsRate = zsSize / zsTime;
          LOG_INFO << "zero suppression data rate: " << zsRate << "MB/s";
        }
      }
      catch (const CCardEx &ex)
      {
        LOG_ERROR << ex.what();
        acqBuffer->parsingZsStatus_ = -1;
        LOG_FATAL << "Exiting ParsingZsThread because of error";
        return;
      }

      // acquisition thread completed successfully
      acqBuffer->parsingZsStatus_ = 1;
    }
    catch (boost::thread_interrupted const &)
    {
      LOG_TRACE << "ParsingZsThread interrupted";
      break;
    }
  }

  LOG_TRACE << "Exiting ParsingZsThread";
}

void CWriter::WriteRawData(AcquisitionBuffer *acqBuffer_)
{
  static uint acqId = 1;

  for (uint i = 0; i < cards_[acqBuffer_->cardIndex_]->GetNbChannels(); ++i)
  {
    std::string fileName = "/DAQ/data/" +
      boost::lexical_cast<std::string>(acqBuffer_->cardIndex_) + "_" +
      boost::lexical_cast<std::string>(acqId) + "_ch" +
      boost::lexical_cast<std::string>(i) + ".raw";

    int fd = open(fileName.c_str(), O_WRONLY | O_CREAT, 0777);
    if (fd == -1)
    {
      std::string err = "could not create file " + fileName + ": " +
        strerror(errno);
      LOG_FATAL << err;
      throw CWriterEx(err);
    }

    size_t rtn = write(fd, acqBuffer_->buffer_[i],
                       cards_[acqBuffer_->cardIndex_]->GetChannelBufferSize());
    if (rtn != cards_[acqBuffer_->cardIndex_]->GetChannelBufferSize())
    {
      std::string err = "could not write file: " + fileName + ": " +
        strerror(errno);
      LOG_FATAL << err;
      close(fd);
      throw CWriterEx(err);
    }

    close(fd);
  }

  acqId++;
}

void CWriter::WritePulseData(int &fd, uint cardIndex)
{
  // for each channel, pulse data are written to disk
  for (uint i = 0; i < cards_[cardIndex]->GetNbChannels(); ++i)
  {
    if (!cards_[cardIndex]->GetChannel(i)->getConfiguration()->enabled)
      continue;

    uint nbPulses = static_cast<uint>(cards_[cardIndex]
                                        ->GetChannel(i)
                                        ->GetZeroSuppression()
                                        .GetPulseDataCollection()
                                        .size());

    for (uint j = 0; j < nbPulses; ++j)
    {
      uint32_t timeStamp = static_cast<uint32_t>(cards_[cardIndex]
                                                   ->GetChannel(i)
                                                   ->GetZeroSuppression()
                                                   .GetPulseDataCollection()[j]
                                                   .timeStamp);
      if (write(fd, &timeStamp, sizeof(timeStamp)) != sizeof(timeStamp))
      {
        std::string s = "could not write pulse data timestamp";
        LOG_FATAL << s + ": " + strerror(errno);
        throw CWriterEx(s);
      }

      uint32_t size = static_cast<uint32_t>(
        cards_[cardIndex]
          ->GetChannel(i)
          ->GetZeroSuppression()
          .GetPulseDataCollection()[j]
          .length *
        sizeof(*(cards_[cardIndex]
                   ->GetChannel(i)
                   ->GetZeroSuppression()
                   .GetPulseDataCollection()[j]
                   .data)));

      if (write(fd, &size, sizeof(size)) != sizeof(size))
      {
        std::string s = "could not write pulse data length";
        LOG_FATAL << s + ": " + strerror(errno);
        throw CWriterEx(s);
      }

      const int16_t *ptr = cards_[cardIndex]
                             ->GetChannel(i)
                             ->GetZeroSuppression()
                             .GetPulseDataCollection()[j]
                             .data;
      ssize_t byteWritten = write(fd, ptr, size);
      if (byteWritten != size)
      {
        std::string s = "could not write pulse data";
        std::ostringstream oss;
        oss << byteWritten << " byte written instead of " << size;
        LOG_FATAL << s + ": " + strerror(errno);
        throw CWriterEx(s);
      }

      if ((size % 4) != 0)
      {
        int16_t tmp = 0;
        byteWritten = write(fd, &tmp, sizeof(tmp));
        if (byteWritten != sizeof(tmp))
        {
          std::string s = "could not write pulse data";
          std::ostringstream oss;
          oss << byteWritten << " byte written instead of " << sizeof(tmp);
          LOG_FATAL << s + ": " + strerror(errno);
          throw CWriterEx(s);
        }
      }
    }
  }
}

void CWriter::WritePulseData(AcquisitionBuffer *acqBuffer)
{
  static uint acqId = 1;

  for (uint i = 0; i < cards_[acqBuffer->cardIndex_]->GetNbChannels(); ++i)
  {
    if (!cards_[acqBuffer->cardIndex_]->GetChannel(i)->getConfiguration()->enabled)
      continue;

    if (cards_[acqBuffer->cardIndex_]
          ->GetChannel(i)
          ->GetZeroSuppression()
          .GetPulseDataCollection()
          .size() == 0)
      continue;

    std::string fileName = "/DAQ/data/" +
      boost::lexical_cast<std::string>(acqId) + "_pulse_card" +
      boost::lexical_cast<std::string>(acqBuffer->cardIndex_) + "_ch" +
      boost::lexical_cast<std::string>(i) + ".raw";

    int fd = open(fileName.c_str(), O_WRONLY | O_CREAT, 0777);
    if (fd == -1)
    {
      std::string err = "can not open file: " + fileName + ": " +
        strerror(errno);
      LOG_FATAL << err;
      throw CWriterEx(err);
    }

    uint pulseIndex = 1;
    for (auto &pulse : cards_[acqBuffer->cardIndex_]
                         ->GetChannel(i)
                         ->GetZeroSuppression()
                         .GetPulseDataCollection())
    {
      write(fd, pulse.data, pulse.length * sizeof(*(pulse.data)));

      if (pulse.length < 20)
        continue;

      std::ostringstream oss;
      oss << "pulse " << pulseIndex++ << ", length = " << pulse.length
          << " samples";
      oss << ", timestamp = "
          << static_cast<float>(pulse.timeStamp) /
          cards_[acqBuffer->cardIndex_]
            ->GetChannel(i)
            ->getConfiguration()
            ->header.sampleRate
          << " us";

      LOG_TRACE << oss.str();
    }

    close(fd);
  }

  acqId++;
}

std::string CWriter::CreateRunDir(uint32_t runNumber)
{
  // check if run directory exists, create it otherwise
  std::string runDirectory = cmdLineParams_.dataPath + "run" +
    boost::lexical_cast<std::string>(runNumber);

  int result = mkdir(runDirectory.c_str(),
                     S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if (result == -1 && errno != EEXIST)
  {
    std::string err = "could not create " + runDirectory + " : " +
      std::string(strerror(errno));
    LOG_ERROR << err;
    runDirectory = cmdLineParams_.dataPath;
  }
  else
    runDirectory += "/";
  return runDirectory;
}

void CWriter::WriteEventToDisk(const std::string &runDirectory,
                               const EventHeader &header)
{
  std::ostringstream fileName;
  fileName << runDirectory << "run" << header.runNumber << "_"
           << header.eventNumber << ".event";

  std::ofstream out;
  out.open(fileName.str().c_str());
  out << header;
  if (!out.good())
  {
    std::string err = "could not write " + fileName.str() + " : " +
      std::string(strerror(errno));
    out.close();
    LOG_FATAL << err;
    throw CWriterEx(err);
  }
  out.close();
}

void CWriter::WriteDataToDisk(const std::string &runDirectory,
                              const CAcquisitionParameters &acqParams,
                              ssize_t length)
{
  // default file name
  std::string fileName;

  // find stream number
  streamNumber_ = -1;
  for (auto &card : cards_)
  {
    for (auto &ch : card->GetChannelCollection())
    {
      if (ch->getConfiguration()->enabled == false)
        continue;
      streamNumber_ = ch->getConfiguration()->header.getStream();
      break;
    }
    if (streamNumber_ != -1)
      break;
  }
  fileName = runDirectory +
    createName(acqParams.GetRunNumber(), acqParams.GetEventNumber());

  // update skip header with final length
  skipHeader_.end = length;
  memcpy(fileBuffer_, &skipHeader_, sizeof(skipHeader_));

  // padding for DIRECT IO
  length = (length + DIRECTIO_BLOCK_SIZE - 1) & ~(DIRECTIO_BLOCK_SIZE - 1);

  // opening file with DIRECT_IO mode
  int fd = open(fileName.c_str(), O_WRONLY | O_CREAT | O_DIRECT | O_SYNC, 0777);
  if (fd == -1)
  {
    std::string err = "could not create file " + fileName + ": " +
      strerror(errno);
    throw CWriterEx(err);
  }

  size_t byteWritten = 0;

  double perfStart = StartCounter();

  while (byteWritten != static_cast<size_t>(length * sizeof(*fileBuffer_)))
  {
    ssize_t ret = ::write(fd, &fileBuffer_[byteWritten / sizeof(*fileBuffer_)],
                          length * sizeof(*fileBuffer_) - byteWritten);
    if (ret < 0)
    {
      std::string err = "could not write " + fileName + " : " +
        std::string(strerror(errno));
      close(fd);
      LOG_FATAL << err;
      throw CWriterEx(err);
    }
    byteWritten += ret;
  }

  double hddTransferTime = GetCounter(perfStart);

  // performance logging
  double hddTransferSize = static_cast<double>(byteWritten) / 1024. / 1024.;
  double hddDataRate = hddTransferSize / hddTransferTime;
  std::ostringstream oss;
  oss << fileName << ", " << hddTransferSize << "MB written at " << hddDataRate
      << "MB/s";
  LOG_INFO << oss.str();

  close(fd);
}

void CWriter::WriteRunToDisk(const std::string &runDirectory, uint32_t runNumber)
{
  std::ostringstream fileName;
  fileName << runDirectory << "run" << runNumber << ".run";

  RunControlHeader rctr;
  rctr.runNumber = runNumber;
  rctr.segmentNumber = 0; /* only used on castor files */
  rctr.streamNumber = 1;
  rctr.updateExperiment();
  rctr.updateDateTime(); // set date/time to "now"
  rctr.totalNumberOfStreams = 1;
  rctr.totalNumberOfChassis = 1;
  rctr.totalNumberOfModules = cards_.size();
  rctr.numberOfChannelsPerModule.resize(cards_.size());

  for (size_t i = 0; i < cards_.size(); ++i)
    rctr.numberOfChannelsPerModule[i] = cards_[i]->GetNbUsedChannels();

  ModuleHeader modh;
  modh.channelsConfig.resize(rctr.numberOfChannels());
  size_t modhIndex = 0;
  for (const std::shared_ptr<CCard> &card : cards_)
  {
    for (const CCard::ChannelCollection::value_type &chan :
         card->GetChannelCollection())
    {
      const ntof::CChannel::Config &config = *(chan->getConfiguration());
      if (config.enabled)
      {
        ChannelConfig chanConf = config.header;

        // use calibrated values
        chanConf.fullScale = chan->GetCalibratedFullScale();
        // CalibratedOffset is relative to config offset
        chanConf.offset = config.header.offset + chan->GetCalibratedOffset();
        chanConf.threshold = chan->GetCalibratedThreshold();

        modh.channelsConfig[modhIndex++] = chanConf;
      }
    }
  }

  std::ofstream out;
  out.open(fileName.str().c_str());
  out << rctr << modh;
  if (!out.good())
  {
    std::string err = "could not write " + fileName.str() + " : " +
      std::string(strerror(errno));
    out.close();
    LOG_FATAL << err;
    throw CWriterEx(err);
  }
  out.close();
}

std::string CWriter::createName(int64_t runNumber, int64_t eventNumber)
{
  std::ostringstream oss;

  oss << "run";
  oss << runNumber;
  oss << "_";
  oss << eventNumber;
  oss << "_s" << streamNumber_;
  oss << ".raw";

  return oss.str();
}

void CWriter::publishWroteNumber(int64_t runNumber,
                                 int64_t eventNumber,
                                 uint64_t size,
                                 pugi::xml_document &doc)
{
  // Build XML data to be sent
  pugi::xml_node root = doc.append_child("RunExtensionNumberWrote");

  root.append_attribute("run").set_value(static_cast<long long int>(runNumber));
  root.append_attribute("event").set_value(
    static_cast<long long int>(eventNumber));
  root.append_attribute("size").set_value(
    static_cast<unsigned long long int>(size));
}

void CWriter::PinThreadToCpu(int cpu)
{
  int ncpus = numa_num_task_cpus();

  if (ncpus >= CPU_SETSIZE)
    throw CWriterEx("wrong number of CPUs detected: " +
                    boost::lexical_cast<std::string>(ncpus) +
                    " cpus detected instead of " +
                    boost::lexical_cast<std::string>(CPU_SETSIZE));

  cpu_set_t cs;
  CPU_ZERO(&cs);
  CPU_SET(cpu, &cs);

  if (CPU_COUNT(&cs) != 1)
  {
    throw CWriterEx("CPU_COUNT(&cs) == 1");
  }

  int ret = sched_setaffinity(0, sizeof(cs), &cs);
  if (ret != 0)
  {
    std::string err = "sched_setaffinity failed: " +
      std::string(strerror(errno));
    throw CWriterEx(err);
  }

  ret = sched_yield();
  if (ret != 0)
  {
    std::string err = "sched_yield failed: " + std::string(strerror(errno));
    throw CWriterEx(err);
  }
}

void CWriter::PinThreadToNode(int cpu)
{
  int node = numa_node_of_cpu(cpu);

  int ret = numa_run_on_node(node);
  if (ret)
  {
    std::string err = "numa_run_on_node failed: " + std::string(strerror(errno));
    throw CWriterEx(err);
  }

  ret = sched_yield();
  if (ret)
  {
    std::string err = "sched_yield failed: " + std::string(strerror(errno));
    throw CWriterEx(err);
  }
}

// Exception for CWriter class
CWriterEx::CWriterEx() throw()
{
  exceptionMsg_ = "none";
}

CWriterEx::CWriterEx(const std::string &msg) throw() : exceptionMsg_(msg) {}

CWriterEx::~CWriterEx() throw() {}

const char *CWriterEx::what() const throw()
{
  return exceptionMsg_.c_str();
}

void CWriterEx::SetMessage(const std::string &msg)
{
  exceptionMsg_ = msg;
}
