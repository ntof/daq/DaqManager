#include <cstdint>
#include <sstream>
#include <string>
#include <vector>

#include <boost/lexical_cast.hpp>

#include <unistd.h>

#include "DIMException.h"
#include "logger.h"

#include "dis.hxx"

#ifdef ACQIRIS
#include "Acqiris/DC270/CCardAcqirisDC270.h"
#include "DaqTypes.h"

namespace Agilent {
namespace Acqiris {
CCardAcqirisDC270::CCardAcqirisDC270(std::string serialNumber,
                                     int cpuCore,
                                     uint32_t cardIndex,
                                     uint32_t crateIndex,
                                     ntof::dim::DIMState *state) :
  CCard("A270", serialNumber, cpuCore, cardIndex, crateIndex, state),
  CCardAcqiris(cardIndex)
{
  LOG_TRACE << "Entering CCardAcqirisDC270::CCardAcqirisDC270 with index " +
      boost::lexical_cast<std::string>(CCard::CCard::cardIndex_);

  ClassInit();

  if (!ready_)
  {
    throw CCardEx("Acqiris driver not ready");
  }

  LOG_TRACE << "checking serial number ...";
  vectorIndex_ = 0;
  bool match = false;
  for (auto &card : dc270_)
  {
    if (serialNumber_ == GetSN(card->GetInstrumentId()))
    {
      match = true;
      break;
    }
    vectorIndex_++;
  }

  if (!match)
    throw CCardEx("serial number given in configuration file does not match "
                  "any installed card");
  else
    LOG_TRACE
      << "vector index: " + boost::lexical_cast<std::string>(vectorIndex_);

  LOG_TRACE << "Exiting CCardAcqirisDC270::CCardAcqirisDC270 with index " +
      boost::lexical_cast<std::string>(CCard::CCard::cardIndex_);
}

void CCardAcqirisDC270::ClassInit()
{
  if (ready_ == false)
  {
    strLastMsg_ = "";
    strLastError_ = "";
    lastErrorCode_ = 0;
    CCard::nbDevices_ = 0;
    nbInitDeviceFailed_ = 0;
  }

  // Get host's name
  char hostname[128] = {};
  if (gethostname(hostname, 128) != 0)
  {
    throw CCardEx("can not get host name");
  }
  hostName_ = std::string(hostname);

  // create zero suppression object for each channel
  try
  {
    channel_.reserve(Dc270::getNbChannels());
    for (uint i = 0; i < Dc270::getNbChannels(); ++i)
    {
      std::ostringstream oss;
      oss << hostName_ << "/CARD" << CCard::cardIndex_ << "/CHANNEL" << i;
      channel_.push_back(std::make_shared<CChannelAcqirisDC270>(
        ChannelConfig::makeLocation(1, CCard::crateIndex_, CCard::cardIndex_, i),
        oss.str(), state_));
    }
  }
  catch (const ntof::ZeroSuppression::CZeroSuppressionEx &ex)
  {
    std::string s = "cannot create zero suppression object";
    LOG_FATAL << s;
    throw CCardEx(s);
  }

  readOutDone_ = false;
  dumpDone_ = false;
}

CCardAcqirisDC270::~CCardAcqirisDC270()
{
  LOG_TRACE << "Entering CCardAcqirisDC270::~CCardAcqirisDC270 with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  Delete();

  dc270_.clear();

  LOG_TRACE << "Exiting CCardAcqirisDC270::~CCardAcqirisDC270 with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

uint32_t CCardAcqirisDC270::GetNbChannels()
{
  return dc270_[vectorIndex_]->getNbChannels();
}

bool CCardAcqirisDC270::IsDumpSequential()
{
  return true;
}

uint32_t CCardAcqirisDC270::GetNbBytesDumped()
{
  return dc270_[vectorIndex_]->GetNbBytesDumped();
}

// Configure driver
void CCardAcqirisDC270::configure()
{
  LOG_TRACE << "Entering CCardAcqirisDC270::configure for index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  if (ready_)
  {
    try
    {
      dc270_[vectorIndex_]->SetClockSource(CLOCK_TYPE_t::INTERNAL);
      dc270_[vectorIndex_]->SetTriggerMode(TRIG_SOURCE_t::EXT_TRIG1,
                                           TRIG_SLOPE_t::POSITIVE, 1.5);

      LOG_TRACE << "configuring channels ...";

      bool firstCheck = true;
      uint chRef = 0;

      for (auto &ch : channel_)
      {
        dc270_[vectorIndex_]->ConfigureChannel(
          ch->GetIndex() + 1, ch->getConfiguration()->header.fullScale,
          ch->getConfiguration()->header.offset,
          ch->getConfiguration()->inputImpedance,
          ch->getConfiguration()->enabled);

        if (ch->getConfiguration()->enabled == false)
          continue;

        ch->GetZeroSuppression().SetGainOffset(
          static_cast<float>(ch->getConfiguration()->header.fullScale) /
            static_cast<float>(1 << Dc270::GetResolutionBits()),
          static_cast<float>(ch->getConfiguration()->header.fullScale) /
            static_cast<float>(1 << Dc270::GetResolutionBits()),
          ch->getConfiguration()->header.offset);

        // time window is in ms, convert it to us and get equivalent number of
        // samples
        uint32_t tw = static_cast<uint32_t>(
          ch->getConfiguration()->header.zeroSuppressionStart * 1E-3 *
          ch->getConfiguration()->header.sampleRate);
        if (tw > (ch->getConfiguration()->header.sampleSize * 1000))
        {
          throw CCardEx(
            "zero suppression window cannot be higher than total sample size");
        }
        ch->GetZeroSuppression().SetTimeWindow(
          tw, ch->getConfiguration()->header.sampleSize * 1000);

        if (firstCheck == true)
        {
          dc270_[vectorIndex_]->SetAcquisitionParameters(
            ch->getConfiguration()->header.sampleRate, 1,
            ch->getConfiguration()->header.sampleSize);

          dc270_[vectorIndex_]->SetTriggerDelay(
            (uint32_t)(ch->getConfiguration()->header.delayTime));

          firstCheck = false;
          chRef = ch->GetIndex();
          continue;
        }

        if (ch->getConfiguration()->header.sampleRate !=
            channel_[chRef]->getConfiguration()->header.sampleRate)
          throw CCardEx("sampling rate is not consistent for all channels");

        if (ch->getConfiguration()->header.sampleSize !=
            channel_[chRef]->getConfiguration()->header.sampleSize)
          throw CCardEx("sampling size is not consistent for all channels");

        if (ch->getConfiguration()->header.delayTime !=
            channel_[chRef]->getConfiguration()->header.delayTime)
          throw CCardEx("delay time is not consistent for all channels");
      }
    }
    catch (const ntof::CChannelEx &ex)
    {
      throw CCardEx(ex.what());
    }
  }

  LOG_TRACE << "Exiting CCardAcqirisDC270::configure";
}

size_t CCardAcqirisDC270::GetNbDevices()
{
  return dc270_.size();
}

void CCardAcqirisDC270::AllocateAcquisitionBuffer()
{
  try
  {
    dc270_[vectorIndex_]->AllocateChannelMemoryBuffers();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }
}

void CCardAcqirisDC270::ArmTrigger()
{
  readOutDone_ = false;
  dumpDone_ = false;

  try
  {
    dc270_[vectorIndex_]->ArmTrigger();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }
}

bool CCardAcqirisDC270::IsTriggered()
{
  bool b;

  try
  {
    b = dc270_[vectorIndex_]->IsTriggered();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  return b;
}

int CCardAcqirisDC270::GetResolution()
{
  return dc270_[vectorIndex_]->GetResolutionBits();
}

int16_t **CCardAcqirisDC270::GetAcquisitionBuffer()
{
  return dc270_[vectorIndex_]->GetAcquisitionBuffer();
}

uint32_t CCardAcqirisDC270::GetDumpBufferSize()
{
  return dc270_[vectorIndex_]->GetDumpBufferSize();
}

int8_t *CCardAcqirisDC270::GetMemoryDumpBuffer()
{
  return dc270_[vectorIndex_]->GetMemoryDumpBuffer();
}

void CCardAcqirisDC270::SetDumpMemoryBuffer(int8_t *buffer)
{
  dc270_[vectorIndex_]->SetDumpMemoryBuffer(buffer);
}

void CCardAcqirisDC270::DumpCardMemory()
{
  LOG_TRACE << "Entering CCardAcqirisDC270::DumpCardMemory for card " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  try
  {
#ifdef MEMORY_PRE_ALLOC
    dc270_[vectorIndex_]->SetAcquisitionBuffer(
      bufferPool_[bufferPoolIndex_]->buffer_.data());
    dc270_[vectorIndex_]->SetDumpMemoryBuffer(
      bufferPool_[bufferPoolIndex_]->dumpBuffer_);
#endif
    dc270_[vectorIndex_]->MemoryDump();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardAcqirisDC270::DumpCardMemory for card " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

uint32_t CCardAcqirisDC270::GetChannelBufferSize()
{
  return dc270_[vectorIndex_]->GetChannelBufferSize();
}

uint32_t CCardAcqirisDC270::GetCardIndex()
{
  return CCard::cardIndex_;
}

void CCardAcqirisDC270::SetChannelBufferFree()
{
  dc270_[vectorIndex_]->SetChannelBufferFree();
}

// Try to clean-up everything ...
void CCardAcqirisDC270::Delete()
{
  LOG_TRACE << "Entering CCardAcqirisDC270::Delete with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  if (ready_ == false)
  {
    LOG_TRACE << "driver instance is already NULL, exiting ...";
    return;
  }

  ready_ = false;

  LOG_TRACE << "Exiting CCardAcqirisDC270::Delete with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

void CCardAcqirisDC270::Calibrate()
{
  LOG_TRACE << "Entering CCardAcqirisDC270::Calibrate for card " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  try
  {
    dc270_[vectorIndex_]->Calibrate();
    for (auto &ch : channel_)
    {
      if (ch->getConfiguration()->enabled == false)

      {
        ch->SetCalibratedFullScale(5000);
        ch->SetCalibratedOffset(0);
        continue;
      }
      ch->SetCalibratedFullScale(
        dc270_[vectorIndex_]->GetCalibratedFullScale(ch->GetIndex()));
      ch->SetCalibratedOffset(
        dc270_[vectorIndex_]->GetCalibratedOffset(ch->GetIndex()) -
        ch->getConfiguration()->header.offset);
    }
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardAcqirisDC270::Calibrate for card " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

void CCardAcqirisDC270::LoadDefaultConfiguration()
{
  LOG_TRACE
    << "Entering CCardAcqirisDC270::LoadDefaultConfiguration for index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  try
  {
    dc270_[vectorIndex_]->SetAcquisitionParameters(500, 1, 8 * 1024);
    dc270_[vectorIndex_]->ConfigureChannels(5000.0, 0.0, 50.0);
    dc270_[vectorIndex_]->SetTriggerMode(TRIG_SOURCE_t::EXT_TRIG1,
                                         TRIG_SLOPE_t::POSITIVE, 1.2);
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  for (auto &ch : channel_)
  {
    ch->GetZeroSuppression().SetThreshold((int16_t) 30);
    ch->GetZeroSuppression().SetPulseDetectionType(
      ntof::ZeroSuppression::PULSE_TYPE_t::POSITIVE);
    ch->GetZeroSuppression().SetPrePost(2048, 2048);
  }

  LOG_TRACE
    << "Exiting CCardAcqirisDC270::LoadDefaultConfiguration for index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}
/*
 void CCardAcqirisDC270::OfflineParsing(int8_t* dumpBuffer)
 {
 LOG_TRACE <<
 "Entering CCardAcqirisDC270::OfflineParsing for card "
 + boost::lexical_cast<std::string>(CCard::cardIndex_));

 if (dumpBuffer != NULL)
 {
 try
 {
 dc270_[vectorIndex_]->GetAcquisition(dumpBuffer);
 }
 catch (const CCardEx& ex)
 {
 throw CCardEx(ex.what());
 }
 }
 else
 {
 throw CCardEx("could not perform offline parsing, buffer is NULL");
 }

 LOG_TRACE <<
 "Exiting CCardAcqirisDC270::OfflineParsing for card "
 + boost::lexical_cast<std::string>(CCard::cardIndex_);
 }
 */

bool CCardAcqirisDC270::HasOfflineParsing()
{
  return false;
}

} // namespace Acqiris
} // namespace Agilent
#endif //#ifdef ACQIRIS
