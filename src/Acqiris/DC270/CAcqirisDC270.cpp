#include <cstdint>
#include <ctime>
#include <fstream>
#include <iostream>
#include <vector>

#include <boost/algorithm/minmax.hpp>
#include <boost/algorithm/minmax_element.hpp>
#include <boost/program_options.hpp>

#include <fcntl.h>
#include <sys/time.h>
#include <unistd.h>

#include "CCard.h"
#include "logger.h"

#ifdef ACQIRIS
#include "Acqiris/DC270/CAcqirisDC270.h"

using namespace boost;
using namespace std;

namespace Agilent {
namespace Acqiris {

Dc270::Dc270(ViSession instrID, uint32_t cardIndex) :
  CAqiris("DC270", instrID, cardIndex, getNbChannels(), GetResolutionBits())
{
  std::ostringstream oss;

  oss << "Entering Dc270::Dc270 with index " << cardIndex_;
  oss << ", intrumentID " << instrumentID_;

  LOG_TRACE << oss.str();

  init();
}

Dc270::~Dc270() {}

void Dc270::init()
{
  targetBuffer_ = NULL;
  dumpBuffer_ = NULL;
  nbValidSamples_ = 0;
}

uint Dc270::getNbChannels()
{
  return 4;
}

uint32_t Dc270::GetResolutionBits()
{
  return 8;
}

void Dc270::MemoryDump()
{
  MemoryDump8();
}

#if 0
void Dc270::MemoryDump()
{
	int8_t* dumpBuffer = dumpBuffer_;

	nbBytesDumped_ = 0;

	for (uint32_t i = 0; i < nbChannels_; ++i)
	{
		if (!channelEnabled_[i])
		{
			dumpBuffer += nbSamplesToDump_;
			continue;
		}

		std::string channel = "Channel" + boost::lexical_cast<std::string>(i + 1);

		ViStatus status = AgMD1_FetchWaveformInt8(instrumentID_, channel.c_str(), nbSamplesToDump_, (ViChar*) dumpBuffer,
		        &(actualPoints_[i]), &(firstValidPoint_[i]), &initialXOffset_, &initialXTimeSeconds_,
		        &initialXTimeFraction_, &xIncrement_, &scaleFactor_, &scaleOffset_);

		if (status < 0)
		{
			char msg[256];
			AgMD1_error_message(instrumentID_, status, msg);
			throw CCardEx("AgMD1_FetchWaveformInt8: " + std::string(msg));
		}

		nbBytesDumped_ += static_cast<uint32_t>(actualPoints_[i] * sizeof(*dumpBuffer));

		// put first valid points and number of points for each channel at the end of the buffer
		for (uint j = 0; j < sizeof(uint32_t); ++j)
		{
			dumpBuffer_[nbSamplesToDump_ - ((i + 1) * sizeof(uint32_t) * 2) + j] = (int8_t) ((firstValidPoint_[i] >> (8 * j))
			        & 0xFF);

			dumpBuffer_[nbSamplesToDump_ - ((i + 1) * sizeof(uint32_t) * 2 - sizeof(uint32_t)) + j] =
			        (int8_t) ((actualPoints_[i] >> (8 * j)) & 0xFF);
		}

		/*{
		 std::string fileName = "/DAQ/data/" + channel + ".raw";
		 FILE* f = fopen(fileName.c_str(), "w");
		 fwrite(&(dumpBuffer[firstValidPoint_[i]]), sizeof(*dumpBuffer), actualPoints_[i], f);
		 fclose(f);
		 }*/

		dumpBuffer += nbSamplesToDump_;
	}
}

void Dc270::GetAcquisition(int8_t* dumpBuffer)
{
	std::string fn;
	uint32_t firstPoint[nbChannels_];
	uint32_t nbPoints[nbChannels_];

	try
	{
		AllocateChannelMemoryBuffers();
	}
	catch (const CCardEx& ex)
	{
		throw CCardEx(ex.what());
	}

	if (targetBuffer_ == NULL)
	{
		std::string s = "targetBuffer = NULL";
		throw CCardEx(s);
	}

	for (uint32_t i = 0; i < nbChannels_; ++i)
	{
		if (!channelEnabled_[i])
		{
			continue;
		}

		firstPoint[i] = 0;
		nbPoints[i] = 0;

		for (uint j = 0; j < sizeof(uint32_t); ++j)
		{
			uint8_t tmp;
			tmp = dumpBuffer[nbSamplesToDump_ - ((i + 1) * sizeof(uint32_t) * 2) + j];
			firstPoint[i] |= tmp << (8 * j);
			tmp = dumpBuffer[nbSamplesToDump_ - ((i + 1) * sizeof(uint32_t) * 2 - sizeof(uint32_t)) + j];
			nbPoints[i] |= tmp << (8 * j);
		}

		nbValidSamples_ = nbPoints[i];

		uint k = 0;
		firstPoint[i] += nbSamplesToDump_ * i;
		for (uint32_t j = firstPoint[i]; j < (firstPoint[i] + nbPoints[i]); ++j)
		{
			targetBuffer_[i][k++] = dumpBuffer[j];
		}

		/*LOG_TRACE <<
				        "channel " + boost::lexical_cast<std::string>(i) + " , nb valid samples: "
				                + boost::lexical_cast<std::string>(nbValidSamples_) + ", first point: "
				                + boost::lexical_cast<std::string>(firstPoint[i]);*/

		/*{
			std::string fileName = "/DAQ/data/targetBuffer" + boost::lexical_cast<std::string>(i) + ".raw";
			FILE* f = fopen(fileName.c_str(), "w");
			fwrite(targetBuffer_[i], sizeof(*targetBuffer_[i]), nbPoints[i], f);
			fclose(f);
		}*/
	}
}
#endif
} // namespace Acqiris
} // namespace Agilent
#endif //#ifdef ACQIRIS
