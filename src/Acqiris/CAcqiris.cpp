#include <cstdint>
#include <ctime>
#include <fstream>
#include <iostream>
#include <vector>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <fcntl.h>
#include <numa.h>
#include <sys/time.h>
#include <unistd.h>

#include "logger.h"

#ifdef ACQIRIS
#include "Acqiris/CAcqiris.h"

using namespace boost;

namespace Agilent {
namespace Acqiris {

static std::string get_error_message(const ViSession &session,
                                     const ViStatus &status)
{
  char msg[256];
  AgMD1_error_message(session, status, msg);
  return std::string(msg);
}

CAqiris::CAqiris(std::string cardModelName,
                 ViSession instrID,
                 uint32_t cardIndex,
                 uint nbChannels,
                 uint resolution) :
  instrumentID_(instrID),
  cardIndex_(cardIndex),
  nbChannels_(nbChannels),
  resolution_(resolution),
  cardModelName_(cardModelName)
{
  std::ostringstream oss;

  oss << "Entering CAqiris::CAqiris with index " << cardIndex_;
  oss << ", intrumentID " << instrumentID_;
  oss << ", model: " << cardModelName_;

  LOG_TRACE << oss.str();

  targetBuffer_ = nullptr;
  dumpBuffer_ = nullptr;
  nbValidSamples_ = 0;
  nbSamplesToDump_ = 0;
  hasTriggered_ = false;
  nbSamples_ = 0;
  initialXOffset_ = 0;       //!< used by API
  initialXTimeSeconds_ = 0;  //!< used by API
  initialXTimeFraction_ = 0; //!< used by API
  xIncrement_ = 0;           //!< used by API
  scaleFactor_ = 0;          //!< used by API
  scaleOffset_ = 0;          //!< used by API
  nbSamplesToDump_ = 0;      //!< used by API
  slotNumber_ = 0;
  nbBytesDumped_ = 0;
  nbChannelEnabled_ = 0;
  nbSegments_ = 0;

  channelEnabled_.reserve(nbChannels);
  firstValidPoint_.reserve(nbChannels);
  actualPoints_.reserve(nbChannels);

  for (uint i = 0; i < nbChannels_; i++)
  {
    channelEnabled_.push_back(false);
    calibratedFullScale_.push_back(0.0);
    calibratedOffset_.push_back(0.0);
  }
}

CAqiris::~CAqiris()
{
  ViStatus status = AgMD1_close(instrumentID_);

  if (status < 0)
    throw CCardEx("AgMD1_close: " + get_error_message(instrumentID_, status));
}

ViSession CAqiris::GetInstrumentId()
{
  return instrumentID_;
}

uint32_t CAqiris::GetNbBytesDumped()
{
  return nbBytesDumped_;
}

void CAqiris::SetTriggerMode(TRIG_SOURCE_t source,
                             TRIG_SLOPE_t slope,
                             double level)
{
  ViStatus status = 0;
  std::string src;

  switch (source)
  {
  case TRIG_SOURCE_t::CHANNEL1: src = "Channel1"; break;
  case TRIG_SOURCE_t::CHANNEL2: src = "Channel2"; break;
  case TRIG_SOURCE_t::CHANNEL3: src = "Channel3"; break;
  case TRIG_SOURCE_t::CHANNEL4: src = "Channel4"; break;
  case TRIG_SOURCE_t::EXT_TRIG1: src = "External1"; break;
  case TRIG_SOURCE_t::EXT_TRIG2: src = "External2"; break;
  default: throw CCardEx("invalid source parameter"); break;
  }

  status = AgMD1_SetAttributeViString(
    instrumentID_, "", AGMD1_ATTR_ACTIVE_TRIGGER_SOURCE, src.c_str());

  if (status < 0)
    throw CCardEx("set trigger source: " +
                  get_error_message(instrumentID_, status));

  status = AgMD1_SetAttributeViInt32(instrumentID_, src.c_str(),
                                     AGMD1_ATTR_TRIGGER_TYPE,
                                     AGMD1_VAL_EDGE_TRIGGER);

  if (status < 0)
    throw CCardEx("set trigger type: " +
                  get_error_message(instrumentID_, status));

  switch (slope)
  {
  case TRIG_SLOPE_t::NEGATIVE:
    status = AgMD1_SetAttributeViInt32(instrumentID_, src.c_str(),
                                       AGMD1_ATTR_TRIGGER_SLOPE,
                                       AGMD1_VAL_TRIGGER_SLOPE_NEGATIVE);
    break;
  case TRIG_SLOPE_t::POSITIVE:
    status = AgMD1_SetAttributeViInt32(instrumentID_, src.c_str(),
                                       AGMD1_ATTR_TRIGGER_SLOPE,
                                       AGMD1_VAL_TRIGGER_SLOPE_POSITIVE);
    break;
  default: throw CCardEx("invalid slope parameter"); break;
  }

  if (status < 0)
    throw CCardEx("set slope: " + get_error_message(instrumentID_, status));

  status = AgMD1_SetAttributeViReal64(instrumentID_, src.c_str(),
                                      AGMD1_ATTR_TRIGGER_LEVEL, level);

  if (status < 0)
    throw CCardEx("set trigger level: " +
                  get_error_message(instrumentID_, status));
}

void CAqiris::SetTriggerDelay(int32_t delay)
{
  ViStatus status = 0;

  LOG_TRACE << "-----------------------------------------------------";
  LOG_TRACE << "card " + cardModelName_ + " #" +
      boost::lexical_cast<std::string>(cardIndex_) +
      " configuration trigger delay: " + boost::lexical_cast<std::string>(delay);
  LOG_TRACE << "-----------------------------------------------------";

  status = AgMD1_SetAttributeViReal64(instrumentID_, "",
                                      AGMD1_ATTR_TRIGGER_DELAY, delay * 1.0E-9);

  if (status < 0)
    throw CCardEx("set trigger delay: " +
                  get_error_message(instrumentID_, status));

  ViReal64 d;
  status = AgMD1_GetAttributeViReal64(instrumentID_, "",
                                      AGMD1_ATTR_TRIGGER_DELAY, &d);

  if (round(d / 1E-9) != delay)
  {
    throw CCardEx("trigger delay adjusted to " +
                  boost::lexical_cast<std::string>(round(d / 1E-9)) +
                  " instead of " + boost::lexical_cast<std::string>(delay));
  }
}

void CAqiris::SetClockSource(Agilent::Acqiris::CLOCK_TYPE_t clkSource)
{
  ViInt32 val;

  switch (clkSource)
  {
  case (CLOCK_TYPE_t::EXTERNAL):
    val = AGMD1_VAL_SAMPLE_CLOCK_SOURCE_EXTERNAL;
    break;
  case (CLOCK_TYPE_t::INTERNAL):
    val = AGMD1_VAL_SAMPLE_CLOCK_SOURCE_INTERNAL;
    break;
  default: throw CCardEx("invalid clock source"); break;
  }

  ViStatus status = AgMD1_SetAttributeViInt32(
    instrumentID_, "", AGMD1_ATTR_SAMPLE_CLOCK_SOURCE, val);

  if (status < 0)
    throw CCardEx("CAqiris::SetClockSource: " +
                  get_error_message(instrumentID_, status));
}

void CAqiris::SetAcquisitionParameters(float samplingFreq,
                                       uint32_t nbSegments,
                                       uint32_t nbSamples)
{
  nbSegments_ = nbSegments;

  ViStatus status = AgMD1_ConfigureAcquisition(
    instrumentID_, 1, static_cast<ViInt64>(nbSamples * 1000),
    static_cast<ViReal64>(samplingFreq * 1E6));

  if (status < 0)
    throw CCardEx(get_error_message(instrumentID_, status));

  ViInt64 samples;
  status = AgMD1_GetAttributeViInt64(instrumentID_, "", AGMD1_ATTR_RECORD_SIZE,
                                     &samples);
  if (status < 0)
    throw CCardEx(get_error_message(instrumentID_, status));

  nbSamples_ = static_cast<uint32_t>(samples);

  if (nbSamples_ != nbSamples * 1000)
  {
    throw CCardEx("number of samples adjusted by driver to " +
                  boost::lexical_cast<std::string>(nbSamples_) + " instead of " +
                  boost::lexical_cast<std::string>(nbSamples * 1000));
  }

  if (cardModelName_ == "DC282")
    status = AgMD1_QueryMinWaveformMemory(instrumentID_, 16, 1, 0, nbSamples_,
                                          &nbSamplesToDump_);
  else
    status = AgMD1_QueryMinWaveformMemory(instrumentID_, 8, 1, 0, nbSamples_,
                                          &nbSamplesToDump_);

  if (status < 0)
    throw CCardEx(get_error_message(instrumentID_, status));

  ViReal64 freq;
  status = AgMD1_GetAttributeViReal64(instrumentID_, "", AGMD1_ATTR_SAMPLE_RATE,
                                      &freq);

  if (round(freq / 1E6) != samplingFreq)
  {
    throw CCardEx("trigger delay adjusted to " +
                  boost::lexical_cast<std::string>(round(freq / 1E6)) +
                  " instead of " +
                  boost::lexical_cast<std::string>(samplingFreq));
  }

  LOG_TRACE << "-----------------------------------------------------";
  LOG_TRACE << "card " + cardModelName_ + " #" +
      boost::lexical_cast<std::string>(cardIndex_) +
      " configuration parameters:";
  LOG_TRACE << "-- sampling frequency: " +
      std::string(boost::str(boost::format("%.2f") % (freq / 1E6))) + "MHz";
  LOG_TRACE << "-- nb samples: " + boost::lexical_cast<std::string>(samples);
  LOG_TRACE << "-- nb samples to be dumped from card: " +
      boost::lexical_cast<std::string>(nbSamplesToDump_);
  LOG_TRACE << "-----------------------------------------------------";
}

void CAqiris::AllocateChannelMemoryBuffers()
{
  // Allocate memory for channel buffers
  try
  {
    targetBuffer_ = new int16_t *[nbChannels_];
  }
  catch (const std::bad_alloc &ba)
  {
    std::string e = "could not allocate targetBuffer: ";
    e += ba.what();
    throw CCardEx(e);
  }
  catch (...)
  {
    std::string e = "could not allocate targetBuffer";
    throw CCardEx(e);
  }

  for (uint32_t i = 0; i < nbChannels_; ++i)
  {
    if (false == channelEnabled_[i])
    {
      targetBuffer_[i] = NULL;
      continue;
    }

    try
    {
      targetBuffer_[i] = new int16_t[nbSamplesToDump_ << 1];
    }
    catch (const std::bad_alloc &ba)
    {
      std::string e = "could not allocate memory for acquisition: ";
      e += ba.what();
      throw CCardEx(e);
    }
    catch (...)
    {
      std::string e = "could not allocate memory for acquisition";
      throw CCardEx(e);
    }
  }
}

void CAqiris::SetDumpMemoryBuffer(int8_t *buffer)
{
  dumpBuffer_ = buffer;
}

uint32_t CAqiris::GetDumpBufferSize()
{
  return static_cast<uint32_t>(sizeof(*dumpBuffer_) *
                               (nbSamplesToDump_ + sizeof(uint32_t) * 2) *
                               nbChannels_);
}

uint32_t CAqiris::GetSlotNumber()
{
  ViStatus status = AgMD1_GetAttributeViInt32(
    instrumentID_, "", AGMD1_ATTR_INSTRUMENT_INFO_SLOT_NUMBER,
    (ViInt32 *) &slotNumber_);

  if (status != AGMD1_SUCCESS)
    throw CCardEx("could not get instrument model" +
                  get_error_message(instrumentID_, status));

  return slotNumber_;
}

uint32_t CAqiris::GetChannelBufferSize()
{
  return static_cast<uint32_t>(sizeof(**targetBuffer_) * nbSamples_);
}

uint32_t CAqiris::GetChannelAllocationSize()
{
  return static_cast<uint32_t>(sizeof(**targetBuffer_) * nbSamplesToDump_);
}

void CAqiris::SetChannelBufferFree()
{
  if (targetBuffer_ == NULL)
    return;

  for (uint i = 0; i < nbChannels_; i++)
    delete[] targetBuffer_[i], targetBuffer_[i] = NULL;

  delete[] targetBuffer_, targetBuffer_ = NULL;
}

void CAqiris::SetAcquisitionBuffer(int16_t **buffer)
{
  targetBuffer_ = buffer;
}

int16_t **CAqiris::GetAcquisitionBuffer()
{
  return targetBuffer_;
}

int8_t *CAqiris::GetMemoryDumpBuffer()
{
  return dumpBuffer_;
}

void CAqiris::ArmTrigger()
{
  hasTriggered_ = false;
  ViStatus status;

  /*ViStatus status = AgMD1_Abort(instrumentID_);

   if (status < 0)
   throw CCardEx("AgMD1_Abort: " + get_error_message(instrumentID_, status));
  */

#ifdef MEMORY_PRE_ALLOC

#endif

  status = AgMD1_InitiateAcquisition(instrumentID_);

  if (status < 0)
    throw CCardEx("AgMD1_InitiateAcquisition: " +
                  get_error_message(instrumentID_, status));
}

bool CAqiris::IsTriggered()
{
  if (hasTriggered_)
    return true;

  ViStatus status = AgMD1_WaitForAcquisitionComplete(instrumentID_, 1000);
  if (status == AGMD1_SUCCESS)
  {
    hasTriggered_ = false;
    return true;
  }
  else
  {
    hasTriggered_ = false;
    return false;
  }
}

void CAqiris::MemoryDump8()
{
  nbBytesDumped_ = 0;

  for (uint32_t i = 0; i < nbChannels_; ++i)
  {
    if (false == channelEnabled_[i])
      continue;

    std::string channel = "Channel" + boost::lexical_cast<std::string>(i + 1);

    std::vector<int8_t> buffer(nbSamplesToDump_ + 1024);

    ViStatus status = AgMD1_FetchWaveformInt8(
      instrumentID_, channel.c_str(), nbSamplesToDump_,
      (ViChar *) (&(buffer[0])), &(actualPoints_[i]), &(firstValidPoint_[i]),
      &initialXOffset_, &initialXTimeSeconds_, &initialXTimeFraction_,
      &xIncrement_, &scaleFactor_, &scaleOffset_);

    if (status < 0)
      throw CCardEx("AgMD1_FetchWaveformInt8: " +
                    get_error_message(instrumentID_, status));

    nbValidSamples_ = static_cast<uint32_t>(actualPoints_[i]);

    nbBytesDumped_ += nbSamplesToDump_ * sizeof(buffer[0]);

    uint j = 0;
    for (int64_t k = firstValidPoint_[i]; k < actualPoints_[i]; ++k)
      targetBuffer_[i][j++] = static_cast<int16_t>(buffer[k]);

    /*{
     LOG_TRACE <<
     "writing raw data for channel " + boost::lexical_cast<std::string>(i) + ",
     1st valid point: "
     + boost::lexical_cast<std::string>(firstValidPoint_[i]) + ", actual points:
     "
     + boost::lexical_cast<std::string>(actualPoints_[i]));

     LOG_TRACE <<
     "scale factor = " + boost::lexical_cast<std::string>(scaleFactor_) + ",
     offset = "
     + boost::lexical_cast<std::string>(scaleOffset_));

     std::string fileName = "/DAQ/data/card" +
     boost::lexical_cast<std::string>(cardIndex_) + "_" + channel + ".raw"; int
     fd = open(fileName.c_str(), O_WRONLY | O_CREAT, 0777); if (fd == -1)
     {
     std::string err = "could not create file " + fileName + ": " +
     strerror(errno); LOG_TRACE << err);
     }
     size_t rtn = write(fd, &(buffer[0]), actualPoints_[i]);
     if (rtn != actualPoints_[i])
     {
     std::string err = "could not write file: " + fileName + ": " +
     strerror(errno); logger_.Fatal(err);
     }
     close(fd);
     }*/
  }
}

void CAqiris::MemoryDump16()
{
  nbBytesDumped_ = 0;

  // AllocateChannelMemoryBuffers();

  for (uint32_t i = 0; i < nbChannels_; ++i)
  {
    if (false == channelEnabled_[i])
      continue;

    std::string channel = "Channel" + boost::lexical_cast<std::string>(i + 1);

    ViStatus status = AgMD1_FetchWaveformInt16(
      instrumentID_, channel.c_str(), nbSamplesToDump_, targetBuffer_[i],
      &(actualPoints_[i]), &(firstValidPoint_[i]), &initialXOffset_,
      &initialXTimeSeconds_, &initialXTimeFraction_, &xIncrement_,
      &scaleFactor_, &scaleOffset_);

    nbValidSamples_ = static_cast<uint32_t>(actualPoints_[i]);

    nbBytesDumped_ += static_cast<uint32_t>(actualPoints_[i] *
                                            sizeof(**targetBuffer_));

    if (status < 0)
      throw CCardEx("AgMD1_FetchWaveformInt16: " +
                    get_error_message(instrumentID_, status));

      // targetBuffer_[i] = &(targetBuffer_[i][firstValidPoint_[i]]);

#if 0
		{
			LOG_TRACE <<
			"writing raw data for channel " + boost::lexical_cast<std::string>(i) + ", 1st valid point: "
			+ boost::lexical_cast<std::string>(firstValidPoint_[i]) + ", actual points: "
			+ boost::lexical_cast<std::string>(actualPoints_[i]));

			LOG_TRACE << "scale factor = " + boost::lexical_cast<std::string>(scaleFactor_) + ", offset = " + boost::lexical_cast<std::string>(scaleOffset_));

			std::string fileName = "/DAQ/data/" + channel + ".raw";
			int fd = open(fileName.c_str(), O_WRONLY | O_CREAT, 0777);
			if (fd == -1)
			{
				std::string err = "could not create file " + fileName + ": " + strerror(errno);
				LOG_TRACE << err);
			}
			size_t rtn = write(fd, &(targetBuffer_[i][firstValidPoint_[i]]), actualPoints_[i] * sizeof(*(targetBuffer_[i])));
			if (rtn != actualPoints_[i] * sizeof(*(targetBuffer_[i])))
			{
				std::string err = "could not write file: " + fileName + ": " + strerror(errno);
				logger_.Fatal(err);
			}
			close(fd);
		}
#endif
  }
}

void CAqiris::ConfigureChannel(uint32_t ch,
                               double range,
                               double offset,
                               double impedance,
                               bool enabled)
{
  channelEnabled_[ch - 1] = enabled;

  std::string channel = "Channel" + boost::lexical_cast<std::string>(ch);

  LOG_TRACE << "-----------------------------------------------------";
  LOG_TRACE << "card " + cardModelName_ + " #" +
      boost::lexical_cast<std::string>(cardIndex_) + " " + channel +
      " configuration parameters:";
  LOG_TRACE << "-- range: " + boost::lexical_cast<std::string>(range) + "mV";
  LOG_TRACE << "-- offset: " + boost::lexical_cast<std::string>(offset) + "mV";
  LOG_TRACE
    << "-- impedance: " + boost::lexical_cast<std::string>(impedance) + "Ohms";
  LOG_TRACE << "-- enabled: " + boost::lexical_cast<std::string>(enabled);
  LOG_TRACE << "-----------------------------------------------------";

  ViStatus status = AgMD1_SetAttributeViBoolean(
    instrumentID_, channel.c_str(), AGMD1_ATTR_CHANNEL_ENABLED, enabled);
  if (status < 0)
    throw CCardEx(
      "cannot enable/disable channel , AgMD1_SetAttributeViReal64: " +
      get_error_message(instrumentID_, status));

  if (!enabled)
  {
    return;
  }

  SetInputRange(ch, range);
  SetAdjustableBias(ch, offset);

  status = AgMD1_SetAttributeViReal64(instrumentID_, channel.c_str(),
                                      AGMD1_ATTR_INPUT_IMPEDANCE, impedance);
  if (status < 0)
    throw CCardEx("cannot set input impedance, AgMD1_SetAttributeViReal64: " +
                  get_error_message(instrumentID_, status));
}

void CAqiris::ConfigureChannels(double range, double offset, double impedance)
{
  for (uint32_t i = 1; i <= nbChannels_; ++i)
  {
    ConfigureChannel(i, range, offset, impedance, true);
  }
}

void CAqiris::SetInputRange(uint32_t ch, double inputRange)
{
  if (ch < 1 || ch > nbChannels_)
  {
    std::string s = "channel number should be < " +
      boost::lexical_cast<std::string>(nbChannels_) + " and > 1";
    throw CCardEx(s);
  }

  std::string channel = "Channel" + boost::lexical_cast<std::string>(ch);

  ViStatus status = AgMD1_SetAttributeViReal64(instrumentID_, channel.c_str(),
                                               AGMD1_ATTR_VERTICAL_RANGE,
                                               inputRange * 1E-3);

  if (status < 0)
    throw CCardEx(get_error_message(instrumentID_, status));

  double range;
  status = AgMD1_GetAttributeViReal64(instrumentID_, channel.c_str(),
                                      AGMD1_ATTR_VERTICAL_RANGE, &range);

  LOG_TRACE << "-----------------------------------------------------";
  LOG_TRACE << "card " << cardModelName_ << " #" << cardIndex_ << " "
            << channel;
  LOG_TRACE << "-- range: " << range << "V";
  LOG_TRACE << "-----------------------------------------------------";
}

void CAqiris::SetInputRange(double inputRange)
{
  for (uint32_t i = 1; i <= nbChannels_; ++i)
  {
    SetInputRange(i, inputRange);
  }
}

void CAqiris::SetAdjustableBias(uint32_t ch, double bias)
{
  if (ch < 1 || ch > nbChannels_)
  {
    std::string s = "channel number should be < " +
      boost::lexical_cast<std::string>(nbChannels_) + " and > 1";
    throw CCardEx(s);
  }

  std::string channel = "Channel" + boost::lexical_cast<std::string>(ch);

  ViStatus status = AgMD1_SetAttributeViReal64(
    instrumentID_, channel.c_str(), AGMD1_ATTR_VERTICAL_OFFSET, -bias * 1E-3);

  if (status < 0)
    throw CCardEx(get_error_message(instrumentID_, status));

  double offset;
  status = AgMD1_GetAttributeViReal64(instrumentID_, channel.c_str(),
                                      AGMD1_ATTR_VERTICAL_OFFSET, &offset);

  LOG_TRACE << "-----------------------------------------------------";
  LOG_TRACE << "card " << cardModelName_ << " #" << cardIndex_ << " " + channel;
  LOG_TRACE << "-- offset: " << offset << "V";
  LOG_TRACE << "-----------------------------------------------------";
}

void CAqiris::SetAdjustableBias(double bias)
{
  for (uint32_t i = 1; i <= nbChannels_; ++i)
  {
    SetAdjustableBias(i, bias);
  }
}

void CAqiris::GetLsbValue(uint32_t ch, double *lsb)
{
  if (ch < 1 || ch > nbChannels_)
  {
    std::string s = "channel number should be < " +
      boost::lexical_cast<std::string>(nbChannels_) + " and > 1";
    throw CCardEx(s);
  }

  ViReal64 fullScale;
  std::string channel = "Channel" + boost::lexical_cast<std::string>(ch);

  ViStatus status = AgMD1_GetAttributeViReal64(
    instrumentID_, channel.c_str(), AGMD1_ATTR_VERTICAL_RANGE, &fullScale);

  if (status < 0)
    throw CCardEx(get_error_message(instrumentID_, status));

  *lsb = (double) (fullScale / (1 << resolution_));
}

double CAqiris::GetLsbValue(uint32_t ch)
{
  double lsb;

  GetLsbValue(ch, &lsb);

  return lsb;
}

void CAqiris::Calibrate()
{
  ViStatus status = AgMD1_SelfCalibrate(instrumentID_);

  if (status < 0)
    throw CCardEx(
      "card # " + boost::lexical_cast<std::string>(cardIndex_) +
      ": AgMD1_SelfCalibrate: " + get_error_message(instrumentID_, status));

  // perform a dummy acquisition to get calibrated fullscale and offset
  ArmTrigger();
  status = AgMD1_SendSoftwareTrigger(instrumentID_);
  status &= AgMD1_WaitForAcquisitionComplete(instrumentID_, 9000);

  if (cardModelName_ == "DC282")
  {
    std::vector<int16_t> buffer(1024);
    for (uint32_t i = 0; i < nbChannels_; ++i)
    {
      std::string channel = "Channel" + boost::lexical_cast<std::string>(i + 1);

      status &= AgMD1_FetchWaveformInt16(
        instrumentID_, channel.c_str(), 1024, &buffer[0], &(actualPoints_[i]),
        &(firstValidPoint_[i]), &initialXOffset_, &initialXTimeSeconds_,
        &initialXTimeFraction_, &xIncrement_, &scaleFactor_, &scaleOffset_);
      calibratedFullScale_[i] = static_cast<float>(scaleFactor_) *
        (1 << resolution_) * 1e3;
      calibratedOffset_[i] = static_cast<float>(scaleOffset_) * 1E3;
    }
  }
  else
  {
    std::vector<ViChar> buffer(1024);
    for (uint32_t i = 0; i < nbChannels_; ++i)
    {
      std::string channel = "Channel" + boost::lexical_cast<std::string>(i + 1);

      status &= AgMD1_FetchWaveformInt8(
        instrumentID_, channel.c_str(), 1024, &buffer[0], &(actualPoints_[i]),
        &(firstValidPoint_[i]), &initialXOffset_, &initialXTimeSeconds_,
        &initialXTimeFraction_, &xIncrement_, &scaleFactor_, &scaleOffset_);

      calibratedFullScale_[i] = static_cast<float>(scaleFactor_) *
        (1 << resolution_) * 1e3;
      calibratedOffset_[i] = static_cast<float>(scaleOffset_) * 1E3;
    }
  }
}

std::vector<float> &CAqiris::GetCalibratedFullScale()
{
  return calibratedFullScale_;
}

std::vector<float> &CAqiris::GetCalibratedOffset()
{
  return calibratedOffset_;
}

float CAqiris::GetCalibratedFullScale(int index)
{
  return calibratedFullScale_[index];
}

float CAqiris::GetCalibratedOffset(int index)
{
  return calibratedOffset_[index];
}

void CAqiris::CancelCalibration()
{
  ViStatus status = AgMD1_CalibrationCancel(instrumentID_);

  if (status < 0)
    throw CCardEx("AgMD1_CalibrationCancel: " +
                  get_error_message(instrumentID_, status));
}

int32_t CAqiris::GetCardIndex()
{
  return instrumentID_;
}

std::string CAqiris::GetCardModelName()
{
  return cardModelName_;
}

void CAqiris::GetAcquisition(int8_t *dumpBuffer)
{
  UNUSED(dumpBuffer);
  LOG_TRACE << "entering, exiting CAqiris::GetAcquisition";
}

} // namespace Acqiris
  // namespace Acqiris
} // namespace Agilent
#endif //#ifdef ACQIRIS
