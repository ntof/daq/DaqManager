
#include <cstdint>
#include <sstream>
#include <string>
#include <vector>

#include <boost/lexical_cast.hpp>

#include <unistd.h>

#include "DIMException.h"
#include "defines.h"
#include "logger.h"

#include "dis.hxx"

#ifdef ACQIRIS
#include "Acqiris/CCardAcqiris.h"

namespace Agilent {
namespace Acqiris {

bool CCardAcqiris::ready_ = false;
std::vector<std::shared_ptr<Dc240>> CCardAcqiris::dc240_;
std::vector<std::shared_ptr<Dc270>> CCardAcqiris::dc270_;
std::vector<std::shared_ptr<Dc282>> CCardAcqiris::dc282_;

CCardAcqiris::CCardAcqiris(uint32_t index) : cardIndex_(index)
{
  try
  {
    FindDevices();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  ready_ = true;
}

CCardAcqiris::~CCardAcqiris()
{
  ready_ = false;
}

void CCardAcqiris::FindDevices()
{
  if (ready_)
  {
    LOG_TRACE << "Exiting CCardAcqiris::FindDevices - cards already probed";
    return;
  }

  nbDevices_ = 0;
  uint32_t index = 0;
  while (1)
  {
    std::string resource = "PCI::INSTR" +
      boost::lexical_cast<std::string>(index++);
    ViSession instrID;
    ViStatus status;

    status = AgMD1_InitWithOptions((char *) resource.c_str(), VI_TRUE, VI_TRUE,
                                   "DriverSetup=CAL=0", &instrID);

    if (status != AGMD1_SUCCESS)
    {
      break;
    }

    char model[128];
    status = AgMD1_GetAttributeViString(
      instrID, "", AGMD1_ATTR_INSTRUMENT_MODEL, 127, model);
    if (status != AGMD1_SUCCESS)
    {
      char msg[256];
      AgMD1_error_message(instrID, status, msg);
      AgMD1_close(instrID);
      throw CCardEx("could not get instrument model" + std::string(msg));
    }

    if (std::string(model) == "DC240")
    {
      dc240_.push_back(std::make_shared<Dc240>(instrID, nbDevices_++));

      std::ostringstream oss;
      oss << "DC240 found by driver, slot number: "
          << dc240_.back()->GetSlotNumber();
      oss << ", serial number: " << GetSN(instrID);
      oss << ", installed options: " << std::string(GetOptions(instrID));
      LOG_TRACE << oss.str();

      continue;
    }
#ifndef DC282_8BIT_MODE
    if (std::string(model) == "DC270")
    {
      dc270_.push_back(std::make_shared<Dc270>(instrID, nbDevices_++));

      std::ostringstream oss;
      oss << "DC270 found by driver, slot number: "
          << dc270_.back()->GetSlotNumber();
      oss << ", serial number: " << GetSN(instrID);
      oss << ", installed options: " << std::string(GetOptions(instrID));
      LOG_TRACE << oss.str();

      continue;
    }

    if (std::string(model) == "DC282")
    {
      dc282_.push_back(std::make_shared<Dc282>(instrID, nbDevices_++));

      std::ostringstream oss;
      oss << "DC282 found by driver, slot number: "
          << dc282_.back()->GetSlotNumber();
      oss << ", serial number: " << GetSN(instrID);
      oss << ", installed options: " << std::string(GetOptions(instrID));
      LOG_TRACE << oss.str();

      continue;
    }
#else
    if (std::string(model) == "DC270" || std::string(model) == "DC282")
    {
      dc270_.push_back(std::make_shared<Dc270>(logger_, instrID, nbDevices_++));

      std::ostringstream oss;
      oss << "DC270/DC282 found by driver, slot number: "
          << dc270_.back()->GetSlotNumber();
      oss << ", serial number: " << GetSN(instrID);
      oss << ", installed options: " << std::string(GetOptions(instrID));
      LOG_TRACE << oss.str();

      continue;
    }
#endif

    AgMD1_close(instrID);
  }

  LOG_TRACE << boost::lexical_cast<std::string>(nbDevices_) +
      " Acqiris instruments found";
}

std::string CCardAcqiris::GetSN(ViSession instrID)
{
  int32_t sn;
  ViStatus status = AgMD1_GetAttributeViInt32(
    instrID, "", AGMD1_ATTR_INSTRUMENT_INFO_SERIAL_NUMBER, &sn);
  if (status != AGMD1_SUCCESS)
  {
    char msg[256];
    AgMD1_error_message(instrID, status, msg);
    AgMD1_close(instrID);
    throw CCardEx("could not get instrument's serial number" + std::string(msg));
  }

  return boost::lexical_cast<std::string>(sn);
}

std::string CCardAcqiris::GetOptions(ViSession instrID)
{
  char options[256];

  ViStatus status = AgMD1_GetAttributeViString(
    instrID, "", AGMD1_ATTR_INSTRUMENT_INFO_OPTIONS, 255, options);

  if (status != AGMD1_SUCCESS)
  {
    char msg[256];
    AgMD1_error_message(instrID, status, msg);
    AgMD1_close(instrID);
    throw CCardEx("could not get instrument's installed options" +
                  std::string(msg));
  }

  return std::string(options);
}

} // namespace Acqiris
} // namespace Agilent
#endif //#ifdef ACQIRIS
