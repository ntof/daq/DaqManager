#include <cstdint>
#include <sstream>
#include <string>
#include <vector>

#include <boost/lexical_cast.hpp>

#include <unistd.h>

#include "DIMException.h"
#include "logger.h"

#include "dis.hxx"

#ifdef ACQIRIS
#include "Acqiris/DC282/CCardAcqirisDC282.h"
#include "DaqTypes.h"

namespace Agilent {
namespace Acqiris {
CCardAcqirisDC282::CCardAcqirisDC282(std::string serialNumber,
                                     int cpuCore,
                                     uint32_t cardIndex,
                                     uint32_t crateIndex,
                                     ntof::dim::DIMState *state) :
  CCard("A282", serialNumber, cpuCore, cardIndex, crateIndex, state),
  CCardAcqiris(cardIndex)
{
  LOG_TRACE << "Entering CCardAcqirisDC282::CCardAcqirisDC282 with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  ClassInit();

  if (!ready_)
  {
    throw CCardEx("Acqiris driver not ready");
  }

  LOG_TRACE << "checking serial number ...";
  vectorIndex_ = 0;
  bool match = false;

  for (auto &card : dc282_)
  {
    if (serialNumber_ == GetSN(card->GetInstrumentId()))
    {
      match = true;
      break;
    }
    vectorIndex_++;
  }

  if (!match)
    throw CCardEx("serial number given in configuration file does not match "
                  "any installed card");
  else
    LOG_TRACE
      << "vector index: " + boost::lexical_cast<std::string>(vectorIndex_);

  LOG_TRACE << "Exiting CCardAcqirisDC282::CCardAcqirisDC282 with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

void CCardAcqirisDC282::ClassInit()
{
  if (ready_ == false)
  {
    strLastMsg_ = "";
    strLastError_ = "";
    lastErrorCode_ = 0;
    CCard::nbDevices_ = 0;
    nbInitDeviceFailed_ = 0;
  }

  // Get host's name
  char hostname[128] = {};
  if (gethostname(hostname, 128) != 0)
  {
    throw CCardEx("can not get host name");
  }
  hostName_ = std::string(hostname);

  // create zero suppression object for each channel
  try
  {
    channel_.reserve(Dc282::getNbChannels());
    for (uint i = 0; i < Dc282::getNbChannels(); ++i)
    {
      std::ostringstream oss;
      oss << hostName_ << "/CARD" << CCard::cardIndex_ << "/CHANNEL" << i;
      channel_.push_back(std::make_shared<CChannelAcqirisDC282>(
        ChannelConfig::makeLocation(1, CCard::crateIndex_, CCard::cardIndex_, i),
        oss.str(), CCard::state_));
    }
  }
  catch (const ntof::ZeroSuppression::CZeroSuppressionEx &ex)
  {
    std::string s = "cannot create zero suppression object";
    LOG_FATAL << s;
    throw CCardEx(s);
  }

  readOutDone_ = false;
  dumpDone_ = false;
}

CCardAcqirisDC282::~CCardAcqirisDC282()
{
  LOG_TRACE << "Entering CCardAcqirisDC282::~CCardAcqirisDC282 with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  Delete();

  dc282_.clear();

  LOG_TRACE << "Exiting CCardAcqirisDC282::~CCardAcqirisDC282 with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

uint32_t CCardAcqirisDC282::GetNbChannels()
{
  return dc282_[vectorIndex_]->getNbChannels();
}

bool CCardAcqirisDC282::IsDumpSequential()
{
  return true;
}

uint32_t CCardAcqirisDC282::GetNbBytesDumped()
{
  return dc282_[vectorIndex_]->GetNbBytesDumped();
}

// Configure driver
void CCardAcqirisDC282::configure()
{
  LOG_TRACE << "Entering CCardAcqirisDC282::configure for index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  if (ready_)
  {
    try
    {
      dc282_[vectorIndex_]->SetClockSource(CLOCK_TYPE_t::INTERNAL);
      dc282_[vectorIndex_]->SetTriggerMode(TRIG_SOURCE_t::EXT_TRIG1,
                                           TRIG_SLOPE_t::POSITIVE, 1.0);

      LOG_TRACE << "configuring channels ...";

      bool firstCheck = true;
      uint chRef = 0;

      for (auto &ch : channel_)
      {
        dc282_[vectorIndex_]->ConfigureChannel(
          ch->GetIndex() + 1, ch->getConfiguration()->header.fullScale,
          ch->getConfiguration()->header.offset,
          ch->getConfiguration()->inputImpedance,
          ch->getConfiguration()->enabled);

        if (ch->getConfiguration()->enabled == false)
          continue;

        ch->GetZeroSuppression().SetGainOffset(
          static_cast<float>(ch->getConfiguration()->header.fullScale) /
            static_cast<float>(1 << Dc282::GetResolutionBits()),
          static_cast<float>(ch->getConfiguration()->header.fullScale) /
            static_cast<float>(1 << Dc282::GetResolutionBits()),
          ch->getConfiguration()->header.offset);

        // time window is in ms, convert it to us and get equivalent number of
        // samples
        uint32_t tw = static_cast<uint32_t>(
          ch->getConfiguration()->header.zeroSuppressionStart * 1E-3 *
          ch->getConfiguration()->header.sampleRate);
        if (tw > (ch->getConfiguration()->header.sampleSize * 1000))
        {
          throw CCardEx(
            "zero suppression window cannot be higher than total sample size");
        }
        ch->GetZeroSuppression().SetTimeWindow(
          tw, ch->getConfiguration()->header.sampleSize * 1000);

        if (firstCheck == true)
        {
          dc282_[vectorIndex_]->SetAcquisitionParameters(
            ch->getConfiguration()->header.sampleRate, 1,
            ch->getConfiguration()->header.sampleSize);

          dc282_[vectorIndex_]->SetTriggerDelay(
            (uint32_t)(ch->getConfiguration()->header.delayTime));

          firstCheck = false;
          chRef = ch->GetIndex();
          continue;
        }

        if (ch->getConfiguration()->header.sampleRate !=
            channel_[chRef]->getConfiguration()->header.sampleRate)
          throw CCardEx("sampling rate is not consistent for all channels");

        if (ch->getConfiguration()->header.sampleSize !=
            channel_[chRef]->getConfiguration()->header.sampleSize)
          throw CCardEx("sampling size is not consistent for all channels");

        if (ch->getConfiguration()->header.delayTime !=
            channel_[chRef]->getConfiguration()->header.delayTime)
          throw CCardEx("delay time is not consistent for all channels");
      }
    }
    catch (const ntof::CChannelEx &ex)
    {
      throw CCardEx(ex.what());
    }
  }

  LOG_TRACE << "Exiting CCardAcqirisDC282::configure";
}

size_t CCardAcqirisDC282::GetNbDevices()
{
  return dc282_.size();
}

void CCardAcqirisDC282::ArmTrigger()
{
  readOutDone_ = false;
  dumpDone_ = false;

  try
  {
    dc282_[vectorIndex_]->ArmTrigger();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }
}

bool CCardAcqirisDC282::IsTriggered()
{
  bool b;

  try
  {
    b = dc282_[vectorIndex_]->IsTriggered();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  return b;
}

int CCardAcqirisDC282::GetResolution()
{
  return dc282_[vectorIndex_]->GetResolutionBits();
}

int16_t **CCardAcqirisDC282::GetAcquisitionBuffer()
{
  return dc282_[vectorIndex_]->GetAcquisitionBuffer();
}

uint32_t CCardAcqirisDC282::GetDumpBufferSize()
{
  return dc282_[vectorIndex_]->GetDumpBufferSize();
}

void CCardAcqirisDC282::DumpCardMemory()
{
  LOG_TRACE << "Entering CCardAcqirisDC282::DumpCardMemory for card "
            << CCard::cardIndex_;

  try
  {
#ifdef MEMORY_PRE_ALLOC
    dc282_[vectorIndex_]->SetAcquisitionBuffer(
      bufferPool_[bufferPoolIndex_]->buffer_.data());
    dc282_[vectorIndex_]->SetDumpMemoryBuffer(
      bufferPool_[bufferPoolIndex_]->dumpBuffer_);
#endif
    dc282_[vectorIndex_]->MemoryDump();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardAcqirisDC282::DumpCardMemory for card "
            << CCard::cardIndex_;
}

uint32_t CCardAcqirisDC282::GetChannelBufferSize()
{
  return dc282_[vectorIndex_]->GetChannelBufferSize();
}

uint32_t CCardAcqirisDC282::GetChannelAllocationSize()
{
  return dc282_[vectorIndex_]->GetChannelAllocationSize();
}

uint32_t CCardAcqirisDC282::GetCardIndex()
{
  return CCard::cardIndex_;
}

void CCardAcqirisDC282::SetChannelBufferFree()
{
  dc282_[vectorIndex_]->SetChannelBufferFree();
}

// Try to clean-up everything ...
void CCardAcqirisDC282::Delete()
{
  LOG_TRACE << "Entering CCardAcqirisDC282::Delete with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  if (ready_ == false)
  {
    LOG_TRACE << "driver instance is already NULL, exiting ...";
    return;
  }

  ready_ = false;

  LOG_TRACE << "Exiting CCardAcqirisDC282::Delete with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

void CCardAcqirisDC282::Calibrate()
{
  LOG_TRACE << "Entering CCardAcqirisDC282::Calibrate for card " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  try
  {
    dc282_[vectorIndex_]->Calibrate();
    for (auto &ch : channel_)
    {
      if (ch->getConfiguration()->enabled == false)

      {
        ch->SetCalibratedFullScale(5000);
        ch->SetCalibratedOffset(0);
        continue;
      }
      ch->SetCalibratedFullScale(
        dc282_[vectorIndex_]->GetCalibratedFullScale(ch->GetIndex()));
      ch->SetCalibratedOffset(
        dc282_[vectorIndex_]->GetCalibratedOffset(ch->GetIndex()) -
        ch->getConfiguration()->header.offset);
      LOG_DEBUG << "conf off: " << ch->getConfiguration()->header.offset
                << ", cal off: "
                << dc282_[vectorIndex_]->GetCalibratedOffset(ch->GetIndex());
    }
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardAcqirisDC282::Calibrate for card " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

void CCardAcqirisDC282::LoadDefaultConfiguration()
{
  LOG_TRACE
    << "Entering CCardAcqirisDC282::LoadDefaultConfiguration for index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  try
  {
    dc282_[vectorIndex_]->SetClockSource(CLOCK_TYPE_t::INTERNAL);
    dc282_[vectorIndex_]->SetAcquisitionParameters(500, 1, 8 * 1024);
    dc282_[vectorIndex_]->ConfigureChannels(5000.0, 0.0, 50.0);
    dc282_[vectorIndex_]->SetTriggerMode(TRIG_SOURCE_t::EXT_TRIG1,
                                         TRIG_SLOPE_t::POSITIVE, 1.0);
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  for (auto &ch : channel_)
  {
    ch->GetZeroSuppression().SetThreshold((int16_t) 30);
    ch->GetZeroSuppression().SetPulseDetectionType(
      ntof::ZeroSuppression::PULSE_TYPE_t::POSITIVE);
    ch->GetZeroSuppression().SetPrePost(128, 128);
  }

  LOG_TRACE
    << "Exiting CCardAcqirisDC282::LoadDefaultConfiguration for index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

bool CCardAcqirisDC282::HasOfflineParsing()
{
  return false;
}

} // namespace Acqiris
} // namespace Agilent
#endif //#ifdef ACQIRIS
