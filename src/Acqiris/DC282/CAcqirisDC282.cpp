#include <cstdint>
#include <ctime>
#include <fstream>
#include <iostream>
#include <vector>

#include <boost/algorithm/minmax.hpp>
#include <boost/algorithm/minmax_element.hpp>
#include <boost/program_options.hpp>

#include <fcntl.h>
#include <sys/time.h>
#include <unistd.h>

#include "CCard.h"
#include "logger.h"

#ifdef ACQIRIS
#include "Acqiris/DC282/CAcqirisDC282.h"

using namespace boost;

namespace Agilent {
namespace Acqiris {

Dc282::Dc282(ViSession instrID, uint32_t cardIndex) :
  CAqiris("DC282", instrID, cardIndex, getNbChannels(), GetResolutionBits())

{
  std::ostringstream oss;

  oss << "Entering Dc282::Dc282 with index " << cardIndex_;
  oss << ", intrumentID " << instrumentID_;

  LOG_TRACE << oss.str();

  init();
}

Dc282::~Dc282() {}

void Dc282::init()
{
  targetBuffer_ = NULL;
  dumpBuffer_ = NULL;
  nbValidSamples_ = 0;
}

uint Dc282::getNbChannels()
{
  return 4;
}

uint Dc282::GetResolutionBits()
{
  return 16;
}

void Dc282::MemoryDump()
{
  MemoryDump16();
}

} // namespace Acqiris
} // namespace Agilent
#endif //#ifdef ACQIRIS
