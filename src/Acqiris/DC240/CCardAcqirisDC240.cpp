#include <cstdint>
#include <sstream>
#include <string>
#include <vector>

#include <boost/lexical_cast.hpp>

#include <unistd.h>

#include "DIMException.h"
#include "logger.h"

#include "dis.hxx"

#ifdef ACQIRIS
#include "Acqiris/DC240/CCardAcqirisDC240.h"
#include "DaqTypes.h"

namespace Agilent {
namespace Acqiris {

CCardAcqirisDC240::CCardAcqirisDC240(std::string serialNumber,
                                     int cpuCore,
                                     uint32_t cardIndex,
                                     uint32_t crateIndex,
                                     ntof::dim::DIMState *state) :
  CCard("A240", serialNumber, cpuCore, cardIndex, crateIndex, state),
  CCardAcqiris(cardIndex)
{
  LOG_TRACE << "Entering CCardAcqirisDC240::CCardAcqirisDC240 with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  ClassInit();

  if (!ready_)
  {
    throw CCardEx("Acqiris driver not ready");
  }

  LOG_TRACE << "checking serial number ...";
  vectorIndex_ = 0;
  bool match = false;

  for (auto &card : dc240_)
  {
    if (serialNumber_ == GetSN(card->GetInstrumentId()))
    {
      match = true;
      break;
    }
    vectorIndex_++;
  }

  if (!match)
    throw CCardEx("serial number given in configuration file does not match "
                  "any installed card");
  else
    LOG_TRACE
      << "vector index: " + boost::lexical_cast<std::string>(vectorIndex_);

  LOG_TRACE << "Exiting CCardAcqirisDC240::CCardAcqirisDC240 with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

void CCardAcqirisDC240::ClassInit()
{
  if (ready_ == false)
  {
    strLastMsg_ = "";
    strLastError_ = "";
    lastErrorCode_ = 0;
    CCard::nbDevices_ = 0;
    nbInitDeviceFailed_ = 0;
  }

  // Get host's name
  char hostname[128] = {};
  if (gethostname(hostname, 128) != 0)
  {
    throw CCardEx("can not get host name");
  }
  hostName_ = std::string(hostname);

  // create channel collection for the card
  try
  {
    channel_.reserve(Dc240::getNbChannels());
    for (uint i = 0; i < Dc240::getNbChannels(); ++i)
    {
      std::ostringstream oss;
      oss << hostName_ << "/CARD" << CCard::cardIndex_ << "/CHANNEL" << i;
      channel_.push_back(std::make_shared<CChannelAcqirisDC240>(
        ChannelConfig::makeLocation(1, CCard::crateIndex_, CCard::cardIndex_, i),
        oss.str(), CCard::state_));
    }
  }
  catch (const ntof::ZeroSuppression::CZeroSuppressionEx &ex)
  {
    std::string s = "cannot create zero suppression object";
    LOG_FATAL << s;
    throw CCardEx(s);
  }

  readOutDone_ = false;
  dumpDone_ = false;
}

CCardAcqirisDC240::~CCardAcqirisDC240()
{
  LOG_TRACE << "Entering CCardAcqirisDC240::~CCardAcqirisDC240 with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  Delete();
  dc240_.clear();

  LOG_TRACE << "Exiting CCardAcqirisDC240::~CCardAcqirisDC240 with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

uint32_t CCardAcqirisDC240::GetNbChannels()
{
  return dc240_[vectorIndex_]->getNbChannels();
}

uint32_t CCardAcqirisDC240::GetNbBytesDumped()
{
  return dc240_[vectorIndex_]->GetNbBytesDumped();
}

bool CCardAcqirisDC240::IsDumpSequential()
{
  return true;
}

// Configure driver
void CCardAcqirisDC240::configure()
{
  LOG_TRACE << "Entering CCardAcqirisDC240::configure for index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  if (ready_)
  {
    try
    {
      dc240_[vectorIndex_]->SetClockSource(CLOCK_TYPE_t::INTERNAL);
      dc240_[vectorIndex_]->SetTriggerMode(TRIG_SOURCE_t::EXT_TRIG1,
                                           TRIG_SLOPE_t::POSITIVE, 1.5);

      LOG_TRACE << "configuring channels ...";

      bool firstCheck = true;
      uint chRef = 0;

      for (auto &ch : channel_)
      {
        dc240_[vectorIndex_]->ConfigureChannel(
          ch->GetIndex() + 1, ch->getConfiguration()->header.fullScale,
          ch->getConfiguration()->header.offset,
          ch->getConfiguration()->inputImpedance,
          ch->getConfiguration()->enabled);

        if (ch->getConfiguration()->enabled == false)
          continue;

        ch->GetZeroSuppression().SetGainOffset(
          static_cast<float>(ch->getConfiguration()->header.fullScale) /
            static_cast<float>(1 << Dc240::GetResolutionBits()),
          static_cast<float>(ch->getConfiguration()->header.fullScale) /
            static_cast<float>(1 << Dc240::GetResolutionBits()),
          ch->getConfiguration()->header.offset);

        // time window is in ms, convert it to us and get equivalent number of
        // samples
        uint32_t tw = static_cast<uint32_t>(
          ch->getConfiguration()->header.zeroSuppressionStart * 1E-3 *
          ch->getConfiguration()->header.sampleRate);
        if (tw > (ch->getConfiguration()->header.sampleSize * 1000))
        {
          throw CCardEx(
            "zero suppression window cannot be higher than total sample size");
        }
        ch->GetZeroSuppression().SetTimeWindow(
          tw, ch->getConfiguration()->header.sampleSize * 1000);

        if (firstCheck == true)
        {
          dc240_[vectorIndex_]->SetAcquisitionParameters(
            ch->getConfiguration()->header.sampleRate, 1,
            ch->getConfiguration()->header.sampleSize);

          dc240_[vectorIndex_]->SetTriggerDelay(
            (uint32_t)(ch->getConfiguration()->header.delayTime));

          firstCheck = false;
          chRef = ch->GetIndex();
          continue;
        }

        if (ch->getConfiguration()->header.sampleRate !=
            channel_[chRef]->getConfiguration()->header.sampleRate)
          throw CCardEx("sampling rate is not consistent for all channels");

        if (ch->getConfiguration()->header.sampleSize !=
            channel_[chRef]->getConfiguration()->header.sampleSize)
          throw CCardEx("sampling size is not consistent for all channels");

        if (ch->getConfiguration()->header.delayTime !=
            channel_[chRef]->getConfiguration()->header.delayTime)
          throw CCardEx("delay time is not consistent for all channels");
      }
    }
    catch (const ntof::CChannelEx &ex)
    {
      throw CCardEx(ex.what());
    }
  }

  LOG_TRACE << "Exiting CCardAcqirisDC240::configure";
}

size_t CCardAcqirisDC240::GetNbDevices()
{
  return dc240_.size();
}

void CCardAcqirisDC240::AllocateAcquisitionBuffer()
{
  try
  {
    dc240_[vectorIndex_]->AllocateChannelMemoryBuffers();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }
}

void CCardAcqirisDC240::ArmTrigger()
{
  readOutDone_ = false;
  dumpDone_ = false;

  try
  {
    dc240_[vectorIndex_]->ArmTrigger();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }
}

bool CCardAcqirisDC240::IsTriggered()
{
  bool b;

  try
  {
    b = dc240_[vectorIndex_]->IsTriggered();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  return b;
}

int CCardAcqirisDC240::GetResolution()
{
  return dc240_[vectorIndex_]->GetResolutionBits();
}

int16_t **CCardAcqirisDC240::GetAcquisitionBuffer()
{
  return dc240_[vectorIndex_]->GetAcquisitionBuffer();
}

uint32_t CCardAcqirisDC240::GetDumpBufferSize()
{
  return dc240_[vectorIndex_]->GetDumpBufferSize();
}

int8_t *CCardAcqirisDC240::GetMemoryDumpBuffer()
{
  return dc240_[vectorIndex_]->GetMemoryDumpBuffer();
}

void CCardAcqirisDC240::SetDumpMemoryBuffer(int8_t *buffer)
{
  dc240_[vectorIndex_]->SetDumpMemoryBuffer(buffer);
}

void CCardAcqirisDC240::DumpCardMemory()
{
  LOG_TRACE << "Entering CCardAcqirisDC240::DumpCardMemory for card " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  try
  {
#ifdef MEMORY_PRE_ALLOC
    dc240_[vectorIndex_]->SetAcquisitionBuffer(
      bufferPool_[bufferPoolIndex_]->buffer_.data());
    dc240_[vectorIndex_]->SetDumpMemoryBuffer(
      bufferPool_[bufferPoolIndex_]->dumpBuffer_);
#endif
    dc240_[vectorIndex_]->MemoryDump();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardAcqirisDC240::DumpCardMemory for card " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

uint32_t CCardAcqirisDC240::GetChannelBufferSize()
{
  return dc240_[vectorIndex_]->GetChannelBufferSize();
}

uint32_t CCardAcqirisDC240::GetCardIndex()
{
  return CCard::cardIndex_;
}

void CCardAcqirisDC240::SetChannelBufferFree()
{
  dc240_[vectorIndex_]->SetChannelBufferFree();
}

// Try to clean-up everything ...
void CCardAcqirisDC240::Delete()
{
  LOG_TRACE << "Entering CCardAcqirisDC240::Delete with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  if (ready_ == false)
  {
    LOG_TRACE << "driver instance is already NULL, exiting ...";
    return;
  }

  ready_ = false;

  LOG_TRACE << "Exiting CCardAcqirisDC240::Delete with index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

void CCardAcqirisDC240::Calibrate()
{
  LOG_TRACE << "Entering CCardAcqirisDC240::Calibrate for card " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  try
  {
    dc240_[vectorIndex_]->Calibrate();
    for (auto &ch : channel_)
    {
      if (ch->getConfiguration()->enabled == false)

      {
        ch->SetCalibratedFullScale(5000);
        ch->SetCalibratedOffset(0);
        continue;
      }
      ch->SetCalibratedFullScale(
        dc240_[vectorIndex_]->GetCalibratedFullScale(ch->GetIndex()));
      ch->SetCalibratedOffset(
        dc240_[vectorIndex_]->GetCalibratedOffset(ch->GetIndex()) -
        ch->getConfiguration()->header.offset);
    }
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardAcqirisDC240::Calibrate for card " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}

void CCardAcqirisDC240::LoadDefaultConfiguration()
{
  LOG_TRACE
    << "Entering CCardAcqirisDC240::LoadDefaultConfiguration for index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);

  try
  {
    dc240_[vectorIndex_]->SetClockSource(CLOCK_TYPE_t::INTERNAL);
    dc240_[vectorIndex_]->SetAcquisitionParameters(500, 1, 16 * 1024);
    dc240_[vectorIndex_]->ConfigureChannels(5000.0, 0.0, 50.0);
    dc240_[vectorIndex_]->SetTriggerMode(TRIG_SOURCE_t::EXT_TRIG1,
                                         TRIG_SLOPE_t::POSITIVE, 1.5);
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  for (auto &ch : channel_)
  {
    ch->GetZeroSuppression().SetThreshold((int16_t) 30);
    ch->GetZeroSuppression().SetPulseDetectionType(
      ntof::ZeroSuppression::PULSE_TYPE_t::POSITIVE);
    ch->GetZeroSuppression().SetPrePost(128, 128);
  }

  LOG_TRACE
    << "Exiting CCardAcqirisDC240::LoadDefaultConfiguration for index " +
      boost::lexical_cast<std::string>(CCard::cardIndex_);
}
/*
 void CCardAcqirisDC240::OfflineParsing(int8_t* dumpBuffer)
 {
 LOG_TRACE <<
 "Entering CCardAcqirisDC240::OfflineParsing for card "
 + boost::lexical_cast<std::string>(CCard::cardIndex_));

 if (dumpBuffer != NULL)
 {
 try
 {
 dc240_[vectorIndex_]->GetAcquisition(dumpBuffer);
 }
 catch (const CCardEx& ex)
 {
 throw CCardEx(ex.what());
 }
 }
 else
 {
 throw CCardEx("could not perform offline parsing, buffer is NULL");
 }

 LOG_TRACE <<
 "Exiting CCardAcqirisDC240::OfflineParsing for card "
 + boost::lexical_cast<std::string>(CCard::cardIndex_);
 }
 */
bool CCardAcqirisDC240::HasOfflineParsing()
{
  return false;
}

} // namespace Acqiris
} // namespace Agilent
#endif //#ifdef ACQIRIS
