#include <cstdint>
#include <ctime>
#include <fstream>
#include <iostream>
#include <vector>

#include <boost/algorithm/minmax.hpp>
#include <boost/algorithm/minmax_element.hpp>
#include <boost/program_options.hpp>

#include <fcntl.h>
#include <sys/time.h>
#include <unistd.h>

#include "CCard.h"
#include "logger.h"

#ifdef ACQIRIS
#include "Acqiris/DC240/CAcqirisDC240.h"

using namespace boost;

namespace Agilent {
namespace Acqiris {

Dc240::Dc240(ViSession instrID, uint32_t cardIndex) :
  CAqiris("DC240", instrID, cardIndex, getNbChannels(), GetResolutionBits())

{
  std::ostringstream oss;

  oss << "Entering Dc240::Dc240 with index " << cardIndex_;
  oss << ", intrumentID " << instrumentID_;

  LOG_TRACE << oss.str();

  init();
}

Dc240::~Dc240() {}

void Dc240::init()
{
  targetBuffer_ = NULL;
  dumpBuffer_ = NULL;
  nbValidSamples_ = 0;
}

uint Dc240::getNbChannels()
{
  return 2;
}

uint Dc240::GetResolutionBits()
{
  return 8;
}

void Dc240::MemoryDump()
{
  MemoryDump8();
}
} // namespace Acqiris
} // namespace Agilent
#endif //#ifdef ACQIRIS
