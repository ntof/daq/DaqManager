
#include "logger.h"

#include <boost/log/core/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/expressions/formatters/date_time.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup.hpp>

namespace logging = boost::log;
namespace sinks = boost::log::sinks;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace attrs = boost::log::attributes;
namespace keywords = boost::log::keywords;

void InitLogger(const std::string &logPath,
                logging::trivial::severity_level level)
{
  logging::add_common_attributes();

  logging::formatter formatter = expr::stream
    << std::setw(7) << std::setfill('0') << expr::attr<unsigned int>("LineID")
    << std::setfill(' ') << " | "
    << expr::format_date_time<boost::posix_time::ptime>("TimeStamp",
                                                        "%Y-%m-%d %H:%M:%S.%f")
    << " "
    << "[" << expr::attr<std::string>("Scope") << "]"
    << "[" << logging::trivial::severity << "]"
    << " - " << expr::message;

  logging::add_file_log(
    keywords::file_name = logPath +
      "%Y-%m-%d_%H-%M-%S.%N.log",              // file name pattern
    keywords::rotation_size = 8 * 1024 * 1024, // rotate files every 8 MB
    keywords::auto_flush = true)
    ->set_formatter(formatter);

  logging::add_console_log(std::cout)->set_formatter(formatter);

  logging::core::get()->set_filter(logging::trivial::severity >= level);
  logging::core::get()->add_global_attribute("Scope", attrs::named_scope());
}
