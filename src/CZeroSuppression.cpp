/*
 * CZeroSuppresion.cpp
 *
 *  Created on: Nov 6, 2014
 *      Author: pperonna
 */

#include "CZeroSuppression.h"

#include <iostream>
#include <string>
#include <vector>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <immintrin.h>

#include "logger.h"

#ifdef USE_SSE
// Number of left shifts equivalent to the number of samples in a seeData
// element (sizeof(__m128i) / sizeof(int16_t))
#define SSE_DATA_SHIFT 3
#endif

namespace ntof {
namespace ZeroSuppression {
CZeroSuppression::CZeroSuppression(uint32_t cardIndex, uint32_t channelIndex) :
  cardIndex_(cardIndex),
  channelIndex_(channelIndex)
{
  LOG_TRACE << "Entering CZeroSuppression::CZeroSuppression with index " +
      boost::lexical_cast<std::string>(cardIndex_) + ":" +
      boost::lexical_cast<std::string>(channelIndex_);

  data_ = nullptr;
  result_ = nullptr;
  threshold_ = 40;
  gain_ = 1.0;
  calibratedGain_ = 1.0;
  offset_ = 0.0;
  size_ = 0;
  pre_ = 64;
  post_ = 256;
  pulseDetectionType_ = PULSE_TYPE_t::POSITIVE;
  resolution_ = 12;
  timeWindow_ = 0;
  thresholdCode_ = 0;
  isZsOff_ = false;

  LOG_TRACE << "Exiting CZeroSuppression::CZeroSuppression with index " +
      boost::lexical_cast<std::string>(cardIndex_) + ":" +
      boost::lexical_cast<std::string>(channelIndex_);
}

CZeroSuppression::~CZeroSuppression() {}

uint32_t CZeroSuppression::GetChannelIndex()
{
  return channelIndex_;
}

uint32_t CZeroSuppression::GetCardIndex()
{
  return cardIndex_;
}

// http://stackoverflow.com/questions/23077025/linear-search-through-uint64-with-sse
// http://stackoverflow.com/questions/23590610/find-index-of-maximum-element-in-x86-simd-vector
// https://software.intel.com/sites/landingpage/IntrinsicsGuide/#

int8_t *CZeroSuppression::MultipleMaster()
{
  return 0;
}

void CZeroSuppression::SingleMaster(
  const int16_t *__restrict data,
  uint64_t nbSamples,
  std::vector<PULSE_DATA_t> &masterPulseCollection,
  const ChannelConfig &channelConfig,
  const ChannelConfig &masterChannelConfig)
{
  LOG_TRACE << "Entering CZeroSuppression::SingleMaster with index "
            << boost::lexical_cast<std::string>(cardIndex_) + ":"
            << boost::lexical_cast<std::string>(channelIndex_);

  data_ = static_cast<int16_t *>(__builtin_assume_aligned(data, 32));
  size_ = nbSamples;

  pulseData_.clear();

  if (data_ == nullptr)
  {
    throw CZeroSuppressionEx("data buffer set to nullptr !");
  }

  if (timeWindow_ > 0)
  {
    PULSE_DATA_t pulseData;
    pulseData.data = data_;
    pulseData.length = timeWindow_ - 1;
    pulseData.isNoSuppressionWindow = true;
    pulseData_.push_back(pulseData);
  }

  for (auto &masterPulse : masterPulseCollection)
  {
    PULSE_DATA_t pulseData;

    if (masterPulse.length == 0 || masterPulse.isNoSuppressionWindow)
      continue;

    pulseData.timeStamp = static_cast<int64_t>(masterPulse.timeStamp /
                                               masterChannelConfig.sampleRate *
                                               channelConfig.sampleRate);

    if (pulseData.timeStamp > channelConfig.sampleSize * 1000)
      break;

    // sample length for master
    pulseData.length = masterPulse.length - masterChannelConfig.postSample -
      masterChannelConfig.preSample;

    // pulse duration for master
    pulseData.length = static_cast<int64_t>(pulseData.length /
                                            masterChannelConfig.sampleRate *
                                            channelConfig.sampleRate);

    double length_ns = pulseData.length /
      (masterChannelConfig.sampleRate / 1000);

    // sample length for slave from pulse duration
    pulseData.length = static_cast<int64_t>(length_ns *
                                            (channelConfig.sampleRate / 1000));

    pulseData.length += channelConfig.postSample + channelConfig.preSample;

    pulseData.startIndex = pulseData.timeStamp - channelConfig.preSample;
    if (pulseData.startIndex < 0)
      pulseData.startIndex = 0;

    // check if the pulse is outside the no-zero suppression window
    if (timeWindow_ > 0)
    {
      if (pulseData.startIndex + pulseData.length <= timeWindow_)
        continue;
    }

    pulseData.data = &(data_[pulseData.startIndex]);

    // check for overflow, exits in case of
    if (pulseData.startIndex + pulseData.length >
        channelConfig.sampleSize * 1000)
    {
      LOG_DEBUG << "pulse overflow for slave channel";

      pulseData.length = channelConfig.sampleSize * 1000 - pulseData.startIndex;

      pulseData_.push_back(pulseData);
      break;
    }

    // if a previous pulse exists, check for overlap
    if (pulseData_.size() > 0)
    {
      if (pulseData.startIndex <
          (pulseData_.back().startIndex + pulseData_.back().length))
      {
        // update present pulse parameters with previous's ones
        pulseData.length = &(pulseData.data[pulseData.length - 1]) -
          pulseData_.back().data;
        pulseData.data = pulseData_.back().data;
        pulseData.startIndex = pulseData_.back().startIndex;
        pulseData.timeStamp = pulseData_.back().timeStamp;
        pulseData.isNoSuppressionWindow = pulseData_.back().isNoSuppressionWindow;
        // previous pulse is deleted
        pulseData_.pop_back();
      }
    }

    // last check to avoid segmentation fault because underflow
    if (pulseData.length > 0)
      pulseData_.push_back(pulseData);
  }

  LOG_TRACE << "Exiting CZeroSuppression::SingleMaster with index "
            << boost::lexical_cast<std::string>(cardIndex_) + ":"
            << boost::lexical_cast<std::string>(channelIndex_);
}

boost::optional<uint64_t> CZeroSuppression::PerformZeroSuppression(
  const int16_t *__restrict data,
  uint64_t nbSamples,
  const int16_t *__restrict result)
{
  LOG_TRACE << "Entering CZeroSuppression::PerformZeroSuppression with index "
            << boost::lexical_cast<std::string>(cardIndex_) + ":"
            << boost::lexical_cast<std::string>(channelIndex_);

  data_ = static_cast<int16_t *>(__builtin_assume_aligned(data, 32));
  result_ = static_cast<int16_t *>(__builtin_assume_aligned(result, 512));
  size_ = nbSamples;

  LOG_TRACE << "performing zero suppression on " +
      boost::lexical_cast<std::string>(size_) + " samples";

#ifdef CHARACTERIZATION
  thresholdCode_ = (threshold_ + offset_) / gain_;
#else
  threshold_ = thresholdCode_ * calibratedGain_ + offset_;
#endif

#ifdef CHARACTERIZATION
  LOG_TRACE << "- zero suppression parameters:";
  LOG_TRACE << "-- pre = " << boost::lexical_cast<std::string>(pre_);
  LOG_TRACE << "-- post = " << boost::lexical_cast<std::string>(post_);
  LOG_TRACE << "-- threshold = " << boost::lexical_cast<std::string>(threshold_)
            << " mV";
  LOG_TRACE << "-- cal gain = "
            << boost::str(boost::format("%.3f mV") % (calibratedGain_));
  LOG_TRACE << "-- gain = " << boost::str(boost::format("%.3f mV") % (gain_));
  LOG_TRACE << "-- offset = "
            << boost::str(boost::format("%.0f mV") % (offset_));
  LOG_TRACE << "-- threshold in ADC code = "
            << boost::lexical_cast<std::string>(thresholdCode_);
  LOG_TRACE << "-- time window = " << timeWindow_;
  LOG_TRACE << "-- is zero suppression off: " << isZsOff_;
#endif

  pulseData_.clear();

  if (data_ == nullptr)
  {
    throw CZeroSuppressionEx("data buffer set to nullptr !");
  }
  //__builtin_assume(dataIndex % 16 == 0);

  uint64_t dataIndex = 0;

  if (timeWindow_ > 0)
  {
    PULSE_DATA_t pulseData;
    pulseData.data = data_;
    pulseData.length = timeWindow_ - 1;
    pulseData.isNoSuppressionWindow = true;
    pulseData_.push_back(pulseData);

    dataIndex += pulseData.length;
  }

  if (isZsOff_)
    return boost::none;

  if (dataIndex >= size_ - 2)
  {
    LOG_TRACE << "Exiting CZeroSuppression::PerformZeroSuppression with index "
              << boost::lexical_cast<std::string>(cardIndex_) + ":"
              << boost::lexical_cast<std::string>(channelIndex_)
              << ", zero suppression is off";
    return boost::none;
  }

  {
    // create a sentinel to avoid a test on the loop size
    int16_t lastSamples[2] = {data_[size_ - 2], data_[size_ - 1]};
    data_[size_ - 2] = thresholdCode_;
    if (pulseDetectionType_ == PULSE_TYPE_t::NEGATIVE)
      // end of pulse
      data_[size_ - 1] = static_cast<int16_t>(thresholdCode_ + 2);
    else
      data_[size_ - 1] = static_cast<int16_t>(thresholdCode_ - 2);

#ifdef USE_SSE
    const __m128i *sseData = (const __m128i *) data_;

    const __m128i sseThr = _mm_set1_epi16(static_cast<int16_t>(thresholdCode_));

    uint64_t sseDataIndex = 0;
    uint64_t lastSamplePos = dataIndex; // Position of the last sample examined
                                        // in data_
    uint64_t currSamplePos = 0; // Position of the current sample examined in
                                // data_
#endif

    while (true)
    {
      PULSE_DATA_t pulseData;

      // searching for start of pulse
      bool found = false;
      if (pulseDetectionType_ == PULSE_TYPE_t::NEGATIVE)
      {
        while (true)
        {
#ifndef USE_SSE
          if (data_[dataIndex] <= thresholdCode_)
            break;

          dataIndex++;

#else
          // SSE does not have less than or equal to instruction for
          // integer so we add 1 to threshold
          const __m128i sseNegPulseThr = _mm_set1_epi16(
            static_cast<int16_t>(thresholdCode_ + 1));

          __m128i cmp0 = _mm_cmplt_epi16(sseData[sseDataIndex], sseNegPulseThr);

          __m128i cmp1 = _mm_cmplt_epi16(sseData[sseDataIndex + 1],
                                         sseNegPulseThr);

          __m128i pack = _mm_packs_epi16(cmp0, cmp1);

          int sseResult = _mm_movemask_epi8(pack);

          while (sseResult != 0)
          {
            currSamplePos = (sseDataIndex << SSE_DATA_SHIFT) +
              __builtin_ffs(sseResult);
            if (currSamplePos > lastSamplePos)
            {
              dataIndex = currSamplePos - 1;
              lastSamplePos = dataIndex; // update last position
              LOG_TRACE << "negative pulse found at sse data index: " +
                  boost::lexical_cast<std::string>(sseDataIndex);
              LOG_TRACE << "data index = " +
                  boost::lexical_cast<std::string>(dataIndex);
              found = true;
              break;
            }

            sseResult &= ~(1 << (__builtin_ffs(sseResult) - 1));
          }
          if (found)
            break;

          sseDataIndex += 2;
#endif
        }
      }

      else
      {
        while (true)
        {
#ifndef USE_SSE
          if (data_[dataIndex] >= thresholdCode_)
            break;

          dataIndex++;
#else
          // SSE does not have greater than or equal to instruction for
          // integer so we sub 1 to threshold
          const __m128i ssePosPulseThr = _mm_set1_epi16(
            static_cast<int16_t>(thresholdCode_ - 1));

          __m128i cmp0 = _mm_cmpgt_epi16(sseData[sseDataIndex], ssePosPulseThr);

          __m128i cmp1 = _mm_cmpgt_epi16(sseData[sseDataIndex + 1],
                                         ssePosPulseThr);

          __m128i pack = _mm_packs_epi16(cmp0, cmp1);

          int sseResult = _mm_movemask_epi8(pack);

          while (sseResult != 0)
          {
            currSamplePos = (sseDataIndex << SSE_DATA_SHIFT) +
              __builtin_ffs(sseResult);
            if (currSamplePos > lastSamplePos)
            {
              dataIndex = currSamplePos - 1;
              lastSamplePos = dataIndex; // update last position
              LOG_TRACE << "positive pulse found at sse data index: " +
                  boost::lexical_cast<std::string>(sseDataIndex);
              LOG_TRACE << "data index = " +
                  boost::lexical_cast<std::string>(dataIndex);
              found = true;
              break;
            }

            sseResult &= ~(1 << (__builtin_ffs(sseResult) - 1));
          }
          if (found)
            break;

          sseDataIndex += 2;
#endif
        }
      }

      // pulse not found
      if (dataIndex >= size_ - 2)
      {
        break;
      }

      pulseData.timeStamp = dataIndex;

      // check if data index is valid with pre-samples
      if (static_cast<int64_t>(pulseData.timeStamp - pre_) <= 0)
      {
        pulseData.data = data_;
        pulseData.startIndex = 0;
      }
      else
      {
        pulseData.data = &(data_[pulseData.timeStamp - pre_]);
        pulseData.startIndex = pulseData.timeStamp - pre_;
      }

      // searching for end of pulse
      found = false;
      if (pulseDetectionType_ == PULSE_TYPE_t::NEGATIVE)
      {
        while (true)
        {
#ifndef USE_SSE
          if (data_[dataIndex] > thresholdCode_)
            break;
          dataIndex++;
#else
          __m128i cmp0 = _mm_cmpgt_epi16(sseData[sseDataIndex], sseThr);

          __m128i cmp1 = _mm_cmpgt_epi16(sseData[sseDataIndex + 1], sseThr);

          __m128i pack = _mm_packs_epi16(cmp0, cmp1);

          int sseResult = _mm_movemask_epi8(pack);

          while (sseResult != 0)
          {
            currSamplePos = (sseDataIndex << SSE_DATA_SHIFT) +
              __builtin_ffs(sseResult);
            if (currSamplePos > lastSamplePos)
            {
              dataIndex = currSamplePos - 1;
              lastSamplePos = dataIndex; // update last position
              LOG_TRACE << "negative pulse ending at sse data index: " +
                  boost::lexical_cast<std::string>(sseDataIndex);
              LOG_TRACE << "data index = " +
                  boost::lexical_cast<std::string>(dataIndex);
              found = true;
              break;
            }

            sseResult &= ~(1 << (__builtin_ffs(sseResult) - 1));
          }
          if (found)
            break;

          sseDataIndex += 2;
#endif
        }
      }
      else
      {
        while (true)
        {
#ifndef USE_SSE
          if (data_[dataIndex] < thresholdCode_)
            break;
          dataIndex++;
#else
          __m128i cmp0 = _mm_cmplt_epi16(sseData[sseDataIndex], sseThr);

          __m128i cmp1 = _mm_cmplt_epi16(sseData[sseDataIndex + 1], sseThr);

          __m128i pack = _mm_packs_epi16(cmp0, cmp1);

          int sseResult = _mm_movemask_epi8(pack);

          while (sseResult != 0)
          {
            currSamplePos = (sseDataIndex << SSE_DATA_SHIFT) +
              __builtin_ffs(sseResult);
            if (currSamplePos > lastSamplePos)
            {
              dataIndex = currSamplePos - 1;
              lastSamplePos = dataIndex; // update last position
              LOG_TRACE << "positive pulse ending at sse data index: " +
                  boost::lexical_cast<std::string>(sseDataIndex);
              LOG_TRACE << "data index = " +
                  boost::lexical_cast<std::string>(dataIndex);
              found = true;
              break;
            }

            sseResult &= ~(1 << (__builtin_ffs(sseResult) - 1));
          }
          if (found)
            break;

          sseDataIndex += 2;
#endif
        }
      }

      // store threshold crossing point index
      pulseData.endIndex = dataIndex;

      // if a previous pulse exists, check for overlap
      if (pulseData_.size() > 0)
      {
        if (pulseData.startIndex <
            (pulseData_.back().startIndex + pulseData_.back().length))
        {
          /*if (pulseData_.back().isNoSuppressionWindow)
          {
                  pulseData.startIndex = pulseData_.back().startIndex +
          pulseData_.back().length;
          }
          else*/
          {
            // update present pulse parameters with previous's ones
            pulseData.data = pulseData_.back().data;
            pulseData.startIndex = pulseData_.back().startIndex;
            pulseData.timeStamp = pulseData_.back().timeStamp;
            pulseData.isNoSuppressionWindow =
              pulseData_.back().isNoSuppressionWindow;
            // previous pulse is deleted
            pulseData_.pop_back();
          }
        }
      }

      // check for overflow
      if ((static_cast<uint64_t>(pulseData.endIndex) + post_) >= size_)
      {
        LOG_TRACE << "pulse overflow";

        pulseData.length = size_ - pulseData.startIndex - 1;

        // last check to avoid segmentation fault because underflow
        if (pulseData.length > 0)
          pulseData_.push_back(pulseData);

        break;
      }
      else
      {
        pulseData.length = pulseData.endIndex - pulseData.startIndex + post_;
      }

      // last check to avoid segmentation fault because underflow
      if (pulseData.length > 0)
        pulseData_.push_back(pulseData);

      // array is fully processed
      if (dataIndex >= size_ - 2)
      {
        break;
      }
    }

    // restore original data
    data_[size_ - 2] = lastSamples[0];
    data_[size_ - 1] = lastSamples[1];
  }

  LOG_TRACE << "Exiting CZeroSuppression::PerformZeroSuppression with index " +
      boost::lexical_cast<std::string>(cardIndex_) + ":" +
      boost::lexical_cast<std::string>(channelIndex_);

  return boost::none;
}

void CZeroSuppression::SetResolution(int bit)
{
  resolution_ = bit;
}

void CZeroSuppression::SetDataPointer(int16_t *&data)
{
  LOG_TRACE << "Entering CZeroSuppression::SetDataPointer with index " +
      boost::lexical_cast<std::string>(cardIndex_) + ":" +
      boost::lexical_cast<std::string>(channelIndex_);
  data_ = data;

  if (data == nullptr)
    LOG_ERROR << "data buffer set to nullptr !";

  LOG_TRACE << "Exiting CZeroSuppression::SetDataPointer with index " +
      boost::lexical_cast<std::string>(cardIndex_) + ":" +
      boost::lexical_cast<std::string>(channelIndex_);
}

void CZeroSuppression::SetPre(uint32_t pre)
{
  pre_ = pre;
}

void CZeroSuppression::SetPost(uint32_t post)
{
  post_ = post;
}

void CZeroSuppression::SetPrePost(uint32_t pre, uint32_t post)
{
  pre_ = pre;
  post_ = post;
}

void CZeroSuppression::SetPulseDetectionType(PULSE_TYPE_t type)
{
  pulseDetectionType_ = type;
}

void CZeroSuppression::SetThreshold(float threshold)
{
  threshold_ = threshold;
}

void CZeroSuppression::SetThreshold(int16_t threshold)
{
  thresholdCode_ = threshold;
}

void CZeroSuppression::SetGainOffset(float gain,
                                     float calibratedGain,
                                     float offset)
{
  LOG_TRACE << "Entering CZeroSuppression::SetGainOffset with index "
            << boost::lexical_cast<std::string>(cardIndex_) + ":"
            << boost::lexical_cast<std::string>(channelIndex_);

  gain_ = gain;
  calibratedGain_ = calibratedGain;
  offset_ = offset;

  LOG_DEBUG << "\t+ gain = " << gain_;
  LOG_DEBUG << "\t+ calibratedGain = " << calibratedGain_;
  LOG_DEBUG << "\t+ offset = " << offset_;

  LOG_TRACE << "Exiting CZeroSuppression::SetGainOffset with index "
            << boost::lexical_cast<std::string>(cardIndex_) + ":"
            << boost::lexical_cast<std::string>(channelIndex_);
}

void CZeroSuppression::SetTimeWindow(uint32_t tw, uint64_t nbSamples)
{
  timeWindow_ = tw;
  isZsOff_ = false;
  if (timeWindow_ >= nbSamples)
    isZsOff_ = true;
}

std::vector<PULSE_DATA_t> &CZeroSuppression::GetPulseDataCollection()
{
  return pulseData_;
}

int16_t CZeroSuppression::GetThresholdCode()
{
  return thresholdCode_;
}

CZeroSuppressionEx::CZeroSuppressionEx() throw()
{
  exceptionMsg_ = "none";
}

CZeroSuppressionEx::CZeroSuppressionEx(const std::string &msg) throw() :
  exceptionMsg_(msg)
{}

CZeroSuppressionEx::~CZeroSuppressionEx() throw() {}

const char *CZeroSuppressionEx::what() const throw()
{
  return exceptionMsg_.c_str();
}

void CZeroSuppressionEx::SetMessage(const std::string &msg)
{
  exceptionMsg_ = msg;
}
} // namespace ZeroSuppression
} // namespace ntof
