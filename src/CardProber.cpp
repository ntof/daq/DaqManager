
#include <cstring>
#include <iostream>
#include <string>

#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <boost/regex.hpp>

#include <signal.h>

#include "config.h"

#ifdef ACQIRIS
#include <AgMD1.h>
#include <AgMD1Fundamental.h>
#endif

#ifdef SPDEVICES
#include <ADQAPI.h>
#endif

#include "logger.h"

#ifdef ACQIRIS
void AcqirisProbe(const std::string &resourceName)
{
  uint32_t index = 0;
  uint32_t nbCards = 0;
  std::string resource;

  LOG_INFO << "Detecting Acqiris cards with " + resourceName +
      " as base resource name ...";

  LOG_INFO << "------------------------------------------";

  while (index < 15)
  {
    ViChar str[256];
    ViInt32 serialNb;
    if (resourceName == "PCI::INSTR")
      resource = resourceName + boost::lexical_cast<std::string>(index);
    else
      resource = resourceName + "::" + boost::lexical_cast<std::string>(index) +
        "::INSTR";

    // std::string resource = "PXI6::" + boost::lexical_cast<std::string>(index)
    // + "::INSTR";
    ViSession instrID;
    ViStatus status;

    status = AgMD1_InitWithOptions((char *) resource.c_str(), VI_TRUE, VI_TRUE,
                                   "DriverSetup=CAL=0", &instrID);
    index++;
    if (status < 0)
    {
      break;
    }

    nbCards++;

    str[255] = '\0';
    // Read and output a few attributes
    AgMD1_GetAttributeViString(instrID, "", AGMD1_ATTR_SPECIFIC_DRIVER_PREFIX,
                               127, str);
    LOG_INFO << "# DRIVER_PREFIX: " + std::string(str);

    AgMD1_GetAttributeViString(instrID, "", AGMD1_ATTR_SPECIFIC_DRIVER_REVISION,
                               127, str);
    LOG_INFO << "# DRIVER_REVISION: " + std::string(str);

    AgMD1_GetAttributeViString(instrID, "", AGMD1_ATTR_SPECIFIC_DRIVER_VENDOR,
                               127, str);
    LOG_INFO << "# DRIVER_VENDOR: " + std::string(str);

    AgMD1_GetAttributeViString(instrID, "", AGMD1_ATTR_INSTRUMENT_MODEL, 127,
                               str);
    LOG_INFO << "# INSTRUMENT_MODEL: " + std::string(str);

    AgMD1_GetAttributeViString(instrID, "", AGMD1_ATTR_INSTRUMENT_INFO_OPTIONS,
                               255, str);
    LOG_INFO << "# INSTRUMENT_OPTIONS: " + std::string(str);

    AgMD1_GetAttributeViString(
      instrID, "", AGMD1_ATTR_INSTRUMENT_FIRMWARE_REVISION, 127, str);
    LOG_INFO << "# FIRMWARE_REVISION: " + std::string(str);

    AgMD1_GetAttributeViInt32(
      instrID, "", AGMD1_ATTR_INSTRUMENT_INFO_SERIAL_NUMBER, &serialNb);
    LOG_INFO << "# SN: " + boost::lexical_cast<std::string>(serialNb);

    AgMD1_GetAttributeViInt32(
      instrID, "", AGMD1_ATTR_INSTRUMENT_INFO_CRATE_NUMBER, &serialNb);
    LOG_INFO << "# Crate number: " + boost::lexical_cast<std::string>(serialNb);

    AgMD1_GetAttributeViInt32(
      instrID, "", AGMD1_ATTR_INSTRUMENT_INFO_SLOT_NUMBER, &serialNb);
    LOG_INFO << "# Slot number: " + boost::lexical_cast<std::string>(serialNb);

    LOG_INFO << "------------------------------------------";

    status = AgMD1_close(instrID);
    if (status)
      LOG_INFO << "\n** Close() Failed!";
  }

  LOG_INFO
    << boost::lexical_cast<std::string>(nbCards) + " acqiris cards found";
}
#endif

#ifdef SPDEVICES
void SpdProbe()
{
  void *driverInstance = CreateADQControlUnit();

  LOG_INFO << "Detecting SPD cards ...";

  ADQControlUnit_EnableErrorTrace(driverInstance, 999, "./");

  if (driverInstance == NULL)
  {
    LOG_INFO << "unable to open API";
    LOG_INFO << "driver not configured";
  }

  int nbDevices_ = ADQControlUnit_FindDevices(driverInstance);
  uint nbAdqDevices = ADQControlUnit_NofADQ(driverInstance);
  int nbInitDeviceFailed_ = ADQControlUnit_GetFailedDeviceCount(driverInstance);

  if (nbInitDeviceFailed_)
  {
    LOG_INFO << "Device found, but failed to be initialized";
  }

  if (!nbDevices_)
  {
    LOG_INFO << "no devices found";
  }

  LOG_INFO
    << boost::lexical_cast<std::string>(nbAdqDevices) + " device(s) found";

  ADQInfoListEntry *ADQlist;
  if (!ADQControlUnit_ListDevices(driverInstance, &ADQlist, &nbAdqDevices))
  {
    LOG_INFO << "ListDevices failed!\n";
  }

  for (uint i = 0; i < nbAdqDevices; ++i)
  {
    switch (ADQlist[i].ProductID)
    {
    case PID_ADQ412: LOG_INFO << "ADQ412 found by driver"; break;

    case PID_ADQ14: LOG_INFO << "ADQ14 found by driver"; break;

    default:
      LOG_INFO << "unknown card type found by driver: " +
          boost::lexical_cast<std::string>(ADQlist[i].ProductID);
      break;
    }

    {
      std::ostringstream oss;
      oss << "serial number: "
          << ADQ_GetBoardSerialNumber(driverInstance, i + 1);
      LOG_INFO << oss.str();
    }

    LOG_INFO << "link rate: " << ADQ_GetPCIeLinkRate(driverInstance, i + 1)
             << ", link width: " << ADQ_GetPCIeLinkWidth(driverInstance, i + 1);

    {
      std::ostringstream oss;
      uint maxSamples = 0;
      ADQ_GetMaxNofSamplesFromNofRecords(driverInstance, i + 1, 1, &maxSamples);
      oss << "max number of samples: " << maxSamples;
      LOG_INFO << oss.str();
    }
  }
}
#endif
