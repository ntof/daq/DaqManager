
#include "CChannel.h"

#include <cstring>

#include <boost/lexical_cast.hpp>

#include "CChannelCalibration.h"
#include "logger.h"

namespace ntof {

bool CChannel::Config::operator==(const CChannel::Config &other) const
{
  return (enabled == other.enabled) &&
    (inputImpedance == other.inputImpedance) &&
    (std::memcmp(&header, &other.header, sizeof(ChannelConfig)) == 0);
}

CChannel::CChannel(uint32_t location,
                   const std::string &moduleType,
                   float sampleRate,
                   uint32_t sampleSize,
                   int32_t threshold,
                   const std::string &dimServiceName,
                   ntof::dim::DIMState *state) :
  cardIndex_(ChannelConfig::locationGetModule(location)),
  channelIndex_(ChannelConfig::locationGetChannel(location)),
  zeroSuppression_(cardIndex_, channelIndex_),
  dimParamList_(dimServiceName),
  dimCalibration_(dimServiceName + "/Calibration"),
  state_(state)
{
  configuration_ = std::make_shared<CChannel::Config>();
  configurationLast_ = std::make_shared<CChannel::Config>();
  configurationValid_ = false;
  isMaster_ = false;
  isIndependent_ = true;

  CChannel::Config &config = *configuration_;
  config.header.setModuleType(moduleType);
  config.header.setLocation(location);
  config.header.setDetectorType("BAF2");
  config.header.detectorId = 1;
  config.header.fullScale = 5000;
  config.header.delayTime = 0;
  config.header.thresholdSign = 1;
  config.header.zeroSuppressionStart = 0;
  config.header.offset = 0.0;
  config.header.preSample = 512;
  config.header.postSample = 512;
  config.header.setClockState("INTC");
  config.header.sampleRate = sampleRate;
  config.header.sampleSize = sampleSize;
  config.header.threshold = threshold;

  config.enabled = false;
  config.inputImpedance = 50.0;
  calibratedThreshold_ = 0.0;

  dimParamList_.addParameter<uint8_t>(
    ChannelConfiguration::channel, "Channel id", "", config.header.getChannel(),
    dim::AddMode::CREATE, false);

  dimParamList_.addParameter<uint8_t>(ChannelConfiguration::module, "Module id",
                                      "", config.header.getModule(),
                                      dim::AddMode::CREATE, false);

  dimParamList_.addParameter<uint8_t>(
    ChannelConfiguration::chassis, "Chassis id", "", config.header.getChassis(),
    dim::AddMode::CREATE, false);

  dimParamList_.addParameter<uint8_t>(ChannelConfiguration::stream, "Stream id",
                                      "", config.header.getStream(),
                                      dim::AddMode::CREATE, false);

  dimParamList_.addParameter<std::string>(
    ChannelConfiguration::detectorType, "Detector type", "",
    config.header.getDetectorType(), dim::AddMode::CREATE, false);

  dimParamList_.addParameter<uint32_t>(
    ChannelConfiguration::detectorId, "Detector id", "",
    config.header.detectorId, dim::AddMode::CREATE, false);

  dimParamList_.addParameter<float>(
    ChannelConfiguration::sampleRate, "Sample rate", "MS/s",
    config.header.sampleRate, dim::AddMode::CREATE, false);

  dimParamList_.addParameter<uint32_t>(
    ChannelConfiguration::sampleSize, "Sample size", "kS",
    config.header.sampleSize, dim::AddMode::CREATE, false);

  dimParamList_.addParameter<float>(ChannelConfiguration::fullScale,
                                    "Full scale", "mV", config.header.fullScale,
                                    dim::AddMode::CREATE, false);

  dimParamList_.addParameter<int32_t>(
    ChannelConfiguration::delayTime, "Delay time", "samples",
    config.header.delayTime, dim::AddMode::CREATE, false);

  dimParamList_.addParameter<float>(
    ChannelConfiguration::threshold, "Threshold", "mV",
    static_cast<float>(config.header.threshold), dim::AddMode::CREATE, false);

  dimParamList_.addParameter<int32_t>(
    ChannelConfiguration::thresholdSign, "Threshold sign", "",
    config.header.thresholdSign, dim::AddMode::CREATE, false);

  dimParamList_.addParameter<uint32_t>(
    ChannelConfiguration::zeroSuppressionStart, "Zero suppression start", "",
    config.header.zeroSuppressionStart, dim::AddMode::CREATE, false);

  dimParamList_.addParameter<float>(ChannelConfiguration::offset, "Offset",
                                    "mV", config.header.offset,
                                    dim::AddMode::CREATE, false);

  dimParamList_.addParameter<uint32_t>(
    ChannelConfiguration::preSample, "Pre samples", "samples",
    config.header.preSample, dim::AddMode::CREATE, false);

  dimParamList_.addParameter<uint32_t>(
    ChannelConfiguration::postSample, "Post samples", "samples",
    config.header.postSample, dim::AddMode::CREATE, false);

  dimParamList_.addParameter<float>(
    ChannelConfiguration::inputImpedance, "Input impedance", "ohms",
    config.inputImpedance, dim::AddMode::CREATE, false);

  dimParamList_.addParameter<bool>(ChannelConfiguration::enabled,
                                   "Is channel enabled?", "", config.enabled,
                                   dim::AddMode::CREATE, false);

  // last param must be true to update the list
  dimParamList_.addParameter<std::string>(
    ChannelConfiguration::clockState, "Clock state", "",
    config.header.getClockState(), dim::AddMode::CREATE, false);

  dimParamList_.addParameter<std::string>(
    ChannelConfiguration::moduleType, "Module type", "",
    config.header.getModuleType(), dim::AddMode::CREATE, true);

  dimCalibration_.addData<float>(ChannelCalibration::calibratedFullScale,
                                 "Calibrated full scale", "mV", 0,
                                 dim::AddMode::CREATE, false);
  dimCalibration_.addData<float>(ChannelCalibration::calibratedOffset,
                                 "Calibrated offset", "mV", 0,
                                 dim::AddMode::CREATE, false);
  dimCalibration_.addData<float>(ChannelCalibration::calibratedThreshold,
                                 "Calibrated threshold", "ADC", 0,
                                 dim::AddMode::CREATE, true);
}

CChannel::~CChannel() {}

ZeroSuppression::CZeroSuppression &CChannel::GetZeroSuppression()
{
  return zeroSuppression_;
}

bool CChannel::isMaster()
{
  return isMaster_;
}

bool CChannel::isIndependent()
{
  return isIndependent_;
}

void CChannel::SetAsIndependent(bool independent)
{
  isIndependent_ = independent;
}

void CChannel::SetAsMaster(bool master)
{
  isMaster_ = master;
  isIndependent_ = false;
}

void CChannel::copyListParam()
{
  LOG_TRACE << "Entering CChannel::copyListParam with index " +
      boost::lexical_cast<std::string>(cardIndex_) + ":" +
      boost::lexical_cast<std::string>(channelIndex_);

  *configurationLast_ = *configuration_;

  LOG_TRACE << "Exiting CChannel::copyListParam with index " +
      boost::lexical_cast<std::string>(cardIndex_) + ":" +
      boost::lexical_cast<std::string>(channelIndex_);
}

bool CChannel::isEqual()
{
  bool res = false;
  configurationValid_ = false;

  if (*configurationLast_ == *configuration_)
  {
    res = true;
  }
  else
    configurationValid_ = true;

  return res;
}

void CChannel::UpdateZeroSuppressionConfiguration()
{
  LOG_TRACE << "Entering UpdateZeroSuppressionConfiguration with index " +
      boost::lexical_cast<std::string>(cardIndex_) + ":" +
      boost::lexical_cast<std::string>(channelIndex_);

  LOG_TRACE << "\t+ presample = " << configurationLast_->header.preSample;
  LOG_TRACE << "\t+ post sample = " << configurationLast_->header.postSample;
  LOG_TRACE << "\t+ threshold = " << configurationLast_->header.threshold;
  LOG_TRACE << "\t+ edge type = " << configurationLast_->header.thresholdSign;

  zeroSuppression_.SetPre(configurationLast_->header.preSample);
  zeroSuppression_.SetPost(configurationLast_->header.postSample);

  if (configurationLast_->header.thresholdSign == 1)
    zeroSuppression_.SetPulseDetectionType(
      ntof::ZeroSuppression::PULSE_TYPE_t::POSITIVE);
  else
    zeroSuppression_.SetPulseDetectionType(
      ntof::ZeroSuppression::PULSE_TYPE_t::NEGATIVE);

  LOG_TRACE << "Exiting UpdateZeroSuppressionConfiguration with index " +
      boost::lexical_cast<std::string>(cardIndex_) + ":" +
      boost::lexical_cast<std::string>(channelIndex_);
}

bool CChannel::IsConfigurationValid()
{
  return configurationValid_;
}

void CChannel::SetConfigurationInvalid()
{
  configurationValid_ = false;
}

std::shared_ptr<CChannel::Config> &CChannel::getConfiguration()
{
  return configuration_;
}

uint32_t CChannel::GetIndex()
{
  return channelIndex_;
}

float CChannel::GetCalibratedFullScale()
{
  return fullScale_;
}

float CChannel::GetCalibratedOffset()
{
  return offset_;
}

float CChannel::GetCalibratedThreshold()
{
  return calibratedThreshold_;
}

void CChannel::SetCalibratedFullScale(float fs)
{
  fullScale_ = fs;
}

void CChannel::SetCalibratedOffset(float offset)
{
  offset_ = offset;
}

void CChannel::SetCalibratedThreshold(float th)
{
  calibratedThreshold_ = th;
}

void CChannel::updateCalibrationService()
{
  dimCalibration_.setValue<float>(ChannelCalibration::calibratedFullScale,
                                  fullScale_, false);
  dimCalibration_.setValue<float>(ChannelCalibration::calibratedOffset,
                                  configuration_->header.offset + offset_,
                                  false);
  dimCalibration_.setValue<float>(ChannelCalibration::calibratedThreshold,
                                  calibratedThreshold_, true);
  LOG_TRACE << "Card #" << cardIndex_ << ", channel #" << channelIndex_
            << ", calibrated fullscale: " << fullScale_
            << ", calibrated offset: " << offset_
            << ", calibrated threshold: " << calibratedThreshold_;
}

// Exception class for CChannel
CChannelEx::CChannelEx() throw()
{
  exceptionMsg_ = "none";
}

CChannelEx::CChannelEx(std::string msg) throw() : exceptionMsg_(msg) {}

CChannelEx::~CChannelEx() throw() {}

const char *CChannelEx::what() const throw()
{
  return exceptionMsg_.c_str();
}

void CChannelEx::SetMessage(std::string msg)
{
  exceptionMsg_ = msg;
}

} // namespace ntof
