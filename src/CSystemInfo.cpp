#include "CSystemInfo.h"

#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <blkid/blkid.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "logger.h"

CSystemInfo::CSystemInfo()
{
  totalVirtualMemory_ = 0;
  systemVirtualMemoryUsed_ = 0;
  processVirtualMem_ = 0;
  totalPhysicalMemory_ = 0;
  systemPhysicalMemoryUsed_ = 0;
  processPhysicalMem_ = 0;
  systemCpuUsage_ = 0.0;
  processCpuUsage_ = 0.0;

#if 0
	// Get host's name
	char hostname[128] =
	{};
	if (gethostname(hostname, 128) != 0)
	{
		//throw CCardEx("can not get host name");
	}
	try
	{
		std::ostringstream oss;
		oss << std::string(hostname);
		oss << "/System";
		dimXmlService_ = std::make_shared<ntof::dim::DIMXMLService>(oss.str());
	}
	catch (...)
	{
		throw "can not create DIM service for system info";
	}
#endif
  thread_ = boost::thread(boost::bind(&CSystemInfo::InfoThread, this));
}

CSystemInfo::~CSystemInfo()
{
  thread_.interrupt();
  thread_.join();
}

void CSystemInfo::InfoThread()
{
  uint displayTimer = 0;
  // ntof::CIpmi ipmi;
  BOOST_LOG_SCOPED_THREAD_ATTR(
    "Scope", boost::log::attributes::constant<std::string>("System"));

  LOG_INFO << "starting thread";

  while (true)
  {
    try
    {
#if 0
			pugi::xml_document xml_doc;

			pugi::xml_node root = xml_doc.append_child("root");

			pugi::xml_node hdd = root.append_child("LocalStorage");
			boost::filesystem::space_info s = boost::filesystem::space(boost::filesystem::path("/DAQ/data"));
			hdd.append_child("Capacity").append_child(pugi::node_pcdata).set_value(
							boost::str(boost::format("%.2fGB") % (static_cast<double>(s.capacity) / 1024.0 / 1024.0 / 1024.0)).c_str());
			hdd.append_child("FreeSpace").append_child(pugi::node_pcdata).set_value(
							boost::str((boost::format("%.2fGB") % (static_cast<double>(s.free) / 1024.0 / 1024.0 / 1024.0))).c_str());
			//hdd.append_child("Available space").append_child(pugi::node_pcdata).set_value(boost::str((boost::format("%.2fGB") % (s.available / 1024.0 / 1024.0 / 1024.0))).c_str());
			hdd.append_child("Used").append_child(pugi::node_pcdata).set_value(
							std::string(boost::str((boost::format("%.2f") % (GetDiskUsedSpace()))) + "%").c_str());

			pugi::xml_node mem = root.append_child("Memory");
			mem.append_child("TotalVirtual").append_child(pugi::node_pcdata).set_value(
							boost::str(
											boost::format("%.2fMB") % (static_cast<double>(GetTotalVirtualMemory()) / 1024.0 / 1024.0)).c_str());
			mem.append_child("SystemVirtualUsage").append_child(pugi::node_pcdata).set_value(
							boost::str(
											boost::format("%.2fMB")
											% (static_cast<double>(GetSystemVirtualMemoryUsage()) / 1024.0 / 1024.0)).c_str());
			mem.append_child("ProcessVirtualUsage").append_child(pugi::node_pcdata).set_value(
							boost::str(boost::format("%.2fMB") % (GetProcessVirtualMemoryUsage() / 1024.0 / 1024.0)).c_str());
			mem.append_child("TotalPhysical").append_child(pugi::node_pcdata).set_value(
							boost::str(
											boost::format("%.2fGB")
											% (static_cast<double>(GetTotalPhysicalMemory()) / 1024.0 / 1024.0 / 1024.0)).c_str());
			mem.append_child("SystemPhysicalUsage").append_child(pugi::node_pcdata).set_value(
							boost::str(
											boost::format("%.2fMB")
											% (static_cast<double>(GetSystemPhysicalMemoryUsage()) / 1024.0 / 1024.0)).c_str());
			mem.append_child("ProcessPhysicalUsage").append_child(pugi::node_pcdata).set_value(
							boost::str(boost::format("%.2fMB") % (GetProcessPhysicalMemoryUsage() / 1024.0 / 1024.0)).c_str());

			pugi::xml_node cpu = root.append_child("CPU");
			cpu.append_child("SystemCPUUsage").append_child(pugi::node_pcdata).set_value(
							std::string(boost::str(boost::format("%.2f") % GetSystemCpuUsage()) + "%").c_str());
			cpu.append_child("ProcessCPUUsage").append_child(pugi::node_pcdata).set_value(
							std::string(boost::str(boost::format("%.2f") % GetProcessCpuUsage()) + "%").c_str());

			pugi::xml_node monitoringNode = root.append_child("monitoring");
			try
			{
				for (auto sensor : ipmi.GetMonitoring())
				{
					//std::string msg = sensor.name + " " + sensor.type + " " + sensor.state + " " + sensor.value +sensor.unit;
					//logger_.Trace(msg);

					pugi::xml_node sensorNode = monitoringNode.append_child(sensor.name.c_str());
					sensorNode.append_attribute("type").set_value(sensor.type.c_str());
					sensorNode.append_attribute("value").set_value(std::string(sensor.value + sensor.unit).c_str());
					sensorNode.append_attribute("state").set_value(sensor.state.c_str());
				}
			}
			catch (std::string const& ex)
			{
				logger_.Error("CSystemInfo::InfoThread exception: " + ex);
			}

			dimXmlService_->setData(xml_doc);
#endif

      if (displayTimer++ > 20)
      {
        LOG_INFO
          << "System CPU Usage: "
          << std::string(
               boost::str(boost::format("%.2f") % GetSystemCpuUsage()) + "%");
        LOG_INFO
          << "Process CPU Usage: "
          << std::string(
               boost::str(boost::format("%.2f") % GetProcessCpuUsage()) + "%");
        LOG_INFO
          << "System Physical Usage: "
          << boost::str(boost::format("%.2fMB") %
                        (static_cast<double>(GetSystemPhysicalMemoryUsage()) /
                         1024.0 / 1024.0));
        LOG_INFO << "Process Physical Usage: " +
            boost::str(boost::format("%.2fMB") %
                       (GetProcessPhysicalMemoryUsage() / 1024.0 / 1024.0));

        LOG_INFO << "Disk usage: " +
            std::string(std::string(boost::str((boost::format("%.2f") %
                                                (GetDiskUsedSpace()))) +
                                    "%")
                          .c_str());
        displayTimer = 0;
      }

      boost::this_thread::sleep(boost::posix_time::seconds(1));
    }
    catch (boost::thread_interrupted const &)
    {
      LOG_TRACE << "System Info thread interrupted";
      break;
    }
    catch (std::exception const &ex)
    {
      // LOG_ERROR << "CSystemInfo::InfoThread exception: " <<
      // boost::diagnostic_information(ex);
      break;
    }
  }
}

double CSystemInfo::GetDiskTotalCapacity()
{
  boost::filesystem::space_info s = boost::filesystem::space(
    boost::filesystem::path("/DAQ"));
  return static_cast<double>(s.capacity) / 1024.0 / 1024.0 / 1024.0;
}

double CSystemInfo::GetDiskUsedSpace()
{
  boost::filesystem::space_info s = boost::filesystem::space(
    boost::filesystem::path("/DAQ"));
  return (1.0 - (double) s.available / (double) s.capacity) * 100.0;
}

int CSystemInfo::parseLine(char *line)
{
  int i = static_cast<int>(strlen(line));

  while (*line < '0' || *line > '9')
    line++;

  line[i - 3] = '\0';

  i = atoi(line);

  return i;
}

long long CSystemInfo::GetTotalVirtualMemory()
{
  sysinfo(&memInfo);
  totalVirtualMemory_ = memInfo.totalram;
  totalVirtualMemory_ += memInfo.totalswap;
  totalVirtualMemory_ *= memInfo.mem_unit;

  return totalVirtualMemory_;
}

long long CSystemInfo::GetSystemVirtualMemoryUsage()
{
  sysinfo(&memInfo);

  systemVirtualMemoryUsed_ = memInfo.totalram - memInfo.freeram;

  systemVirtualMemoryUsed_ += memInfo.totalswap - memInfo.freeswap;
  systemVirtualMemoryUsed_ *= memInfo.mem_unit;

  return systemVirtualMemoryUsed_;
}

int CSystemInfo::GetProcessVirtualMemoryUsage()
{
  FILE *file = fopen("/proc/self/status", "r");
  processVirtualMem_ = -1;
  char line[128];

  while (fgets(line, 128, file) != NULL)
  {
    if (strncmp(line, "VmSize:", 7) == 0)
    {
      processVirtualMem_ = parseLine(line);
      break;
    }
  }

  fclose(file);

  return processVirtualMem_;
}

long long CSystemInfo::GetTotalPhysicalMemory()
{
  sysinfo(&memInfo);

  totalPhysicalMemory_ = memInfo.totalram;

  totalPhysicalMemory_ *= memInfo.mem_unit;

  return totalPhysicalMemory_;
}

long long CSystemInfo::GetSystemPhysicalMemoryUsage()
{
  sysinfo(&memInfo);

  systemPhysicalMemoryUsed_ = memInfo.totalram - memInfo.freeram;

  systemPhysicalMemoryUsed_ *= memInfo.mem_unit;

  return systemPhysicalMemoryUsed_;
}

int CSystemInfo::GetProcessPhysicalMemoryUsage()
{
  FILE *file = fopen("/proc/self/status", "r");
  char line[128];

  processPhysicalMem_ = -1;

  while (fgets(line, 128, file) != NULL)
  {
    if (strncmp(line, "VmRSS:", 6) == 0)
    {
      processPhysicalMem_ = parseLine(line);
      break;
    }
  }

  fclose(file);

  return processPhysicalMem_;
}

double CSystemInfo::GetSystemCpuUsage()
{
  static unsigned long long lastTotalUser, lastTotalUserLow, lastTotalSys,
    lastTotalIdle;
  FILE *file;
  unsigned long long totalUser, totalUserLow, totalSys, totalIdle;

  file = fopen("/proc/stat", "r");

  fscanf(file, "cpu %llu %llu %llu %llu", &totalUser, &totalUserLow, &totalSys,
         &totalIdle);

  fclose(file);

  if (totalUser < lastTotalUser || totalUserLow < lastTotalUserLow ||
      totalSys < lastTotalSys || totalIdle < lastTotalIdle)
  {
    // Overflow detection. Just skip this value.
    systemCpuUsage_ = -1.0;
  }
  else
  {
    unsigned long long total = (totalUser - lastTotalUser) +
      (totalUserLow - lastTotalUserLow) + (totalSys - lastTotalSys);
    systemCpuUsage_ = static_cast<double>(total);
    total += (totalIdle - lastTotalIdle);
    systemCpuUsage_ /= static_cast<double>(total);
    systemCpuUsage_ *= 100;
  }

  lastTotalUser = totalUser;
  lastTotalUserLow = totalUserLow;
  lastTotalSys = totalSys;
  lastTotalIdle = totalIdle;

  return systemCpuUsage_;
}

double CSystemInfo::GetProcessCpuUsage()
{
  static clock_t lastCPU, lastSysCPU, lastUserCPU;
  struct tms timeSample;
  clock_t now;

  now = times(&timeSample);

  if (now <= lastCPU || timeSample.tms_stime < lastSysCPU ||
      timeSample.tms_utime < lastUserCPU)
  {
    // Overflow detection. Just skip this value.
    processCpuUsage_ = -1.0;
  }
  else
  {
    processCpuUsage_ = static_cast<double>((timeSample.tms_stime - lastSysCPU) +
                                           (timeSample.tms_utime - lastUserCPU));
    processCpuUsage_ /= static_cast<double>(now - lastCPU);
    processCpuUsage_ /= boost::thread::hardware_concurrency();
    processCpuUsage_ *= 100;
  }

  lastCPU = now;
  lastSysCPU = timeSample.tms_stime;
  lastUserCPU = timeSample.tms_utime;

  return processCpuUsage_;
}

std::string CSystemInfo::GetFilesystem(std::string path)
{
  struct stat s;
  char *name;

  if (lstat(path.c_str(), &s) != 0)
  {
    std::ostringstream oss;
    oss << "Unable to stat " << path.c_str();
    throw std::runtime_error(oss.str() + ": " + strerror(errno));
  }

  name = blkid_devno_to_devname(s.st_dev);
  if (name == NULL)
  {
    std::ostringstream oss;
    oss << "Unable to get devname";
    throw std::runtime_error(oss.str() + ": " + strerror(errno));
  }

  return std::string(name);
}
