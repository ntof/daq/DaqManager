/*
 * CAcquisition.cpp
 *
 *  Created on: Nov 16, 2014
 *      Author: pperonna
 */

#include "CAcquisition.h"

#include <boost/chrono/chrono.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>

#include <numa.h>
#include <sched.h>
#include <sys/time.h>

#include "CWriter.h"
#include "NTOFException.h"
#include "PerformanceCounter.h"
#include "logger.h"

#include "dis.hxx"

//#define TIME_SERVER_ENABLE
//#define SINGLE_THREAD

using ntof::utils::Flock;

int CAcquisition::bufferPoolIndex_ = -1;

CAcquisition::CAcquisition(
  std::string hostName,
  CBoundedCircularBuffer<CAcquisitionParameters> &acqQueue,
  std::vector<std::shared_ptr<CCard>> &cards,
  ntof::dim::DIMState *writerState,
  std::string timingEventName,
  CMD_LINE_PARAM_t cmdLineParams) :
  state_(hostName + "/AcquisitionState"),
  acqQueue_(acqQueue),
  cards_(cards),
  writerState_(writerState),
  triggerTimeStamp_(timingEventName),
  cmdLineParams_(cmdLineParams),
  eventReader_("Timing/event"),
  startAcquisition_(false)
{
  // Get host's name
  char hostname[128] = {};
  if (gethostname(hostname, 128) != 0)
  {
    throw CAcquisitionEx("can not get host name");
  }
  hostName_ = std::string(hostname);

  // Initialize DIM service for state
  try
  {
    state_.addStateValue(static_cast<int32_t>(ACQUISITION_STATE_t::IDLE),
                         "Idle");
    state_.addStateValue(static_cast<int32_t>(ACQUISITION_STATE_t::THREAD_INIT),
                         "Initialization");
    state_.addStateValue(
      static_cast<int32_t>(ACQUISITION_STATE_t::ALLOCATING_MEMORY),
      "Allocating memory");
    state_.addStateValue(
      static_cast<int32_t>(ACQUISITION_STATE_t::WAITING_TO_START),
      "Waiting to start");
    state_.addStateValue(
      static_cast<int32_t>(ACQUISITION_STATE_t::ARMING_TRIGGER),
      "Arming card triggers");
    state_.addStateValue(
      static_cast<int32_t>(ACQUISITION_STATE_t::WAITING_TRIGGER),
      "Waiting for trigger");
    state_.addStateValue(
      static_cast<int32_t>(ACQUISITION_STATE_t::WAITING_END_ACQUISITION),
      "Dumping card memory buffers");
    state_.addStateValue(
      static_cast<int32_t>(ACQUISITION_STATE_t::QUEUING_MESSAGE),
      "Queuing buffers for writer");
    state_.addStateValue(
      static_cast<int32_t>(ACQUISITION_STATE_t::WAITING_QUEUE_SPACE),
      "Waiting for memory space");
    state_.addStateValue(static_cast<int32_t>(ACQUISITION_STATE_t::STOPPING),
                         "Stopping thread");
    state_.setValue(static_cast<int32_t>(ACQUISITION_STATE_t::IDLE));
  }
  catch (const ntof::dim::DIMException &ex)
  {
    throw CAcquisitionEx(ex.what());
  }
  catch (const std::bad_alloc &ex)
  {
    throw CAcquisitionEx(ex.what());
  }

  previousTimeServer_ = 0.0;
  stop_ = false;

  // set margin of +/-250ms
  // must be smaller than timing machine min-period (500ms)
  eventReader_.setTimeMargin(250000);
}

CAcquisition::~CAcquisition() {}

void CAcquisition::setLockFile(const std::string &lockFile)
{
  if (lockFile.empty())
    lockFile_.reset();
  else
    lockFile_.reset(new Flock(Flock::Shared, lockFile));
}

ACQUISITION_STATE_t CAcquisition::GetState()
{
  return static_cast<ACQUISITION_STATE_t>(state_.getValue());
}

void CAcquisition::Start(uint32_t runNumber)
{
  stop_ = false;
  eveh_.runNumber = runNumber;
  startAcquisition_ = false;
  triggerTimeStamp_.ResetStatus();
  thread_ = boost::thread(boost::bind(&CAcquisition::Acquisition, this));
}

void CAcquisition::StartAcq()
{
  std::unique_lock<std::mutex> lock(startMutex_);
  startAcquisition_ = true;
  startCV_.notify_all();
}

void CAcquisition::Stop()
{
  LOG_TRACE << "Entering CAcquisition::Stop";

  if (state_.getValue() != static_cast<int32_t>(ACQUISITION_STATE_t::ERROR))
  {
    thread_.interrupt();
    // waiting for thread to end
    thread_.join();
  }

  startAcquisition_ = false;

  LOG_TRACE << "Exiting CAcquisition::Stop";
}

void CAcquisition::ForceStop()
{
  LOG_TRACE << "Entering CAcquisition::ForceStop";

  stop_ = true;
  startCV_.notify_all();
  thread_.interrupt();
  // waiting for thread to end
  thread_.join();
  stop_ = false;

  LOG_TRACE << "Exiting CAcquisition::Stop";
}

void CAcquisition::Reset()
{
  stop_ = true;
  startCV_.notify_all();
  state_.clearError();
  state_.clearWarnings();
  state_.setValue(static_cast<int32_t>(ACQUISITION_STATE_t::IDLE));
}

void CAcquisition::AcqThreadInit()
{
  stop_ = false;

  cardPoolIndex_.clear();
  threadStatus_.clear();
  cardEnabled_.clear();
  crateCardMap_.clear();

  previousTimeServer_ = 0;

  cardPoolIndex_.reserve(cards_.size());
  threadStatus_.reserve(cards_.size());
  cardEnabled_.reserve(cards_.size());

  for (auto &card : cards_)
  {
    try
    {
      cardPoolIndex_.push_back(std::make_shared<CBoundedCircularBuffer<int>>(1));
    }
    catch (const std::bad_alloc &ex)
    {
      std::string err = "card #" +
        boost::lexical_cast<std::string>(card->GetCardIndex()) +
        " could not allocate memory for acqBufferQueue_";
      state_.setError(-1, err);
      break;
    }

    if (card->IsCardEnabled() == false)
    {
      LOG_TRACE << "acquisition thread: card #" << card->GetCardIndex()
                << " (" + card->GetSerialNumber() << ") is disabled";

      threadStatus_.push_back(1);
      cardEnabled_.push_back(0);

      continue;
    }

    threadStatus_.push_back(0);
    cardEnabled_.push_back(1);

    LOG_TRACE << "acquisition thread: card #" << card->GetCardIndex() << " ("
              << card->GetSerialNumber() << ") is enabled";
  }
}

void CAcquisition::AllocateMemory()
{
  // the threads will allocate the pool of buffers upon creation
  for (auto &card : cards_)
    cardMemoryDumpThreads_.create_thread(
      boost::bind(&CAcquisition::CardMemoryDump, this, card->GetCardIndex()));

  // allocating huge memory blocks may take time, we wait until all the threads
  // are ready ...
  for (auto &status : threadStatus_)
  {
    while (!status)
    {
      boost::this_thread::sleep(boost::posix_time::milliseconds(10));
    }
  }
}

void CAcquisition::Acquisition()
{
  BOOST_LOG_SCOPED_THREAD_ATTR(
    "Scope", boost::log::attributes::constant<std::string>("Acquisition"));

  LOG_TRACE << "Entering CAcquisition::AcquisitionThread";

  boost::chrono::steady_clock::time_point startTime;
  typedef boost::chrono::duration<int, boost::milli> duration_ms_t;

  // setting high priority for acquisition
  struct sched_param p;
  p.sched_priority = sched_get_priority_max(SCHED_FIFO);
  LOG_INFO << "Acquisition sched priority: " << p.sched_priority;
  p.sched_priority -= 1;

  if (pthread_setschedparam(pthread_self(), SCHED_FIFO, &p) != 0)
  {
    std::string err = "could not set priority for acquisition thread";
    LOG_ERROR << "!!! " + err;
    state_.setError(-1, err);
  }

  uint nbTriggeredCards = 0;
  bool skipQueueState = false;
  std::vector<int> hasTriggered(cards_.size());

  state_.clearError();
  state_.clearWarnings();
  state_.setValue(static_cast<int32_t>(ACQUISITION_STATE_t::THREAD_INIT));

  std::shared_ptr<Flock> lockFile(lockFile_);
  std::unique_lock<Flock> fileLocker;
  if (lockFile)
    fileLocker = std::unique_lock<Flock>(*lockFile);

  while (static_cast<WRITER_STATE_t>(writerState_->getValue()) !=
           WRITER_STATE_t::ERROR &&
         (stop_ != true))
  {
    try
    {
      switch (static_cast<ACQUISITION_STATE_t>(state_.getValue()))
      {
      case ACQUISITION_STATE_t::IDLE: break;

      case ACQUISITION_STATE_t::THREAD_INIT:
        LOG_TRACE << "Acq thread: initialization";

        AcqThreadInit();

        // wait until Writer is ready, takes some time to allocate large aligned
        // buffers
        while (static_cast<WRITER_STATE_t>(writerState_->getValue()) !=
               WRITER_STATE_t::WAITING_QUEUE)
        {
          boost::this_thread::sleep(boost::posix_time::milliseconds(10));
        }

        state_.setValue(
          static_cast<int32_t>(ACQUISITION_STATE_t::ALLOCATING_MEMORY));
        break;

      // Allocating memory buffers for cards
      case ACQUISITION_STATE_t::ALLOCATING_MEMORY:
        LOG_TRACE << "Acq thread: allocating memory";

        try
        {
          AllocateMemory();
        }
        catch (const CCardEx &ex)
        {
          LOG_FATAL << ex.what();
          state_.setError(-1, ex.what());
        }

        bufferPoolIndex_ = -1;

        if (static_cast<ACQUISITION_STATE_t>(state_.getValue()) ==
            ACQUISITION_STATE_t::ERROR)
        {
          break;
        }

        state_.setValue(
          static_cast<int32_t>(ACQUISITION_STATE_t::WAITING_TO_START));
        break;

        break;

      case ACQUISITION_STATE_t::WAITING_TO_START:
      {
        LOG_TRACE << "waiting for start acquisition command";
        std::unique_lock<std::mutex> lock(startMutex_);
        // loop to avoid spurious wakeups
        while (!startAcquisition_ && !stop_)
        {
          startCV_.wait(lock);
        }
        state_.setValue(
          static_cast<int32_t>(ACQUISITION_STATE_t::ARMING_TRIGGER));
        break;
      }

      case ACQUISITION_STATE_t::ARMING_TRIGGER:
        LOG_TRACE << "arming trigger";

        if (++bufferPoolIndex_ == cmdLineParams_.acqPoolSize)
        {
          bufferPoolIndex_ = 0;
        }

        LOG_TRACE << "pool index: " << bufferPoolIndex_;

        for (auto &card : cards_)
        {
          if (cardEnabled_[card->GetCardIndex()] == false)
          {
            continue;
          }

          card->GetBuffer(bufferPoolIndex_)->dumpStatus_ = 0;
#ifndef CHARACTERIZATION
          if (card->GetBuffer(bufferPoolIndex_)->used_)
          {
            std::string err = "acquisition buffer pool is full";
            LOG_FATAL << "!!!" + err;
            state_.setError(-1, err);
            break;
          }
#endif
          try
          {
            LOG_TRACE << "Arming trigger for card #" << card->GetCardIndex()
                      << " (" << card->GetSerialNumber() << ")";
            card->ArmTrigger();
          }
          catch (const CCardEx &ex)
          {
            std::ostringstream err;
            err << "could not arm trigger for card #" << card->GetCardIndex()
                << " (" << card->GetSerialNumber() << ")"
                << ": " << ex.what();

            LOG_FATAL << "!!! " << err.str();
            state_.setError(-1, err.str());
            break;
          }
        }

        if (static_cast<ACQUISITION_STATE_t>(state_.getValue()) ==
            ACQUISITION_STATE_t::ERROR)
        {
          break;
        }

        LOG_TRACE << "polling cards's status till detecting one trigger ...";
        state_.setValue(
          static_cast<int32_t>(ACQUISITION_STATE_t::WAITING_TRIGGER));
        break;

      case ACQUISITION_STATE_t::WAITING_TRIGGER:
      {
        // the first triggered card starts the timeout counter
        for (auto &card : cards_)
        {
          if (cardEnabled_[card->GetCardIndex()] == false)
          {
            continue;
          }

          if (card->IsTriggered())
          {
            LOG_TRACE << "card #" << card->GetCardIndex() << " ("
                      << card->GetSerialNumber() << ")"
                      << " has triggered. Starting timeout detection ...";

            // Retrieve event number as soon as one card has triggered
            if (!RetrieveEvent())
            {
              std::string err = "retrieve event number failed";
              LOG_ERROR << "!!! " + err;
              state_.addWarning(-1, err);
              state_.setValue(
                static_cast<int32_t>(ACQUISITION_STATE_t::ARMING_TRIGGER));
            }
            else
            {
              state_.setValue(static_cast<int32_t>(
                ACQUISITION_STATE_t::WAITING_END_ACQUISITION));
            }
            break;
          }
        }

        if (static_cast<ACQUISITION_STATE_t>(state_.getValue()) ==
            ACQUISITION_STATE_t::ARMING_TRIGGER)
        {
          break;
        }

        if (triggerTimeStamp_.GetStatus().code != 0)
        {
          std::string err = "trigger timestamp service error: " +
            triggerTimeStamp_.GetStatus().err;
          LOG_ERROR << err;
          state_.setError(-1, err);
          break;
        }

        if (static_cast<ACQUISITION_STATE_t>(state_.getValue()) ==
            ACQUISITION_STATE_t::WAITING_TRIGGER)
        {
          boost::this_thread::sleep(boost::posix_time::millisec(10));
          break;
        }

        startTime = boost::chrono::steady_clock::now();
        LOG_TRACE << "waiting for all the trigger or timeout detection";
        break;
      }

      case ACQUISITION_STATE_t::WAITING_END_ACQUISITION:
        // Awake threads to dump cards's memory into host's memory
        for (auto &card : cards_)
        {
          if (cardEnabled_[card->GetCardIndex()] == false)
          {
            continue;
          }

          // queuing acquisition buffers to data gathering threads
          cardPoolIndex_[card->GetCardIndex()]->push(bufferPoolIndex_);
        }

        // check if all cards have triggered
        nbTriggeredCards = 0;
        for (auto &b : hasTriggered)
        {
          b = 0;
        }

        skipQueueState = false;
        while (nbTriggeredCards != cards_.size())
        {
          boost::this_thread::interruption_point();

          nbTriggeredCards = 0;
          for (auto &card : cards_)
          {
            if (cardEnabled_[card->GetCardIndex()] == false ||
                hasTriggered[card->GetCardIndex()])
            {
              nbTriggeredCards++;
              continue;
            }

            if (card->GetBuffer(bufferPoolIndex_)->dumpStatus_ == 1)
            {
              nbTriggeredCards++;
              hasTriggered[card->GetCardIndex()] = 1;
            }

            // error reported by the thread
            if (card->GetBuffer(bufferPoolIndex_)->dumpStatus_ == -1)
            {
              std::string err = "card #" +
                boost::lexical_cast<std::string>(card->GetCardIndex()) + " (" +
                card->GetSerialNumber() + ") did not complete memory dump";
              LOG_ERROR << "!!! " + err;
              state_.addWarning(-1, err);
              skipQueueState = true;
              hasTriggered[card->GetCardIndex()] = 1;
              // break; // we should wait for the "good" cards anyway before
              // rearming
            }

            if (card->GetBuffer(bufferPoolIndex_)->dumpStatus_ == -2)
            {
              std::string err = "card #" +
                boost::lexical_cast<std::string>(card->GetCardIndex()) + " (" +
                card->GetSerialNumber() + ") did not receive hardware trigger";
              LOG_ERROR << "!!! " + err;
              state_.setError(-1, err);
              break;
            }
          }

          if (static_cast<ACQUISITION_STATE_t>(state_.getValue()) ==
              ACQUISITION_STATE_t::ERROR)
          {
            break;
          }

          boost::this_thread::sleep(boost::posix_time::microseconds(100));
        }

        if (static_cast<ACQUISITION_STATE_t>(state_.getValue()) ==
            ACQUISITION_STATE_t::ERROR)
        {
          break;
        }

        if (skipQueueState)
        {
          for (auto &card : cards_)
          {
            card->GetBuffer(bufferPoolIndex_)->used_ = false;
          }

          LOG_INFO << "Skipping queue state";

          state_.setValue(
            static_cast<int32_t>(ACQUISITION_STATE_t::ARMING_TRIGGER));
          break;
        }

        if (!cmdLineParams_.standAlone)
        {
          if (triggerTimeStamp_.GetStatus().code != 0)
          {
            std::string err = "trigger timestamp service error: " +
              triggerTimeStamp_.GetStatus().err;
            LOG_ERROR << err;
            state_.setError(-1, err);
            break;
          }
        }

        state_.setValue(
          static_cast<int32_t>(ACQUISITION_STATE_t::QUEUING_MESSAGE));
        break;

      case ACQUISITION_STATE_t::QUEUING_MESSAGE:
        LOG_TRACE << "posting the acquisition buffers for Writer thread";

        LOG_TRACE << "run number: " << eveh_.runNumber;
        if (!cmdLineParams_.standAlone)
        {
          acqQueue_.push(CAcquisitionParameters(eveh_, bufferPoolIndex_));
        }
        else
        {
          static int64_t eventNumber = 0;
          eveh_.eventNumber = eventNumber++;
          acqQueue_.push(CAcquisitionParameters(eveh_, bufferPoolIndex_));
        }

#ifdef CHARACTERIZATION
        // 2s between each soft trigger
        boost::this_thread::sleep(boost::posix_time::seconds(2));
#endif
        state_.setValue(
          static_cast<int32_t>(ACQUISITION_STATE_t::ARMING_TRIGGER));
        break;

      case ACQUISITION_STATE_t::WAITING_FREE_MEMORY: break;

      case ACQUISITION_STATE_t::WAITING_QUEUE_SPACE:
      {
        duration_ms_t duration(
          boost::chrono::duration_cast<boost::chrono::milliseconds>(
            boost::chrono::steady_clock::now() - startTime));

        if (duration.count() > 10000)
        {
          std::string err = "still no space in queue after " +
            boost::lexical_cast<std::string>(10000) + "ms";
          LOG_TRACE << "!!! " + err;
          LOG_TRACE << "!!! stopping acquisition thread because of timeout";
          state_.setError(-1, err);
          break;
        }

        state_.setValue(
          static_cast<int32_t>(ACQUISITION_STATE_t::ARMING_TRIGGER));
        break;
      }

      case ACQUISITION_STATE_t::ERROR:
        boost::this_thread::sleep(boost::posix_time::millisec(100));
        StopThread(true);
        LOG_TRACE << "Exiting CAcquisition::AcquisitionThread";
        return;
        break;

      default:
        state_.setValue(static_cast<int32_t>(ACQUISITION_STATE_t::ERROR));
        break;
      }
    }
    catch (boost::thread_interrupted const &)
    {
      break;
    }
  }

  StopThread(false);

  LOG_TRACE << "Exiting CAcquisition::AcquisitionThread";
}

bool CAcquisition::RetrieveEvent(void)
{
  // retrieve event number associated to this trigger
  if (!cmdLineParams_.standAlone)
  {
    ntof::utils::Timestamp now;
    ntof::dim::EventReader::Data evt;

    if (!eventReader_.getEventByTime(evt, now, 1000))
    {
      LOG_ERROR << "event not found: timeout" << std::endl;
      return false;
    }
    LOG_TRACE << "event number: " << evt.evtNumber << ", " << evt.dest << "/"
              << evt.dest2;
    eveh_ << evt; /* store event information */
    eveh_.setCompTS();
  }

  return true;
}

void CAcquisition::StopThread(bool error)
{
  LOG_TRACE << "Entering CAcquisition::StopThread";

  if (!error)
  {
    state_.setValue(static_cast<int32_t>(ACQUISITION_STATE_t::STOPPING));
  }

  LOG_TRACE
    << "Acquisition thread interrupted, waiting for memory dump threads ...";

  // wake up card threads to stop them
  for (auto &index : cardPoolIndex_)
  {
    index->push(-1);
  }

  // wait until all threads are terminated before stopping the writer (it will
  // free the buffer pool)
  if (cardMemoryDumpThreads_.size())
  {
    cardMemoryDumpThreads_.join_all();
  }

  // post an invalid element to stop the writer thread
  LOG_TRACE << "informing writer that the acquisition is stopped";
  CAcquisitionParameters acqParams;
  acqParams.SetStop();
  acqQueue_.push(acqParams);

  cardPoolIndex_.clear();

  if (!error)
  {
    state_.setValue(static_cast<int32_t>(ACQUISITION_STATE_t::IDLE));
  }

  LOG_TRACE << "Exiting CAcquisition::StopThread";
}

void CAcquisition::CardMemoryDump(int cardIndex)
{
  std::string scope = "Dump C" + boost::lexical_cast<std::string>(cardIndex);
  BOOST_LOG_SCOPED_THREAD_ATTR(
    "Scope", boost::log::attributes::constant<std::string>(scope));

  int bufferPoolIndex = -1;

  LOG_TRACE << "Entering CAcquisition::CardMemoryDump";

  // Allocate memory on dedicated CPU's memory controller associated to each card
  try
  {
    PinThreadToCpu(cards_[cardIndex]->GetAcqCore());
    PinThreadToNode(cards_[cardIndex]->GetAcqCore());
  }
  catch (const CAcquisitionEx &ex)
  {
    LOG_ERROR << ex.what();
  }

  try
  {
    cards_[cardIndex]->AllocateBufferPool(cmdLineParams_.acqPoolSize);
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  threadStatus_[cardIndex] = 1;

  while (true)
  {
    try
    {
      LOG_TRACE << "waiting for acquisition buffer";

      bufferPoolIndex = cardPoolIndex_[cardIndex]->pop();

      LOG_TRACE << "acquisition buffer popped out from the queue";

      if (bufferPoolIndex == -1)
      {
        LOG_TRACE << "negative pool index popped out from the queue, stopping "
                     "the thread ...";
        break;
      }

      cards_[cardIndex]->SetBufferPoolIndex(bufferPoolIndex);
      int32_t rtn = WaitForTriggerAndDump(
        cardIndex, cards_[cardIndex]->GetBuffer(bufferPoolIndex).get());

      if (rtn != 0)
      {
        return;
      }
    }
    catch (boost::thread_interrupted const &)
    {
      break;
    }
  }

  LOG_TRACE << "Exiting CAcquisition::CardMemoryDump";
}

int32_t CAcquisition::WaitForTriggerAndDump(uint32_t cardIndex,
                                            AcquisitionBuffer *acqBuffer)
{
  typedef boost::chrono::duration<int, boost::milli> duration_ms_t;

  // waiting for trigger or timeout
  boost::chrono::steady_clock::time_point startTime =
    boost::chrono::steady_clock::now();

  // Check card's trigger status
  while (cards_[cardIndex]->IsTriggered() == false)
  {
    duration_ms_t duration(
      boost::chrono::duration_cast<boost::chrono::milliseconds>(
        boost::chrono::steady_clock::now() - startTime));

    if (duration.count() > 10000)
    {
      acqBuffer->dumpStatus_ = -2;
      LOG_ERROR << "did not receive trigger after 10s";
      return -2;
    }

    boost::this_thread::sleep(boost::posix_time::milliseconds(100));
  }

  // transferring data from card to host's memory
  try
  {
    double perfStart = StartCounter();

    try
    {
      cards_[cardIndex]->DumpCardMemory();
    }
    catch (const CCardEx &ex)
    {
      acqBuffer->dumpStatus_ = -1;
      LOG_FATAL << ex.what();
      return 0;
    }

    double cardTransferTime = GetCounter(perfStart);
    double cardTransferSize = cards_[cardIndex]->GetNbBytesDumped() / 1024. /
      1024.;
    double cardTransferRate = cardTransferSize / cardTransferTime;

    LOG_INFO << "data rate: " << cardTransferRate << "MB/s";
  }
  catch (const CCardEx &ex)
  {
    if (acqBuffer != nullptr)
      acqBuffer->dumpStatus_ = -1;
    LOG_FATAL << ex.what();
    return -1;
  }

  acqBuffer->used_ = true;
  acqBuffer->cardIndex_ = cardIndex;

  // telling to acquisition thread that data is ready
  acqBuffer->dumpStatus_ = 1;

  LOG_TRACE << "dump ok";

  return 0;
}

void CAcquisition::PinThreadToCpu(int cpu)
{
  int ncpus = numa_num_task_cpus();

  if (ncpus >= CPU_SETSIZE)
  {
    throw CAcquisitionEx("wrong number of CPUs detected: " +
                         boost::lexical_cast<std::string>(ncpus) +
                         " cpus detected instead of " +
                         boost::lexical_cast<std::string>(CPU_SETSIZE));
  }

  cpu_set_t cs;
  CPU_ZERO(&cs);
  CPU_SET(cpu, &cs);

  if (CPU_COUNT(&cs) != 1)
  {
    throw CAcquisitionEx("CPU_COUNT(&cs) == 1");
  }

  int ret = sched_setaffinity(0, sizeof(cs), &cs);
  if (ret != 0)
  {
    std::string err = "sched_setaffinity failed: " +
      std::string(strerror(errno));
    throw CAcquisitionEx(err);
  }

  ret = sched_yield();
  if (ret != 0)
  {
    std::string err = "sched_yield failed: " + std::string(strerror(errno));
    throw CAcquisitionEx(err);
  }
}

void CAcquisition::PinThreadToNode(int cpu)
{
  int node = numa_node_of_cpu(cpu);

  int ret = numa_run_on_node(node);
  if (ret)
  {
    std::string err = "numa_run_on_node failed: " + std::string(strerror(errno));
    throw CAcquisitionEx(err);
  }

  ret = sched_yield();
  if (ret)
  {
    std::string err = "sched_yield failed: " + std::string(strerror(errno));
    throw CAcquisitionEx(err);
  }
}

// Exception class
CAcquisitionEx::CAcquisitionEx() throw()
{
  exceptionMsg_ = "none";
}

CAcquisitionEx::CAcquisitionEx(const std::string &msg) throw() :
  exceptionMsg_(msg)
{}

CAcquisitionEx::~CAcquisitionEx() throw() {}

const char *CAcquisitionEx::what() const throw()
{
  return exceptionMsg_.c_str();
}
