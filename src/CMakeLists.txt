
include_directories(
    ${CMAKE_SOURCE_DIR}/include
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_BINARY_DIR})
include_directories(SYSTEM
    ${DIM_INCLUDE_DIRS}
    ${NTOFUTILS_INCLUDE_DIRS}
    ${Boost_INCLUDE_DIRS}
    ${FREEIPMI_INCLUDE_DIRS}
    ${NUMA_INCLUDE_DIRS}
    ${READLINE_INCLUDE_DIRS}
    ${BLKID_INCLUDE_DIRS}
    ${SPDEVICES_INCLUDE_DIRS}
    ${PUGI_INCLUDE_DIRS})

set(DAQMANAGER_SRC
    CAcquisitionBuffer.cpp
    CAcquisition.cpp
    CardProber.cpp
    CCard.cpp
    CCardVirtual.cpp
    CChannel.cpp
    CChannelVirtual.cpp
    CDaq.cpp
    CDaqEx.cpp
    CDIMZeroSuppressionParameters.cpp
    CIpmi.cpp
    CMakeLists.txt
    CSystemInfo.cpp
    CWriter.cpp
    CZeroSuppression.cpp
    logger.cpp
    TimerClient.cpp
    )

if(ACQIRIS)
    list(APPEND DAQMANAGER_SRC
        Acqiris/CAcqiris.cpp
        Acqiris/CCardAcqiris.cpp
        Acqiris/DC240/CAcqirisDC240.cpp
        Acqiris/DC240/CCardAcqirisDC240.cpp
        Acqiris/DC240/CChannelAcqirisDC240.cpp
        Acqiris/DC270/CAcqirisDC270.cpp
        Acqiris/DC270/CCardAcqirisDC270.cpp
        Acqiris/DC270/CChannelAcqirisDC270.cpp
        Acqiris/DC282/CAcqirisDC282.cpp
        Acqiris/DC282/CCardAcqirisDC282.cpp
        Acqiris/DC282/CChannelAcqirisDC282.cpp)
endif(ACQIRIS)

if(SPDEVICES)
    list(APPEND DAQMANAGER_SRC
        spd/CCardSpd.cpp
        spd/CSpd.cpp
        spd/ADQ14/CCardSpdAdq14.cpp
        spd/ADQ14/CChannelSpdAdq14.cpp
        spd/ADQ14/CSpdAdq14.cpp
        spd/ADQ412/CCardSpdAdq412.cpp
        spd/ADQ412/CChannelSpdAdq412.cpp
        spd/ADQ412/CSpdAdq412.cpp)
endif(SPDEVICES)

add_library(DaqManagerLib STATIC ${DAQMANAGER_SRC})
add_dependencies(DaqManagerLib Boost ntofutils)

add_executable(DaqManager DaqManager.cpp)

target_link_libraries(DaqManager DaqManagerLib ntofutils
    ${DIM_LIBRARIES} ${Boost_LIBRARIES} ${FREEIPMI_LIBRARIES}
    ${PTHREAD_LIBRARIES} ${NUMA_LIBRARIES} ${READLINE_LIBRARIES}
  ${BLKID_LIBRARIES} ${SPDEVICES_LIBRARIES} ${PUGI_LIBRARIES})

install(TARGETS DaqManager
    RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}" COMPONENT Runtime)
