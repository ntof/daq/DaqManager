#include <cstring>
#include <exception>
#include <iostream>
#include <regex>
#include <string>
#include <type_traits>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <boost/version.hpp>

#include <DIMException.h>
#include <NTOFLogging.hpp>
#include <fcntl.h>
#include <linux/fs.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/utsname.h>

#include "CDaq.h"
#include "CSystemInfo.h"
#include "CardProber.h"
#include "config.h"
#include "logger.h"
#include "typedefs.h"

#include <dis.hxx>

static volatile sig_atomic_t stop = 0;

static int ProcessCmdLineParams(int,
                                char **,
                                boost::program_options::variables_map &,
                                CMD_LINE_PARAM_t &);
static void UpdateCalibratedFsOffsetService(
  const std::vector<std::shared_ptr<CCard>> &cards);
static uint64_t ParseSizeOption(std::string option);

static void term(int sig)
{
  if (sig == SIGINT)
    stop = 1;
}

int main(int argc, char **argv)
{
  ntof::log::init(argc, argv);
  CMD_LINE_PARAM_t cmdLineParams;
  boost::program_options::variables_map vm;

  cmdLineParams.loggerLevel = boost::log::trivial::info;
  cmdLineParams.logPath = "/DAQ/log/DaqManager/";
  cmdLineParams.cardCalibrationEnabled = true;
  cmdLineParams.writePulseRawData = false;
  cmdLineParams.writeCardRawData = false;
  cmdLineParams.timingEventName = "Timing/event";
  cmdLineParams.standAlone = false;
  cmdLineParams.dataPath = "/DAQ/data/";
  cmdLineParams.cardProbingMode = false;
  cmdLineParams.sectorSize = 512;
  cmdLineParams.fullScale = 5000.0;
  cmdLineParams.offset = 0.0;
  cmdLineParams.simulation = false;
  cmdLineParams.simBufferSize = 4 * 1024;
  cmdLineParams.writerFileBufferSize = 48 * 1024 * 1024 * 1024LL;
  cmdLineParams.nbVirtualCards = 2;
  cmdLineParams.acqPoolSize = 20;
  cmdLineParams.spdLogTrace = false;
  cmdLineParams.lockFile = "/run/lock/puppetlock";

  // Catch ctrl+c signal
  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_handler = term;
  sigaction(SIGINT, &action, NULL);

  try
  {
    if (ProcessCmdLineParams(argc, argv, vm, cmdLineParams) != 0)
      return 0;
  }
  catch (const std::runtime_error &ex)
  {
    std::cerr << ex.what() << std::endl;
    return -1;
  }

  if (!boost::filesystem::is_directory(cmdLineParams.logPath))
  {
    std::cerr << "Log directory not found: " << cmdLineParams.logPath
              << std::endl;
    return -1;
  }
  else if (!boost::filesystem::is_directory(cmdLineParams.dataPath))
  {
    std::cerr << "Data directory not found: " << cmdLineParams.dataPath
              << std::endl;
    return -1;
  }

  // Initialize logger
  InitLogger(cmdLineParams.logPath, cmdLineParams.loggerLevel);

  BOOST_LOG_SCOPED_THREAD_ATTR(
    "Scope", boost::log::attributes::constant<std::string>("Main"));

  /*using namespace g3;
   std::unique_ptr<LogWorker> logworker{ LogWorker::createLogWorker() };
   auto sinkHandle = logworker->addSink(std2::make_unique<g3logSink>(),
   &g3logSink::ReceiveLogMessage);

   // initialize the logger before it can receive LOG calls
   initializeLogging(logworker.get());
   LOG(WARNING) << "This log call, may or may not happend before"
   << "the sinkHandle->call below";*/

  // You can call in a thread safe manner public functions on your sink
  // The call is asynchronously executed on your custom sink.
  // std::future<void> received = sinkHandle->call(&g3logSink::Foo, param1,
  // param2);
  // get linux version
  struct utsname linuxVer;
  if (uname(&linuxVer) != 0)
  {
    perror("could not get linux version:");
    LOG_FATAL << "DAQ error: could not get linux version";
    return -1;
  }

  LOG_INFO << "----------------------------------------------------------------"
              "----------------";
  LOG_INFO << "DaqManager " << DAQMANAGER_VERSION << " (" << GIT_SHORT << ")";
  LOG_INFO << "-- compiled with: ";
  LOG_INFO << "GCC " << __GNUG__ << "." << __GNUC_MINOR__ << "."
           << __GNUC_PATCHLEVEL__;
  LOG_INFO << "Boost " << std::string(BOOST_LIB_VERSION);
  LOG_INFO << "readline " << std::string(rl_library_version);
  LOG_INFO << "-- linux version: ";
  LOG_INFO << std::string(linuxVer.release);
  LOG_INFO << std::string(linuxVer.version);
  LOG_INFO << "-- options: ";
  LOG_INFO
    << "calibration enabled: "
    << boost::lexical_cast<std::string>(cmdLineParams.cardCalibrationEnabled);
  LOG_INFO << "dim dns server: " << cmdLineParams.dimDnsServer;
  LOG_INFO << "hw config file name: " << cmdLineParams.hwConfigFilename;
  LOG_INFO << "standalone mode: "
           << boost::lexical_cast<std::string>(cmdLineParams.standAlone);
  LOG_INFO << "nb of ksamples: "
           << boost::lexical_cast<std::string>(cmdLineParams.sampleSize);
  LOG_INFO << "fullscale: "
           << boost::lexical_cast<std::string>(cmdLineParams.fullScale);
  LOG_INFO << "zero threshold: "
           << boost::lexical_cast<std::string>(cmdLineParams.zsThreshold);
  LOG_INFO
    << "writer file buffer size: "
    << boost::lexical_cast<std::string>(cmdLineParams.writerFileBufferSize);
  LOG_INFO << "nb of virtual cards created: "
           << boost::lexical_cast<std::string>(cmdLineParams.nbVirtualCards);
  LOG_INFO << "virtual card buffer size: "
           << boost::lexical_cast<std::string>(cmdLineParams.simBufferSize);
  LOG_INFO << "acquisition pool size: "
           << boost::lexical_cast<std::string>(cmdLineParams.acqPoolSize);
  LOG_INFO << "SP-Device log trace enabled: "
           << boost::lexical_cast<std::string>(cmdLineParams.spdLogTrace);
  if (cmdLineParams.simulation)
  {
    LOG_INFO << "!! running in simulation mode, virtual cards will be created, "
                "buffer size = "
             << cmdLineParams.simBufferSize;
  }
  LOG_INFO << "----------------------------------------------------------------"
              "----------------";

  if (cmdLineParams.cardProbingMode)
  {
#ifdef ACQIRIS
    AcqirisProbe("PCI::INSTR");
#endif
#ifdef SPDEVICES
    SpdProbe();
#endif
    return 0;
  }

  // Force memory allocation to be in RAM
  if (mlockall(MCL_CURRENT | MCL_FUTURE) != 0)
  {
    perror("mlockall failed:");
    LOG_FATAL << "DAQ error: cannot force memory allocation to RAM";
    return -1;
  }

  // get sector size for memory alignment
  try
  {
    int fd = open(CSystemInfo::GetFilesystem(cmdLineParams.dataPath).c_str(),
                  O_RDONLY | O_NONBLOCK);
    size_t sectorSize = 0;
    ioctl(fd, BLKSSZGET, &sectorSize);
    close(fd);

    cmdLineParams.sectorSize = sectorSize;

    LOG_INFO << "--------------------------------------------------------------"
                "------------------";
    LOG_INFO << "sector size: " + boost::lexical_cast<std::string>(sectorSize) +
        ", page size: " + boost::lexical_cast<std::string>(getpagesize());
    LOG_INFO << "--------------------------------------------------------------"
                "------------------";
  }
  catch (const std::runtime_error &ex)
  {
    LOG_FATAL << std::string(ex.what());
    return -1;
  }

  // Start DIM server
  char hostname[128] = {};
  if (gethostname(hostname, 128) != 0)
  {
    LOG_FATAL << "can not get host name";
    return -1;
  }
  try
  {
    DimClient::setDnsNode(cmdLineParams.dimDnsServer.c_str());
    DimServer::setDnsNode(cmdLineParams.dimDnsServer.c_str());
    DimServer::start(hostname);
  }
  catch (const ntof::dim::DIMException &ex)
  {
    LOG_FATAL << "DAQ error: " + std::string(ex.what());
    return -1;
  }

  // Start thread to get system's information
  CSystemInfo systemInfo;

  CDaq daq(cmdLineParams.hwConfigFilename, hostname, cmdLineParams);
  try
  {
    daq.InitServiceDim();
  }
  catch (const CDaqException &ex)
  {
    LOG_FATAL << "DAQ error: " + std::string(ex.what());
    stop = 1;
  }

  daq.setCardCalibrationEnabled(cmdLineParams.cardCalibrationEnabled);

  // Main state machine
  try
  {
    while (!stop)
    {
      switch (daq.GetState())
      {
      case DAQ_STATE_t::DETECTING_HARDWARE:
        try
        {
          daq.FindDevices();
          daq.InitializeThreads(cmdLineParams.lockFile);
        }
        catch (const CDaqException &ex)
        {
          LOG_FATAL
            << "error while detecting hardware: " + std::string(ex.what());
          daq.DeleteAll();
          daq.SetStateError(ex.what());
          break;
        }
#ifdef CHARACTERIZATION
        LOG_INFO << "hardware detection ok, going to super_user state ...";
        daq.SetState(DAQ_STATE_t::SUPER_USER);
#else
        LOG_INFO << "hardware detection ok, going to unconfigured state ...";
        daq.SetState(DAQ_STATE_t::UNCONFIGURED);
#endif
        break;

      case DAQ_STATE_t::UNCONFIGURED:
      {
        // Checking if all cards and channels have received and accepted their
        // configuration
        size_t nbValid = 0;
        for (auto &card : daq.GetCardCollection())
        {
          if (card->IsConfigurationValid() == true)
            nbValid++;
        }

        if (nbValid == daq.GetCardCollection().size())
        {
          // if calibration is enabled, jump to calibration state
          if (cmdLineParams.cardCalibrationEnabled)
          {
            daq.SetState(DAQ_STATE_t::CALIBRATING_CARDS);
          }
          else
          {
            for (auto &card : daq.GetCardCollection())
            {
              for (auto &ch : card->GetChannelCollection())
              {
                ch->SetCalibratedFullScale(
                  static_cast<float>(cmdLineParams.fullScale));
                ch->SetCalibratedOffset(0);
                ch->SetCalibratedThreshold(cmdLineParams.zsThreshold);
              }
            }
            LOG_INFO << "card calibration is disabled, waiting for command ...";
            daq.SetState(DAQ_STATE_t::WAITING_FOR_CMD);
          }
        }
        break;
      }

      case DAQ_STATE_t::CALIBRATING_CARDS:
      {
        bool error = false;
        std::vector<boost::thread> calibrationThreads;

        LOG_INFO << "calibrating cards ...";
        for (auto &card : daq.GetCardCollection())
        {
          try
          {
            card->configure();
            if (card->IsCardEnabled())
            {
              calibrationThreads.push_back(
                boost::thread(boost::bind(&CCard::Calibrate, card)));
            }
          }
          catch (const CCardEx &ex)
          {
            LOG_FATAL << "could not configure card " << card->GetCardIndex()
                      << ", " << card->GetSerialNumber() << ", " << ex.what();
            daq.SetStateError(ex.what());
            error = true;
            break;
          }
        }

        if (error)
          break;

        // waiting for end of calibration
        LOG_INFO << "waiting for end of calibration ...";
        try
        {
          for (boost::thread &t : calibrationThreads)
            t.join();
        }
        catch (const CCardEx &ex)
        {
          LOG_FATAL << ex.what();
          daq.SetStateError(ex.what());
          break;
        }

        // update fullscale values with calibrated ones
        UpdateCalibratedFsOffsetService(daq.GetCardCollection());

        LOG_INFO << "card are calibrated, waiting for command ...";
        daq.SetState(DAQ_STATE_t::WAITING_FOR_CMD);
        break;
      }

      case DAQ_STATE_t::WAITING_FOR_CMD: break;

      case DAQ_STATE_t::PROCESSING_CMD: break;

      case DAQ_STATE_t::STOPPING_ACQ:
        daq.StopAcq();
        daq.SetConfigurationInvalid();
        daq.SetState(DAQ_STATE_t::UNCONFIGURED);
        break;

      case DAQ_STATE_t::SUPER_USER:
      {
        daq.ResetErrors();
        std::vector<boost::thread> calibrationThreads;
        LOG_INFO << "enabling all channels ...";
        for (auto &card : daq.GetCardCollection())
        {
          for (auto &ch : card->GetChannelCollection())
          {
            ch->getConfiguration()->enabled = true;
            ch->getConfiguration()->header.fullScale = cmdLineParams.fullScale;
            ch->getConfiguration()->header.offset = cmdLineParams.offset;
            ch->getConfiguration()->header.sampleSize = cmdLineParams.sampleSize;
            ch->getConfiguration()->header.zeroSuppressionStart = 0;
            ch->getConfiguration()->header.setClockState("INTC");
            ch->GetZeroSuppression().SetThreshold(cmdLineParams.zsThreshold);

            // set default value in case the calibration is disabled
            if (!cmdLineParams.cardCalibrationEnabled)
            {
              ch->SetCalibratedFullScale(
                static_cast<float>(cmdLineParams.fullScale));
              ch->SetCalibratedOffset(0);
              ch->SetCalibratedThreshold(cmdLineParams.zsThreshold);
            }

            ch->copyListParam();
            ch->UpdateZeroSuppressionConfiguration();
          }

          try
          {
            card->configure();
          }
          catch (const CCardEx &ex)
          {
            LOG_ERROR << "could not configure card: " + std::string(ex.what());
            daq.ResetErrors();
            daq.SetConfigurationInvalid();
            daq.SetState(DAQ_STATE_t::UNCONFIGURED);
            break;
          }

          if (cmdLineParams.cardCalibrationEnabled)
          {
            calibrationThreads.push_back(
              boost::thread(boost::bind(&CCard::Calibrate, card)));
          }
        }

        if (daq.GetState() != DAQ_STATE_t::SUPER_USER)
          break;

        // waiting for end of calibration
        LOG_INFO << "waiting for end of calibration ...";
        try
        {
          for (boost::thread &t : calibrationThreads)
            t.join();
        }
        catch (const CCardEx &ex)
        {
          LOG_FATAL << ex.what();
          daq.SetStateError(ex.what());
          break;
        }

        // update fullscale values with calibrated ones
        UpdateCalibratedFsOffsetService(daq.GetCardCollection());

        LOG_INFO << "calibration done";
#ifdef CHARACTERIZATION
        daq.SetState(DAQ_STATE_t::WAITING_FOR_CMD);
        daq.InitAcq();
        daq.StartAcq();
#else
        daq.SetState(DAQ_STATE_t::WAITING_FOR_CMD);
#endif
        break;
      }

      case DAQ_STATE_t::INITIALIZING_ACQ:
        daq.InitAcq();
        daq.SetState(DAQ_STATE_t::WAITING_FOR_START_ACQ_CMD);
        break;

      case DAQ_STATE_t::WAITING_FOR_START_ACQ_CMD: break;

      case DAQ_STATE_t::STARTING_ACQ:
        daq.StartAcq();
        daq.SetState(DAQ_STATE_t::WAITING_FOR_CMD);
        break;

      case DAQ_STATE_t::RESETING_DAQ:
        LOG_INFO << "Reseting errors";
        daq.ResetErrors();
        LOG_INFO << "Set configurations as invalids";
        daq.SetConfigurationInvalid();
        daq.SetState(DAQ_STATE_t::UNCONFIGURED);
        break;

      case DAQ_STATE_t::ERROR:
        LOG_ERROR << "DAQ in error state";
        boost::this_thread::sleep(boost::posix_time::seconds(2));
        break;

      default: throw std::runtime_error("DAQ is in an undefined state"); break;
      }

      boost::this_thread::sleep(boost::posix_time::milliseconds(100));
    }
  }
  catch (std::runtime_error const &ex)
  {
    LOG_FATAL << ex.what();
  }

  DimServer::stop();

  LOG_INFO << "exiting application...";

  return 0;
}

static void UpdateCalibratedFsOffsetService(
  const std::vector<std::shared_ptr<CCard>> &cards)
{
  LOG_TRACE << "entering UpdateCalibratedFsOffsetService";

  for (auto &card : cards)
  {
    for (auto &ch : card->GetChannelCollection())
    {
      ch->updateCalibrationService();
    }
  }

  LOG_TRACE << "exiting UpdateCalibratedFsService";
}

static int ProcessCmdLineParams(int argc,
                                char *argv[],
                                boost::program_options::variables_map &vm,
                                CMD_LINE_PARAM_t &cmd_line_params)
{
  /* process command line parameters */
  // when a default value is set, the option is present (i.e. vm.count returns
  // true) even if it is not specified on the command line
  try
  {
    boost::program_options::options_description desc("option list");

    desc.add_options()("help,h", "produce help message")(
      "hw-config",
      boost::program_options::value<std::string>(
        &cmd_line_params.hwConfigFilename)
        ->default_value("daq-m6.xml"),
      "xml hardware configuration file")(
      "log-path",
      boost::program_options::value<std::string>(&cmd_line_params.logPath)
        ->default_value("/DAQ/log/DaqManager/"),
      "set path for log file")(
      "lock-file",
      boost::program_options::value<std::string>(&cmd_line_params.lockFile)
        ->default_value("/run/lock/puppetlock"),
      "set flock(2) file to lock during operation")(
      "v,v", "set logger to debug level")("vv", "set logger to trace level")(
      "no-cal", "disable card calibration")(
      "dim-dns",
      boost::program_options::value<std::string>(&cmd_line_params.dimDnsServer)
        ->default_value("ntofproxy-1"),
      "set DIM DNS server")("raw-pulses",
                            "write pulse raw data to dedicated file")(
      "card-raw-data", "write card raw data to dedicated file")(
      "no-eacs", "standalone mode without EACS")(
      "zs-threshold",
      boost::program_options::value<float>(&cmd_line_params.zsThreshold)
        ->default_value(0.0),
      "zero suppression threshold")(
      "probe",
      "enable card probing mode, application will exit at the end of probing")(
      "nb-ksamples",
      boost::program_options::value<uint32_t>(&cmd_line_params.sampleSize)
        ->default_value(0),
      "set the number of kilo-samples, if set to 0, 4096 samples will be used "
      "for characterization configuration")(
      "fullscale",
      boost::program_options::value<int32_t>(&cmd_line_params.fullScale)
        ->default_value(5000),
      "set the fullscale")(
      "offset",
      boost::program_options::value<float>(&cmd_line_params.offset)
        ->default_value(0),
      "set the offset")("sim",
                        "card simulation mode, virtual cards will be created")(
      "sim-buffer-size",
      boost::program_options::value<std::string>(
        &cmd_line_params.simBufferSizeString)
        ->default_value("4K"),
      "set the buffer size of the virtual card, use K, M, G for units")(
      "writer-buffer-size",
      boost::program_options::value<std::string>(
        &cmd_line_params.writerFileBufferSizeString)
        ->default_value("16G"),
      "set the file buffer size for writer, use K, M, G for units")(
      "nb-virtual-cards",
      boost::program_options::value<int>(&cmd_line_params.nbVirtualCards)
        ->default_value(2),
      "set the number of virtual cards to be created")(
      "acq-pool-size",
      boost::program_options::value<int>(&cmd_line_params.acqPoolSize)
        ->default_value(20),
      "set the number of acquisition buffer to be allocated in pool")(
      "enable-spd-trace", "enable trace buffer for SP-Device driver");

    boost::program_options::positional_options_description pod;
    pod.add("log_filename", -1);

    boost::program_options::store(
      boost::program_options::command_line_parser(argc, argv)
        .options(desc)
        .positional(pod)
        .run(),
      vm);
    if (boost::filesystem::exists(DAQ_CONFIG))
    {
      boost::program_options::store(
        boost::program_options::parse_config_file(DAQ_CONFIG, desc), vm);
    }
    boost::program_options::notify(vm);

    if (vm.count("help"))
    {
      std::cout << desc << std::endl;
      return 1;
    }

    if (vm.count("v"))
      cmd_line_params.loggerLevel = boost::log::trivial::severity_level::debug;

    if (vm.count("vv"))
      cmd_line_params.loggerLevel = boost::log::trivial::severity_level::trace;

    if (vm.count("no-cal"))
      cmd_line_params.cardCalibrationEnabled = false;

    if (vm.count("raw-pulses"))
      cmd_line_params.writePulseRawData = true;

    if (vm.count("card-raw-data"))
      cmd_line_params.writeCardRawData = true;

    if (vm.count("no-eacs"))
      cmd_line_params.standAlone = true;

    if (vm.count("probe"))
      cmd_line_params.cardProbingMode = true;

    if (vm.count("sim"))
      cmd_line_params.simulation = true;

    cmd_line_params.simBufferSize = ParseSizeOption(
      cmd_line_params.simBufferSizeString);

    cmd_line_params.writerFileBufferSize = ParseSizeOption(
      cmd_line_params.writerFileBufferSizeString);

    if (cmd_line_params.acqPoolSize < 0 || cmd_line_params.acqPoolSize > 20)
      throw std::runtime_error("acq-pool-size should be >0 and <20");

    if (!vm.count("hw-config"))
      throw std::runtime_error("hardware configuration file not specified");

    if (vm.count("enable-spd-trace"))
      cmd_line_params.spdLogTrace = true;
  }
  catch (const boost::program_options::error &ex)
  {
    std::string err = "program terminated with error: " + std::string(ex.what());
    throw std::runtime_error(err);
  }

  return 0;
}

static uint64_t ParseSizeOption(std::string option)
{
  std::regex r("([[:digit:]]+)([K|M|G]?)");
  std::smatch match;
  uint64_t val;

  if (std::regex_match(option, r))
  {
    std::regex_search(option, match, r);
    val = boost::lexical_cast<std::uint32_t>(match[1]);
    if (match[2].length() > 0)
    {
      if (match[2].str().c_str()[0] == 'K')
        val *= 1024;
      else if (match[2].str().c_str()[0] == 'M')
        val *= 1024 * 1024;
      else if (match[2].str().c_str()[0] == 'G')
        val *= 1024 * 1024 * 1024;
    }
  }
  else
    throw std::runtime_error("invalid " + option + " option");

  return val;
}
