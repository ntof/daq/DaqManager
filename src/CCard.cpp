#include "CCard.h"

#include <boost/lexical_cast.hpp>

#include <NTOFLogging.hpp>
#include <numa.h>

#include "logger.h"

uint CCard::bufferPoolIndex_ = 0;

CCard::CCard(std::string type,
             std::string serialNumber,
             int cpuCore,
             uint32_t cardIndex,
             uint32_t crateIndex,
             ntof::dim::DIMState *state,
             bool /*enableTrace*/) :
  cardIndex_(cardIndex),
  crateIndex_(crateIndex),
  serialNumber_(serialNumber),
  state_(state),
  type_(type)
{
  nbDevices_ = 0;
  configurationValid_ = false;
  slotNumber_ = 0;
  vectorIndex_ = 0;
  cardEnabled_ = true;
  nbBytesDumped_ = 0;
  isMaster_ = false;
  poolSize_ = 12;

  SetCpuCore(cpuCore, cpuCore + 1);

  // Get host's name
  char hostname[128] = {};
  if (gethostname(hostname, 128) != 0)
  {
    throw CCardEx("can not get host name");
  }
  hostName_ = std::string(hostname);
}

CCard::~CCard()
{
  LOG_TRACE << "Entering CCard::~CCard";

  FreeBufferPool();

  channel_.clear();

  LOG_TRACE << "Exiting CCard::~CCard";
}

bool CCard::isMaster()
{
  return isMaster_;
}

void CCard::SetAsMaster(bool master)
{
  isMaster_ = master;
}

uint32_t CCard::GetCrateIndex()
{
  return crateIndex_;
}

std::string CCard::GetType()
{
  return type_;
}

void CCard::SetCpuCore(int acq, int writer)
{
  int ncpus = numa_num_task_cpus();

  if (ncpus >= CPU_SETSIZE)
    throw CCardEx("wrong number of CPUs detected: " +
                  boost::lexical_cast<std::string>(ncpus) +
                  " cpus detected instead of " +
                  boost::lexical_cast<std::string>(CPU_SETSIZE));

  if (acq < 0 || writer < 0)
    throw CCardEx("core number cannot be negative");

  if (acq > ncpus - 1 || writer > ncpus - 1)
    throw CCardEx("core number should be less than " +
                  boost::lexical_cast<std::string>(ncpus));

  acqCore_ = acq;
  writerCore_ = writer;
}

bool CCard::IsCardEnabled()
{
  cardEnabled_ = true;

  uint nbChannelEnabled = 0;
  for (auto &ch : channel_)
  {
    if (ch->getConfiguration()->enabled)
      nbChannelEnabled++;
  }

  if (nbChannelEnabled == 0)
    cardEnabled_ = false;

  return cardEnabled_;
}

bool CCard::HasHardwareZeroSuppression()
{
  return false;
}

void CCard::GetCpuCore(int &acq, int &writer)
{
  acq = acqCore_;
  writer = writerCore_;
}

int CCard::GetWriterCore()
{
  return writerCore_;
}

int CCard::GetAcqCore()
{
  return acqCore_;
}

uint CCard::GetBufferPoolIndex()
{
  return bufferPoolIndex_;
}

void CCard::SetBufferPoolIndex(uint index)
{
  bufferPoolIndex_ = index;
}

std::unique_ptr<AcquisitionBuffer> &CCard::GetBuffer(int index)
{
  if (index < 0 || index > ACQ_BUFFER_POOL_SIZE)
    throw CCardEx("invalid value");

  return bufferPool_[index];
}

void CCard::FreeBufferPool()
{
  bufferPool_.clear();
}

uint32_t CCard::GetChannelAllocationSize()
{
  return GetChannelBufferSize();
}

void CCard::AllocateBufferPool(int poolSize)
{
  LOG_TRACE << "[card #" << cardIndex_
            << "] entering CCard::AllocateAcqBufferPool";

  if (channel_.size() == 0)
    throw CCardEx("channel vector size is 0");

  if (poolSize < 0 || poolSize > 20)
    throw CCardEx("pool size should be >0 and <21");

  LOG_DEBUG << "pool size = " << poolSize;

  bufferPool_.reserve(poolSize);

  try
  {
    for (int i = 0; i < poolSize; ++i)
    {
      bufferPool_.emplace_back(
        std::unique_ptr<AcquisitionBuffer>(new AcquisitionBuffer()));
      bufferPool_.back()->AllocateBufferPool(channel_.size(),
                                             GetChannelAllocationSize());

      // allocate buffer for unparsed data
      if (HasOfflineParsing())
      {
        bufferPool_.back()->AllocateDumpBuffer(GetDumpBufferSize());
      }
    }
  }
  catch (const std::bad_alloc &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "[card #" + boost::lexical_cast<std::string>(cardIndex_) +
      "] exiting CCard::AllocateAcqBufferPool";
}

int16_t **CCard::GetChannelBuffer(uint index)
{
  if (index >= bufferPool_.size())
    throw CCardEx("invalid index");

  return bufferPool_[index]->buffer_.data();
}

int8_t *CCard::GetUnparsedBuffer(uint index)
{
  if (index >= bufferPool_.size())
    throw CCardEx("invalid index");

  return bufferPool_[index]->dumpBuffer_;
}

void CCard::PerformZeroSuppression(ZeroSuppression::zsMode mode,
                                   const int16_t **channelBuffers)
{
  // performing zero suppression on enabled channels
  for (auto &ch : channel_)
  {
    if (ch->getConfiguration()->enabled == false)
      continue;

    if (mode != ZeroSuppression::zsMode::INDEPENDENT && !ch->isIndependent())
    {
      if (!isMaster_)
        continue;
      else if (!ch->isMaster())
        continue;
    }

    ch->GetZeroSuppression().PerformZeroSuppression(
      channelBuffers[ch->GetIndex()],
      (uint64_t)(GetChannelBufferSize() / sizeof(**channelBuffers)), nullptr);

    // logging
    LOG_TRACE << "[C" << cardIndex_ << ":CH" << ch->GetIndex()
              << "] zero suppression: "
              << ch->GetZeroSuppression().GetPulseDataCollection().size()
              << " pulse(s) found";

#ifdef CHARACTERIZATION
    if (ch->GetZeroSuppression().GetPulseDataCollection().size())
    {
      static int fileIndex = 0;
      int fd = open(
        std::string("/DAQ/data/" + serialNumber_ + "_pulse_ch" +
                    boost::lexical_cast<std::string>(ch->GetIndex()) + "_" +
                    boost::lexical_cast<std::string>(++fileIndex) + ".raw")
          .c_str(),
        O_WRONLY | O_CREAT, 0777);

      if (fd == -1)
      {
        LOG_ERROR << "could not create file for raw data: " +
            std::string(strerror(errno));
        return;
      }

      for (auto &pulse : ch->GetZeroSuppression().GetPulseDataCollection())
      {
        ssize_t b = write(fd, pulse.data, pulse.length * sizeof(*pulse.data));

        if (b != pulse.length * sizeof(*pulse.data))
        {
          LOG_ERROR << "could not write pulse data file, byte written: " +
              boost::lexical_cast<std::string>(b);
        }
      }

      fsync(fd);
      close(fd);
    }
#endif
  }
}

std::shared_ptr<ntof::CChannel> &CCard::GetChannel(uint channel)
{
  return channel_[channel];
}

CCard::ChannelCollection &CCard::GetChannelCollection()
{
  return channel_;
}

bool CCard::IsConfigurationValid()
{
  bool valid = true;
  for (auto &ch : channel_)
  {
    // if ((*it)->getConfiguration()->enabled)
    valid &= ch->IsConfigurationValid();
  }

  return valid /* & configurationValid_*/;
}

std::string CCard::GetSerialNumber()
{
  return serialNumber_;
}

bool CCard::IsDumpSequential()
{
  return false;
}

void CCard::SetConfigurationInvalid()
{
  configurationValid_ = false;
  for (auto &ch : channel_)
  {
    ch->SetConfigurationInvalid();
  }
}

uint32_t CCard::GetSlotNumber()
{
  return slotNumber_;
}

void CCard::OfflineParsing(int8_t *dumpBuffer)
{
  UNUSED(dumpBuffer);

  LOG_TRACE << "entering and exiting CCard::OfflineParsing";
}

void CCard::SetUnparsedBuffer(int8_t *buffer)
{
  UNUSED(buffer);

  LOG_TRACE << "entering and exiting CCard::SetDumpMemoryBuffer";
}

void CCard::readout()
{
  LOG_TRACE << "entering and exiting CCard::readout";
}

int8_t *CCard::GetMemoryDumpBuffer()
{
  return NULL;
}

int CCard::CheckStatus()
{
  return 0;
}

uint32_t CCard::GetDumpBufferSize()
{
  return 0;
}

uint32_t CCard::GetNbUsedChannels()
{
  uint32_t i = 0;
  for (auto &ch : channel_)
  {
    if (ch->getConfiguration()->enabled == false)
      continue;
    i++;
  }

  return i;
}

/*void CCard::DumpCardMemory()
 {
 LOG_TRACE << "entering and exiting CCard::DumpCardMemory");
 }*/

int CCard::GetBitMask()
{
  return 0xFFFF;
}

// Exception class for CCard
CCardEx::CCardEx() throw()
{
  exceptionMsg_ = "none";
}

CCardEx::CCardEx(std::string msg) throw() : exceptionMsg_(msg) {}

CCardEx::~CCardEx() throw() {}

const char *CCardEx::what() const throw()
{
  return exceptionMsg_.c_str();
}

void CCardEx::SetMessage(std::string msg)
{
  exceptionMsg_ = msg;
}
