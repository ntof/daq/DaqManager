#include "spd/ADQ14/CSpdAdq14.h"

#include <cstdint>
#include <ctime>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <ADQAPI.h>
#include <fcntl.h>
#include <numa.h>
#include <sys/time.h>
#include <unistd.h>

#include "CCard.h"
#include "PerformanceCounter.h"
#include "logger.h"
#include "spd/spd_typedef.h"

namespace spdevices {

Adq14::Adq14(void *driverInstance_, int adqNumber) :
  CSpd("ADQ14",
       driverInstance_,
       adqNumber,
       GetNbChannels(),
       GetResolutionBits(),
       14)
{
  performanceCounterEnabled = false;
  triggerTime = 0.;
  transferDataRate = 0.;
  parsingDataRate = 0.;
  hddDataRate = 0.;

  temperature_.name.push_back("PCB");
  temperature_.name.push_back("ADC1");
  temperature_.name.push_back("ADC2");
  temperature_.name.push_back("FPGA");
  temperature_.name.push_back("DCDC2A");
  temperature_.name.push_back("DCDC2B");
  temperature_.name.push_back("DCDC1");

  temperature_.value.reserve(temperature_.name.size());
  for (uint i = 0; i < temperature_.name.size(); ++i)
    temperature_.value.push_back(0.0);
}

Adq14::~Adq14() {}

uint Adq14::GetNbChannels()
{
  return 4;
}

uint Adq14::GetResolutionBits()
{
  return 16;
}

double Adq14::GetBaseSamplingFreq()
{
  return 1000.0;
}

void Adq14::MemoryDump()
{
  nbBytesDumped_ = 0;

  if (ADQ_GetStreamOverflow(
        driverInstance_,
        adqNumber_)) // This part is needed to prevent a lock-up in case of
                     // overflow, which can happen very rarely in normal use
  {
    // ADQ_ResetDevice(driverInstance_, adqNumber_, 4);
    throw CCardEx("Stream Overflow");
  }

  if (targetBuffer_ == nullptr)
  {
    throw(CCardEx("invalid target buffer"));
  }

  int rtn = ADQ_GetData(driverInstance_, adqNumber_, (void **) targetBuffer_,
                        nbSamples_, sizeof(**targetBuffer_), 0, 1, 0xF, 0,
                        nbSamples_, ADQ_TRANSFER_MODE_NORMAL);

  if (rtn != 1)
  {
    std::string err = "API GetData: " +
      boost::lexical_cast<std::string>(
                        ADQ_GetLastError(driverInstance_, adqNumber_));

    rtn = ADQ_GetStreamOverflow(driverInstance_, adqNumber_);
    if (rtn == 1)
      err += ", stream overflow: " +
        boost::lexical_cast<std::string>(
               ADQ_GetLastError(driverInstance_, adqNumber_));

    throw CCardEx(err);
  }

  nbBytesDumped_ = static_cast<uint32_t>(nbChannels_ * nbSamples_ *
                                         sizeof(**targetBuffer_));

  if (!ADQ_DisarmTrigger(driverInstance_, adqNumber_))
  {
    throw CCardEx("API DisarmTrigger: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

#ifdef CHARACTERIZATION
  for (uint i = 0; i < temperature_.value.size(); ++i)
  {
    ADQ_GetTemperatureFloat(driverInstance_, adqNumber_, i,
                            &temperature_.value[i]);
    LOG_INFO << temperature_.name[i] << ": " << temperature_.value[i];
  }
#endif
}

void Adq14::SetAcquisitionParameters(float samplingFreq,
                                     int32_t nbSamples,
                                     int32_t delay)
{
#ifdef CHARACTERIZATION
  nbSamples_ = nbSamples == 0 ? 4096 : nbSamples * 1000;
#else
  nbSamples_ = static_cast<uint32_t>(nbSamples * 1000);
#endif

  nbSamples_ = (nbSamples_ + DIRECTIO_BLOCK_SIZE - 1) &
    ~(DIRECTIO_BLOCK_SIZE - 1);

  double divider = GetBaseSamplingFreq() / samplingFreq;

  if (divider >= 1.0 && divider < 2.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::NONE);
  else if (divider >= 2.0 && divider < 3.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_2);
  else if (divider >= 3.0 && divider < 5.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_4);
  else if (divider >= 5.0 && divider < 9.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_8);
  else if (divider >= 9.0 && divider < 17.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_16);
  else if (divider >= 17.0 && divider < 33.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_32);
  else if (divider >= 33.0 && divider < 65.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_64);
  else
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_128);

  try
  {
    // granularity for SPD14 is 4 samples
    SetTriggerDelay(
      delay,
      static_cast<float>(GetBaseSamplingFreq() /
                         ADQ_GetSampleSkip(driverInstance_, adqNumber_)),
      4);
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  // The linux driver has a default to 2x512k buffers, but only 1 is used
  // let's allocate a couple more to have better performance
  if (ADQ_SetTransferBuffers(driverInstance_, adqNumber_, 4, 524288) != 1)
  {
    throw CCardEx("API SetTransferBuffers" +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  if (ADQ_MultiRecordSetup(driverInstance_, adqNumber_, 1, nbSamples_) != 1)
  {
    throw CCardEx("API MultiRecordSetup: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  if (ADQ_GetMaxNofSamplesFromNofRecords(driverInstance_, adqNumber_, 1,
                                         &maxSamples_) != 1)
  {
    throw CCardEx("API GetMaxNofSamplesFromNofRecords: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  if (maxSamples_ < nbSamples_)
  {
    throw CCardEx("too many samples required");
  }

  LogAcquisitionParams(samplingFreq, GetBaseSamplingFreq(), divider);
}

} // namespace spdevices
