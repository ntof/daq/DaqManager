#include "spd/ADQ14/CChannelSpdAdq14.h"

#include <boost/lexical_cast.hpp>

#include "defines.h"
#include "logger.h"

namespace spdevices {

CChannelSpdAdq14::CChannelSpdAdq14(uint32_t location,
                                   std::string dimServiceName,
                                   ntof::dim::DIMState *state) :
  ntof::CChannel(location, "S014", 1000.0, 128E3, 130, dimServiceName, state)
{
  copyListParam();
  UpdateZeroSuppressionConfiguration();

  // Register a callback handler to be notified in case of setting
  // modification/republication
  dimParamList_.setHandler(this);
}

CChannelSpdAdq14::~CChannelSpdAdq14() {}

int CChannelSpdAdq14::parameterChanged(
  std::vector<ntof::dim::DIMData> &settingsChanged,
  const ntof::dim::DIMParamList &list,
  int &errCode,
  std::string &errMsg)
{
  UNUSED(list);
  UNUSED(errCode);

  configurationValid_ = false;

  for (auto &it : settingsChanged)
  {
    switch (it.getIndex())
    {
    case ChannelConfiguration::enabled:
      configuration_->enabled = it.getBoolValue();
      if (configuration_->enabled == false)
      {
        std::ostringstream oss;
        oss << "configuration received for card " << cardIndex_;
        oss << ", channel " << channelIndex_ << " disabled";
        LOG_INFO << oss.str();
        configurationValid_ = true;
        return 0;
      }
      break;

    case ChannelConfiguration::channel:
      if (it.getUByteValue() != configuration_->header.getChannel())
      {
        std::ostringstream oss;
        oss << "channel number mismatch for card " << cardIndex_;
        oss << ", " << channelIndex_ << " != " << it.getUByteValue();
        errMsg = oss.str();
        state_->setError(100, errMsg);
        LOG_ERROR << errMsg;
        return -1;
      }
      break;

    case ChannelConfiguration::module:
      if (it.getUByteValue() != configuration_->header.getModule())
      {
        std::ostringstream oss;
        oss << "module number mismatch for card " << cardIndex_;
        oss << ", " << configuration_->header.getModule()
            << " != " << it.getUByteValue();
        errMsg = oss.str();
        state_->setError(100, errMsg);
        LOG_ERROR << errMsg;
        return -1;
      }
      break;

    case ChannelConfiguration::moduleType:
      if (it.getValueAsString() != cardName_)
      {
        std::ostringstream oss;
        oss << "module type configuration error for card " << cardIndex_;
        oss << ", channel " << channelIndex_;
        oss << ": should be ";
        oss << cardName_;
        errMsg = oss.str();
        state_->setError(100, errMsg);
        LOG_ERROR << errMsg;
        return -1;
      }
      break;

    case ChannelConfiguration::chassis:
      if (it.getUByteValue() != configuration_->header.getChassis())
      {
        std::ostringstream oss;
        oss << "chassis number mismatch for card " << cardIndex_;
        oss << ", " << configuration_->header.getChassis()
            << " != " << it.getUByteValue();
        errMsg = oss.str();
        state_->setError(100, errMsg);
        LOG_ERROR << errMsg;
        return -1;
      }
      break;

    case ChannelConfiguration::stream:
      configuration_->header.setStream(it.getUByteValue());
      break;

    case ChannelConfiguration::detectorType:
      configuration_->header.setDetectorType(it.getValueAsString());
      break;

    case ChannelConfiguration::detectorId:
      configuration_->header.detectorId = it.getUIntValue();
      break;

    case ChannelConfiguration::sampleRate:
      configuration_->header.sampleRate = it.getFloatValue();
      if (configuration_->header.sampleRate > maxSamplingRate_)
      {
        std::ostringstream oss;
        oss << "sampling rate configuration error for card " << cardIndex_;
        oss << ", channel " << channelIndex_;
        oss << ": cannot be higher than " +
            boost::lexical_cast<std::string>(maxSamplingRate_) + "MS/s";
        errMsg = oss.str();
        state_->setError(100, errMsg);
        LOG_ERROR << errMsg;
        return -1;
      }
      break;

    case ChannelConfiguration::sampleSize:
      configuration_->header.sampleSize = it.getUIntValue();
      if (configuration_->header.sampleSize > maxSamplingSize_ ||
          configuration_->header.sampleSize == 0)
      {
        std::ostringstream oss;
        oss << "sample size configuration error for card " << cardIndex_;
        oss << ", channel " << channelIndex_;
        oss << ": cannot be higher than ";
        oss << boost::lexical_cast<std::string>(maxSamplingSize_);
        oss << " or equals to 0";
        errMsg = oss.str();
        state_->setError(100, errMsg);
        LOG_ERROR << errMsg;
        return -1;
      }
      break;

    case ChannelConfiguration::fullScale:
      configuration_->header.fullScale = static_cast<uint32_t>(
        it.getFloatValue());
      if (configuration_->header.fullScale > maxFullScale_ ||
          configuration_->header.fullScale < minFullScale_)
      {
        std::ostringstream oss;
        oss << "full scale configuration error for card " << cardIndex_;
        oss << ", channel " << channelIndex_;
        oss << ": cannot be less than ";
        oss << boost::lexical_cast<std::string>(minFullScale_);
        oss << " or higher than ";
        oss << boost::lexical_cast<std::string>(maxFullScale_);
        errMsg = oss.str();
        state_->setError(100, errMsg);
        LOG_ERROR << errMsg;
        return -1;
      }
      break;

    case ChannelConfiguration::delayTime:
      configuration_->header.delayTime = static_cast<int32_t>(it.getIntValue());
      break;

    case ChannelConfiguration::threshold:
      configuration_->header.threshold = it.getFloatValue();
      break;

    case ChannelConfiguration::thresholdSign:
      configuration_->header.thresholdSign = it.getIntValue();
      if (configuration_->header.thresholdSign != 0 &&
          configuration_->header.thresholdSign != 1)
      {
        std::ostringstream oss;
        oss << "threshold sign configuration error for card " << cardIndex_;
        oss << ", channel " << channelIndex_;
        oss << ": should be 1 or 0";
        errMsg = oss.str();
        state_->setError(100, errMsg);
        LOG_ERROR << errMsg;
        return -1;
      }
      break;

    case ChannelConfiguration::zeroSuppressionStart:
      configuration_->header.zeroSuppressionStart = it.getUIntValue();
      break;

    case ChannelConfiguration::offset:
      configuration_->header.offset = it.getFloatValue();
      break;

    case ChannelConfiguration::preSample:
      configuration_->header.preSample = it.getUIntValue();
      break;

    case ChannelConfiguration::postSample:
      configuration_->header.postSample = it.getUIntValue();
      break;

    case ChannelConfiguration::clockState:
      if (it.getValueAsString() != "INTC" && it.getValueAsString() != "EXTC")
      {
        std::ostringstream oss;
        oss << "clock state configuration error for card " << cardIndex_;
        oss << ", channel " << channelIndex_;
        oss << ": should be INTC or EXTC";
        errMsg = oss.str();
        state_->setError(100, errMsg);
        LOG_ERROR << errMsg;
        return -1;
      }
      configuration_->header.setClockState(it.getValueAsString());
      break;

    case ChannelConfiguration::inputImpedance:
      configuration_->inputImpedance = it.getFloatValue();
      if (configuration_->inputImpedance != 50.0)
      {
        std::ostringstream oss;
        oss << "input impedance configuration error for card " << cardIndex_;
        oss << ", channel " << channelIndex_;
        oss << ": should be 50.0";
        errMsg = oss.str();
        state_->setError(100, errMsg);
        LOG_ERROR << errMsg;
        return -1;
      }
      break;

    default:
      //! TODO implement error is unknown param
      break;
    }
  }

  if (!isEqual() || !configurationValid_)
  {
    std::ostringstream oss;
    oss << "new channel configuration received for card " << cardIndex_;
    oss << ", channel " << channelIndex_;
    LOG_INFO << oss.str();
    copyListParam();
    UpdateZeroSuppressionConfiguration();
    configurationValid_ = true;
  }

  return 0;
}

} // namespace spdevices
