#include "spd/ADQ14/CCardSpdAdq14.h"

#include <cstdint>
#include <sstream>
#include <string>
#include <vector>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <DIMException.h>
#include <unistd.h>

#include "ADQAPI.h"
#include "CZeroSuppression.h"
#include "logger.h"

#include "dis.hxx"

namespace spdevices {
CCardSpdAdq14::CCardSpdAdq14(std::string serialNumber,
                             int cpuCore,
                             uint32_t cardIndex,
                             uint32_t crateIndex,
                             ntof::dim::DIMState *state,
                             bool enableTrace) :
  CCardSpd("S014",
           serialNumber,
           cpuCore,
           cardIndex,
           crateIndex,
           state,
           enableTrace)
{
  LOG_TRACE << "Entering CCardSpdAdq14::CCardSpdAdq14 with index " +
      boost::lexical_cast<std::string>(cardIndex_);

  try
  {
    if (!ready_)
    {
      throw CCardEx("SPD driver not ready");
    }

    // create channel collection for the card
    channel_.reserve(spdCards_[spdIndex_]->GetNbChannels());
    for (uint i = 0; i < spdCards_[spdIndex_]->GetNbChannels(); ++i)
    {
      std::ostringstream oss;
      oss << hostName_ << "/CARD" << cardIndex_ << "/CHANNEL" << i;
      LOG_TRACE << "creating channel: " + oss.str();
      channel_.push_back(std::make_shared<CChannelSpdAdq14>(
        ChannelConfig::makeLocation(1, crateIndex, cardIndex, i), oss.str(),
        state_));
    }
  }
  catch (const ntof::ZeroSuppression::CZeroSuppressionEx &ex)
  {
    throw CCardEx(ex.what());
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardSpdAdq14::CCardSpdAdq14 with index " +
      boost::lexical_cast<std::string>(cardIndex_);
}

CCardSpdAdq14::~CCardSpdAdq14()
{
  LOG_TRACE << "Entering CCardSpdAdq14::~CCardSpdAdq14 with index " +
      boost::lexical_cast<std::string>(cardIndex_);

  LOG_TRACE << "Exiting CCardSpdAdq14::~CCardSpdAdq14 with index " +
      boost::lexical_cast<std::string>(cardIndex_);
}

// Configure SP Devices driver
void CCardSpdAdq14::configure()
{
  LOG_TRACE << "Entering CCardSpdAdq14::configure for index " +
      boost::lexical_cast<std::string>(cardIndex_);

  if (ready_)
  {
    try
    {
#ifdef CHARACTERIZATION
      spdCards_[spdIndex_]->SetClockSource(CLOCK_SOURCE::INT_10M);
#else
      spdCards_[spdIndex_]->SetClockSource(CLOCK_SOURCE::EXT_10M);
#endif
      spdCards_[spdIndex_]->SetTriggerMode(TRIGGER_MODE::EXTERNAL1);

      ConfigureChannels();
    }
    catch (const ntof::CChannelEx &ex)
    {
      throw CCardEx(ex.what());
    }
    catch (const CCardEx &ex)
    {
      throw CCardEx(ex.what());
    }
  }

  LOG_TRACE << "Exiting CCardSpdAdq14::configure";
}

// Try to clean-up everything ...
void CCardSpdAdq14::Delete()
{
  LOG_TRACE << "Entering CCardSpdAdq14::Delete with index " +
      boost::lexical_cast<std::string>(cardIndex_);

  if (ready_ == false)
  {
    LOG_TRACE << "driver instance is already NULL, exiting ...";
    return;
  }

  ready_ = false;

  LOG_TRACE << "Exiting CCardSpdAdq14::Delete with index " +
      boost::lexical_cast<std::string>(cardIndex_);
}

void CCardSpdAdq14::LoadDefaultConfiguration()
{
  LOG_TRACE << "Entering CCardSpdAdq14::LoadDefaultConfiguration for index " +
      boost::lexical_cast<std::string>(cardIndex_);

  try
  {
    spdCards_[spdIndex_]->SetTriggerMode(TRIGGER_MODE::EXTERNAL1);
    spdCards_[spdIndex_]->SetClockSource(CLOCK_SOURCE::INT_10M);
    spdCards_[spdIndex_]->SetInputRange(5000);
    spdCards_[spdIndex_]->SetAcquisitionParameters(1000, 256 * 1000, 0.0);
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardSpdAdq14::LoadDefaultConfiguration for index " +
      boost::lexical_cast<std::string>(cardIndex_);
}

bool CCardSpdAdq14::HasOfflineParsing()
{
  return false;
}
} // namespace spdevices
