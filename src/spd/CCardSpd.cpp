#include "spd/CCardSpd.h"

#include <cstdint>
#include <sstream>
#include <string>
#include <vector>

#include <boost/lexical_cast.hpp>

#include <unistd.h>

#include "DIMException.h"
#include "logger.h"

#include <dis.hxx>

namespace spdevices {

void *CCardSpd::driverInstance_ = nullptr;
bool CCardSpd::ready_ = false;
std::vector<std::shared_ptr<CSpd>> CCardSpd::spdCards_;
bool CCardSpd::enableTrace_ = false;

CCardSpd::CCardSpd(std::string type,
                   std::string serialNumber,
                   int cpuCore,
                   uint32_t cardIndex,
                   uint32_t crateIndex,
                   ntof::dim::DIMState *state,
                   bool enableTrace) :
  CCard(type, serialNumber, cpuCore, cardIndex, crateIndex, state, enableTrace)
{
  adqNumber_ = 0;
  enableTrace_ = enableTrace;

  try
  {
    FindDevices();

    LOG_TRACE << "checking serial number ...";
    spdIndex_ = 0;
    bool match = false;

    for (auto &card : spdCards_)
    {
      if (serialNumber_ == card->GetSerialNumber())
      {
        match = true;
        break;
      }
      spdIndex_++;
    }

    if (!match)
      throw CCardEx("serial number given in configuration file does not match "
                    "any installed card");
    else
      LOG_TRACE << "serial number " + serialNumber_ +
          " matches with spd card index: " +
          boost::lexical_cast<std::string>(spdIndex_);
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }
}

CCardSpd::~CCardSpd()
{
  if (false == ready_)
  {
    return;
  }

  spdCards_.clear();
  DeleteADQControlUnit(driverInstance_);
  driverInstance_ = nullptr;
  ready_ = false;
}

void CCardSpd::FindDevices()
{
  if (ready_)
  {
    LOG_TRACE << "Exiting CCardSpd::FindDevices - cards already probed";
    return;
  }

  nbDevices_ = 0;
  LOG_TRACE << "Entering CCardSpd::findDevices for index " +
      boost::lexical_cast<std::string>(cardIndex_);

  if (ready_)
  {
    LOG_TRACE << "Exiting CCardSpd::findDevices - cards already probed" +
        boost::lexical_cast<std::string>(cardIndex_);
    return;
  }

  driverInstance_ = CreateADQControlUnit();
  if (enableTrace_)
  {
    std::string logDirectory = "/DAQ/log/spd/";

    int result = mkdir(logDirectory.c_str(),
                       S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    if (result == -1 && errno != EEXIST)
    {
      std::string err = "could not create " + logDirectory + " : " +
        std::string(strerror(errno));
      LOG_ERROR << err;
      throw CCardEx(err);
    }

    ADQControlUnit_EnableErrorTraceAppend(driverInstance_, 3,
                                          logDirectory.c_str());
  }

  if (driverInstance_ == NULL)
  {
    throw CCardEx("unable to open API");
  }

  LOG_INFO << "ADQ API Revision: " +
      boost::lexical_cast<std::string>(ADQAPI_GetRevision());

  nbDevices_ = ADQControlUnit_FindDevices(driverInstance_);

  if (ADQControlUnit_GetFailedDeviceCount(driverInstance_))
  {
    throw CCardEx("Device found, but failed to be initialized");
  }

  if (!nbDevices_)
  {
    throw CCardEx("no SPD ADQ devices found");
  }

  ADQInfoListEntry *ADQlist;
  if (!ADQControlUnit_ListDevices(driverInstance_, &ADQlist, &nbDevices_))
  {
    throw CCardEx("ListDevices failed!\n");
  }

  spdCards_.reserve(nbDevices_);

  // Creating collection of cards
  for (uint i = 0; i < nbDevices_; ++i)
  {
    switch (ADQlist[i].ProductID)
    {
    case PID_ADQ412:
    {
      LOG_INFO << "ADQ412 found";
      spdCards_.push_back(std::make_shared<Adq412>(driverInstance_, i + 1));
      break;
    }

    case PID_ADQ14:
    {
      LOG_INFO << "ADQ14 found";
      spdCards_.push_back(std::make_shared<Adq14>(driverInstance_, i + 1));
      break;
    }

    default:
      LOG_INFO << "unknown card type found by driver: " +
          boost::lexical_cast<std::string>(ADQlist[i].ProductID);
      continue;
    }

    adqNumber_ = i + 1;
    LOG_INFO << "serial number: " +
        std::string(ADQ_GetBoardSerialNumber(driverInstance_, adqNumber_));
    uint32_t *fw_rev = ADQ_GetRevision(driverInstance_, adqNumber_);
    LOG_INFO
      << "Firmware revision: " + boost::lexical_cast<std::string>(fw_rev[0]);
    LOG_INFO << "link rate: " << GetPCIeLinkRate()
             << ", link width: " << GetPCIeLinkWidth();
  }

  ready_ = true;

  LOG_TRACE
    << boost::lexical_cast<std::string>(nbDevices_) + " SPD instruments found";
  LOG_TRACE << "Exiting CCardSpd::findDevices";
}

uint32_t CCardSpd::GetNbChannels()
{
  return spdCards_[spdIndex_]->GetNbChannels();
}

uint32_t CCardSpd::GetNbBytesDumped()
{
  return spdCards_[spdIndex_]->GetNbBytesDumped();
}

int CCardSpd::GetResolution()
{
  return spdCards_[spdIndex_]->GetResolutionBits();
}

void CCardSpd::ConfigureChannels()
{
  bool firstCheck = true;
  uint chRef = 0;

  for (auto &ch : channel_)
  {
    if (ch->getConfiguration()->enabled == false)
    {
      spdCards_[spdIndex_]->ConfigureChannel(ch->GetIndex() + 1, 5000, 0, 50,
                                             false);

      ch->SetCalibratedFullScale(5000);
      ch->SetCalibratedOffset(0);
      LOG_INFO << spdCards_[spdIndex_]->GetSerialNumber() << ", channel"
               << ch->GetIndex() << " is disabled";
      continue;
    }

    LOG_INFO << "-------" << spdCards_[spdIndex_]->GetSerialNumber()
             << ", channel" << ch->GetIndex() << "-------";
    LOG_INFO << "fullscale: " << ch->getConfiguration()->header.fullScale;
    LOG_INFO << "offset: " << ch->getConfiguration()->header.offset;
    LOG_INFO << "---------------------------------------------";

    spdCards_[spdIndex_]->ConfigureChannel(
      ch->GetIndex() + 1, ch->getConfiguration()->header.fullScale,
      ch->getConfiguration()->header.offset,
      ch->getConfiguration()->inputImpedance, true);

    ch->SetCalibratedFullScale(
      spdCards_[spdIndex_]->GetCalibratedFullScale(ch->GetIndex()));
    ch->SetCalibratedOffset(
      spdCards_[spdIndex_]->GetCalibratedOffset(ch->GetIndex()));

    ch->GetZeroSuppression().SetGainOffset(
      static_cast<float>(ch->getConfiguration()->header.fullScale) /
        (1 << spdCards_[spdIndex_]->GetResolutionBits()),
      static_cast<float>(ch->GetCalibratedFullScale() /
                         (1 << spdCards_[spdIndex_]->GetResolutionBits())),
      ch->getConfiguration()->header.offset);

    // time window is in ns, convert it to us (Fs is in MS/s) and get equivalent
    // number of samples
    uint32_t tw = static_cast<uint32_t>(
      ch->getConfiguration()->header.zeroSuppressionStart * 1E-3 *
      ch->getConfiguration()->header.sampleRate);
    if (tw > (ch->getConfiguration()->header.sampleSize * 1000))
    {
      throw CCardEx(
        "zero suppression window cannot be higher than total sample size");
    }
    ch->GetZeroSuppression().SetTimeWindow(
      tw, ch->getConfiguration()->header.sampleSize * 1000);

    if (firstCheck == true)
    {
      try
      {
        spdCards_[spdIndex_]->SetAcquisitionParameters(
          ch->getConfiguration()->header.sampleRate,
          ch->getConfiguration()->header.sampleSize,
          ch->getConfiguration()->header.delayTime);
      }
      catch (const CCardEx &ex)
      {
        throw CCardEx(ex.what());
      }

      firstCheck = false;
      chRef = ch->GetIndex();
      continue;
    }

    if (ch->getConfiguration()->header.sampleRate !=
        channel_[chRef]->getConfiguration()->header.sampleRate)
      throw CCardEx("sampling rate is not consistent for all channels");

    if (ch->getConfiguration()->header.sampleSize !=
        channel_[chRef]->getConfiguration()->header.sampleSize)
      throw CCardEx("sampling size is not consistent for all channels");

    if (ch->getConfiguration()->header.delayTime !=
        channel_[chRef]->getConfiguration()->header.delayTime)
      throw CCardEx("delay time is not consistent for all channels");
  }

  // in case where all channels are disabled, we set a default configuration
  if (firstCheck == true)
  {
    try
    {
      spdCards_[spdIndex_]->SetAcquisitionParameters(1000, 1000, 0);
    }
    catch (const CCardEx &ex)
    {
      throw CCardEx(ex.what());
    }
  }
}

void CCardSpd::SetDumpMemoryBuffer(int8_t *buffer)
{
  spdCards_[spdIndex_]->SetDumpMemoryBuffer(buffer);
}

int8_t *CCardSpd::GetMemoryDumpBuffer()
{
  return (int8_t *) (spdCards_[spdIndex_]->GetMemoryDumpBuffer());
}

int16_t **CCardSpd::GetAcquisitionBuffer()
{
  return spdCards_[spdIndex_]->GetAcquisitionBuffer();
}

void CCardSpd::SetChannelBufferFree()
{
  spdCards_[spdIndex_]->FreeMemory();
}

void CCardSpd::ArmTrigger()
{
  readOutDone_ = false;
  dumpDone_ = false;

  try
  {
    spdCards_[spdIndex_]->ArmTrigger();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }
}

bool CCardSpd::IsTriggered()
{
  return spdCards_[spdIndex_]->IsTriggered();
}

uint32_t CCardSpd::GetDumpBufferSize()
{
  return spdCards_[spdIndex_]->GetDumpBufferSize();
}

uint32_t CCardSpd::GetChannelBufferSize()
{
  return spdCards_[spdIndex_]->GetChannelBufferSize();
}

uint32_t CCardSpd::GetCardIndex()
{
  return cardIndex_;
}

void CCardSpd::DumpCardMemory()
{
  if (dumpDone_)
    return;

  LOG_TRACE << "Entering CCardSpd::DumpCardMemory for card " +
      boost::lexical_cast<std::string>(cardIndex_);

  try
  {
    spdCards_[spdIndex_]->SetAcquisitionBuffer(
      bufferPool_[bufferPoolIndex_]->buffer_.data());
    spdCards_[spdIndex_]->SetDumpMemoryBuffer(
      bufferPool_[bufferPoolIndex_]->dumpBuffer_);
    spdCards_[spdIndex_]->MemoryDump();
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx("could not dump memory: " + std::string(ex.what()));
  }

  dumpDone_ = true;

  LOG_TRACE << "Exiting CCardSpd::DumpCardMemory for card " +
      boost::lexical_cast<std::string>(cardIndex_);
}

void CCardSpd::OfflineParsing(int8_t *dumpBuffer)
{
  LOG_TRACE << "Entering CCardSpd::OfflineParsing for card " +
      boost::lexical_cast<std::string>(cardIndex_);

  if (dumpBuffer != nullptr)
  {
    try
    {
      spdCards_[spdIndex_]->GetAcquisition(dumpBuffer);
    }
    catch (const CCardEx &ex)
    {
      throw CCardEx(ex.what());
    }
  }
  else
  {
    throw CCardEx("could not perform offline parsing, buffer is NULL");
  }

  LOG_TRACE << "Exiting CCardSpd::OfflineParsing for card " +
      boost::lexical_cast<std::string>(cardIndex_);
}

void CCardSpd::Calibrate()
{
  LOG_TRACE << "Entering CCardSpd::Calibrate for card " +
      boost::lexical_cast<std::string>(cardIndex_);

  spdCards_[spdIndex_]->Calibrate();

  for (auto &ch : channel_)
  {
    if (ch->getConfiguration()->enabled == false)
    {
      ch->SetCalibratedFullScale(5000);
      ch->SetCalibratedOffset(0);
      ch->SetCalibratedThreshold(0);
      continue;
    }

    ch->SetCalibratedFullScale(
      spdCards_[spdIndex_]->GetCalibratedFullScale(ch->GetIndex()));
    ch->SetCalibratedOffset(
      spdCards_[spdIndex_]->GetCalibratedOffset(ch->GetIndex()));

    // update the threshold for the zero suppression
    int16_t th = (ch->getConfiguration()->header.threshold +
                  ch->getConfiguration()->header.offset +
                  ch->GetCalibratedOffset()) /
      (ch->GetCalibratedFullScale() /
       static_cast<float>(1 << spdCards_[spdIndex_]->GetResolutionBits()));

    ch->GetZeroSuppression().SetThreshold(th);
    ch->SetCalibratedThreshold(th);
    ch->GetZeroSuppression().SetGainOffset(
      static_cast<float>(ch->getConfiguration()->header.fullScale) /
        (1 << spdCards_[spdIndex_]->GetResolutionBits()),
      spdCards_[spdIndex_]->GetCalibratedFullScale(ch->GetIndex()) /
        (1 << spdCards_[spdIndex_]->GetResolutionBits()),
      ch->getConfiguration()->header.offset + ch->GetCalibratedOffset());
  }

  LOG_TRACE << "Exiting CCardSpd::Calibrate for card " +
      boost::lexical_cast<std::string>(cardIndex_);
}

uint32_t CCardSpd::GetPCIeLinkRate()
{
  return ADQ_GetPCIeLinkRate(CCardSpd::driverInstance_, adqNumber_);
}

uint32_t CCardSpd::GetPCIeLinkWidth()
{
  return ADQ_GetPCIeLinkWidth(CCardSpd::driverInstance_, adqNumber_);
}

} // namespace spdevices
