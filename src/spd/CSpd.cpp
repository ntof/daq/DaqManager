
#include "spd/CSpd.h"

#include <cstdint>
#include <ctime>
#include <fstream>
#include <iostream>
#include <vector>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <ADQAPI.h>
#include <fcntl.h>
#include <numa.h>
#include <sys/time.h>
#include <unistd.h>

#include "defines.h"
#include "logger.h"

namespace spdevices {

CSpd::CSpd(std::string cardModelName,
           void *driverInstance_,
           int adqNumber,
           uint nbChannels,
           uint resolution,
           uint realResolution) :
  driverInstance_(driverInstance_),
  adqNumber_(adqNumber),
  nbChannels_(nbChannels),
  resolution_(resolution),
  realResolution_(realResolution),
  cardModelName_(cardModelName)
{
  std::ostringstream oss;

  oss << "Entering CSpd::CSpd with index " << adqNumber_;
  oss << ", model: " << cardModelName_;

  LOG_TRACE << oss.str();

  // ADQ_SetDataFormat(driverInstance_, adqNumber_, 1);

  mrInfo_ = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  memoryDumpByteRead_ = 0;
  nbValidSamples_ = 0;
  maxSamples_ = 0;
  nbChannelEnabled_ = 0;
  cardIndex_ = 0;
  interleavingMode_ = INTERLEAVING_MODE::FOUR_CHANNEL;
  slotNumber_ = 0;
  nbSamples_ = 0;
  nbBytesDumped_ = 0;
  offlineParsingEnabled_ = true;
  channelMask_ = 0;
  triggerMode_ = TRIGGER_MODE::EXTERNAL1;

  targetBuffer_ = nullptr;
  dramShadow_ = nullptr;

  channelEnabled_.reserve(nbChannels_);
  calibratedFullScale_.reserve(nbChannels_);
  calibratedOffset_.reserve(nbChannels_);
  fullScale_.reserve(nbChannels_);

  for (uint i = 0; i < nbChannels_; i++)
  {
    channelEnabled_.push_back(false);
    calibratedFullScale_.push_back(0.0);
    calibratedOffset_.push_back(0.0);
    fullScale_.push_back(0.0);
  }
}

CSpd::~CSpd()
{
  channelEnabled_.clear();
}

uint CSpd::GetNbChannels()
{
  return nbChannels_;
}

uint CSpd::GetResolutionBits()
{
  return resolution_;
}

CARD_TEMPERATURE_t CSpd::GetTemperature()
{
  for (uint i = 0; i < temperature_.name.size(); ++i)
  {
    ADQ_GetTemperatureFloat(driverInstance_, adqNumber_, i,
                            &(temperature_.value[i]));
  }

  return temperature_;
}

int32_t CSpd::GetBufferOverflow()
{
  return ADQ_GetStreamOverflow(driverInstance_, adqNumber_);
}

uint32_t CSpd::GetDumpBufferSize()
{
  return mrInfo_.shadowSize;
}

int16_t **CSpd::GetAcquisitionBuffer()
{
  return targetBuffer_;
}

void CSpd::SetDumpMemoryBuffer(int8_t *buffer)
{
  dramShadow_ = buffer;
}

int8_t *CSpd::GetMemoryDumpBuffer()
{
  return dramShadow_;
}

void CSpd::FreeMemory()
{
  for (uint i = 0; i < nbChannels_; ++i)
    delete[] targetBuffer_[i], targetBuffer_[i] = nullptr;

  delete[] targetBuffer_, targetBuffer_ = nullptr;

  delete[] dramShadow_, dramShadow_ = nullptr;
}

void CSpd::ArmTrigger()
{
  if (ADQ_DisarmTrigger(driverInstance_, adqNumber_) != 1)
  {
    throw CCardEx("API DisarmTrigger: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  if (ADQ_ArmTrigger(driverInstance_, adqNumber_) != 1)
  {
    throw CCardEx("API ArmTrigger: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }
#ifdef CHARACTERIZATION
  ADQ_SWTrig(driverInstance_, adqNumber_);
#endif
}

bool CSpd::IsTriggered()
{
  return ADQ_GetAcquiredAll(driverInstance_, adqNumber_) ? true : false;
}

void CSpd::SetAcquisitionBuffer(int16_t **buffer)
{
  targetBuffer_ = buffer;
}

void CSpd::GetAcquisition(int8_t *dumpBuffer)
{
  UNUSED(dumpBuffer);

  throw CCardEx("should not be called from base class");
}

uint32_t CSpd::GetChannelBufferSize()
{
  return static_cast<uint32_t>(sizeof(int16_t) * nbSamples_);
}

uint32_t CSpd::GetNbBytesDumped()
{
  return nbBytesDumped_;
}

void CSpd::SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER divider)
{
  if (ADQ_SetSampleSkip(driverInstance_, adqNumber_, divider) != 1)
  {
    throw CCardEx("API SetSampleSkip: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }
}

void CSpd::SetTriggerMode(TRIGGER_MODE triggerMode)
{
  int32_t rtn;

  triggerMode_ = triggerMode;

#ifdef CHARACTERIZATION
  rtn = ADQ_SetTriggerMode(driverInstance_, adqNumber_, ADQ_SW_TRIGGER_MODE);
#else
  rtn = ADQ_SetTriggerMode(driverInstance_, adqNumber_, (int32_t) triggerMode);
  rtn &= ADQ_SetExternTrigEdge(driverInstance_, adqNumber_, 1);
#endif

  if (rtn != 1)
  {
    throw CCardEx("API SetTriggerMode: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }
}

void CSpd::SetClockSource(CLOCK_SOURCE clkSource)
{
  int32_t rtn;
  rtn = ADQ_SetClockSource(driverInstance_, adqNumber_, (int32_t) clkSource);

  if (rtn != 1)
  {
    throw CCardEx("API SetClockSource: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }
}

void CSpd::SetTriggerDelay(int32_t delay, float samplingFreq, uint rounding)
{
  uint32_t preTrigSamples;
  // delay is in ns, sampling freq is in MS/s
  if (delay < 0)
  {
    preTrigSamples = static_cast<uint>(abs(delay) * 1E-9 * samplingFreq * 1E6);
    preTrigSamples = (preTrigSamples + rounding - 1) & ~(rounding - 1);

    if (ADQ_SetPreTrigSamples(driverInstance_, adqNumber_, preTrigSamples) != 1)
    {
      throw CCardEx("API SetPreTrigSamples: " +
                    boost::lexical_cast<std::string>(
                      ADQ_GetLastError(driverInstance_, adqNumber_)));
    }
  }
  else
  {
    preTrigSamples = static_cast<uint>(delay * 1E-9 * samplingFreq * 1E6);
    preTrigSamples = (preTrigSamples + rounding - 1) & ~(rounding - 1);

    if (ADQ_SetTriggerHoldOffSamples(driverInstance_, adqNumber_,
                                     preTrigSamples) != 1)
    {
      throw CCardEx("API SetTriggerHoldOffSamples: " +
                    boost::lexical_cast<std::string>(
                      ADQ_GetLastError(driverInstance_, adqNumber_)));
    }
  }

  LOG_TRACE << "card" << adqNumber_ << ", delay: " << delay
            << ", in samples: " << preTrigSamples;
}

void CSpd::SetInterleavingMode(INTERLEAVING_MODE intMode)
{
  int32_t rtn;
  interleavingMode_ = intMode;
  rtn = ADQ_SetInterleavingMode(driverInstance_, adqNumber_,
                                static_cast<char>(intMode));

  if (rtn != 1)
  {
    throw CCardEx("API SetInterleavingMode: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }
}

void CSpd::ConfigureChannel(uint32_t ch,
                            double range,
                            double offset,
                            double impedance,
                            bool enabled)
{
  channelEnabled_[ch - 1] = enabled;

  if (!enabled)
  {
    channelMask_ &= static_cast<uint8_t>(~(1 << (ch - 1)));
    return;
  }

  channelMask_ |= static_cast<uint8_t>(1 << (ch - 1));

  if (impedance != 50.0)
  {
    throw CCardEx("channel impedance should be 50.0");
  }

  SetInputRange(ch, range, &calibratedFullScale_[ch - 1]);
  SetAdjustableBias(ch, offset);
}

std::vector<float> &CSpd::GetCalibratedFullScale()
{
  return calibratedFullScale_;
}

std::vector<float> &CSpd::GetCalibratedOffset()
{
  return calibratedOffset_;
}

void CSpd::SetCalibratedFullScale(int ch, float fs)
{
  calibratedFullScale_[ch] = fs;
}

float CSpd::GetCalibratedFullScale(int index)
{
  float actualInputRange;

  ADQ_GetInputRange(driverInstance_, adqNumber_, index + 1, &actualInputRange);

  calibratedFullScale_[index] = actualInputRange;

  return calibratedFullScale_[index];
}

float CSpd::GetCalibratedOffset(int index)
{
  return calibratedOffset_[index];
}

void CSpd::SetInputRange(int32_t ch,
                         double inputRange,
                         float * /*actualInputRange*/)
{
  float d;

  if (ch < 1 || ch > 4)
  {
    throw CCardEx("channel number should be < 4 and > 1");
  }

  fullScale_[ch - 1] = static_cast<float>(inputRange);

  if (ADQ_SetInputRange(driverInstance_, adqNumber_, ch, (float) inputRange,
                        &d) != 1)
  {
    throw CCardEx("API SetInputRange: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }
}

void CSpd::SetInputRange(int32_t ch, double inputRange)
{
  SetInputRange(ch, inputRange, nullptr);
}

void CSpd::SetInputRange(double inputRange)
{
  try
  {
    if (cardModelName_ == "ADQ412" || cardModelName_ == "ADQ208")
    {
      switch (interleavingMode_)
      {
      case FOUR_CHANNEL:
        for (uint i = 0; i < nbChannels_; i++)
        {
          SetInputRange(i + 1, inputRange, nullptr);
        }
        break;

      case TWO_CHANNEL_AC:
        SetInputRange(1, inputRange, nullptr);
        SetInputRange(3, inputRange, nullptr);
        break;

      case TWO_CHANNEL_BD:
        SetInputRange(2, inputRange, nullptr);
        SetInputRange(4, inputRange, nullptr);
        break;

      default: break;
      }
    }
    else
    {
      for (uint i = 0; i < nbChannels_; i++)
      {
        SetInputRange(i + 1, inputRange, nullptr);
      }
    }
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }
}

void CSpd::SetAdjustableBias(int32_t ch, double bias)
{
  int32_t b;
  double lsb;
  float inputRange;

  if (ch < 1 || ch > 4)
  {
    throw CCardEx("channel number should be < 4 and > 1");
  }

  if (cardModelName_ == "ADQ412")
  {
    lsb = (static_cast<double>(fullScale_[ch - 1]) / (1 << resolution_));
  }
  else
  {
    if (ADQ_GetInputRange(driverInstance_, adqNumber_, ch, &inputRange) != 1)
    {
      throw CCardEx("API GetInputRange: " +
                    boost::lexical_cast<std::string>(
                      ADQ_GetLastError(driverInstance_, adqNumber_)));
    }
    lsb = (static_cast<double>(inputRange) / (1 << resolution_));
  }

  b = (int32_t)(bias / lsb);
  if (ADQ_SetAdjustableBias(driverInstance_, adqNumber_, ch, b) != 1)
  {
    throw CCardEx("API SetAdjustableBias: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }
}

void CSpd::SetAdjustableBias(int32_t ch, int bias)
{
  if (ch < 1 || ch > 4)
  {
    throw CCardEx("channel number should be < 4 and > 1");
  }

  if (ADQ_SetAdjustableBias(driverInstance_, adqNumber_, ch, bias) != 1)
  {
    throw CCardEx("API SetAdjustableBias: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }
}

void CSpd::SetAdjustableBias(double bias)
{
  try
  {
    for (uint i = 0; i < nbChannels_; i++)
    {
      SetAdjustableBias(i + 1, bias);
    }
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }
}

void CSpd::Calibrate()
{
  LOG_TRACE << "Entering CSpd::Calibrate for ADQ # " << adqNumber_;

  for (uint i = 0; i < nbChannels_; ++i)
  {
    int gain, offset;
    float range;
    ADQ_GetGainAndOffset(driverInstance_, adqNumber_, (i + 1) | 0x80, &gain,
                         &offset);
    ADQ_GetInputRange(driverInstance_, adqNumber_, i + 1, &range);
    //(int16_t)offset * range / (1 << resolution_);
    LOG_DEBUG << "channel" << i << ", offset = " << (int16_t) offset << ", "
              << (int16_t) offset * range / (1 << resolution_);
    calibratedOffset_[i] = ((int16_t) offset * range / (1 << resolution_));
  }

  LOG_TRACE << "Exiting CSpd::Calibrate for ADQ # " << adqNumber_;
}

void CSpd::CalibrationUpdate(bool enable)
{
  UNUSED(enable);
  LOG_TRACE << "Entering/Exiting CSpd::Calibrate for ADQ # " << adqNumber_;
}

int32_t CSpd::GetCardIndex()
{
  return adqNumber_;
}

std::string CSpd::GetCardModelName()
{
  return cardModelName_;
}

std::string CSpd::GetSerialNumber()
{
  char *str = ADQ_GetBoardSerialNumber(driverInstance_, adqNumber_);

  if (str != nullptr)
    return std::string(str);
  else
    throw CCardEx("unknown, nullptr pointer returned");
}

std::string CSpd::GetMultiRecordInfo()
{
  std::string id = "[board " + boost::lexical_cast<std::string>(adqNumber_) +
    "] ";
  std::string s;

  s = id + "--------------- multi record info --------------\n";
  s += id + "dramStartAddress: 0x " +
    boost::lexical_cast<std::string>(mrInfo_.dramStartAddress) + '\n';
  s += id + "dramEndAddress: 0x " +
    boost::lexical_cast<std::string>(mrInfo_.dramEndAddress) + '\n';
  s += id + "dramAddressPerRecord: 0x " +
    boost::lexical_cast<std::string>(mrInfo_.dramAddressPerRecord) + '\n';
  s += id + "dramBytePerAddress: 0x " +
    boost::lexical_cast<std::string>(mrInfo_.dramBytePerAddress) + '\n';
  s += id +
    "setupRecords: " + boost::lexical_cast<std::string>(mrInfo_.setupRecords) +
    '\n';
  s += id +
    "setupSamples: " + boost::lexical_cast<std::string>(mrInfo_.setupSamples) +
    '\n';
  s += id + "setupPaddedSamplesSize: 0x " +
    boost::lexical_cast<std::string>(mrInfo_.setupPaddedSamplesSize) + '\n';
  s += id + "maxNumberRecords: 0x " +
    boost::lexical_cast<std::string>(mrInfo_.maxNumberRecords) + '\n';
  s += id +
    "shadow size: " + boost::lexical_cast<std::string>(mrInfo_.shadowSize) +
    '\n';
  s += id + "------------------------------------------------\n";

  return s;
}

void CSpd::LogAcquisitionParams(double samplingFreq,
                                double baseFreq,
                                double divider)
{
  uint sampleSkip = ADQ_GetSampleSkip(driverInstance_, adqNumber_);
  sampleSkip = sampleSkip == 0 ? 1 : sampleSkip;

  LOG_TRACE << "-----------------------------------------------------";
  LOG_TRACE << "card SPD #" + boost::lexical_cast<std::string>(adqNumber_) +
      " configuration parameters:";
  LOG_TRACE
    << "-- max nb of samples: " + boost::lexical_cast<std::string>(maxSamples_);
  LOG_TRACE
    << "-- samples skipped: " + boost::lexical_cast<std::string>(sampleSkip);
  LOG_TRACE << "-- sampling frequency: " +
      std::string(boost::str(boost::format("%.2f") %
                             (baseFreq / static_cast<double>(sampleSkip)))) +
      "MHz";
  LOG_TRACE << "-- nb samples: " + boost::lexical_cast<std::string>(nbSamples_);
  LOG_TRACE << "-- desired sampling frequency: " +
      boost::lexical_cast<std::string>(samplingFreq);
  LOG_TRACE
    << "-- frequency divider: " + boost::lexical_cast<std::string>(divider);
  LOG_TRACE << "-- sample skip: " +
      boost::lexical_cast<std::string>(
                 ADQ_GetSampleSkip(driverInstance_, adqNumber_));
  LOG_TRACE << "-----------------------------------------------------";
}

} // namespace spdevices
  // namespace CSpd
