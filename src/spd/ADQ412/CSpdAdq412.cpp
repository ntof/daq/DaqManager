#include "spd/ADQ412/CSpdAdq412.h"

#include <cstdint>
#include <ctime>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <fcntl.h>
#include <numa.h>
#include <sys/time.h>
#include <unistd.h>

#include "ADQAPI.h"
#include "CCard.h"
#include "logger.h"
#include "spd/spd_typedef.h"

#define LOW_LEVEL_FILE_WRITING

namespace spdevices {

Adq412::Adq412(void *driverInstance_, uint adqNumber) :
  CSpd("ADQ412", driverInstance_, adqNumber, 4, 12, 12)
{
  performanceCounterEnabled = false;
  triggerTime = 0.;
  transferDataRate = 0.;
  parsingDataRate = 0.;
  hddDataRate = 0.;
  hddTransferSize = 0.;
  hddTransferTime = 0.;

  temperature_.name.push_back("Local");
  temperature_.name.push_back("ADC0");
  temperature_.name.push_back("ADC1");
  temperature_.name.push_back("FPGA");
  temperature_.name.push_back("PCB");

  temperature_.value.reserve(temperature_.name.size());
  for (uint i = 0; i < temperature_.name.size(); ++i)
    temperature_.value.push_back(0.0);
}

Adq412::~Adq412() {}

double Adq412::GetBaseSamplingFreq()
{
  return 1800.0;
}

void Adq412::Calibrate()
{
  LOG_TRACE << "Entering Adq412::Calibrate for ADQ # " +
      boost::lexical_cast<std::string>(adqNumber_);

  if (ADQ_RunCalibrationADQ412DC(driverInstance_, adqNumber_, 0) != 1)
  {
    throw CCardEx("API RunCalibrationADQ412DC: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  sleep(4);

  if (ADQ_RunCalibrationADQ412DC(driverInstance_, adqNumber_, 1) != 1)
  {
    throw CCardEx("API RunCalibrationADQ412DC: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  sleep(4);

  for (uint i = 0; i < nbChannels_; ++i)
  {
    calibratedOffset_[i] = 0.0;
  }

  LOG_TRACE << "Exiting Adq412::Calibrate for ADQ # " +
      boost::lexical_cast<std::string>(adqNumber_);
}

void Adq412::CalibrationUpdate(bool enable)
{
  LOG_TRACE << "Entering Adq412::CalibrationUpdate for ADQ # " +
      boost::lexical_cast<std::string>(adqNumber_);

  unsigned int calmode = enable ? 1 : 0;
  if (ADQ_SetCalibrationModeADQ412DC(driverInstance_, adqNumber_, calmode) != 1)
  {
    throw CCardEx("API SetCalibrationModeADQ412DC: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  LOG_TRACE << "Exiting Adq412::CalibrationUpdate for ADQ # " +
      boost::lexical_cast<std::string>(adqNumber_);
}

void Adq412::SetAcquisitionParameters(float samplingFreq,
                                      int32_t nbSamples,
                                      int32_t delay)
{
#ifdef CHARACTERIZATION
  nbSamples_ = nbSamples == 0 ? 4096 : nbSamples * 1000;
#else
  nbSamples_ = static_cast<uint32_t>(nbSamples * 1000);
#endif

  nbSamples_ = (nbSamples_ + DIRECTIO_BLOCK_SIZE - 1) &
    ~(DIRECTIO_BLOCK_SIZE - 1);

  double divider = 1800.0 / samplingFreq;
  if (divider >= 1.0 && divider < 2.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::NONE);
  else if (divider >= 2.0 && divider < 3.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_2);
  else if (divider >= 3.0 && divider < 5.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_4);
  else if (divider >= 5.0 && divider < 9.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_8);
  else if (divider >= 9.0 && divider < 17.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_16);
  else if (divider >= 17.0 && divider < 33.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_32);
  else if (divider >= 33.0 && divider < 65.0)
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_64);
  else
    SetSamplingFrequencyDivider(SAMPLING_FREQ_DIVIDER::DIVIDER_128);

  SetTriggerDelay(
    delay,
    static_cast<float>(1800.0 / ADQ_GetSampleSkip(driverInstance_, adqNumber_)),
    8);

  // The linux driver requires a minimum of two buffers to work properly
  if (ADQ_SetTransferBuffers(driverInstance_, adqNumber_, 4,
                             131072 * GetNbChannels()) != 1)
  {
    throw CCardEx("API SetTransferBuffers" +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  if (ADQ_MultiRecordSetupGP(driverInstance_, adqNumber_, 1, nbSamples_,
                             (uint32_t *) &mrInfo_) != 1)
  {
    throw CCardEx("API MultiRecordSetup: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  if (ADQ_GetMaxNofSamplesFromNofRecords(driverInstance_, adqNumber_, 1,
                                         &maxSamples_) != 1)
  {
    throw CCardEx("API GetMaxNofSamplesFromNofRecords: " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  LOG_TRACE
    << "max nb of samples: " + boost::lexical_cast<std::string>(maxSamples_);

  if (maxSamples_ < nbSamples_)
  {
    throw CCardEx("too many samples required");
  }

  LogAcquisitionParams(samplingFreq, GetBaseSamplingFreq(), divider);

  if (ADQ_MemoryShadow(driverInstance_, adqNumber_, (void *) 1,
                       GetDumpBufferSize()) != 1)
  {
    throw CCardEx("API MemoryShadow: error code = " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }
}

void Adq412::GetAcquisition(int8_t *dumpBuffer)
{
  nbBytesDumped_ = 0;

  LOG_TRACE << "memory shadow size: " +
      boost::lexical_cast<std::string>(GetDumpBufferSize());
  if (ADQ_MemoryShadow(driverInstance_, adqNumber_, dumpBuffer,
                       GetDumpBufferSize()) != 1)
  {
    throw CCardEx("API MemoryShadow: error code = " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

  if (targetBuffer_ == NULL)
  {
    throw CCardEx("invalid targetBuffer");
  }

  if (ADQ_GetData(driverInstance_, adqNumber_, (void **) targetBuffer_,
                  nbSamples_, sizeof(**targetBuffer_), 0, 1, 0xF, 0, nbSamples_,
                  ADQ_TRANSFER_MODE_NORMAL) != 1)
  {
    std::string err = "API GetData: error code = " +
      boost::lexical_cast<std::string>(
                        ADQ_GetLastError(driverInstance_, adqNumber_));

    if (ADQ_GetStreamOverflow(driverInstance_, adqNumber_) == 1)
      err += ", stream overflow: " +
        boost::lexical_cast<std::string>(
               ADQ_GetLastError(driverInstance_, adqNumber_));

    throw CCardEx(err);
  }

  for (uint i = 0; i < nbChannels_; i++)
    if (channelEnabled_[i])
      nbBytesDumped_ += static_cast<uint32_t>(nbSamples_ *
                                              sizeof(**targetBuffer_));
}

void Adq412::MemoryDump()
{
  if (dramShadow_ == nullptr)
  {
    throw CCardEx("shadow memory pointer is null");
  }

  if (ADQ_MemoryDumpRecords(driverInstance_, adqNumber_, 0, 1,
                            (uint8_t *) dramShadow_, &memoryDumpByteRead_,
                            0) != 1)
  {
    throw CCardEx("API MemoryDumpRecords: error code = " +
                  boost::lexical_cast<std::string>(
                    ADQ_GetLastError(driverInstance_, adqNumber_)));
  }

#ifdef CHARACTERIZATION
  for (uint i = 0; i < temperature_.value.size(); ++i)
  {
    ADQ_GetTemperatureFloat(driverInstance_, adqNumber_, i,
                            &temperature_.value[i]);
    LOG_INFO << temperature_.name[i] << ": " << temperature_.value[i];
  }
#endif

#if 0
	LOG_TRACE << "writing shadow file, byte read: " + boost::lexical_cast<std::string>(memoryDumpByteRead_));
	static int index = 0;
	int fd = open(("./shadow" + boost::lexical_cast<std::string>(index++)).c_str(), O_WRONLY | O_CREAT, 0777);
	if (fd == -1)
	{
		std::string err = "could not create file for shadow: " + std::string(strerror(errno));
		LOG_TRACE << err);
		return 0;
	}
	ssize_t s = write(fd, dramShadow_, memoryDumpByteRead_);
	if (s != memoryDumpByteRead_)
	{
		LOG_TRACE << "could not write shadow file, byte writen: " + boost::lexical_cast<std::string>(s);
	}
	fsync(fd);
	close(fd);
#endif
}
} // namespace spdevices
