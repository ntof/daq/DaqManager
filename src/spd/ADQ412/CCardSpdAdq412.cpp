#include "spd/ADQ412/CCardSpdAdq412.h"

#include <cstdint>
#include <sstream>
#include <string>
#include <vector>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <unistd.h>

#include "ADQAPI.h"
#include "CZeroSuppression.h"
#include "DIMException.h"
#include "logger.h"

#include "dis.hxx"

namespace spdevices {

CCardSpdAdq412::CCardSpdAdq412(std::string serialNumber,
                               int cpuCore,
                               uint32_t cardIndex,
                               uint32_t crateIndex,
                               ntof::dim::DIMState *state,
                               bool enableTrace) :
  CCardSpd("S412",
           serialNumber,
           cpuCore,
           cardIndex,
           crateIndex,
           state,
           enableTrace)
{
  LOG_TRACE << "Entering CCardSpdAdq412::CCardSpdAdq412 with index " +
      boost::lexical_cast<std::string>(cardIndex_);

  try
  {
    if (!ready_)
    {
      throw CCardEx("SPD driver not ready");
    }

    channel_.reserve(spdCards_[spdIndex_]->GetNbChannels());
    for (uint i = 0; i < spdCards_[spdIndex_]->GetNbChannels(); ++i)
    {
      std::ostringstream oss;
      oss << hostName_ << "/CARD" << cardIndex_ << "/CHANNEL" << i;
      channel_.push_back(std::make_shared<CChannelSpdAdq412>(
        ChannelConfig::makeLocation(1, crateIndex, cardIndex, i), oss.str(),
        CCard::state_));
    }
  }
  catch (const ntof::ZeroSuppression::CZeroSuppressionEx &ex)
  {
    throw CCardEx(ex.what());
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardSpdAdq412::CCardSpdAdq412 with index " +
      boost::lexical_cast<std::string>(cardIndex_);
}

CCardSpdAdq412::~CCardSpdAdq412()
{
  LOG_TRACE << "Entering CCardSpdAdq412::~CCardSpdAdq412 with index " +
      boost::lexical_cast<std::string>(cardIndex_);

  LOG_TRACE << "Exiting CCardSpdAdq412::~CCardSpdAdq412 with index " +
      boost::lexical_cast<std::string>(cardIndex_);
}

// Configure SP Devices driver
void CCardSpdAdq412::configure()
{
  LOG_TRACE << "Entering CCardSpdAdq412::configure for index " +
      boost::lexical_cast<std::string>(cardIndex_);

  if (ready_)
  {
    try
    {
      spdCards_[spdIndex_]->SetInterleavingMode(INTERLEAVING_MODE::FOUR_CHANNEL);
#ifdef CHARACTERIZATION
      spdCards_[spdIndex_]->SetClockSource(CLOCK_SOURCE::INT_10M);
#else
      spdCards_[spdIndex_]->SetClockSource(CLOCK_SOURCE::EXT_10M);
#endif
      spdCards_[spdIndex_]->SetTriggerMode(TRIGGER_MODE::EXTERNAL1);
      spdCards_[spdIndex_]->CalibrationUpdate(false);

      ConfigureChannels();
    }
    catch (const ntof::CChannelEx &ex)
    {
      throw CCardEx(ex.what());
    }
    catch (const CCardEx &ex)
    {
      throw CCardEx(ex.what());
    }
  }

  LOG_TRACE << "Exiting CCardSpdAdq412::configure";
}

void CCardSpdAdq412::Calibrate()
{
  LOG_TRACE << "Entering CCardSpdAdq412::Calibrate for card " +
      boost::lexical_cast<std::string>(cardIndex_);

  spdCards_[spdIndex_]->Calibrate();

  for (auto &ch : channel_)
  {
    if (ch->getConfiguration()->enabled == false)

    {
      ch->SetCalibratedFullScale(5000);
      ch->SetCalibratedOffset(0);
      ch->SetCalibratedThreshold(0);
      continue;
    }

    // Already corrected by the card
    ch->SetCalibratedFullScale(
      static_cast<float>(ch->getConfiguration()->header.fullScale));
    ch->SetCalibratedOffset(0.0);

    // LOG_TRACE << "<<<<<<<<< threshold: " <<
    // ch->getConfiguration()->header.threshold
    // << ", offset: " << ch->getConfiguration()->header.offset << ", calFS: "
    // << ch->GetCalibratedFullScale();

    // update the threshold for the zero suppression
    int16_t th = static_cast<int32_t>(
      (ch->getConfiguration()->header.threshold +
       ch->getConfiguration()->header.offset + ch->GetCalibratedOffset()) /
      (ch->GetCalibratedFullScale() /
       (1 << spdCards_[spdIndex_]->GetResolutionBits())));

    ch->GetZeroSuppression().SetThreshold(th);
    ch->SetCalibratedThreshold(th);
    ch->GetZeroSuppression().SetGainOffset(
      static_cast<float>(ch->getConfiguration()->header.fullScale) /
        (1 << spdCards_[spdIndex_]->GetResolutionBits()),
      spdCards_[spdIndex_]->GetCalibratedFullScale(ch->GetIndex()) /
        (1 << spdCards_[spdIndex_]->GetResolutionBits()),
      ch->getConfiguration()->header.offset + ch->GetCalibratedOffset());
  }

  LOG_TRACE << "Exiting CCardSpdAdq412::Calibrate for card " +
      boost::lexical_cast<std::string>(cardIndex_);
}

// Try to clean-up everything ...
void CCardSpdAdq412::Delete()
{
  LOG_TRACE << "Entering CCardSpdAdq412::Delete with index " +
      boost::lexical_cast<std::string>(cardIndex_);

  LOG_TRACE << "Exiting CCardSpdAdq412::Delete with index " +
      boost::lexical_cast<std::string>(cardIndex_);
}

void CCardSpdAdq412::LoadDefaultConfiguration()
{
  LOG_TRACE << "Entering CCardSpdAdq412::LoadDefaultConfiguration for index " +
      boost::lexical_cast<std::string>(cardIndex_);

  if (driverInstance_ == NULL)
  {
    throw CCardEx("driver not configured");
  }

  try
  {
    spdCards_[spdIndex_]->SetTriggerMode(TRIGGER_MODE::EXTERNAL1);
    spdCards_[spdIndex_]->SetClockSource(CLOCK_SOURCE::INT_10M);
    spdCards_[spdIndex_]->SetInterleavingMode(INTERLEAVING_MODE::FOUR_CHANNEL);
    spdCards_[spdIndex_]->SetInputRange(5000);
    spdCards_[spdIndex_]->SetAcquisitionParameters(1800, 175 * 1000, 0.0);
  }
  catch (const CCardEx &ex)
  {
    throw CCardEx(ex.what());
  }

  LOG_TRACE << "Exiting CCardSpdAdq412::LoadDefaultConfiguration for index " +
      boost::lexical_cast<std::string>(cardIndex_);
}

bool CCardSpdAdq412::HasOfflineParsing()
{
  return true;
}
} // namespace spdevices
