---
# DisableFormat: false
---
## See https://clang.llvm.org/docs/ClangFormatStyleOptions.html
### GLOBAL ###
Language: Cpp
Standard: Cpp11
BasedOnStyle: Google

### ALIGNMENTS ###
AlignAfterOpenBracket: Align
AlignConsecutiveAssignments: false
AlignConsecutiveDeclarations: false
# Clang >= 10
# AlignConsecutiveMacros: false
AlignEscapedNewlines: Left
AlignOperands: false
AlignTrailingComments: true

# Clang >= 10
# AllowAllArgumentsOnNextLine: true
# Clang >= 10
# AllowAllConstructorInitializersOnNextLine: true
AllowAllParametersOfDeclarationOnNextLine: false

PointerAlignment: Right
DerivePointerAlignment: false

# Short lines
AllowShortBlocksOnASingleLine: true
AllowShortCaseLabelsOnASingleLine: true
AllowShortFunctionsOnASingleLine: Inline
AllowShortIfStatementsOnASingleLine: false
# Clang >= 10
# AllowShortLambdasOnASingleLine: All
AllowShortLoopsOnASingleLine: false

AlwaysBreakAfterReturnType: None
AlwaysBreakBeforeMultilineStrings: false
AlwaysBreakAfterDefinitionReturnType: false
AlwaysBreakTemplateDeclarations: true

BinPackArguments: true
BinPackParameters: false

## Breaking
ColumnLimit: 80
BreakBeforeBraces: Custom
BraceWrapping: # close to Allman style
  IndentBraces: false
  # AfterCaseLabel: true
  AfterControlStatement: true
  AfterFunction: true
  SplitEmptyFunction: false
  AfterClass: true
  AfterEnum: true
  AfterStruct: true
  AfterUnion: true
  SplitEmptyRecord: false
  AfterNamespace: false
  AfterExternBlock: false
  SplitEmptyNamespace: false
  BeforeCatch: true
  BeforeElse: true

BreakBeforeBinaryOperators: false
BreakBeforeTernaryOperators: false
BreakConstructorInitializers: AfterColon
BreakInheritanceList: AfterColon
BreakStringLiterals: true
BreakAfterJavaFieldAnnotations: false

CompactNamespaces: false
ConstructorInitializerAllOnOneLineOrOnePerLine: true

## Indent
UseTab: Never
IndentWidth: 2
ConstructorInitializerIndentWidth: 2
ContinuationIndentWidth: 2
AccessModifierOffset: -2
IndentCaseLabels: false ## FIXME: check
IndentPPDirectives: None
IndentWrappedFunctionNames: true
NamespaceIndentation: None
ReflowComments: true

### SORTING ###
# we sort includes and using declarations
# includes are automatically grouped and sorted
IncludeBlocks: Regroup
IncludeCategories:
  # TUT
  - Regex: '^<tut\/tut(_.+)?.hpp>$'
    Priority: 6
  # QScintilla
  - Regex: '^<Qsci\/qsci[[:lower:]]+.h>$'
    Priority: 5
  # Qt
  - Regex: '^<Q[[:upper:]][[:alnum:]]+>$'
    Priority: 4
  # Eigen
  - Regex: '^<Eigen\/[[:upper:]][[:alnum:]]+>$'
    Priority: 3
  # Boost
  - Regex: '^<boost\/.+\.hpp>$'
    Priority: 2
  # STL
  - Regex: '^<[[:lower:]_]+>$'
    Priority: 1
  # local includes
  - Regex: '^".+\.h(pp)?"$'
    Priority: 10
  # internal global includes
  - Regex: '^<.+\.h(pp)?>$'
    Priority: 9
IncludeIsMainRegex: '$'
SortIncludes: true
SortUsingDeclarations: true

### SPACES ###
SpaceAfterCStyleCast: true
# Clang >= 10
# SpaceAfterLogicalNot: false
SpaceAfterTemplateKeyword: false
SpaceBeforeAssignmentOperators: true
# Doesn't exist in MSVC 2017
#SpaceBeforeCpp11BracedList: false
SpaceBeforeCtorInitializerColon: true
SpaceBeforeInheritanceColon: true
SpaceBeforeParens: ControlStatements
SpaceBeforeRangeBasedForLoopColon: true
# Clang >= 10
# SpaceInEmptyBlock: false
SpacesBeforeTrailingComments: 1
# nested angles requires this in c++
SpacesInAngles: false
SpacesInCStyleCastParentheses: false
SpacesInContainerLiterals: true
SpacesInParentheses: false
SpacesInSquareBrackets: false
Cpp11BracedListStyle: true

### EMPTY LINES ###
KeepEmptyLinesAtTheStartOfBlocks: false
MaxEmptyLinesToKeep: 1

### TWEAKING ###
PenaltyBreakAssignment: 100
PenaltyBreakBeforeFirstCallParameter: 10
PenaltyBreakComment: 100
PenaltyBreakFirstLessLess: 0
PenaltyBreakString: 100
PenaltyExcessCharacter: 40
PenaltyReturnTypeOnItsOwnLine: 1000

## Misc
FixNamespaceComments: true

## Add more if needed
MacroBlockBegin: "CPPUNIT_NS_BEGIN"
MacroBlockEnd: "CPPUNIT_NS_END"
# StatementMacros
# TypenameMacros
---
